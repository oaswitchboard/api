/* Creates a pool of connections for Postgres */
const { Pool } = require('pg');
const pool = new Pool({
    user: process.env.USERNAME,
    host: process.env.HOST,
    database: process.env.DATABASE,
    password: process.env.PASSWORD,
    port: 5432
});

module.exports = pool;