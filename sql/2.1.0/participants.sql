-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS participants_id_seq;

-- Table Definition
CREATE TABLE "public"."participants" (
    "cognito_id" varchar(50),
    "email" varchar(255),
    "id" int4 NOT NULL,
    "institution" varchar(250),
    "pubsweet_id" varchar(50),
    "ror" varchar(50),
    "type" varchar(50),
    "webhook" varchar(255),
    "persistent" bool,
    "fullname" varchar(255),
    "oafund" bool DEFAULT false,
    "invoice" varchar,
    "rules" json,
    "delegate" varchar(255),
    "affiliates" _text,
    "apikey" text,
    "pio" bool NOT NULL DEFAULT false,
    "callback" varchar(255),
    PRIMARY KEY ("id")
);