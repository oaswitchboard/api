-- -------------------------------------------------------------
-- TablePlus 4.0.2(374)
--
-- https://tableplus.com/
--
-- Database: oasb_development
-- Generation Time: 2023-01-24 14:51:46.4310
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."webhooks";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS webhooks_id_seq;

-- Table Definition
CREATE TABLE "public"."webhooks" (
    "id" int4 NOT NULL DEFAULT nextval('webhooks_id_seq'::regclass),
    "participant_id" int4,
    "url" varchar,
    "apikey" varchar,
    PRIMARY KEY ("id")
);

