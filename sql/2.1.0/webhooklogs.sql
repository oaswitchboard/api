
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS webhooklogs_id_seq;

-- Table Definition
CREATE TABLE "public"."webhooklogs" (
    "id" int4 NOT NULL DEFAULT nextval('webhooklogs_id_seq'::regclass),
    "sent_timestamp" timestamp,
    "url" varchar,
    "apikey" varchar,
    "payload" varchar,
    "status" int4,
    "response" varchar,
    PRIMARY KEY ("id")
);

