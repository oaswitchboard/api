-- -------------------------------------------------------------
-- TablePlus 4.0.2(374)
--
-- https://tableplus.com/
--
-- Database: oasb_development
-- Generation Time: 2023-01-25 09:35:32.3230
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."failures";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS failures_id_seq;

-- Table Definition
CREATE TABLE "public"."failures" (
    "id" int4 NOT NULL DEFAULT nextval('failures_id_seq'::regclass),
    "payload" varchar,
    "error" varchar,
    "step" varchar DEFAULT ''::character varying,
    "created_at" timestamp DEFAULT now(),
    PRIMARY KEY ("id")
);

