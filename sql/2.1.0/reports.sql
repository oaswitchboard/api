-- This script only contains the view creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

CREATE OR REPLACE VIEW public.reports
 AS
 SELECT messages.id,
    messages.header,
    messages.data,
    messages.type,
    messages.created,
    messages.sender,
    messages.state,
    messages.receiver,
    ( SELECT e1.id
           FROM messages e1
          WHERE (e1.state::text = ANY (ARRAY['SENT'::character varying::text, 'AUTOREJECT'::character varying::text, 'OVERRIDDEN'::character varying::text])) AND e1.type::text = 'e1'::text AND ((e1.header ->> 'e1'::text) = (messages.header ->> 'e1'::text) OR ((messages.header -> 'overridden'::text) ->> 'id'::text) = e1.id::text AND e1.type::text = 'e1'::text)
          ORDER BY e1.id DESC
         LIMIT 1) AS e1,
    ( SELECT e2.id
           FROM messages e2
          WHERE (e2.state::text = ANY (ARRAY['SENT'::character varying::text, 'AUTOREJECT'::character varying::text, 'OVERRIDDEN'::character varying::text])) AND e2.type::text = 'e2'::text AND ((e2.header ->> 'e1'::text) = (messages.header ->> 'e1'::text) OR ((messages.header -> 'overridden'::text) ->> 'id'::text) = e2.id::text AND e2.type::text = 'e2'::text)
          ORDER BY e2.id DESC
         LIMIT 1) AS e2,
    ( SELECT p1.id
           FROM messages p1
          WHERE (p1.state::text = ANY (ARRAY['SENT'::character varying::text, 'AUTOREJECT'::character varying::text, 'OVERRIDDEN'::character varying::text])) AND p1.type::text = 'p1'::text AND ((p1.header ->> 'e1'::text) = (messages.header ->> 'e1'::text) OR (p1.header ->> 'e2'::text) = (messages.header ->> 'e2'::text) OR ((messages.header -> 'overridden'::text) ->> 'id'::text) = p1.id::text AND p1.type::text = 'p1'::text)
          ORDER BY p1.id DESC
         LIMIT 1) AS p1,
    ( SELECT p2.id
           FROM messages p2
          WHERE (p2.state::text = ANY (ARRAY['SENT'::character varying::text, 'AUTOREJECT'::character varying::text, 'OVERRIDDEN'::character varying::text])) AND p2.type::text = 'p2'::text AND ((p2.header ->> 'p1'::text) = (messages.header ->> 'p1'::text) OR (p2.header ->> 'e2'::text) = (messages.header ->> 'e2'::text) OR (p2.header ->> 'e1'::text) = (messages.header ->> 'e1'::text) OR ((messages.header -> 'overridden'::text) ->> 'id'::text) = p2.id::text AND p2.type::text = 'p2'::text)
          ORDER BY p2.id DESC
         LIMIT 1) AS p2
   FROM messages
  ORDER BY messages.id DESC;

