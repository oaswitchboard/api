-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS organisations_id_seq;

-- Table Definition
CREATE TABLE "public"."organisations" (
    "id" int4 NOT NULL DEFAULT nextval('organisations_id_seq'::regclass),
    "name" varchar NOT NULL,
    "participant_id" int4,
    "ror_id" varchar,
    "ringgold_id" varchar,
    "isni" varchar,
    "domainnames" varchar,
    "grid" varchar,
    "fundref" varchar,
    PRIMARY KEY ("id")
);
