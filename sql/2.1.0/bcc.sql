-- -------------------------------------------------------------
-- TablePlus 4.0.2(374)
--
-- https://tableplus.com/
--
-- Database: oasb_production
-- Generation Time: 2023-09-28 13:10:53.9540
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS bcc_id_seq;

-- Table Definition
CREATE TABLE "public"."bcc" (
    "id" int4 NOT NULL DEFAULT nextval('bcc_id_seq'::regclass),
    "header" json,
    "data" json,
    "type" varchar,
    "created" timestamp DEFAULT CURRENT_TIMESTAMP,
    "sender" int4,
    "state" varchar DEFAULT 'SENT'::character varying,
    "receiver" int4,
    "organisation" int4,
    "hash" varchar,
    PRIMARY KEY ("id")
);

