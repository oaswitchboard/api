-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS id_journals_seq;

-- Table Definition
CREATE TABLE "public"."journals" (
    "id" int4 NOT NULL DEFAULT nextval('id_journals_seq'::regclass),
    "participant_id" int4,
    "journal_id" varchar,
    "title" varchar,
    "issn_print" varchar,
    "issn_electronic" varchar,
    "inDOAJ" bool,
    "crossref" varchar,
    "type" varchar,
    "deposition" varchar,
    "researchdata" varchar,
    "license" varchar,
    PRIMARY KEY ("id")
);