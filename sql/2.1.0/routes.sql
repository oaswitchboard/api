-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS routes_id_seq;

-- Table Definition
CREATE TABLE "public"."routes" (
    "id" int8 NOT NULL DEFAULT nextval('routes_id_seq'::regclass),
    "participant_id" int4 NOT NULL,
    "organisation_id" int4 NOT NULL,
    "condition" json,
    "action" varchar,
    "notification" varchar,
    "created_by" varchar,
    "created_at" timestamp,
    "updated_by" varchar,
    "updated_at" timestamp,
    "deleted_by" varchar,
    "deleted_at" timestamp,
    "is_deleted" bool NOT NULL DEFAULT false,
    PRIMARY KEY ("id")
);