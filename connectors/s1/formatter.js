const xml2js = require('xml2js');
const parser = new xml2js.Parser();
const moment = require('moment');
const datemask = 'YYYY-MM-DD';
const ror = require(`../../libs/ror`);

module.exports = formatter = {

    e1: {

		create: async (documentxml, authorsxml, deals) => {
			const deal = await hasApplicableDeal (documentxml, authorsxml, deals);
			return deal ?
				await formatter.e1.prioragreement(documentxml, authorsxml, deal) :
				await formatter.e1.noprioragreement (documentxml, authorsxml) ;
		},

        noprioragreement: async (documentxml, authorsxml) => {
			console.log('noprioragreement --------------------------------------------------')
			const document = await formatAsJSON(documentxml);
            const authors = await formatAsJSON(authorsxml, true);
			// Important! We do not have a deal, we use Ringgold Id from the documentxml
			const header = await formatter.common.formatHeaderWithRor(authors);
			const status = formatter.common.formatStatus(document);
            const data = await formatter.common.formatData(document, authors);
            header.meta = {
				prioragreement: false,
				source: 'S1',
				status,
			}
			return {
				header,
				data,
			};
        },
        prioragreement: async (documentxml, authorsxml, deal) => {

			console.log('prioragreement --------------------------------------------------')

			const document = await formatAsJSON(documentxml);
            const authors = await formatAsJSON(authorsxml, true);

			// Important! We have a deal, we do not use Ringgold Id from the documentxml
			const header = await formatter.common.formatHeaderBasedOnADeal(authors, deal);
			const status = formatter.common.formatStatus(document);
            const data = await formatter.common.formatData(document, authors, deal);
            header.meta = {
				prioragreement: true,
				source: 'S1',
				status,
			}
			return {
				header,
				data,
			};
        },
    },

    common: {

		formatStatus: (document) => {
			return document.submissionStatus[0].documentStatusName[0].toUpperCase() || 'UNKNOWN'
		},

        formatHeaderWithRor: async (authors = []) => {
			const from = {id: parseInt(process.env.SENDER, 10)};
			const validity = moment().add(1, 'year').format(datemask);
			const to = await createHeaderToWithRor (authors)
			return {
				type: 'e1',
				from,
				to,
				validity,
				template: 'templates/MessageRequest.html',
				persistent: true,
			};
		},

        formatHeaderBasedOnADeal: async (authors = [], deal) => {
			const from = {id: parseInt(process.env.SENDER, 10)};
			const validity = moment().add(1, 'year').format(datemask);
			const to = createHeaderToFromDeal (deal)
			return {
				type: 'e1',
				from,
				to,
				validity,
				template: 'templates/MessageRequest.html',
				persistent: true,
			};
		},

		formatData: (document, persons, deal) => {
            const authors = formatter.common.formatAuthors(persons);
			const article = formatter.common.formatArticle(document);
            const journal = formatter.common.formatJournal(document);
			const charges = deal ?
			 	formatter.common.formatChargesPriorAgreement(deal):
			 	formatter.common.formatChargesNoPriorAgreement(document);
			return {
				authors,
				article,
				journal,
				charges,
			};
		},

		formatAuthors: (authors) => authors.map((author, index) => ({
            lastName: formatter.common.formatLastName(author),
			firstName: formatter.common.formatFirstName(author),
			initials: formatter.common.formatInitials(author),
			ORCID: formatter.common.formatORCId(author),
			email: formatter.common.formatEmail(author),
			collaboration: formatter.common.formatCollaboration(author),
			listingorder: formatter.common.formatListingOrder(index),
			isCorrespondingAuthor: formatter.common.formatIsCorrespondingAuthor(author),
			institutions: formatter.common.formatInstitutions(author),
			currentaddress: formatter.common.formatCurrentAddress(author),
			affiliation: formatter.common.formatAffiliations(author),
		})),

		formatLastName: (author) => flattenProperty(author.authorLastName) || '',

		formatFirstName: (author) => flattenProperty(author.authorFirstName) || '',

		formatInitials: (author) => {

            const lastname = flattenProperty(author.authorLastName);
            const firstname = flattenProperty(author.authorFirstName);
			const fullname = `${firstname} ${lastname}`;
			return fullname.split(' ').map(name => name.substring(1, 1)).join('')
		},

		formatORCId: (author) => flattenProperty(author.authorORCIDId) || '',

		formatEmail: (author) => flattenProperty(author.authorPrimaryEmailAddress) || '',

		formatCollaboration: () => '',

		formatListingOrder: (index) => index + 1,

		formatCreditRoles: () => [],

		formatIsCorrespondingAuthor: (author) => (flattenProperty(author.isCorresponding) === 'true'),

		formatInstitutions: (author) => {
            return author.departments.map(department => {
                return { name: flattenProperty(department.institution) }
            });
        },

		formatCurrentAddress: (author) => {
			return author.departments.map(department => {
				return { name: flattenProperty(department.institution) }
			});
		},

		formatAffiliations: (author) => {
			return formatter.common.formatInstitutions(author).map(institution => institution.name).join(',');
		},

        formatArticle: (document) => {
			const grants = formatter.common.formatGrants(document);
			const manuscript = formatter.common.formatManuscript(document);
			const preprint = formatter.common.formatPreprint(document);
			const vor = formatter.common.formatVOR(document);
			const funders = formatter.common.formatFunders(document);
			const type = formatter.common.formatArticleType(document);
			return {
				'title': formatter.common.formatSubmissionTitle(document) || 'Unknown',
				grants,
				manuscript,
				preprint,
				vor,
				type,
				funders,
				'submissionId': formatter.common.formatSubmissionId(document),
				'acknowledgement': '',
				'doi': document.doi || 'Unknown',
				'doiurl': document.doiurl || 'Unknown',
			};
		},

        formatSubmissionTitle: (document) => flattenProperty(document.submissionTitle),

		formatSubmissionId: (document) => flattenProperty(document.submissionId),

		formatGrants: (document) => {
            if (document.submissionFunders && Array.isArray(document.submissionFunders)){
				return document.submissionFunders.map(grant => {
					if (grant.grants && Array.isArray(grant.grants) && grant.grants[0].number && Array.isArray(grant.grants[0].number) && grant.grants[0].number.length) {
						return { name: grant.grants[0].number[0] || null };
					}
					return null;
				})
			}
			return [];
        },

		formatFunders: (document) => {
			if (document.submissionFunders && Array.isArray(document.submissionFunders)){
				return document.submissionFunders.map(grant => {
					if (grant.name && Array.isArray(grant.name) && grant.name.length) {
						return { name: grant.name[0] || null };
					}
					return null;
				})
			}
			return [];
        },

		formatManuscript: (document) => ({
			'dates': {
				'submission': moment(flattenProperty(document.submissionDate)).format(datemask),
			},
			'id': formatter.common.formatSubmissionId(document),
		}),

		formatPreprint: (document) => ({}),

		formatVOR: (document) => ({
			'publication': formatter.common.formatPublication(document),
			'license': formatter.common.formatLicense(document),
			'deposition': formatter.common.formatDeposition(document),
			'researchdata': formatter.common.formatResearchData(document),
		}),

        formatPublication: (document) => {
            return 'XXXXXX';
        },

        formatLicense: (document) => {
            return 'XXXXXX';
        },

        formatDeposition: (document) => {
            return 'XXXXXX';
        },

        formatResearchData: (document) => {
            return 'XXXXXX';
        },

		formatArticleType: (document) => 'research-article',

		formatJournal: (document) => ({
			name: formatter.common.formatJournalName(document),
			id: formatter.common.formatJournalID(document),
			inDOAJ: formatter.common.formatJournalInDOAJ(document),
		}),

		formatJournalName: (document) => {
			return flattenProperty(document.journalName) || '';
		},

		formatJournalID: (document) => {
			return flattenProperty(document.journalDigitalIssn) || '';
		},

		formatJournalInDOAJ: () => {
			return false;
		},

		formatChargesPriorAgreement: (deal) => ({
			prioragreement: true,
			agreement: {
				name: deal.Dealname,
				id: deal.DealID,
			},
		}),

		formatChargesNoPriorAgreement: () => {
			return {
				prioragreement: false,
				currency: 'EUR',
				fees: {
					apc: {
						type: 'per article APC list price',
						name: 'estimated',
						amount: 0
					},
					total: {
						name: 'estimated',
						amount: 0
					},
				},
			};
		},
    },

}

async function hasApplicableDeal (documentxml, authorsxml, deals) {
	const authors = await formatAsJSON(authorsxml, true);
	const receivers = extractReceivers (authors);
	console.log('hasApplicableDeal() --------------------------------------------------')
	console.log('receivers', receivers);
	let applicabledeal = null;
	deals.forEach(deal => {
		receivers.forEach(receiver => {
			if (matchRinggoldId(deal, receiver) || matchMailDomain(deal, receiver)) {
				applicabledeal = deal;
			}
		});
	});
	return applicabledeal;
}

function matchRinggoldId(deal, receiver) {
	return deal.RinggoldID && deal.RinggoldID.toString() === receiver.affiliations.RinggoldID.toString()
}

function matchMailDomain(deal, receiver) {
	return deal.Maildomain && deal.Maildomain.toString() === receiver.maildomain.toString()
}

function extractReceivers (authors) {
	return authors.filter(author => {
		return author.isCorresponding[0] === 'true';
	}).map(author => {
		return {
			author: author.authorFullName[0],
			email: author.authorPrimaryEmailAddress[0],
			affiliations: extractAffiliations(author.departments[0]),
			maildomain: author.authorPrimaryEmailAddress[0].split('@').pop(),
		}
	});
}

function extractAffiliations (departments) {
	const { institution, ringgoldId } = departments;
	return {
		institution: Array.isArray(institution) ? institution.pop() : null,
		RinggoldID: Array.isArray(ringgoldId) ? ringgoldId.pop() : null,
	} ;
}

async function parseXML(xml) {
    return new Promise ((resolve, reject) => {
        parser.parseString(xml, function (err, json) {
            if (err) {
                reject(err);
            } else {
                resolve(json);
            }
        });
    })
};

async function formatAsJSON(xml, array=false) {
    const parsed = await parseXML(xml);
    return array ? parsed.Response.result : parsed.Response.result[0];
}

function flattenProperty(array = []) {
    return array.join('');
}

function createHeaderToFromDeal (deal) {
	let address = null;
	let name = null;
	if (deal) {
		name = deal.Institution;
		address = extractToAddressFromDeal(deal);
	}
	return { address, name, };
}

async function createHeaderToWithRor (authors) {
	const receivers = extractReceivers (authors);
	let address = null;
	let name = null;
	if (receivers.length) {
		address = receivers[0].affiliations.RinggoldID;
		name = receivers[0].affiliations.institution;
		const enriched = await ror.generic.search(name);
		console.log('enriched', enriched);
		if (enriched && enriched.rorid) {
			address = enriched.rorid;
		}
	}
	return { address, name, };
}

function extractToAddressFromDeal(deal) {
	if (deal.RORID) {
		return deal.RORID;
	}
	if (deal.RinggoldID) {
		return deal.RinggoldID;
	}
	if (deal.ISNI) {
		return deal.ISNI;
	}
	if (deal.maildomain) {
		return deal.maildomain;
	}
	return null;
}