const AWS = require('aws-sdk');
const request = require('request');
const xlsx = require('xlsx');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = connector = {

    getDocument: async (submissionId, siteName) => {
        const endpoint = getManuscriptEndpoint(submissionId, siteName);
        const auth = {
            'user': process.env.S1_USERNAME,
            'pass': process.env.S1_PASSWORD,
            'sendImmediately': false,
        };
        return new Promise ((resolve, reject) => {
            request({url: endpoint, auth}, async (err, response) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response.body);
                }
            });
        })
    },

    getAuthors: async (submissionId, siteName) => {
        const endpoint = getAuthorEndpoint(submissionId, siteName);
        const auth = {
            'user': process.env.S1_USERNAME,
            'pass': process.env.S1_PASSWORD,
            'sendImmediately': false,
        };
        return new Promise ((resolve, reject) => {
            request({url: endpoint, auth}, async (err, response) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response.body);
                }
            });
        })
    },

	getDeals: async (key) => {
		try {
			const buffer = await getAssetFromS3AsBuffer(key);
			const workbook = xlsx.read(buffer);
			const sheet = workbook.SheetNames[0];
			return xlsx.utils.sheet_to_json(workbook.Sheets[sheet])
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

}

function getManuscriptEndpoint(submissionId, siteName) {
    return `${process.env.S1_HOST}/api/s1m/v3/submissions/full/metadata/submissionids?ids=%27${submissionId}%27&site_name=${siteName}`;
}

function getAuthorEndpoint(submissionId, siteName) {
    return `${process.env.S1_HOST}/api/s1m/v2/submissions/full/contributors/authors/submissionids?ids=%27${submissionId}%27&site_name=${siteName}`;
}

function getAssetFromS3AsBuffer(key) {
	return new Promise((resolve, reject) => {
		getBufferFromS3(key, (error, s3buffer) => {
			if (error) return reject(error);
			return resolve(s3buffer);
		});
	});
};

function getBufferFromS3(key, callback){
	const buffers = [];
	const s3 = new AWS.S3();
	const stream = s3.getObject(getS3Params(key)).createReadStream();
	stream.on('data', data => buffers.push(data));
	stream.on('end', () => callback(null, Buffer.concat(buffers)));
	stream.on('error', error => callback(error));
}

function getS3Params (key) {
	return {
        Bucket: process.env.S3_BUCKET,
        Key: key
    };
}