const AWS = require('aws-sdk');
const DigestFetch = require('digest-fetch');
const xlsx = require('xlsx');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

	getDocument: async (journal, documentid) => {
		try {
			const endpoint = documentEndpoint(journal, documentid);
			const response = await client().fetch(endpoint);
			if (response.ok && response.status === 200) {
				const document = await response.json();
				return document.Response.result.e;
			}
			throw "getDocument() -> Invalid response from Editorial manager. Connection problems";
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from Editorial manager. Parsing problems";
		}
	},

	getAuthors: async (journal, documentid) => {
		try {
			const endpoint = authorEndpoint(journal, documentid);
			const response = await client().fetch(endpoint);
			if (response.ok && response.status === 200) {
				const author = await response.json();
				return arrayifyAuthors(author.Response.result.e);
			}
			throw "getAuthors() -> Invalid response from Editorial manager. Connection problems";
		} catch (error) {
			console.error(error);
			throw "getAuthors() -> Invalid response from Editorial manager. Parsing problems";
		}
	},

	getDeals: async (key) => {
		try {
			const buffer = await getAssetFromS3AsBuffer(key);
			const workbook = xlsx.read(buffer);
			const sheet = workbook.SheetNames[0];
			return xlsx.utils.sheet_to_json(workbook.Sheets[sheet])
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

}

function arrayifyAuthors (authors) {
	return Array.isArray(authors) ? authors: [ authors ];
}

function authorEndpoint(journal, documentid) {
	return `${process.env.EM_BASE_URL}/api/em/v1/submissions/full/contributors/authors/documentids?_type=json&ids=${documentid}&site_name=${journal}`;
}

function documentEndpoint(journal, documentid) {
	return `${process.env.EM_BASE_URL}/api/em/v1/submissions/full/metadata/documentids?_type=json&ids=${documentid}&site_name=${journal}`;
}

function client() {
	return new DigestFetch(
		process.env.EM_USERNAME,
		process.env.EM_PASSWORD, {
			basic: false,
			algorithm: 'MD5',
			precomputeHash: true
		},
	);
}

function getAssetFromS3AsBuffer(key) {
	return new Promise((resolve, reject) => {
		getBufferFromS3(key, (error, s3buffer) => {
			if (error) return reject(error);
			return resolve(s3buffer);
		});
	});
};

function getBufferFromS3(key, callback){
	const buffers = [];
	const s3 = new AWS.S3();
	const stream = s3.getObject(getS3Params(key)).createReadStream();
	stream.on('data', data => buffers.push(data));
	stream.on('end', () => callback(null, Buffer.concat(buffers)));
	stream.on('error', error => callback(error));
}

function getS3Params (key) {
	return {
        Bucket: process.env.S3_BUCKET,
        Key: key
    };
}