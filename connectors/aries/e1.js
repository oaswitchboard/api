const AWS = require('aws-sdk');
const utils = require(`../../libs/utils`);
const convert = require(`../../libs/convert`);
let formatter = require(`./formatter`);

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

   prioragreementWithoutDealsheet: async (document, authors, overrides = {}) => {
      customformatter = Object.assign(formatter.e1, overrides);
      const message = await customformatter.prioragreementWithoutDealsheet(document, authors);
      const v2message = convert.V1ToV2(message);
      const valid = validate(v2message);
      if (valid.valid) {
         console.log('Creating a E1 message');
         return await deliver(v2message);
      } else {
         console.log('Validation errors');
         console.error(valid.errors);
         return valid.errors;
      }
   },

   prioragreementWithDealsheet: async (document, authors, overrides = {}, dealsheet = [], datastore) => {
      customformatter = Object.assign(formatter.e1, overrides);
      const message = await customformatter.prioragreementWithDealsheet(document, authors, dealsheet);
      const v2message = convert.V1ToV2(message);
      const valid = validate(v2message);
      if (valid.valid) {
         console.log('Creating a E1 message');
         return await deliver(v2message);
      } else {
         console.log('Validation errors');
         console.error(valid.errors);
         return valid.errors;
      }
   },

   noprioragreement: async (document, authors, overrides = {}) => {
      customformatter = Object.assign(formatter.e1, overrides);
      const message = await customformatter.noprioragreement(document, authors);
      const v2message = convert.V1ToV2(message);
      const valid = validate(v2message);
      if (valid.valid) {
         console.log('Creating a E1 message');
         return await deliver(v2message);
      } else {
         console.log('Validation errors');
         console.error(valid.errors);
         return valid.errors;
      }
   },
}

async function deliver (message) {
   const delivery = process.env.DATASTORE || 'oasb';
   if (['oasb', 'both'].includes(delivery)) {
      console.log('Creating a E1 message in OASB');
      return await ingest(message);
   }
   if (['private', 'both'].includes(delivery)) {
      console.log('Creating a E1 message in Private datastore');
      return message;
   }
}

async function ingest(message) {
   const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
   const params = {
      MessageAttributes: {
         "Sender": {
            DataType: "String",
            StringValue: JSON.stringify({
               sub: process.env.PARTICIPANT
            }),
         }
      },
      MessageBody: JSON.stringify(message),
      QueueUrl: process.env.SQS_INCOMING_URL
   };
   return await sqs.sendMessage(params).promise();
}

function validate (message) {
   const Validator = require('jsonschema').Validator;
   const validator = new Validator();
   const schema = require(`../../switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
   const header = require(`../../switchboard/schemas/${process.env.VERSION}/header`).header;
   validator.addSchema(header, '/Header');
   return validator.validate(message, schema);
}
