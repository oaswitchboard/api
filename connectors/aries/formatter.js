const moment = require('moment');
const datemask = 'YYYY-MM-DD';

module.exports = formatter = {

	e1: {

		prioragreementWithoutDealsheet: async (document, authors) => {
			const header = await formatter.e1.formatHeaderWithExternalMatching(authors);
			const data = await formatter.e1.formatData(document, authors, true);
			const status = document.submissionStatus.documentStatusName.toUpperCase() || 'UNKNOWN';
			header.meta = {
				prioragreement: true,
				dealsheet: false,
				source: 'EM',
				status,
			}
			return {
				header,
				data,
			};
		},

		prioragreementWithDealsheet: async (document, authors, dealsheet=[]) => {
			const emails = authors.filter(author => author.isCorresponding == 'true').map(author => author.authorPrimaryEmailAddress.split("@").pop());
			const author =  dealsheet.filter(deal => {
				return emails.includes(deal.Maildomain);
			}).pop();
			const header = await formatter.e1.formatHeaderWithoutExternalMatching(author);
			const data = await formatter.e1.formatData(document, authors, true);
			const status = document.submissionStatus.documentStatusName.toUpperCase() || 'UNKNOWN';
			header.meta = {
				prioragreement: true,
				dealsheet: true,
				source: 'EM',
				status,
			}
			return {
				header,
				data,
			};
		},

		noprioragreement: async (document, authors) => {
			const header = await formatter.e1.formatHeaderWithExternalMatching(authors, false, false);
			const data = await formatter.e1.formatData(document, authors, false);
			const status = document.submissionStatus.documentStatusName.toUpperCase() || 'UNKNOWN';
			header.meta = {
				prioragreement: false,
				dealsheet: false,
				source: 'EM',
				status,
			}
			return {
				header,
				data,
			};
		},

		formatHeaderWithoutExternalMatching: async (authors = [], pa) => {
			const from = {id: parseInt(process.env.SENDER, 10)};
			const validity = moment().add(1, 'year').format(datemask);
			const to = await createHeaderToWithoutExternalMatching (authors)
			return {
				type: 'e1',
				from,
				to,
				validity,
				template: 'templates/MessageRequest.html',
				persistent: true,
			};
		},

		formatHeaderWithExternalMatching: async (authors = []) => {
			const from = {id: parseInt(process.env.SENDER, 10)};
			const validity = moment().add(1, 'year').format(datemask);
			const to = await createHeaderToWithExternalMatching (authors)

			return {
				type: 'e1',
				from,
				to,
				validity,
				template: 'templates/MessageRequest.html',
				persistent: true,
			};
		},

		formatData: (document, persons, prioragreement = true) => {
			const authors = formatter.e1.formatAuthors(persons);
			const article = formatter.e1.formatArticle(document);
			const journal = formatter.e1.formatJournal(document);
			const charges = prioragreement ?
				formatter.e1.formatChargesPriorAgreement(document):
				formatter.e1.formatChargesNoPriorAgreement(document);
			return {
				authors,
				article,
				journal,
				charges,
			};
		},

		formatAuthors: (authors) => authors.map(author => ({
			lastName: formatter.e1.formatLastName(author),
			firstName: formatter.e1.formatFirstName(author),
			initials: formatter.e1.formatInitials(author),
			ORCID: formatter.e1.formatORCId(author),
			email: formatter.e1.formatEmail(author),
			collaboration: formatter.e1.formatCollaboration(author),
			listingorder: formatter.e1.formatListingOrder(author),
			creditroles: formatter.e1.formatCreditRoles(author),
			isCorrespondingAuthor: formatter.e1.formatIsCorrespondingAuthor(author),
			institutions: formatter.e1.formatInstitutions(author),
			currentaddress: formatter.e1.formatCurrentAddress(author),
			affiliation: formatter.e1.formatAffiliations(author),
		})),


		formatLastName: (author) => author.authorLastName || '',

		formatFirstName: (author) => author.authorFirstName || '',

		formatORCId: (author) => author.authorORCIDId || '',

		formatEmail: (author) => author.authorPrimaryEmailAddress || '',

		formatCollaboration: () => 'XXXXXX',

		formatListingOrder: (author) => parseInt(author.inputIndex, 10) + 1,

		formatCreditRoles: () => [],

		formatIsCorrespondingAuthor: (author) => author.isCorresponding === 'true',

		formatInstitutions: (author) => [{
			name: author.departments && author.departments.institution ? author.departments.institution : '',
		}],

		formatCurrentAddress: (author) => [{
			name: author.departments && author.departments.institution ? author.departments.institution : '',
		}],

		formatAffiliations: (author) => {
			return formatter.e1.formatInstitutions(author).filter(institution => institution.name).map(institution => institution.name).join(',');
		},

		formatInitials: (author) => author.authorFullName.split(' ').map(name => name.substring(1, 1)).join(''),

		formatArticle: (document) => {
			const grants = formatter.e1.formatGrants(document);
			const manuscript = formatter.e1.formatManuscript(document);
			const preprint = formatter.e1.formatPreprtint();
			const vor = formatter.e1.formatVOR();
			const funders = formatter.e1.formatFunders(document);
			const type = formatter.e1.formatType(document);
			const originaltype = formatter.e1.formatOriginalType(document);
			return {
				'title': document.submissionTitle || 'Unknown',
				grants,
				manuscript,
				preprint,
				vor,
				type,
				originaltype,
				funders,
				'submissionId': document.submissionId,
				'acknowledgement': '',
				'doi': document.doi || 'Unknown',
				'doiurl': document.doiurl || 'Unknown',
			};
		},

		formatGrants: (document) => [],

		formatManuscript: (document) => ({
			'dates': {
				'submission': moment(document.submissionDate).format(datemask),
				'acceptance': moment(document.decisionDate).format(datemask),
			},
			'id': document.submissionId
		}),

		formatPreprtint: () => ({}),

		formatVOR: () => ({
			'publication': 'XXXXXX',
			'license': 'XXXXXX',
			'deposition': 'XXXXXX',
			'researchdata': 'XXXXXX'
		}),

		formatFunders: (document) => {
			if (Array.isArray(document.submissionFunders)) {
				const funders = document.submissionFunders.map(funder => funder.name);
				return [...new Set(funders)].map(name => ({name}));
			}
			return [];
		},

		formatType: (document) => 'research-article',

		formatOriginalType: (document) => 'research-article',

		formatJournal: (document) => ({
			name: document.journalName,
			id: 'UNKNOWN',
			inDOAJ: false
		}),

		formatChargesPriorAgreement: () => ({
			prioragreement: true,
			agreement: {
				name: '',
				id: '',
			},
		}),

		formatChargesNoPriorAgreement: () => {
			return {
				prioragreement: false,
				currency: 'EUR',
				fees: {
					apc: {
						type: 'per article APC list price',
						name: 'estimated',
						amount: 0
					},
					total: {
						name: 'estimated',
						amount: 0
					},
				},
			};
		},
	},
};


async function createHeaderToWithExternalMatching (authors) {
	const ror = require("../../libs/ror")
	const corresponding = [...authors].filter(author => author.isCorresponding == 'true');
	if (corresponding.length) {
		const author = corresponding.pop();
		if (author.departments && author.departments.institution) {
			const receiver = await ror.e1.enrichReceiver(author);
			if (receiver.rorid) {
				return {address: receiver.rorid};
			}
		}
		return {address: author.authorPrimaryEmailAddress};
	}
	return {address: null};
}

async function createHeaderToWithoutExternalMatching (author) {
	return {address: author.RORID};
}