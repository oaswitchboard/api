const AWS = require('aws-sdk');
const xml2js = require('xml2js');
const xlsx = require('xlsx');
const parser = new xml2js.Parser();

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

	getDocument: async (key) => {
		try {
			const manuscript = await getAssetFromS3(key);
			return manuscript.Body.toString('utf-8');
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

	getDeals: async (key) => {
		try {
			const buffer = await getAssetFromS3AsBuffer(key);
			const workbook = xlsx.read(buffer);
			const sheet = workbook.SheetNames[0];
			return xlsx.utils.sheet_to_json(workbook.Sheets[sheet])
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

	format: {

		success: (message) => {
			return {
				statusCode: 200,
				body: JSON.stringify({
				  message,
				  processed: true,
				}),
			};
		},

		error: (message, statusCode = 500) => {
			return {
				statusCode,
				body: JSON.stringify({
				  message,
				  processed: false,
				}),
			};
		},
	},

};

async function getAssetFromS3(key) {
	const s3 = new AWS.S3();
	return await s3.getObject(getS3Params(key)).promise();
};

async function parseXML(xml) {
	return new Promise ((resolve, reject) => {
		parser.parseString(xml, function (err, json) {
			if (err) {
				reject(err);
			} else {
				resolve(json);
			}
		});
	})
};

function getAssetFromS3AsBuffer(key) {
	return new Promise((resolve, reject) => {
		getBufferFromS3(key, (error, s3buffer) => {
			if (error) return reject(error);
			return resolve(s3buffer);
		});
	});
};

function getBufferFromS3(key, callback){
	const buffers = [];
	const s3 = new AWS.S3();
	const stream = s3.getObject(getS3Params(key)).createReadStream();
	stream.on('data', data => buffers.push(data));
	stream.on('end', () => callback(null, Buffer.concat(buffers)));
	stream.on('error', error => callback(error));
}

function getS3Params (key) {
	return {
        Bucket: process.env.S3_BUCKET,
        Key: key
    };
}