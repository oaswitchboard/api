const AWS = require('aws-sdk');
const utils = require(`../../libs/utils`);
const sqs = require(`../../libs/sqs`);
const convert = require(`../../libs/convert`);
const standardrouter = require(`./router`);
let formatter = require(`./formatter`);

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = connector = {

   create: async (params) => {

      /**
       * Standard connector provides basic functionality to create P1 messages either in a default OASB store or in a Private datastore
       * Accepted parameters:
       *
       * - document - JATS XML as provided by Literatum etc
       * - deals - JSON array of deals containing institutional identifiers and deal details for P1 PA messages
       * - custom - Custom formatter, that extends the standard P1 message formatter.
       * - router - Custom router that extend the standard P1 message router
       * - key - Source document S3 key (if applicable)
       *
       */

      const { document, deals, custom, router, key='' } = params;

      console.log('p1.create()--------------------------------------------------')

      const p1 = Object.assign(formatter.p1, custom);
      const customformatter = Object.assign(formatter, p1);
      const customrouter = Object.assign(standardrouter, router);
      const receivers = await customrouter.p1.receivers(document, deals, false);

      let relations = [];


      if (process.env.LAMBDA_DOI) {
         try {
           relations = await lookupRelationsBy(customformatter, document);
         } catch (err) {
           console.error('Problem with relations lookup')
         }  
      }

      if (!receivers || !receivers.length) {
         throw('No receivers found');
      }

      for (const receiver of receivers) {
         const message = await customformatter.p1.format(document, deals, receiver, relations, key);
         const v2message = convert.V1ToV2(message);
         const valid = validate(v2message);
         if (valid.valid) {
            await deliver(v2message);
            return {
               message,
               success: true,
               errors: valid.errors,
            }
         } else {
            console.log('--------------------------------------------------')
            console.log('Invalid message');
            console.error(valid.errors);
            throw(valid.errors)
         }
      }
   },
}

async function deliver (message) {
   const delivery = process.env.DATASTORE || 'oasb';
   if (['oasb', 'both'].includes(delivery)) {
      console.log('Creating a P1 message in OASB');
      await ingest(message);
      return message;
   }
   if (['private', 'both'].includes(delivery)) {
      console.log('Creating a P1 message for the Private datastore');
      return message;
   }
}

async function lookupRelationsBy (customformatter, document) {
   const template = {id: null, e1: null, e2: null, p1: null, p2: null };
   const lambda = new AWS.Lambda();
   const doi = customformatter.formatDOIWithParser(document);
   try {
      const response = await lambda.invoke({
         FunctionName: process.env.LAMBDA_DOI,
         InvocationType: 'RequestResponse',
         Payload: JSON.stringify({
            doi,
            participant: process.env.SENDER,
            type: 'e1',
         }),
       }).promise();
       const parsed = JSON.parse(JSON.parse(response.Payload).body);
       return { ...template, ...parsed[0] };
   } catch (err) {
      console.error(err);
      return template;
   }
}

async function ingest(message) {
   const params = sqs.formatMessageForFifoQueue(
      message,
      process.env.PARTICIPANT,
      process.env.SQS_CONNECTORS_URL
   );
   if (process.env.STAGE === 'test') {
      return { success: true }
   }
   return await sqs.sendMessage(params);
}

function validate (message) {
   const Validator = require('jsonschema').Validator;
   const validator = new Validator();
   const schema = require(`../../switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
   const header = require(`../../switchboard/schemas/${process.env.VERSION}/header`).header;
   validator.addSchema(header, '/Header');
   return validator.validate(message, schema);
}

