const moment = require('moment');
const datemask = 'YYYY-MM-DD';
const xmlQuery = require('xml-query');
const XmlReader = require('xml-reader');
const ror = require(`../../libs/ror`);
const utils = require(`../../libs/utils`);

const formatter = {

	common: {
		stripPersonalData: (message, pio = true) =>{
			try {
				if (message.header) {
					message.header.pio = true;
				}
				if (message.data && message.data.authors) {
					message.data.authors.forEach(author => {
						delete author.email;
					});
				}
				if (message.data && message.data.charges) {
					delete message.data.charges;
				}
				if (message.data && message.data.settlement) {
					delete message.data.settlement;
				}

				if (pio) {
					message.header.pio = true;
					message.header.deduplicator = utils.uuid();
				}

				return message;
			} catch (error) {
				throw 'Message cannot be stripped from personal data';
			}
		},
	},

	p1: {

		format: async (article, deals, receiver, relations=[], key) => {
			const header = await formatter.p1.formatHeader(receiver, relations, key);
			const data = await formatter.p1.formatData(article, deals, receiver);
			const prioragreement = data.charges.prioragreement;
			header.meta = {
				prioragreement,
				source: 'JATS',
				status: 'PUBLICATION',
			};
			let message = { header, data };
			if (!prioragreement && isNPAPIOEnabled()) {
				console.log('PIO: stripping personal data and adding PIO flag');
				return formatter.common.stripPersonalData(message);
			}
			return message;
		},

		formatHeader: async (receiver, relations, key) => {
			const address = getRoutingReceiverId(receiver);
			const name = getRoutingReceiverName(receiver);
			if (address) {
				const header = {
					type: 'p1',
					from: { id: parseInt(process.env.SENDER, 10) },
					to: { address, name },
					validity: moment().add(1, 'year').format(datemask),
					template: 'templates/MessageRequest.html',
					persistent: true,
				};
				if (key) {
					header.key = key;
				}
				if (relations.e1) {
					header.e1 = relations.e1;
				}
				if (relations.e2) {
					header.e2 = relations.e2;
				}
				return header;
			}
			return {
				type: 'p1',
			};
		},

		formatData: async (document, deals, receiver) => {
			const ast = XmlReader.parseSync(document);
			const authors = await formatter.p1.formatAuthors(ast);
			const article = await formatter.p1.formatArticle(ast);
			const journal = await formatter.p1.formatJournal(ast);
			const charges = await formatter.p1.formatCharges(ast, deals, receiver);

			return {
				authors,
				article,
				journal,
				charges,
			};
		},

		formatJournal: (document) => {
			const journal = getJournal(document);
			return { ...journal, ...{ inDOAJ: false }};
		},

		formatLastName: (author) => author.lastName || '',

		formatFirstName: (author) => author.firstName || '',

		formatORCId: (author) => author.ORCID || '',

		formatEmail: (author) => author.email || '',

		formatCollaboration: () => '',

		formatListingOrder: (author) => parseInt(author.inputIndex, 10) + 1,

		formatCreditRoles: (author) => {
			return author.creditroles;
		},

		formatIsCorrespondingAuthor: (author) => !!author.isCorresponding,

		formatInstitutions: (author) => Array.from(new Set(author.institutions)) || [],

		formatCurrentAddress: (author) => Array.from(new Set(author.currentaddress)) || [],

		formatAffiliation: (author) => {
			const affiliations = author.institutions.map(institution => institution.name);
			return Array.from(new Set(affiliations)).join(',');
		},

		formatInitials: (author) => {
			return author.fullName.split(' ').map(name => name.substring(0, 1)).join('');
		},

		formatArticle: async (document) => {
			const grants = await formatter.p1.formatGrants(document);
			const manuscript = await formatter.p1.formatManuscript(document);
			const preprint = await formatter.p1.formatPreprtint(document);
			const vor = await formatter.p1.formatVOR(document);
			const funders = await formatter.p1.formatFunders(document);
			const acknowledgement = formatter.p1.formatFunderAcknowledgement(document);
			const type = await formatter.p1.formatArticleType(document);
			const originaltype = await formatter.p1.formatOriginalArticleType(document);
			const doi = await formatter.p1.formatDOIURL(document);
			const title = formatter.p1.formatArticleTitle(document);
			return {
				title,
				grants,
				manuscript,
				preprint,
				vor,
				type,
				originaltype,
				funders,
				acknowledgement,
				'submissionId': formatter.p1.formatSubmissionId(document),
				doi,
			};
		},

		formatArticleTitle: (document) => {
			return getArticleTitle(document) || 'Unknown';
		},

		formatArticleType: (document) => {
			const type = getType(document);
			if (type === 'research-article') {
				return 'research-article';
			}
			return 'Unknown'
		},

		formatOriginalArticleType: (document) => {
			return getType(document);
		},

		formatArticleTitle: (document) => {
			return getArticleTitle(document);
		},

		formatManuscript: (document) => {
			const submission = formatter.p1.formatSubmissionDate(document);
			const acceptance = formatter.p1.formatAcceptanceDate(document);
			const publication = formatter.p1.formatPublicationDate(document);
			const manuscript = {
				'dates': {},
				'id': formatter.p1.formatSubmissionId(document)
			};
			if (submission) {
				manuscript.dates.submission = submission;
			}
			if (acceptance) {
				manuscript.dates.acceptance = acceptance;
			}
			if (publication) {
				manuscript.dates.publication = publication;
			}
			return manuscript;
		},

		formatSubmissionId: (document) => {
			return getSubmissionId(document);
		},

		formatPublicationDate: (document) => {
			const publicationDate = getPublicationDate(document);
			return moment(publicationDate).isValid ? publicationDate : null;
		},

		formatSubmissionDate: (document) => {
			const submissionDate = getSumbissionDate(document);
			return moment(submissionDate).isValid ? submissionDate : null;
		},

		formatAcceptanceDate: (document) => {
			const acceptanceDate = getAcceptanceDate(document);
			return moment(acceptanceDate).isValid ? acceptanceDate : null;
		},

		formatPreprtint: () => ({}),

		formatVOR: (document) => {
			return {
				'publication': '',
				'license': formatter.p1.formatLicense(document) ,
				'deposition': '',
				'researchdata': ''
			}
		},

		formatLicense (document) {
			const license = getLicense(document);
			if (license === 'open-access') {
				return 'CC BY';
			}
			return 'Unknown'
		},

		formatFunders: async (document) => {
			let funders = getFunders(document);
			for (const funder of funders) {
				const enriched = await ror.generic.search(funder.name);
				if (enriched && enriched.rorid) {
					funder.rorid = enriched.rorid;
				}
			}
			return funders;
		},

		formatFunderAcknowledgement: (document) => (getAcknowledgement(document)),

		formatGrants: (document) => {
			return getGrants(document).map(name => {
				return {
					name,
				}
			}).filter(grant => grant.name);
		},

		formatAuthors: async (document) => {
			const authors = getAuthors(document);
			const formattedauthors = authors.map((author, index) => {
				return {
					lastName: formatter.p1.formatLastName(author),
					firstName: formatter.p1.formatFirstName(author),
					initials: formatter.p1.formatInitials(author),
					ORCID: formatter.p1.formatORCId(author),
					email: formatter.p1.formatEmail(author, document), // Please note that sometimes email is listed outside the contrib tag. Thus document
					collaboration: formatter.p1.formatCollaboration(author),
					listingorder: index + 1,
					creditroles: formatter.p1.formatCreditRoles(author),
					isCorrespondingAuthor: formatter.p1.formatIsCorrespondingAuthor(author),
					institutions: formatter.p1.formatInstitutions(author).flat(),
					currentaddress: formatter.p1.formatCurrentAddress(author).flat(),
					affiliation: formatter.p1.formatAffiliation(author),
				};
			});
			for (const formattedauthor of formattedauthors) {
				for (const institution of formattedauthor.institutions) {
					try {
						const enriched = await ror.generic.search(institution.name);
						if (enriched && enriched.rorid) {
							institution.rorid = enriched.rorid;
						}
					} catch (err) {
						console.log(err);
					}

				}

                for (const currentaddress of formattedauthor.currentaddress) {
					try {
						const enriched = await ror.generic.search(currentaddress.name);
						if (enriched && enriched.rorid) {
							currentaddress.rorid = enriched.rorid;
						}
					} catch (err) {
						console.log(err);
					}

				}
			}
			return formattedauthors;
		},

		formatCharges: (document, deals) => {
			const authors = getAuthors(document);
			const corresponding = [...authors].filter(author => author.isCorresponding);
			const agreements = getApplicableDeals (corresponding, deals);
			const deal = [...agreements].pop();
			if (agreements.length) {
				return {
					prioragreement: true,
					currency: 'EUR',
					agreement: {
						name: deal.Dealname,
						id: deal.DealID,
					}
				};
			}
			return {
				prioragreement: false,
				currency: 'EUR',
				fees: {
					apc: {
						type: 'per article APC list price',
						name: 'firm',
						amount: 0
					},
					total: {
						name: 'firm',
						amount: 0
					},
				},
			};
		},

		formatDOI: (document) => getDOI(document) || '',

		formatDOIURL: (document) => getDOIURL(document) || '',

		formatDOIWithParser: (document) => {
			const ast = XmlReader.parseSync(document);
			return getDOI(ast);
		},

	}

};

module.exports = formatter;

function getApplicableDeals (corresponding, deals) {
	const email = corresponding.map(author => author.email).map(email => email.split('@').pop()).pop();
	return deals.filter(deal => {
		return corresponding.filter(author => {
			const institutions = author.institutions.map(institution => institution.name);
			if (deal.Maildomain && deal.Maildomain.length && deal.Maildomain === email) {
				return true;
			}
			if (institutions.includes(deal.Institution)) {
				return true;
			}
			return false;
		}).length;
	});
}

function getType(ast) {
	return xmlQuery(ast).attr('article-type');
}

function getLicense(ast) {
	return xmlQuery(ast).find('license').attr('license-type');
}

function getSumbissionDate(ast) {
	return getDates(ast, 'received');
}

function getAcceptanceDate(ast) {
	return getDates(ast, 'accepted');
}

function getPublicationDate(ast) {
	const dates = xmlQuery(ast).find('pub-date').ast.filter(meta => xmlQuery(meta).attr('publication-format') === 'electronic')
	return getDate(dates);
}

function getDates (ast, type) {
	const dates = getArticleDates(ast).filter(meta => xmlQuery(meta).attr('date-type') === type);
	return getDate(dates);
}

function getDate (ast) {
    const referencedate = ast.length === 1 ? ast : ast[0] ;
	const year = xmlQuery(referencedate).find('year').text();
	const month = xmlQuery(referencedate).find('month').text();
    const day = xmlQuery(referencedate).find('day').text() || '1';
    const date = utils.isNumeric(year) && utils.isNumeric(month) && utils.isNumeric(day)
        ? moment.utc(`${year}-${month}-${day}`, 'YYYY-MM-DD') : null;
    return date && date.isValid()
        ? moment(date).format('YYYY-MM-DD') : null
}

function getSubmissionId (ast) {
	return getArticleMetadata(ast).filter(meta => xmlQuery(meta).attr('pub-id-type') === 'publisher-id').map(meta => xmlQuery(meta).text()).pop();
}

function getDOI (ast) {
	return getArticleMetadata(ast).filter(meta => xmlQuery(meta).attr('pub-id-type') === 'doi').map(meta => xmlQuery(meta).text()).pop();
}

function getDOIURL (ast) {
	const doi = getDOI(ast);
	if (doi) {
		return `https://doi.org/${doi}`;
	}
	return '';
}

function getJournal(ast) {
	const name = xmlQuery(ast).find('journal-title').text();
	const id = xmlQuery(ast).find('issn').ast
		.filter(tag => xmlQuery(tag).attr('publication-format') === 'electronic')
		.map(tag => xmlQuery(tag).text())
		.pop();
	return {
		name, id
	};
}

function getArticleTitle (ast) {
	return xmlQuery(ast).find('title-group').text();
}

function getFundingGroup (ast) {
	return xmlQuery(ast).find('funding-group').ast;
}

function getFunders (ast) {
	const funders = [];
	const fundingsources = getFundingGroup(ast).map(funder => xmlQuery(funder).find('award-group').ast);
	xmlQuery(fundingsources).each(source => {
		xmlQuery(source).each(node => {
			const funder = {
				name: xmlQuery(node).find('institution').text(),
				fundref: xmlQuery(node).find('institution-id').text().split('/').pop(),
			};
			funders.push(funder);
		});

	});
	return funders;
}

function getGrants (ast) {
	return getFundingGroup(ast).map(grant => xmlQuery(grant).find('award-id').map(id => xmlQuery(id).text()).flat()).flat();
}

function getAcknowledgement(ast) {
    return xmlQuery(ast).find('ack').children().find('p').text();

}

function getAuthors(ast) {
	const contributors = xmlQuery(ast).find('contrib');
	const affiliations = getAffiliations(ast);
	return contributors.ast.filter(contributor => {
		return xmlQuery(contributor).attr('contrib-type') === 'author';
	}).map(contributor => {
		const firstName = xmlQuery(contributor).find('given-names').text();
		const lastName = xmlQuery(contributor).find('surname').text();
		const email = xmlQuery(contributor).find('email').text();
		const ORCID = xmlQuery(contributor).find('contrib-id').text();
		const isCorresponding = xmlQuery(contributor).attr('corresp') === 'yes';
		const fullName = `${firstName} ${lastName}`;
		const institutions = Array.from(new Set(getAuthorInstitutions(contributor, affiliations)));
		const currentaddress = Array.from(new Set(getAuthorInstitutions(contributor, affiliations)));
		const creditroles = getCreditRoles (contributor);
		return {
			fullName,
			firstName,
			lastName,
			email,
			ORCID,
			isCorresponding,
			institutions,
			currentaddress,
			creditroles,
		};
	});
}

function getCreditRoles (contributor) {
	const allowed = [
		"conceptualization",
		"methodology",
		"software",
		"validation",
		"formal analysis",
		"investigation",
		"resources",
		"data curation",
		"writing—original draft",
		"writing—review & editing",
		"writing",
		"visualization",
		"supervision",
		"project administration",
		"funding acquisition",
	];
	return xmlQuery(contributor)
		.find('role')
		.map(role => xmlQuery(role)
		.attr('vocab-term').toLowerCase())
		.map(role => americanize(role))
		.filter(role => allowed.includes(role.toLowerCase()));
}

function americanize(role) {
	if (role === 'conceptualisation') {
		return 'conceptualization';
	}
	return role;
}

function getAffiliations (ast) {
	return xmlQuery(ast).find('aff').ast;
}

function getAuthorInstitutions (author, affiliations) {
    const institutions = xmlQuery(author).find('xref').ast
	.filter(ref => {
		return xmlQuery(ref).attr('ref-type') === 'aff';
	})
	.map(ref => xmlQuery(ref).attr("rid"))
	.map(ref => {
        return xmlQuery(affiliations).ast
			.filter(affiliation => xmlQuery(affiliation).attr("id") === ref)
			.map(affiliation => {
                const institution = xmlQuery(affiliation).find("institution").text();
				const processed = institution.length ?
                    institution :
                    xmlQuery(affiliation).text();
				return xmlQuery(affiliation).children().length ?
					xmlQuery(affiliation).children().last().text() :
					processed;
			});
        })
		.map(affiliation => {
		return { name: affiliation.pop() };
	});
    return institutions;
}

function getArticleMetadata (ast) {
	return xmlQuery(ast).find('article-id').ast;
}

function getArticleDates (ast) {
	return xmlQuery(ast).find('date').ast;
}

function getRoutingReceiverId (receiver) {

	if (receiver.ror && receiver.ror.length) {
		// console.log('Available for routing with id: rorid', receiver.rorid)
		return receiver.ror;
	}
	if (receiver.ringgoldid && receiver.ringgoldid.length) {
		// console.log('Available for routing with id: ringgoldid', receiver.ringgoldid)
		return receiver.ringgoldid;
	}
	if (receiver.maildomain && receiver.maildomain.length) {
		// console.log('Available for routing with id: maildomain', receiver.maildomain)
		return receiver.maildomain;
	}

	console.log("Receiver does not provide a usable routing identifier");

	return null;
}

function getRoutingReceiverName (receiver) {
	if (receiver.name && receiver.name.length) {
		return receiver.name;
	}
	if (receiver.affiliations && receiver.affiliations.length) {
		return receiver.affiliations[0].name;
	}
}

function isNPAPIOEnabled () {
	return !! process.env.NPA_PIO;
}
