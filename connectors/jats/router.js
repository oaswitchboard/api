const xmlQuery = require('xml-query');
const XmlReader = require('xml-reader');
const log = '/aws/unmatched';
const logger = require(`../../libs/logger`);
const ror = require(`../../libs/ror`);

module.exports = {
    p1: {
        receivers: async (document, deals, validate = false) => {
            const ast = XmlReader.parseSync(document);
            const receivers = [];
            for (const receiver of getCorrespondingAuthors(ast, deals)) {
                const enriched = await ror.p1.enrichReceiver(receiver);
                if (!hasValidRouting(enriched)) {
                    await logger.logEvent(JSON.stringify(enriched), "log", '/aws/unmatched');
                }
                receivers.push(enriched);
            }
            return receivers.filter(receiver => !validate || hasValidRouting(receiver));
        },
    },
};

function getCorrespondingAuthors (ast, deals) {
    const affiliations = xmlQuery(ast).find('aff').ast;
    return xmlQuery(ast).find('contrib').ast
        .filter(contributor => {
            return xmlQuery(contributor).attr('contrib-type') === 'author' && xmlQuery(contributor).attr('corresp') === 'yes';
        })
        .map(contributor => ({
            author: xmlQuery(contributor).find('string-name').text(),
            email: getEmail (ast),
            affiliations: getAuthorAffiliation(contributor, affiliations) || [],
            type: 'author',
            maildomain: getEmailDomain (ast),
        }))
        .map(contributor => getAuthorOrganisationSecondaryIds(contributor, deals));
}

function getEmail (ast) {
    return xmlQuery(ast).find('corresp').find('email').text();
}

function getEmailDomain (ast) {
    return getEmail(ast).split('@').pop();
}

function getFunders (ast, deals) {
    return xmlQuery(ast).find('funding-group').ast
        .map(funder => xmlQuery(funder)
        .find('institution').text())
        .map(funder => ({
            name: funder,
            type: 'funder',
        }))
        .map(funder => getFunderSecondaryIds(funder, deals));
}

function getAuthorOrganisationSecondaryIds(contributor, deals){
    const applicable = getApplicableAuthorDeals (contributor, deals);
    if (applicable.length) {
        const deal = applicable.pop();
        return { ...contributor, ...convertKeysToLowerCase(deal), ...{ name: deal.Institution} };
    }
    return contributor;
}

function getFunderSecondaryIds(funder, deals){
    const applicable = getApplicableFunderDeals (funder, deals);
    if (applicable.length) {
        const deal = applicable.pop();
        return { ...funder, ...convertKeysToLowerCase(deal) };
    }
    return funder;
}

function getApplicableAuthorDeals (contributor, deals) {
    const domainname = contributor.email ? contributor.email.split('@').pop() : '';
    return deals.filter(deal => {
        return (domainname.length && deal.Maildomain === domainname) || contributor.affiliations.filter(affiliation => affiliation.name === deal.Institution).length
	});
}

function getApplicableFunderDeals (funder, deals) {
    return deals.filter(deal => {
        return funder.name === deal.Institution;
	});
}

function getAuthorAffiliation (contributor, affiliations) {
    return xmlQuery(contributor).find('xref').ast
        .filter(ref => {
            return xmlQuery(ref).attr('ref-type') === 'aff';
        })
        .map(ref => xmlQuery(ref).attr("rid"))
        .map(ref => {
            return xmlQuery(affiliations).ast
                .filter(affiliation => xmlQuery(affiliation).attr("id") === ref)
                .map(affiliation => {
                    const institution = xmlQuery(affiliation).find("institution").text();
                    return institution.length ?
                        institution :
                        xmlQuery(affiliation).text();
                });
            })
            .map(affiliation => {
            return { name: affiliation.pop() };
        });
}

function convertKeysToLowerCase(object) {
    const converted = {};
    Object.keys(object).forEach(key => {
        converted[key.toLowerCase()] = object[key];
    });
    return converted;
}

function hasValidRouting(receiver) {
    return receiver.rorid;
 }

