const xmlQuery = require('xml-query');

const getAuthorInstitutions = (author, affiliations) => {
    const rids = xmlQuery(author).attr().rid;
    // Case with rid attribute ppointing to an affiliation
    if (rids) {
        const affiliationIds = rids.split(' ');
        let institutions = affiliationIds.map(id => {
            const affiliation = affiliations.find(affiliation => {
                return affiliation.attributes.id === id ? affiliation : null
            });
            const institution = xmlQuery(affiliation).find('institution').text();
            return {
                name: sanitize(institution),
            };
        });
        return institutions;
    }
    // Case with single institution tag
    const institution = xmlQuery(affiliations).find('institution').text();
    if (institution) {
        return [{
            name: sanitize(institution),
        }];
    }
    return [];
}

const sanitize = (name) => name.replace(/\n/g, ' ');

module.exports = {
    getAuthorInstitutions,
}