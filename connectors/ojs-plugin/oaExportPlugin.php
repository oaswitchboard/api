<?php

import('lib.pkp.classes.plugins.ImportExportPlugin');

class oaExportPlugin extends ImportExportPlugin {

	public function usage($scriptName) {
		echo "Usage: " . $scriptName . " " . $this->getName() . " [command]\n";
		echo "Commands:\n";
		echo "  import [filename]";
		echo "  export [filename]";
	}

	/**
	 * @copydoc ImportExportPlugin::executeCLI()
	 */
	public function executeCLI($scriptName, &$args) {
		$command = array_shift($args);
		$filename = array_shift($args);

		if ($command === 'import') {
			$data = file_get_contents($filename);
			/* now import the data */

		} elseif ($command === 'export') {
			$fp = fopen($filename, 'wt');
			fputcsv($fp, [/* export dataset */]);
			fclose($fp);
		}
	}

	function getName() {
		return 'oaExportsPlugin';
	}

	function getPluginSettingsPrefix() {
		return 'oaswitchboard';
	}

	public function register($category, $path, $mainContextId = NULL) {

        $success = parent::register($category, $path, $mainContextId);
        $this->addLocaleData();

//        if ($success && $this->getEnabled()) {
//
////            $request = Application::getRequest();
//
//            HookRegistry::register('TemplateManager::display', function (){
//                $request = $this->request;
//
//                $plugin_path = $this->getPluginPath();
//                $base_url = Application::getRequest()->getBaseUrl();
//
//                $js_url = $base_url . $plugin_path . '/scripts/oa_exports.js';
//                $css_url = $base_url . $plugin_path . '/scripts/oa_exports.css';
//
//                $templateMgr = TemplateManager::getManager($request);
//                $templateMgr->addJavaScript('oa_exports_js', $js_url, ['context'=>'backend', 'frontend']);
//                $templateMgr->addStyleSheet('oa_exports_css', $css_url, ['context'=>'backend', 'frontend']);
//            });
//        }
        return $success;
	}

	/**
	 * Provide a name for this plugin to appear in the plugins list where editors can
	 * enable and disable plugins.
	 */
	public function getDisplayName() {
		return __('plugins.importexport.oaExportPlugin.name');
	}

	public function getDescription() {
		return 'This plugin allows you to export article metadata to OA Switchboard.';
	}

	public function display($args, $request) {

		parent::display($args, $request);

		$templateMgr = TemplateManager::getManager($request);

		switch (array_shift($args)) {

			default:

            $templateMgr->display($this->getTemplatePath() . 'templates/index.tpl');

            break;
		}
	}
	public function getActions($request, $actionArgs) {

		// Get the existing actions
		$actions = parent::getActions($request, $actionArgs);
		if (!$this->getEnabled()) {
			return $actions;
		}

		// Create a LinkAction that will call the plugin's
		// `manage` method with the `settings` verb.
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.AjaxModal');
		$linkAction = new LinkAction(
			'settings',
			new AjaxModal($router->url($request,null,null,'manage',null,array(
						'verb' => 'settings',
						'plugin' => $this->getName(),
						'category' => 'importexport'
					)
				),
				$this->getDisplayName()
			),
			__('manager.plugins.settings'),
//			"nini hii?",
			null
		);

		// Add the LinkAction to the existing actions.
		// Make it the first action to be consistent with
		// other plugins.
		array_unshift($actions, $linkAction);

		return $actions;
	}

	public function manage($args, $request) {

		switch ($request->getUserVar('verb')) {

			case 'settings':

				// Load the custom form
				$this->import('oaExportSettingsForm');
				$form = new oaExportSettingsForm($this);

				// Fetch the form the first time it loads, before
				// the user has tried to save it
				if (!$request->getUserVar('save')) {
					$form->initData();
					return new JSONMessage(true, $form->fetch($request));
				}

				// Validate and execute the form
				$form->readInputData();
				if ($form->validate()) {
					$form->execute();
					return new JSONMessage(true);
				}
				break;
            case 'singleExport':
                if ($request->getUserVar('export_object')) {

                    $export_object = $request->getUserVar('export_object');

                    $response_msg = $this->exportSingle($export_object, $request);

                    return new JSONMessage(true, "<span style='color: #00b24e;'>{$response_msg}</span>");

                }else{
                    return new JSONMessage(true, "Failed");
                }

                break;
            case 'singleReject':
                if ($request->getUserVar('reject_object')) {

                    $reject_object = $request->getUserVar('reject_object');

                    $response_msg = $this->rejectSingle($reject_object, $request);

                    return new JSONMessage(true, "<span style='color: #f00;'>{$response_msg}</span>");

                }else{
                    return new JSONMessage(true, "Failed");
                }

                break;
			case 'rejectSubmissions':
                // Load the custom form
                $this->import('oaExportsForm');
                $form = new oaExportsForm($this);

                // Fetch the form the first time it loads, before
                // the user has tried to save it
                if (!$request->getUserVar('bulkReject')) {
                    $form->initData();
                    return new JSONMessage(true, "form not submitted");
                }else{
                    // Validate and execute the form
                    $form->readInputData();
                    if ($form->validate()) {
                        $selected_submissions = $form->getData('submissions_elem');

                        $context_id = $request->getContext()->getId();

                        $response_msg = array();

                        foreach ($selected_submissions as $submission){

                            try {

                                $this->updateSetting($context_id, "{$submission}_status", 'Ignored');

                                $response_msg[$submission] = "Success";

                                import('classes.notification.NotificationManager');

                                $notificationMgr = new NotificationManager();
                                $notificationMgr->createTrivialNotification(
                                    Application::getRequest()->getUser()->getId(),
                                    NOTIFICATION_TYPE_SUCCESS,
                                    ["contents" => "Item {$submission} ignored"]
                                );

                            } catch (Exception $e) {

                                $response_msg[$submission] = $e->getMessage();
                            }
                        }
                        return new JSONMessage(true, json_encode($response_msg));
                    }
                }

                break;
			case 'exportSubmissions':
                // Load the custom form
                $this->import('oaExportsForm');
                $form = new oaExportsForm($this);

                // Fetch the form the first time it loads, before
                // the user has tried to save it
                if (!$request->getUserVar('save')) {
                    $form->initData();
                    return new JSONMessage(true, $form->fetch($request));
                }else{
                    // Validate and execute the form
                    $form->readInputData();
                    if ($form->validate()) {

                        $selected_submissions = $form->getData('submissions_elem');

                        $json = new JSONMessage(true);

                        $context_id = $request->getContext()->getId();

                        //Send $selected_submissions to deposit_url
                        $exported_submissions = array();
                        $deposit_url = $this->getSetting($context_id, 'deposit_url');
                        $deposit_folder = $this->getSetting($context_id, 'deposit_folder');
                        $username = $this->getSetting($context_id, 'username');
                        $password = $this->getSetting($context_id, 'password');
                        $files_dir = Config::getVar('files', 'files_dir');

                        $this->import('SFTPConnection');
                        try {
                            $sftp = new SFTPConnection($deposit_url, 22);
                            $sftp->login($username, $password);

                        } catch (Exception $e) {
                            $json->setContent(json_encode($e->getMessage()));

                            return $json->getString();
                        }

                        foreach ($selected_submissions as $submission){

                            $articleDao = DAORegistry::getDAO('ArticleDAO');
                            $article_info = $articleDao->getById($id=$submission, $context=$context_id);

                            $oa_status = $this->getSetting($context_id, "{$submission}_status");

                            $journal_info = $request->getContext()->getAllData();

                            $primary_locale = $request->getContext()->getPrimaryLocale();

                            $json_file_contents = json_encode(array(
                                "submission_id" => $submission,
                                "authors" => $this->getAuthors($submission, $request),
                                'journal_id' => $article_info->_data['journalId'],
                                'journal_info' => $journal_info,
                                'stage_id' => isset($article_info->_data['stageId']) ? $article_info->_data['stageId']: "",
                                'status_id' => isset($article_info->_data['statusId']) ? $article_info->_data['statusId']: "",
                                'submission_step' => isset($article_info->_data['submissionProgress']) ? $article_info->_data['submissionProgress']: "",
                                'date_submitted' => isset($article_info->_data['dateSubmitted']) ? $article_info->_data['dateSubmitted']: "",
                                'date_status_modified' => isset($article_info->_data['dateStatusModified']) ? $article_info->_data['dateStatusModified']: "",
                                'last_modified' => isset($article_info->_data['lastModified']) ? $article_info->_data['lastModified']: "",
                                'language' => isset($article_info->_data['language']) ? $article_info->_data['language']: "",
                                'abstract' => isset($article_info->_data['abstract']) ? $article_info->_data['abstract'][$primary_locale]: "",
                                'clean_title' => isset($article_info->_data['cleanTitle']) ? $article_info->_data['cleanTitle'][$primary_locale]: "",
                                'title_prefix' => isset($article_info->_data['prefix']) ? $article_info->_data['prefix'][$primary_locale]: "",
                                'subtitle' => isset($article_info->_data['subtitle']) ? $article_info->_data['subtitle'][$primary_locale]: "",
                                'title' => isset($article_info->_data['title']) ? $article_info->_data['title'][$primary_locale]: "",
                                'section_id' => isset($article_info->_data['sectionId']) ? $article_info->_data['sectionId']: "",
                                'section_abbreviation' => isset($article_info->_data['sectionAbbrev']) ? $article_info->_data['sectionAbbrev']: "",
                                'section_title' => isset($article_info->_data['sectionTitle']) ? $article_info->_data['sectionTitle'][$primary_locale]: "",
                            ));

                            $json_file = fopen("{$files_dir}/submission_{$submission}.json", "w") or die("Unable to open file!");

                            fwrite($json_file, $json_file_contents);

                            fclose($json_file);

                            $this->import('SFTPConnection');
                            try {

                                $sftp->uploadFile("{$files_dir}/submission_{$submission}.json", "{$deposit_folder}/submission_{$submission}.json");

                                $setting_name = "{$submission}_status";
                                $this->updateSetting($context_id, $setting_name, 'Exported');

                                //Delete file from disk
                                unlink($json_file);

                            } catch (Exception $e) {
                                $json->setContent(json_encode($e->getMessage()));
                            }

                        }
                        $json->setEvent('addTab', array(
                            'title' => __('plugins.importexport.oaExportPlugin.forms.results'),
                            'url' => $request->url($request->getContext(), null, null,
                                array('plugin', $this->getName()),
                                array(
                                    'verb' => 'exportResults',
                                    'category' => $request->getUserVar('category'),
                                    'plugin' => $request->getUserVar('plugin'),
                                )
                            ),
                        ));
                        return $json->getString();

//                        header("Location: {$_SERVER['HTTP_REFERER']}");

                    }else{ // form not valid
                        return new JSONMessage(true, $form->fetch($request));
                    }
                }
                break;
            case 'exportResults':
                return new JSONMessage(true, "Submissions exported");
                break;
            case 'submittedManuscripts':

                // Load the custom form
                $this->import('oaExportsForm');
                $form = new oaExportsForm($this);


                $qb = new \OJS\Services\QueryBuilders\SubmissionListQueryBuilder($request->getContext()->getId());

                $qb->filterByStageIds([1]);

                $qo = $qb->get();
                $submissionDao = DAORegistry::getDAO('ArticleDAO');
                $result = $submissionDao->retrieve($qo->toSql(), $qo->getBindings());
                $result_set = new DAOResultFactory($result,  $submissionDao, '_fromRow');

                $submitted_manuscripts = array();

                foreach ($result_set->toArray() as $submissions){

                    $articleDao = DAORegistry::getDAO('ArticleDAO');
                    $article_info = $articleDao->getById($id=$submissions->_data['id'], $context=$request->getContext()->getId());

                    $authors = $this->getAuthors($submissions->_data['id'], $request);
                    $primary_author = $this->getPrimaryAuthor($authors);

                    $primary_locale = $request->getContext()->getPrimaryLocale();

                    //Skip incomplete submissions
                    //Incomplete submissions have values 1 - 4 and 0 when complete
                    if ($submissions->_data['submissionProgress'] > 0){ continue; }

                    //Skip submissions already accepted
                    if ( ! $submissions->_data['stageId'] == 1){ continue; }

                    $oa_status = $this->getSetting($request->getContext()->getId(), "{$submissions->_data['id']}_status");

                    $submission_metadata = array(
                        'oa_status' => $oa_status,
                        'submission_id' => $submissions->_data['id'],
                        'section_abbreviation' => isset($article_info->_data['sectionAbbrev']) ? $article_info->_data['sectionAbbrev']: "",
                        'journal' => $request->getContext()->getLocalizedName(),
                        'primary_author' => $primary_author['last_name'] . ', '. $primary_author['first_name'],
                        'title' => isset($submissions->_data['title']) ? $submissions->_data['title'][$primary_locale]: ""
                    );
                    array_push($submitted_manuscripts, $submission_metadata);
                }

                // Output the metadata
                $templateMgr = TemplateManager::getManager($request);

                $templateMgr->assign('submitted_manuscripts', $submitted_manuscripts);

                $templateMgr->assign('pluginName', $this->getName());

                $template_file = $templateMgr->fetch($this->getTemplatePath(). 'templates/submitted_manuscripts.tpl');
                $json = new JSONMessage(true, $template_file);

                return $json;

                break;
            case 'acceptedManuscripts':

                $qb = new \OJS\Services\QueryBuilders\SubmissionListQueryBuilder($request->getContext()->getId());

                //Accepted manuscripts have stage ID >= 2 < 5
                $qb->filterByStageIds([2, 3, 4]);

                $qo = $qb->get();
                $submissionDao = DAORegistry::getDAO('ArticleDAO');
                $result = $submissionDao->retrieve($qo->toSql(), $qo->getBindings());
                $result_set = new DAOResultFactory($result,  $submissionDao, '_fromRow');

                $accepted_manuscripts = array();

                if( $result_set->getCount() > 0 ){

                    foreach ($result_set->toArray() as $submissions){

                        $articleDao = DAORegistry::getDAO('ArticleDAO');
                        $article_info = $articleDao->getById($id=$submissions->_data['id'], $context=$request->getContext()->getId());


                        $authors = $this->getAuthors($submissions->_data['id'], $request);

                        $primary_author = $this->getPrimaryAuthor($authors);

                        //Skip incomplete submissions
                        //Incomplete submissions have values 1 - 4, and 0 when complete
                        if ($submissions->_data['submissionProgress'] > 0){ continue; }

                        $oa_status = $this->getSetting($request->getContext()->getId(), "{$submissions->_data['id']}_status");

                        $submission_metadata = array(
                            'submission_id' => $submissions->_data['id'],
                            'section_abbreviation' => isset($article_info->_data['sectionAbbrev']) ? $article_info->_data['sectionAbbrev']: "",
                            'oa_status' => $oa_status,
                            'journal' => $request->getContext()->getLocalizedName(),
                            'primary_author' => $primary_author['last_name'] . ', '. $primary_author['first_name'],
                            'title' => $submissions->_data['title'][$request->getContext()->getPrimaryLocale()]
                        );
                        array_push($accepted_manuscripts, $submission_metadata);
                    }
                }

                // Output the metadata
                $templateMgr = TemplateManager::getManager($request);

                $templateMgr->assign('accepted_manuscripts', $accepted_manuscripts);
                $templateMgr->assign('pluginName', $this->getName());

                $template_file = $templateMgr->fetch($this->getTemplatePath() . 'templates/accepted_manuscripts.tpl');
                $json = new JSONMessage(true, $template_file);

                return $json;

                break;
            case 'publishedManuscripts':

                $qb = new \OJS\Services\QueryBuilders\SubmissionListQueryBuilder($request->getContext()->getId());

                //Published manuscripts have stage ID == 5
                $qb->filterByStageIds([5]);

                $qo = $qb->get();
                $submissionDao = DAORegistry::getDAO('ArticleDAO');
                $result = $submissionDao->retrieve($qo->toSql(), $qo->getBindings());
                $result_set = new DAOResultFactory($result,  $submissionDao, '_fromRow');

                $published_manuscripts = array();

                foreach ($result_set->toArray() as $submissions){

                    $articleDao = DAORegistry::getDAO('ArticleDAO');
                    $article_info = $articleDao->getById($id=$submissions->_data['id'], $context=$request->getContext()->getId());


                    $authors = $this->getAuthors($submissions->_data['id'], $request);
                    $primary_author = $this->getPrimaryAuthor($authors);

                    $oa_status = $this->getSetting($request->getContext()->getId(), "{$submissions->_data['id']}_status");

                    $submission_metadata = array(
                        'submission_id' => $submissions->_data['id'],
                        'section_abbreviation' => isset($article_info->_data['sectionAbbrev']) ? $article_info->_data['sectionAbbrev']: "",
                        'oa_status' => $oa_status,
                        'journal' => $request->getContext()->getLocalizedName(),
                        'primary_author' => $primary_author['last_name'] . ', '. $primary_author['first_name'],
                        'title' => $submissions->_data['title'][$request->getContext()->getPrimaryLocale()]
                    );
                    array_push($published_manuscripts, $submission_metadata);
                }

                // Output the metadata
                $templateMgr = TemplateManager::getManager($request);

                $templateMgr->assign('published_manuscripts', $published_manuscripts);
                $templateMgr->assign('pluginName', $this->getName());

                $template_file = $templateMgr->fetch($this->getTemplatePath() . 'templates/published_manuscripts.tpl');
                $json = new JSONMessage(true, $template_file);

                return $json;

                break;
		}
		return parent::manage($args, $request);
	}

	public function getPrimaryAuthor($authors){

	    $primary_author = null;

	    foreach ($authors as $author){
	        if ($author['primary_contact'] == 1){
	            $primary_author = $author ;

	            break;

	        } else {
	            continue;
            }
        }
	    return $primary_author;
    }
	public function getAuthors($submission_id, $request){
        $authorDao = DAORegistry::getDAO('AuthorDAO');

        $authors = array();
        $primary_locale = $request->getContext()->getPrimaryLocale();

        foreach ($authorDao->getBySubmissionId($submission_id) as $author){

//            print_r($author->_data['biography']);

            array_push($authors, array(
                'author_id' => isset($author->_data['id']) ? $author->_data['id']: "",
                'country' => isset($author->_data['country']) ? $author->_data['country']: "", //not available
                'email' => isset($author->_data['email']) ? $author->_data['email']: "",
                'url' => isset($author->_data['url']) ? $author->_data['url']: "",
                'primary_contact' => isset($author->_data['primaryContact']) ? $author->_data['primaryContact']: "",
                'orcid' => isset($author->_data['orcid']) ? $author->_data['orcid']: "",
                'affiliation' => isset($author->_data['affiliation']) ? $author->_data['affiliation'][$primary_locale]: "",
                'biography' => isset($author->_data['biography']) ? $author->_data['biography'][$primary_locale]: "",
                'first_name' => isset($author->_data['firstName']) ? $author->_data['firstName']: "",
                'middle_name' => isset($author->_data['middleName']) ? $author->_data['middleName']: "",
                'last_name' => isset($author->_data['lastName']) ? $author->_data['lastName']: "",
                'preferred_public_name' => isset($author->_data['preferredPublicName']) ? $author->_data['preferredPublicName'][$primary_locale]: "",
                'sequence' => isset($author->_data['sequence']) ? $author->_data['sequence']: ""
            ));
        }

        return $authors;
    }

    function exportSingle($submission_id, $request){

	    $context_id = $request->getContext()->getId();
        $articleDao = DAORegistry::getDAO('ArticleDAO');
        $article_info = $articleDao->getById($id=$submission_id, $context=$context_id);
        $journal_info = $request->getContext()->getAllData();
        $primary_locale = $request->getContext()->getPrimaryLocale();
        $files_dir = Config::getVar('files', 'files_dir');

        $json_file_contents = json_encode(array(
            "submission_id" => $submission_id,
            "authors" => $this->getAuthors($submission_id, $request),
            'journal_id' => $article_info->_data['journalId'],
            'journal_info' => $journal_info,
            'stage_id' => isset($article_info->_data['stageId']) ? $article_info->_data['stageId']: "",
            'status_id' => isset($article_info->_data['statusId']) ? $article_info->_data['statusId']: "",
            'submission_step' => isset($article_info->_data['submissionProgress']) ? $article_info->_data['submissionProgress']: "",
            'date_submitted' => isset($article_info->_data['dateSubmitted']) ? $article_info->_data['dateSubmitted']: "",
            'date_status_modified' => isset($article_info->_data['dateStatusModified']) ? $article_info->_data['dateStatusModified']: "",
            'last_modified' => isset($article_info->_data['lastModified']) ? $article_info->_data['lastModified']: "",
            'language' => isset($article_info->_data['language']) ? $article_info->_data['language']: "",
            'abstract' => isset($article_info->_data['abstract']) ? $article_info->_data['abstract'][$primary_locale]: "",
            'clean_title' => isset($article_info->_data['cleanTitle']) ? $article_info->_data['cleanTitle'][$primary_locale]: "",
            'title_prefix' => isset($article_info->_data['prefix']) ? $article_info->_data['prefix'][$primary_locale]: "",
            'subtitle' => isset($article_info->_data['subtitle']) ? $article_info->_data['subtitle'][$primary_locale]: "",
            'title' => isset($article_info->_data['title']) ? $article_info->_data['title'][$primary_locale]: "",
            'section_id' => isset($article_info->_data['sectionId']) ? $article_info->_data['sectionId']: "",
            'section_abbreviation' => isset($article_info->_data['sectionAbbrev']) ? $article_info->_data['sectionAbbrev']: "",
            'section_title' => isset($article_info->_data['sectionTitle']) ? $article_info->_data['sectionTitle']: ""
        ));

        $deposit_url = $this->getSetting($context_id, 'deposit_url');
        $deposit_folder = $this->getSetting($context_id, 'deposit_folder');
        $username = $this->getSetting($context_id, 'username');
        $password = $this->getSetting($context_id, 'password');

        $json_file = fopen("{$files_dir}/submission_{$submission_id}.json", "w") or die("Unable to open file!");

        fwrite($json_file, $json_file_contents);

        fclose($json_file);

        $this->import('SFTPConnection');
        try {
            $sftp = new SFTPConnection($deposit_url, 22);
            $sftp->login($username, $password);
            $sftp->uploadFile("{$files_dir}/submission_{$submission_id}.json", "{$deposit_folder}/submission_{$submission_id}.json");

            $setting_name = "{$submission_id}_status";
            $this->updateSetting($context_id, $setting_name, 'Exported');

            $response_msg = "Exported";

            unlink("{$files_dir}/submission_{$submission_id}.json");

        } catch (Exception $e) {
            $response_msg = $e->getMessage();
        }
        return $response_msg;
    }

    function rejectSingle($submission_id, $request){

        $context_id = $request->getContext()->getId();

        try {

            $this->updateSetting($context_id, "{$submission_id}_status", 'Ignored');

            $response_msg = "Ignored";

        } catch (Exception $e) {

            $response_msg = $e->getMessage();
        }

        return $response_msg;
    }

}
