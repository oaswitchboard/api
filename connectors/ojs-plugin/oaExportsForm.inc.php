<?php
import('lib.pkp.classes.form.Form');
class oaExportsForm extends Form {

	/** @var oaExportsForm  */
	public $plugin;

	public function __construct($plugin) {

		// Define the settings template and store a copy of the plugin object
		parent::__construct($plugin->getTemplatePath() . 'templates/exports_form.tpl');
		$this->plugin = $plugin;

		// Always add POST and CSRF validation to secure your form.
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
	}

	/**
	 * Load settings already saved in the database
	 *
	 * Settings are stored by context, so that each journal or press
	 * can have different settings.
	 */
	public function initData() {
		$contextId = Application::getRequest()->getContext()->getId();

		$this->setData('pluginName', $this->plugin->getName());
		parent::initData();
	}

	/**
	 * Load data that was submitted with the form
	 */
	public function readInputData() {
		$this->readUserVars(['submissions_elem']);
//		$this->readUserVars(['password']);
//		$this->readUserVars(['deposit_url']);
		parent::readInputData();
	}

	/**
	 * Fetch any additional data needed for your form.
	 *
	 * Data assigned to the form using $this->setData() during the
	 * initData() or readInputData() methods will be passed to the
	 * template.
	 */
	public function fetch($request, $template = null, $display = false) {

		$context = $request->getContext();

        import('lib.pkp.controllers.list.submissions.SelectSubmissionsListHandler');
        $exportSubmissionsListHandler = new SelectSubmissionsListHandler(array(

            'inputName' => 'selectedSubmissions[]',
            'lazyLoad' => true,
        ));

        $submissions_list = array();

        foreach ($exportSubmissionsListHandler->getItems() as $key=>$val){

            //skip unpublished submissions

            if( ! ( $val['status']['label'] == "Published" || $val['status']['id'] == 3 ) ){ continue; }

            $submissions_list[$val['id']]['full_title'] = $val['fullTitle'][$context->getPrimaryLocale()];
            $status_label = $val['status']['label'];
            $status_id = $val['status']['id'];
            $submissions_list[$val['id']]['authors'] = $val['authorString'];
            $url_published = $val['urlPublished'];
        }


        $templateMgr = TemplateManager::getManager($request);
        $templateMgr->assign('submissions_list', $submissions_list);

		$templateMgr->assign('pluginName', $this->plugin->getName());
		$templateMgr->assign('full_object',  $exportSubmissionsListHandler->getItems());

		return parent::fetch($request, $template, $display);
	}

	/**
	 * Save the settings
	 */
	public function execute(...$functionArgs) {

        // Tell the user that the save was successful.
		import('classes.notification.NotificationManager');

		$notificationMgr = new NotificationManager();
		$notificationMgr->createTrivialNotification(
			Application::getRequest()->getUser()->getId(),
			NOTIFICATION_TYPE_SUCCESS,
			['contents' => __('plugins.importexport.oaExportPlugin.forms.export_success_message')]
		);
		return parent::execute();
	}

}
