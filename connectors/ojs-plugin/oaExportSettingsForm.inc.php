<?php
import('lib.pkp.classes.form.Form');
class oaExportSettingsForm extends Form {

	/** @var oaExportSettingsForm  */
	public $plugin;

	public function __construct($plugin) {

		// Define the settings template and store a copy of the plugin object
		parent::__construct($plugin->getTemplatePath() . 'templates/settings.tpl');
		$this->plugin = $plugin;

		// Always add POST and CSRF validation to secure your form.
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
	}

	/**
	 * Load settings already saved in the database
	 *
	 * Settings are stored by context, so that each journal or press
	 * can have different settings.
	 */
	public function initData() {
		$contextId = Application::getRequest()->getContext()->getId();

		$this->setData('username', $this->plugin->getSetting($contextId, 'username'));
		$this->setData('password', $this->plugin->getSetting($contextId, 'password'));
		$this->setData('deposit_url', $this->plugin->getSetting($contextId, 'deposit_url'));
		$this->setData('deposit_folder', $this->plugin->getSetting($contextId, 'deposit_folder'));
		$this->setData('pluginName', $this->plugin->getName());
		parent::initData();
	}

	/**
	 * Load data that was submitted with the form
	 */
	public function readInputData() {
		$this->readUserVars(['username']);
		$this->readUserVars(['password']);
		$this->readUserVars(['deposit_url']);
		$this->readUserVars(['deposit_folder']);
		parent::readInputData();
	}

	/**
	 * Fetch any additional data needed for your form.
	 *
	 * Data assigned to the form using $this->setData() during the
	 * initData() or readInputData() methods will be passed to the
	 * template.
	 */
	public function fetch($request, $template = null, $display = false) {

		// Pass the plugin name to the template so that it can be
		// used in the URL that the form is submitted to
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->assign('pluginName', $this->plugin->getName());

		return parent::fetch($request, $template, $display);
	}

	/**
	 * Save the settings
	 */
	public function execute(...$functionArgs) {
		$contextId = Application::getRequest()->getContext()->getId();

		$this->plugin->updateSetting($contextId, 'username', $this->getData('username'));
		$this->plugin->updateSetting($contextId, 'password', $this->getData('password'));
		$this->plugin->updateSetting($contextId, 'deposit_url', $this->getData('deposit_url'));
		$this->plugin->updateSetting($contextId, 'deposit_folder', $this->getData('deposit_folder'));

		// Tell the user that the save was successful.
		import('classes.notification.NotificationManager');
		$notificationMgr = new NotificationManager();
		$notificationMgr->createTrivialNotification(
			Application::getRequest()->getUser()->getId(),
			NOTIFICATION_TYPE_SUCCESS,
			['contents' => __('common.changesSaved')]
		);
		return parent::execute();
	}
}
