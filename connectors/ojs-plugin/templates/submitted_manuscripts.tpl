{**
 * @file plugins/importexport/oaexports/templates/submitted_manuscripts.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * List of operations this plugin can perform
 *}
<script type="text/javascript">
    $(function() {ldelim}
        // Attach the form handler.
        $('#submitted_manuscripts_form').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
        $('#submitted_manuscripts_table').DataTable();

        {rdelim});
</script>

{$raw_resultset}

{if !empty($submitted_manuscripts)}
    <form class="pkp_form" id="submitted_manuscripts_form" method="POST"
          action="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="exportSubmissions" save=true}"
    >
        {csrf}
        {fbvFormArea id="oaExportSettingsFormArea"}
            {fbvFormSection}
                <div class="pkp_controllers_grid">
                    <table id="submitted_manuscripts_table">
                        <caption class="header pkp_tasks" style="text-align: left; border-bottom: solid 1px #ddd;">
                            <h4><span class="task-count">{$submitted_manuscripts|@count}</span> Items total</h4>
                        </caption>
                        <thead class="header">
                        <th>Select</th><th>ID</th><th>Journal</th><th>Title</th><th>Status</th>
                        </thead>
                        <tbody>
                        {section name=map loop=$submitted_manuscripts}
                            <tr>
                                <td>
                                    <input type="checkbox" id="submission_{$submitted_manuscripts[map].submission_id}" name="submissions_elem[]" value="{$submitted_manuscripts[map].submission_id}"/>
                                </td>
                                <td>{$submitted_manuscripts[map].submission_id}</td>
                                <td>
                                    {$submitted_manuscripts[map].journal} :
                                    {if !empty($submitted_manuscripts[map].section_abbreviation)}
                                        <span style="font-weight: bold;">
                                            {$submitted_manuscripts[map].section_abbreviation}
                                        </span>
                                    {/if}
                                </td>
                                <td>
                                    {if !empty($submitted_manuscripts[map].primary_author)}
                                        <span style="font-weight: bold;">
                                            {$submitted_manuscripts[map].primary_author}:
                                        </span>
                                    {/if}
                                    {$submitted_manuscripts[map].title}
                                </td>
                                <td>
                                    <span id="{$submitted_manuscripts[map].submission_id}_status">
                                        {if !empty($submitted_manuscripts[map].oa_status)}
                                            {if $submitted_manuscripts[map].oa_status == "Exported"}
                                                <span style='color: #0f0;'>
                                            {else}
                                                <span style='color: #f00;'>
                                            {/if}
                                                {$submitted_manuscripts[map].oa_status}
                                            </span>
                                        {else}
                                            New
                                        {/if}
                                    </span><br />
                                    <a onclick="singleExport({$submitted_manuscripts[map].submission_id}); return false;" id="{$submitted_manuscripts[map].submission_id}_single_export" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleExport" export_object=$submitted_manuscripts[map].submission_id}">Export</a> |
                                    <a onclick="singleReject({$submitted_manuscripts[map].submission_id}); return false;" id="{$submitted_manuscripts[map].submission_id}_single_reject" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleReject" reject_object=$submitted_manuscripts[map].submission_id}">Ignore</a>
                                </td>
                            </tr>
                        {/section}
                        </tbody>
                    </table>
                </div>
            {/fbvFormSection}
        {/fbvFormArea}
{*        {fbvFormButtons submitText="plugins.importexport.oaExportPlugin.forms.export" hideCancel=true}*}
        <div class="section formButtons form_buttons">
            <button class="pkp_button" type="submit">Export</button>
            <button class="pkp_button" type="button" onclick="bulkReject($('#submitted_manuscripts_form input')); return false;">Ignore</button>
            <span class="pkp_spinner"></span>
        </div>
    </form>

{else}
	<h4>No queued manuscripts</h4>
{/if}


