{**
 * @file plugins/importexport/oaexports/index.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * List of operations this plugin can perform
 *}
{literal}
	<link type="text/css" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
	<style type="text/css">
		#submitted_manuscripts_table_wrapper, #accepted_manuscripts_table_wrapper, #published_manuscripts_table_wrapper{
			box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 8px; padding: 12px;
		}
		table thead th{ text-align: left;}
	</style>
{/literal}
{strip}
	{include file="common/header.tpl" pageTitle="plugins.importexport.oaExportPlugin.name"}
{/strip}
{literal}
	<script defer src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
{/literal}

	<script type="text/javascript">
		// Attach the JS file tab handler.
		$(function() {ldelim}
			$('#importExportTabs').pkpHandler('$.pkp.controllers.TabHandler');
			$('#importExportTabs').tabs('option', 'cache', false);

		{rdelim});
	</script>

	<div id="importExportTabs">
		<ul>
			<li><a href="#settings-tab">Settings</a></li>
			<li><a href="#submitted_manuscripts-tab">Submitted manuscripts</a></li>
			<li><a href="#accepted_manuscripts-tab">Accepted manuscripts</a></li>
			<li><a href="#published_manuscripts-tab">Published manuscripts</a></li>
		</ul>
		<div id="settings-tab">
			{capture assign=oaExportSettingsGridUrl}
				{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" plugin="oaExportsPlugin" category="importexport" verb="settings" escape=false}
			{/capture}
			{load_url_in_div id="oaExportSettingsGridContainer" url=$oaExportSettingsGridUrl}
		</div>
		<div id="submitted_manuscripts-tab">
			{capture assign=oaSubmittedManuscriptsGridUrl}
				{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" plugin="oaExportsPlugin" category="importexport" verb="submittedManuscripts" escape=false}
			{/capture}
			{load_url_in_div id="oaSubmittedManuscriptsGridContainer" url=$oaSubmittedManuscriptsGridUrl}

		</div>
		<div id="accepted_manuscripts-tab">
			{capture assign=oaAcceptedManuscriptsGridUrl}
				{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" plugin="oaExportsPlugin" category="importexport" verb="acceptedManuscripts" escape=false}
			{/capture}
			{load_url_in_div id="oaAcceptedManuscriptsGridContainer" url=$oaAcceptedManuscriptsGridUrl}
		</div>
		<div id="published_manuscripts-tab">
			{capture assign=oaPublishedManuscriptsGridUrl}
				{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" plugin="oaExportsPlugin" category="importexport" verb="publishedManuscripts" escape=false}
			{/capture}
			{load_url_in_div id="oaPublishedManuscriptsGridContainer" url=$oaPublishedManuscriptsGridUrl}
		</div>
	</div>
<script type="text/javascript">

	{literal}
	// $(window).off('beforeunload');
	function singleExport(obj_id) {

		$("#" + obj_id + "_status").html("exporting ...");

		$.ajax({
			url: $("#" + obj_id + "_single_export").attr('href'),
			success: function (result) {
				// console.log(result.content);
				let results = JSON.parse(result);
				$("#" + obj_id + "_status").html(results.content);
			}
		});
	}
	function singleReject(obj_id) {

		$("#" + obj_id + "_status").html("Ignoring ...");

		$.ajax({
			url: $("#" + obj_id + "_single_reject").attr('href'),
			success: function (result) {
				// window.console.log(result);
				let results = JSON.parse(result);
				$("#" + obj_id + "_status").html(results.content);
			}
		});
	}
	{/literal}
	function bulkReject(form_obj) {ldelim}
		$(".pkp_spinner").html('Ignoring ...');

		let bulk_reject_url = "{url router=$smarty.const.ROUTE_COMPONENT component='grid.settings.plugins.settingsPluginGridHandler' op='manage' plugin='oaExportsPlugin' category='importexport' verb='rejectSubmissions' bulkReject=true escape=false}";

		// alert(form_obj.serialize());
		$.ajax({ldelim}
			url: bulk_reject_url,
			data: form_obj.serialize(),
			method: "POST",
			success: function (result) {ldelim}
				$(window).off('beforeunload');
				return window.location.reload();
			{rdelim},
			error: function (err) {ldelim}
				alert(err);
			{rdelim}
		{rdelim});

		return false;
		{rdelim}
</script>

{strip}
	{include file="common/footer.tpl"}

{/strip}