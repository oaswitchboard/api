{**
 * @file plugins/importexport/oaexports/templates/settings.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * List of operations this plugin can perform
 *}

<script type="text/javascript">
	$(function() {ldelim}
		// Attach the form handler.
		$('#oaSettingsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
		{rdelim});
</script>

<form class="pkp_form" id="oaSettingsForm" method="POST"
		action="{url router=$smarty.const.ROUTE_COMPONENT op="manage" category="importexport" plugin=$pluginName verb="settings" save=true}"
>
	{csrf}

	{fbvFormArea id="oaExportSettingsFormArea"}
		{fbvFormSection}
			{fbvElement type="text" id="username" value=$username maxlength="50" label="plugins.importexport.oaExportPlugin.forms.username" required="true" size=$fbvStyles.size.MEDIUM}
		{/fbvFormSection}
		{fbvFormSection}
			{fbvElement type="text" password="true" id="password" value=$password label="plugins.importexport.common.settings.form.password" maxLength="50" required="true" size=$fbvStyles.size.MEDIUM}
		{/fbvFormSection}
		{fbvFormSection}
			{fbvElement type="text" id="deposit_url" value=$deposit_url label="plugins.importexport.oaExportPlugin.forms.deposit_url" maxlength="300" required="true" size=$fbvStyles.size.MEDIUM}
		{/fbvFormSection}
		{fbvFormSection}
			{fbvElement type="text" id="deposit_folder" value=$deposit_folder label="plugins.importexport.oaExportPlugin.forms.deposit_folder" maxlength="300" required="true" size=$fbvStyles.size.MEDIUM}
		{/fbvFormSection}
	{/fbvFormArea}
	{fbvFormButtons submitText="common.save"}
</form>
