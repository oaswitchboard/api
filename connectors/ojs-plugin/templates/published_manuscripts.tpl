{**
 * @file plugins/importexport/oaexports/templates/accepted_manuscripts.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 *}

{if !empty($published_manuscripts)}
	<script type="text/javascript">
		// Attach the JS file tab handler.
		$(function() {ldelim}
			$('#published_manuscripts_form').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
			$('#published_manuscripts_table').DataTable();
		{rdelim});
	</script>
<form class="pkp_form" id="published_manuscripts_form" method="POST"
	  action="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="exportSubmissions" save=true}"
>
	{csrf}
	{fbvFormArea id="oaExportSettingsFormArea"}
		{fbvFormSection}
			<div class="pkp_controllers_grid">
				<table id="published_manuscripts_table">
					<caption class="header pkp_tasks" style="text-align: left; border-bottom: solid 1px #ddd;">
						<h4><span class="task-count">{$published_manuscripts|@count}</span> Items total</h4>
					</caption>
					<thead class="header">
					<th>Select</th><th>ID</th><th>Journal</th><th>Title</th><th>Status</th>
					</thead>
					<tbody>
					{section name=map loop=$published_manuscripts}
						<tr>
							<td>
								<input type="checkbox" id="submission_{$published_manuscripts[map].submission_id}" name="submissions_elem[]" value="{$published_manuscripts[map].submission_id}"/>
							</td>
							<td>{$published_manuscripts[map].submission_id}</td>
							<td>
								{$published_manuscripts[map].journal} :
								<span style="font-weight: bold;">
									{$published_manuscripts[map].section_abbreviation}
								</span>
							</td>
							<td>
								<span style="font-weight: bold;">
									{$published_manuscripts[map].primary_author}:
								</span>
								{$published_manuscripts[map].title}
							</td>
							<td>
                                    <span id="{$published_manuscripts[map].submission_id}_status">
                                        {if !empty($published_manuscripts[map].oa_status)}
										{if $published_manuscripts[map].oa_status == "Exported"}
                                                <span style='color: #0f0;'>
                                            {else}
                                                <span style='color: #f00;'>
                                            {/if}
												{$published_manuscripts[map].oa_status}
                                            </span>
                                        {else}
                                            New
                                        {/if}
                                    </span><br />
                                    <a onclick="singleExport({$published_manuscripts[map].submission_id}); return false;" id="{$published_manuscripts[map].submission_id}_single_export" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleExport" export_object=$published_manuscripts[map].submission_id}">Export</a> |
                                    <a onclick="singleReject({$published_manuscripts[map].submission_id}); return false;" id="{$published_manuscripts[map].submission_id}_single_reject" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleReject" reject_object=$published_manuscripts[map].submission_id}">Ignore</a>
							</td>
						</tr>
					{/section}
					</tbody>
				</table>
			</div>
		{/fbvFormSection}
	{/fbvFormArea}
{*	{fbvFormButtons submitText="plugins.importexport.oaExportPlugin.forms.export" hideCancel=false}*}
	<div class="section formButtons form_buttons">
		<button class="pkp_button" type="submit">Export</button>
		<button class="pkp_button" type="button" onclick="bulkReject($('#published_manuscripts_form input')); return false;">Reject</button>
		<span class="pkp_spinner"></span>
	</div>
</form>
{else}
	<h4>No published manuscripts</h4>
{/if}




