{**
 * @file plugins/importexport/oaexports/templates/settings.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * List of operations this plugin can perform
 *}

<script type="text/javascript">
	$(function() {ldelim}
		// Attach the form handler.
		$('#oaExportsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
		{rdelim});
</script>

<form class="pkp_form" id="oaExportsForm" method="POST"
		action="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="exportSubmissions" save=true}"
>

	{csrf}

	{fbvFormArea id="oaExportSettingsFormArea"}

		{if !empty($submissions_list)}
			{fbvFormSection}
				<div class="pkp_controllers_grid">
				<table class="">
					<caption class="header pkp_tasks" style="text-align: left; border-bottom: solid 1px #ddd;">
						<h4><span class="task-count">{$submissions_list|@count}</span> Items total</h4>
					</caption>
					<thead class="header">
						<th>Select</th><th>ID</th><th>Author: Title</th><th>Issue</th><th>Status</th>
					</thead>
					{foreach from=$submissions_list item=submission}
						<tr class="export_action" style="background: {cycle values='#fff, #eee'}">
							<td>
								<input type="checkbox" id="{$submission@key}" name="submissions_elem[]" value="{$submission@key}"/>
							</td>
							<td>{$submission@key}</td>
							{foreach from=$submission item=submission_item}
								<td>{$submission_item}</td>
							{/foreach}
							<td>Not exported</td>
						</tr>
					{/foreach}
				</table>
				</div>
			{/fbvFormSection}
		{/if}
	{/fbvFormArea}

	{fbvFormButtons submitText="plugins.importexport.oaExportPlugin.forms.export" hideCancel=false}
</form>


