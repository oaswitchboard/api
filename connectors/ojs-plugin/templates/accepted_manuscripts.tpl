{**
 * @file plugins/importexport/oaexports/templates/accepted_manuscripts.tpl
 * Tested
 * Copyright (c) 2021 Otuoma Sanya
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 *}
<script type="text/javascript">
	// Attach the JS file tab handler.
	$(function() {ldelim}
		$('#accepted_manuscripts_form').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
		$('#accepted_manuscripts_table').DataTable();
		{rdelim});
</script>

{*{$accepted_manuscripts|@print_r}*}
{if !empty($accepted_manuscripts)}
<form class="pkp_form" id="accepted_manuscripts_form" method="POST"
	  action="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="exportSubmissions" save=true}"
>
	{csrf}
	{fbvFormArea id="oaExportSettingsFormArea"}

		{fbvFormSection}
			<div class="pkp_controllers_grid">
				<table id="accepted_manuscripts_table">
					<caption class="header pkp_tasks" style="text-align: left; border-bottom: solid 1px #ddd;">
						<h4><span class="task-count">{$accepted_manuscripts|@count}</span> Items total</h4>
					</caption>
					<thead class="header">
					<th>Select</th><th>ID</th><th>Journal</th><th>Title</th><th>Status</th>
					</thead>
					<tbody>
					{section name=map loop=$accepted_manuscripts}
						<tr>
							<td>
								<input type="checkbox" id="submission_{$submitted_manuscripts[map].submission_id}" name="submissions_elem[]" value="{$accepted_manuscripts[map].submission_id}"/>
							</td>
							<td>{$accepted_manuscripts[map].submission_id}</td>
							<td>
								{$accepted_manuscripts[map].journal} :
								<span style="font-weight: bold;">
									{$accepted_manuscripts[map].section_abbreviation}
								</span>
							</td>
							<td>
								{if !empty($accepted_manuscripts[map].primary_author)}
									<span style="font-weight: bold;">
										{$accepted_manuscripts[map].primary_author}:
									</span>
								{/if}
								{$accepted_manuscripts[map].title}
							</td>
							<td>
								<span id="{$accepted_manuscripts[map].submission_id}_status">
									{if !empty($accepted_manuscripts[map].oa_status)}
									{if $accepted_manuscripts[map].oa_status == "Exported"}
											<span style='color: #0f0;'>
										{else}
											<span style='color: #f00;'>
										{/if}
											{$accepted_manuscripts[map].oa_status}
										</span>
									{else}
										New
									{/if}
								</span><br />
								<a onclick="singleExport({$accepted_manuscripts[map].submission_id}); return false;" id="{$accepted_manuscripts[map].submission_id}_single_export" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleExport" export_object=$accepted_manuscripts[map].submission_id}">Export</a> |
								<a onclick="singleReject({$accepted_manuscripts[map].submission_id}); return false;" id="{$accepted_manuscripts[map].submission_id}_single_reject" href="{url router=$smarty.const.ROUTE_COMPONENT component="grid.settings.plugins.settingsPluginGridHandler" op="manage" category="importexport" plugin=$pluginName verb="singleReject" reject_object=$accepted_manuscripts[map].submission_id}">Ignore</a>
							</td>
						</tr>
					{/section}
					</tbody>
				</table>
			</div>
		{/fbvFormSection}
	{/fbvFormArea}
{*	{fbvFormButtons submitText="plugins.importexport.oaExportPlugin.forms.export" hideCancel=false}*}
	<div class="section formButtons form_buttons">
		<button class="pkp_button" type="submit">Export</button>
		<button class="pkp_button" type="button" onclick="bulkReject($('#accepted_manuscripts_form input')); return false;">Reject</button>
		<span class="pkp_spinner"></span>
	</div>
</form>
{else}
	<h4>No accepted manuscripts</h4>
{/if}




