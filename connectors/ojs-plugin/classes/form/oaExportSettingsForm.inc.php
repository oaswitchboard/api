<?php

/**
 * @file plugins/importexport/doaj/classes/form/DOAJSettingsForm.inc.php
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class DOAJSettingsForm
 * @ingroup plugins_importexport_doaj
 *
 * @brief Form for journal managers to setup DOAJ plugin
 */


import('lib.pkp.classes.form.Form');
class oaExportSettingsForm extends Form {

	/** @var oaExportPlugin  */
	public $plugin;

	public function __construct($plugin) {

		// Define the settings template and store a copy of the plugin object
		parent::__construct($plugin->getTemplateResource('settings.tpl'));
		$this->plugin = $plugin;

		// Add POST and CSRF validation to secure form.
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
	}

	/**
	 * Load settings already saved in the database
	 *
	 * Settings are stored by context, so that each journal or press
	 * can have different settings.
	 */
	public function initData() {
		$contextId = Application::get()->getRequest()->getContext()->getId();
		$this->setData('secretKey', $this->plugin->getSetting($contextId, 'secretKey'));
		parent::initData();
	}

	/**
	 * Load data that was submitted with the form
	 */
	public function readInputData() {
		$this->readUserVars(['username']);
		$this->readUserVars(['password']);
		$this->readUserVars(['depositUrl']);
		parent::readInputData();
	}

	/**
	 * Fetch any additional data needed for the form.
	 *
	 * Data assigned to the form using $this->setData() during the
	 * initData() or readInputData() methods will be passed to the
	 * template.
	 */
	public function fetch($request, $template = null, $display = false) {

		// Pass the plugin name to the template so that it can be
		// used in the URL that the form is submitted to
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->assign('pluginName', $this->plugin->getName());

		return parent::fetch($request, $template, $display);
	}

	/**
	 * Save the settings
	 */
	public function execute() {
		$contextId = Application::get()->getRequest()->getContext()->getId();
		$this->plugin->updateSetting($contextId, 'username', $this->getData('username'));
		$this->plugin->updateSetting($contextId, 'password', $this->getData('password'));
		$this->plugin->updateSetting($contextId, 'depositUrl', $this->getData('depositUrl'));

		// Tell the user that the save was successful.
		import('classes.notification.NotificationManager');
		$notificationMgr = new NotificationManager();
		$notificationMgr->createTrivialNotification(
			Application::get()->getRequest()->getUser()->getId(),
			NOTIFICATION_TYPE_SUCCESS,
			['contents' => __('common.changesSaved')]
		);

		return parent::execute();
	}
}


