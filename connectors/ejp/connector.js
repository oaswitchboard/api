const AWS = require('aws-sdk');
const utils = require(`../../libs/utils`);
const router = require(`./router`);
let formatter = require(`./formatter`);

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

   create: async (document) => {




   },
}

async function ingest(message) {
   const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
   const params = {
      MessageAttributes: {
         "Sender": {
            DataType: "String",
            StringValue: JSON.stringify({
               sub: process.env.PARTICIPANT
            }),
         }
      },
      MessageBody: JSON.stringify(message),
      QueueUrl: process.env.SQS_INCOMING_URL
   };
   return await sqs.sendMessage(params).promise();
}

function validate (message) {
   const Validator = require('jsonschema').Validator;
   const validator = new Validator();
   const schema = require(`../../switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
   const header = require(`../../switchboard/schemas/${process.env.VERSION}/header`).header;
   validator.addSchema(header, '/Header');
   return validator.validate(message, schema);
}

