const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const utils = require(`../../libs/utils`);
const sqs = require(`../../libs/sqs`);
const router = require(`./router`);
let formatter = require(`./formatter`);

module.exports = {

   create: async (document, overridesE1 = {}, overridesRouter = {},) => {
      const e1 = Object.assign(formatter.e1, overridesE1);
      const customformatter = Object.assign(formatter, e1);
      const customrouter = Object.assign(router, overridesRouter)
      const receivers = await customrouter.e1.receivers(document);

      let sent = 0;
      let unmatched = 0;
      let errors = 0;

      for (const receiver of receivers) {
         try {
            const message = await customformatter.e1.noprioragreement(document, receiver);
            const valid = validate(message);

            if (valid.valid) {
               console.log('Creating a E1 message');
               await ingest(message);
               sent++;
            } else {
               console.log('Validation errors');
               console.error(valid)
               unmatched ++;
            }
         } catch (err) {
            console.log('error', err);
         }
      }

      return { sent, unmatched, errors };
   },
}

async function ingest(message) {
   const params = sqs.formatMessageForFifoQueue(
      message,
      process.env.PARTICIPANT,
      process.env.SQS_CONNECTORS_URL
   );
   return await sqs.sendMessage(params);
}

function validate (message) {
   const Validator = require('jsonschema').Validator;
   const validator = new Validator();
   const schema = require(`../../switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
   const header = require(`../../switchboard/schemas/${process.env.VERSION}/header`).header;
   validator.addSchema(header, '/Header');
   return validator.validate(message, schema);
}

