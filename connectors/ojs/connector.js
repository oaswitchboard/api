const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

	getDocumentFromS3: async (key) => {
		try {
			const manuscript = await getAssetFromS3(key);
			return manuscript.Body.toString('utf-8');
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

	getDocumentFromEndpoint: async (event) => {
		try {
			return event.body;
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

	getDocumentFromEndpointWithProxy: async (event) => {

		const parsed = JSON.parse(event.body);

		try {
			return parsed;
		} catch (error) {
			console.error(error);
			throw "getDocument() -> Invalid response from S3";
		}
	},

	format: {

		success: (message) => {
			return {
				statusCode: 200,
				body: JSON.stringify({
				  message,
				  processed: true,
				}),
			};
		},

		error: (message, statusCode = 500) => {
			return {
				statusCode,
				body: JSON.stringify({
				  message,
				  processed: false,
				}),
			};
		},
	},

};

async function getAssetFromS3(key) {
	const s3 = new AWS.S3();
	return await s3.getObject(getS3Params(key)).promise();
};

function getS3Params (key) {
	return {
        Bucket: process.env.S3_BUCKET,
        Key: key,
    };
}
