const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const utils = require(`../../libs/utils`);
const sqs = require(`../../libs/sqs`);
const router = require(`./router`);
let formatter = require(`./formatter`);

module.exports = {

   create: async (document, overridesP1 = {}, overridesRouter = {},) => {
      const p1 = Object.assign(formatter.p1, overridesP1);
      const customformatter = Object.assign(formatter, p1);
      const customrouter = Object.assign(router, overridesRouter)
      const receivers = await customrouter.p1.receivers(document);

      let sent = 0;
      let unmatched = 0;
      let errors = 0;

      for (const receiver of receivers) {
         try {
            const message = await customformatter.p1.noprioragreement(document, receiver, 'p1');
            console.log('[DEBUG] p1 message:', JSON.stringify(message));
            const valid = validate(message);

            if (valid.valid) {
               console.log('Creating a P1 message');
               await ingest(message);
               sent++;
            } else {
               console.log('Validation errors');
               console.error(valid)
               unmatched ++;
            }
         } catch (err) {
            console.log('error', err);
         }
      }
      return { sent, unmatched, errors };
   },
}

async function ingest(message) {
   const params = sqs.formatMessageForFifoQueue(
      message,
      process.env.PARTICIPANT,
      process.env.SQS_CONNECTORS_URL
   );
   return await sqs.sendMessage(params);
}

function validate (message) {
   const Validator = require('jsonschema').Validator;
   const validator = new Validator();
   const schema = require(`../../switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
   const header = require(`../../switchboard/schemas/${process.env.VERSION}/header`).header;
   validator.addSchema(header, '/Header');
   return validator.validate(message, schema);
}

