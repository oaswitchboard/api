const xmlQuery = require('xml-query');
const XmlReader = require('xml-reader');
const log = '/aws/unmatched';
const logger = require(`../../libs/logger`);
const ror = require(`../../libs/ror`);

const defaultRouter = {
    receivers: async (document) => {
        const corresponding = [];
        const authors = document.authors.filter(author => author.affiliation);
        for (let author of authors) {
            const enriched = await ror.ojs.enrichReceiver(author);
            corresponding.push({
                email: enriched.email,
                affiliation: enriched.affiliation,
                name: getAuthorName(enriched),
                rorid: enriched.rorid
            });
        }
        return corresponding;
    },
};
const router = {
    e1: {
        ...defaultRouter,
    },
    p1: {
        ...defaultRouter,
    }
};

function getAuthorName (author) {
    let name = '';
    if (author.first_name) {
        name += author.first_name;
    }
    if (author.middle_name) {
        name += ' ' + author.middle_name;
    }
    if (author.last_name) {
        name += ' ' + author.last_name;
    }
    return author.preferred_public_name ? author.preferred_public_name : name;
}

module.exports = router;