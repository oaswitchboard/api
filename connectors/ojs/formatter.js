const moment = require('moment')
const ror = require(`../../libs/ror`)

const datemask = 'YYYY-MM-DD'
const defaultFormater = {
  prioragreement: async (document, receiver, type = 'e1') => {
    const header = formatter[type].formatHeader(receiver)
    const data = await formatter[type].formatData(document, true)
    header.meta = {
      prioragreement: true,
      source: 'OJS',
      status: type === 'e1' ? 'ENQUIRY' : '',
    }
    return {
      header,
      data,
    }
  },

  noprioragreement: async (document, receiver, type = 'e1') => {
    const header = formatter[type].formatHeader(receiver)
    const data = await formatter[type].formatData(document, false)
    header.meta = {
      prioragreement: false,
      source: 'OJS',
      status: type === 'e1' ? 'ENQUIRY' : '',
    }
    return {
      header,
      data,
    }
  },

  formatHeader: (receiver) => {
    const domain = receiver.email.split('@').pop()
    const from = { id: parseInt(process.env.SENDER, 10) }
    const validity = moment().add(1, 'year').format(datemask)
    let to = {
      name: receiver.affiliation,
    }
    if (receiver.email) {
      to.address = domain;
    }
    if (receiver.rorid) {
      to.address = receiver.rorid
      to.ror = receiver.rorid
    }
    return {
      type: 'e1',
      from,
      to,
      validity,
      template: 'templates/MessageRequest.html',
      persistent: true,
    }
  },

  formatData: async function (document, prioragreement = true) {
    const authors = await this.formatAuthors(document.authors)
    const article = this.formatArticle(document)
	const journal = this.formatJournal(document)
    const charges = prioragreement
      ? this.formatChargesPriorAgreement(document)
      : this.formatChargesNoPriorAgreement(document)
    return {
      authors,
      article,
      journal,
      charges,
    }
  },

  formatAuthors: async function (authors) {
    return Promise.all(
      authors.map(async (author, index) => {
        return {
          lastName: this.formatLastName(author),
          firstName: this.formatFirstName(author),
          initials: this.formatInitials(author),
          ORCID: this.formatORCId(author),
          email: this.formatEmail(author),
          collaboration: this.formatCollaboration(author),
          listingorder: index + 1,
          isCorrespondingAuthor: this.formatIsCorrespondingAuthor(author),
          institutions: await this.formatInstitutions(author),
          currentaddress: await this.formatCurrentAddress(author),
          affiliation: this.formatAffiliations(author),
        }
      })
    )
  },

  formatLastName: (author) => author.last_name || '',

  formatFirstName: (author) => author.first_name || '',

  formatInitials: (author) => {
    const name = `${author.last_name} ${author.first_name}`
    return name
      .split(' ')
      .map((name) => name.substring(0, 1))
      .join('')
  },

  formatORCId: (author) => author.orcid || '',

  formatEmail: (author) => author.email || '',

  formatCollaboration: () => '',

  formatListingOrder: (author) => parseInt(author.sequence, 10),

  formatCreditRoles: () => [],

  formatIsCorrespondingAuthor: (author) => !!author.primary_contact,

  formatInstitutions: (author) => [
    {
      name: author.affiliation || ''
    },
  ],

  formatCurrentAddress: (author) => [
    {
      name: author.affiliation || ''
    },
  ],

  formatAffiliations: (author) => author.affiliation,

  formatArticle: function (document) {
    const grants = this.formatGrants(document)
    const manuscript = this.formatManuscript(document)
    const preprint = this.formatPreprint(document)
    const vor = this.formatVOR(document)
    const funders = this.formatFunders(document)
    const type = this.formatType(document)
    const originaltype = this.formatOriginalType(document)
    return {
      title: this.formatSubmissionTitle(document) || 'Unknown',
      grants,
      manuscript,
      preprint,
      vor,
      type,
      originaltype,
      funders,
      submissionId: this.formatSubmissionId(document),
      acknowledgement: '',
      doi: document.doi || 'Unknown',
      doiurl: document.doiurl || 'Unknown',
    }
  },

  formatFunders: (document) => {
    return []
  },

  formatSubmissionTitle: (document) => document.clean_title,

  formatSubmissionId: (document) => document.submission_id.toString(),

  formatGrants: (document) => [],

  formatManuscript: (document) => {
	let dates = {};
	const submitted = moment.utc(document.date_submitted || '', datemask)
	if (submitted.isValid()) {
		dates.submission = submitted.format(datemask);
	}
	const accepted = moment.utc(document.date_status_modified || '', datemask)
	if (accepted.isValid()) {
		dates.acceptance = accepted.format(datemask);
	}
	return {
		id: document.submissionId,
		dates,
	};
  },

  formatPreprint: (document) => ({}),

  formatVOR: (document) => ({
    publication: 'XXXXXX',
    license: 'XXXXXX',
    deposition: 'XXXXXX',
    researchdata: 'XXXXXX',
  }),

  formatType: (document) => 'research-article',

  formatOriginalType: (document) => 'research-article',

  formatJournal: function (document) {
    return {
      name: this.formatJournalName(document),
      id: this.formatJournalID(document),
      inDOAJ: this.formatJournalInDOAJ(document),
    }
  },

  formatJournalName: (document) => {
    const locale = document.journal_info.primaryLocale || 'en_US'
    return document.journal_info.name[locale] || ''
  },

  formatJournalID: (document) => {
    return document.journal_info.onlineIssn || ''
  },

  formatJournalInDOAJ: () => {
    return false
  },

  formatChargesPriorAgreement: () => ({
    prioragreement: true,
    agreement: {
      name: '',
      id: '',
    },
  }),

  formatChargesNoPriorAgreement: () => {
    return {
      prioragreement: false,
      currency: 'EUR',
      fees: {
        apc: {
          type: 'per article APC list price',
          name: 'estimated',
          amount: 0,
        },
        total: {
          name: 'estimated',
          amount: 0,
        },
      },
    }
  },
}

const formatter = {
  e1: Object.create(defaultFormater),
  p1: Object.create(defaultFormater),
}

formatter.p1.formatHeader = function (receiver) {
  const prototype = Object.getPrototypeOf(this)
  const defaultHeaderFormating = prototype.formatHeader(receiver)
  return {
    ...defaultHeaderFormating,
    type: 'p1',
    pio: true,
  }
}

formatter.p1.formatArticle = function (document) {
  const prototype = Object.getPrototypeOf(this)
  const defaultArticleFormating = prototype.formatArticle(document)
  const vor = this.formatVOR(document)
  return {
    ...defaultArticleFormating,
    vor,
    doi: document.doi || '',
    doiurl: document.doiurl || '',
  }
}

formatter.p1.formatInstitutions = async function (author) {
  try {
    let institution = {
      name: author.affiliation || '',
    }
    const enriched = await ror.generic.search(author.affiliation)
    if (enriched && enriched.rorid) {
      institution.rorid = enriched.rorid
    }
    return [institution]
  } catch (err) {
    console.log(err)
    throw err
  }
}

module.exports = formatter
