Ver 2.02
Feb 12 2025

## Overview


OA Switchboard MVP is required to solve two main problems:

*   Allow neutral and secure message routing between users and participants

*   Allow reporting on messaging


We intend to implement a solution that consists of two parts: an Application Programming Interface (API) and a web application with User Interface (UI). The API is a set of standard (managed) services such as SQS/SNS/SES and custom code that runs serverless, exposed through an API Gateway. The UI is the frontend of a PubSweet application that is accessible through a React UI. All data is stored in a PostgreSQL database as searchable objects.

The above will be implemented as an Amazon Webservices (AWS)-based solution in a Virtual Private Cloud (VPC).

## Message lifecycle

A message is received through an AWS API Gateway endpoint either directly or composed in the UI and then submitted to the (same) API endpoint. The endpoint includes an authorizer to authenticate the sender. Each message is formally validated using message JSON Schema before it is passed further down the line. Validated messages are inserted into an SQS queue. SQS events trigger a Lambda function that validates the sender, enriches the message metadata and performs an optional insert into the PostgreSQL database and triggers an SNS notification that allows us to transport the message to the intended recipient using push messaging (e.g. webhooks) and send (if required) other notifications (e.g. email). All actions are logged using AWS CloudWatch logs.

Undelivered messages will be forwarded to a Dead Letter Queue that will trigger an SNS notification resulting in a failure notification to the original sender.

## Message datastore queries

Datastore can be queried for two purposes:

*   Selection and display of the messages

*   Reporting


The datastore will be accessed using an API endpoint. After successful authorization the request will be forwarded to a Lambda function accessing the PostgreSQL database, formatting the query results and returning them as JSON to the requesting client.
Queries will be pre-defined in Lambdas thus the endpoint selection will define the query.

## Web application / UI

The web application with UI be realized with the PubSweet framework. It will function as a stand-alone application with its own repository.

The connector from the application towards the API will be realized as a component to extend the PubSweet framework, allowing other clients to integrate OA Switchboard within their own publishing workflows, based on the same component. The UI itself is a standalone collection of React components that call the backend endpoints and display their output.

It is important to note that the PubSweet datastore (UI access) and the OA Switchboard datastore (API access) are separated, although they are stored in the same database.

## Authentication and authorization

The API uses its own user standalone management module. We cannot use PubSweet session to authorize with OA Switchboard API as we need to link users of two systems first. Therefore, we will create a setup wizard for first time OA Switchboard users that will register their name (can be taken from the PubSweet session), organization name and RoR ID within the OA Switchboard user management system and issue a long-lived token that will be saved as a part of the PubSweet profile. Note: for MVP, this “setup wizard” is most likely not an actual screen feature. It might be beyond the MVP. Also, in the future releases we will address the issue of token invalidation and re-issue.

## Deployment

The API is a set of AWS services and is deployed as a Serverless application.
The web application with UI is a compiled React PubsSweet component deployed to S3 behind the CloudFront CDN used for SSL termination and faster delivery.
Deployment is done automatically using manually triggered Bitbucket Pipelines.

## Datamodel

Message data is stored as a JSON object. We use JSON Schema to ensure the validity of the data. Message schemas used for data validation can be found in the API repository in the /messages/schemas folder.

*   v2/e1.js

*   v2/e2.js

*   v2/p1.js

*   v2/p2.js

*   v2/header.js (shared across all schemas)


Old v1 API is fully supported. Schemas are available at:

*   v1/e1.js

*   v1/e2.js

*   v1/p1.js

*   v1/p2.js

*   v1/header.js (shared across all schemas)


Version 2 introduces a number of breaking changes to the format, primarily changed enumerations.
Both versions are fully supported, older v1 messages are on the fly translated to v2.

Message examples that can be used for test purposes can be found in the /messages/samples folder.

*   e1-no-prior-agreement.json

*   e1-no-prior-agreement-with-apc.json

*   e1-prior-agreement.json

*   e2-no-prior-agreement.json

*   e2-prior-agreement-with-apc.json

*   e2-prior-agreement.json

*   p1-no-prior-agreement.json

*   p1-pio.json

*   p1-prior-agreement-with-apc.json

*   p1-prior-agreement.json

*   p2-no-prior-agreement.json

*   p2-prior-agreement-with-apc.json

*   p2-prior-agreement.json


## Endpoints (listed alphabetically)

All endpoints (except for the authorisation endpoint) are protected using Cognito authorizers for participants and administrators.

### POST /v2/authorize

Authorises user and returns either a 401 or a 200 with a JWT token that should be used with all following requests.

```java
POST /v2/authorize HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
{
  "email": "test@oaswitchboard.org",
  "password": "xxxxxxxxxxxxxxxxx"
}
```

### GET /v2/schema/{id}

Retrieves messages schema (e1, e2, p1 and p2)

```java
GET /v2/schema/e1 HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```

### POST /v2/message

Validates input and starts a new message flow. Returns an error message if the submitted message doesn’t conform with the corresponding v2 schema

```java
POST /v2/message HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"

{
   "header":{
      "type":"e1",
      "to":{
         "ror":"<https://ror.org/008xxew50">
      },
      "persistent":true
   }, … rest of the message
}
```

### GET /v2/messages

Retrieves all messages for a participant.

```java
GET /v2/messages HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```

Returns a total number of records and the first 25 messages in v2 format
Pagination parameters:

*   startrow (int, defaults to 1)

*   maxrows (int, defaults to 25)

*   filter (string, defaults to 'all'. Other options: 'e-messages', 'p-messages', 'primary', 'awaitingresponse')

*   orderby (string, defaults to 'id'. Other options: 'type', 'state', 'created')

*   orderdir (string, defaults to 'desc'. Other options: 'asc')

### GET /v2/dlq

Returns a list of undeliverable messages.

```java
GET /v2/dlq HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```

Returns a total number of records and the first 25 messages in v2 format
Pagination parameters:

*   startrow (int, defaults to 1)

*   maxrows (int, defaults to 25)

*   filter (string, defaults to 'all')

*   orderby (string, defaults to id)

*   orderdir (string, defaults to desc)


### GET /v2/message/{id}

Retrieves a single message formatted for v2.

```java
GET /v2/message/2020 HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```

### POST /v2/suggester

Accepts a preformatted v2 message.
Retrieves list of suggested recipients with their availability state

```java
POST /v2/suggester HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
{
   "header":{
      "type":"e1",
      "to":{
         "ror":"<https://ror.org/008xxew50">
      },
      "persistent":true
   }, … rest of the message
}
```

### GET /v2/hash/{type}/id

Retrieves messages matching type and type id.

```java
GET /v2/hash/e2/20201113154901-b62bbf3b-0ec5 HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```

### POST /v2/report

Creates an Excel or a JSON report.
Select type by adding a query string type = json | excel.
Excel is default.

```java
POST /v2/report?type=json HTTP/1.1
Content-Type: application/json
Host: api.oaswitchboard.org
Authorization: "Bearer {JWT token}"
```


HTML is NOT sent on server. All done in your browser. 😇