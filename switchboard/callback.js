const errorhandler = require('./../libs/errorhandler');
const formatter = require('./../libs/formatter');
const Participants = require('./services/participants');
const Utils = require('../libs/utils');
const SQS = require('../libs/sqs');
const Webhook = require('../libs/webhook');

module.exports = { ingest, send }

async function ingest (event) {
    console.log(JSON.stringify(event));
    try {
        let sent = 0;
        for (const record of event.Records) {
            const message = JSON.parse(record.Sns.Message);
            const queue = process.env.SQS_CALLBACK_URL;
            const sender = 'Callback webhook'
            const formatted = SQS.formatMessageForRegularQueue (message, sender, queue);
            const receipt = await SQS.sendMessage(formatted);
            console.log(receipt)
            sent++;
        }
        return formatter.render({ sent });
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error queueing callback message');
        return formatter.error('Error queueing callback message', 500);
    }
}

async function send (event) {
    let sent = 0;
    let skipped = 0;
    console.log(JSON.stringify(event));
    try {
        for (const record of event.Records) {
            const payload = JSON.parse(record.body);
            const sender = payload.header.from.id || null;
            const participant = await Participants.findById(sender);
            if (isValidRouting(payload) && isValidReceiver(participant)) {
                const { callback, apikey }= participant;
                const dispatched = await Webhook.dispatch(payload, callback, apikey);
                if (dispatched) {
                    sent++;
                } else {
                    throw "Error dispatching callback request. Re-trying";
                }
            } else {
                skipped++
            }
        }
        return formatter.render({ sent, skipped, });
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error dispatching callback request');
        return formatter.error('Error dispatching callback request', 428);
    }
}

function isValidReceiver(participant) {
    return participant && Utils.isURL(participant.callback);
}

function isValidRouting(event) {
    return event.header && event.header.routing && event.header.routing === 'DEFAULT';
}

