//@ts-check
const AWS = require("aws-sdk");

AWS.config.region = process.env.AWSREGION || "eu-central-1";

const pool = require("./../connection");
const formatter = require("./../libs/formatter");
const errorhandler = require("./../libs/errorhandler");
const utils = require("./../libs/utils");
const ror = require("./../libs/ror");
const orcid = require("./../libs/orcid");
const decode = require("html-entities-decoder");
const { V1ToV2 } = require("./../libs/convert");

// OAS Data layer services
const MessageService = require("./services/messages");
const ParticipantService = require("./services/participants");

// Globals
const allowedMessageTypes = ["e1", "e2", "p1", "p2"];

module.exports = {
	/* suggester () - creates a list of respondents */
	suggester: async (event) => {
		const client = await pool.connect();
		try {
			const recipients = await extractReceivers(JSON.parse(event.body));
			if (!recipients.length) {
				return formatter.render([]);
			}
			for (const recipient of recipients) {
				const args = [recipient.ror];
				const query = `
                    SELECT participants.oafund
                    FROM participants
                    LEFT JOIN routes ON participants.id = routes.participant_id
                    LEFT JOIN organisations ON routes.organisation_id = organisations.id
                    WHERE
                        (   ror_id = $1
                            OR ringgold_id = $1
                            OR isni = $1
                            OR fundref = $1)
                    `;
				const participants = await client.query(query, args);
				const hasOAFund = participants.rows.filter((participant) => participant.oafund).length;
				recipient.participant = !!participants.rows.length;
				recipient.hasOAFund = !!hasOAFund;
			}
			return formatter.render(recipients);
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving recievers");
			return formatter.error("Error retrieving recievers", 500);
		} finally {
			client.release();
		}
	},

	/* list () - returns a list of delivered user messages */
	list: async (event) => {
		const client = await pool.connect();
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const params = extractSearchArguments(event.queryStringParameters, false);
			const createSQL = (count = false) => {
				const selection = count
					? `COUNT(messages.id)::int AS total`
					: `   messages.*,
                        messages.organisation AS organisation_id,
                        senders.institution AS sendfrom,
                        receivers.institution AS receivedby,
                        organisations.name AS organisation
                    `;
				return `
                    SELECT ${selection}
                    FROM messages
                    LEFT JOIN participants AS senders ON messages.sender = senders.id
                    LEFT JOIN participants AS receivers ON messages.receiver = receivers.id
                    LEFT JOIN organisations ON messages.organisation = organisations.id
                    WHERE (senders.cognito_id = $1 OR receivers.cognito_id = $1)
                `;
			};

			const messages = await client.query(augumentSearchSQL(createSQL(), params), [cognito_id]);
			const count = await client.query(createSQL(true), [cognito_id]);
			const participant = await ParticipantService.findByCognitoId(cognito_id);
			for (let message of messages.rows) {
				message.header.relations = await MessageService.getRelations(message, participant);
			}
			return formatter.render({
				total: count.rows[0].total,
				messages: formatFoundMessages(messages.rows, participant),
			});
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message list");
			return formatter.error(error, 500);
		} finally {
			client.release();
		}
	},

	/* related () - returns a list of related user messages */
	related: async (event) => {
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const participant = await ParticipantService.findByCognitoId(cognito_id);
			const messageid = event.pathParameters.id || 0;
			const messages = await MessageService.findRelatedToId(messageid, participant.id);
			return formatter.render({
				total: messages ? messages.length : 0,
				messages: formatFoundMessages(messages, participant),
			});
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving related message list");
			return formatter.error(error, 500);
		}
	},

	search: async (event) => {
		const client = await pool.connect();
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const params = extractSearchArguments(event.queryStringParameters);
			const criteria = [cognito_id, params.searchq];
			const participant = await ParticipantService.findByCognitoId(cognito_id);

			let sql = null;
			let sqlcount = null;
			let count = 0;
			let messages = [];

			if (utils.isNumeric(params.searchq)) {
				sql = createSearchSQLForNumerics();
				sqlcount = createSearchSQLForNumerics(true);
				const foundnumerics = await client.query(augumentSearchSQL(sql, params), criteria);
				const countnumerics = await client.query(sqlcount, criteria);
				count += countnumerics.rows[0].total;
				messages = [...messages, ...foundnumerics.rows];
			}

			sql = createSearchSQLForStrings();
			sqlcount = createSearchSQLForStrings(true);

			const foundstrings = await client.query(augumentSearchSQL(sql, params), criteria);
			const countstrings = await client.query(sqlcount, criteria);
			count += countstrings.rows[0].total;
			messages = [...messages, ...foundstrings.rows];

			return formatter.render({
				total: count,
				messages: formatFoundMessages(messages, participant),
			});
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message searching list");
			return formatter.render({ total: 0, messages: [] });
		} finally {
			client.release();
		}
	},

	scheduled: async () => {
		const client = await pool.connect();
		try {
			try {
				await client.query(`REFRESH MATERIALIZED VIEW search WITH DATA`);
				await client.query(`REINDEX INDEX idx_fts_search`);
				await client.query(`REINDEX INDEX idx_unq_search`);
			} catch {
				await client.query(`
                    CREATE MATERIALIZED VIEW search AS
                    SELECT
                    messages.id,
                    messages.header ->> 'e1'::varchar AS e1,
                    messages.header ->> 'e2'::varchar AS e2,
                    messages.header ->> 'p1'::varchar AS p1,
                    messages.header ->> 'p2'::varchar AS p2,
                    messages.data -> 'article' ->> 'doi'::varchar AS doi,
                    messages.type,
                    messages.state,
                    messages.created,
                    senders.cognito_id AS sender_id,
                    receivers.cognito_id AS receiver_id,
                    senders.institution AS sendfrom,
                    receivers.institution AS receivedby,
                    organisations.name AS organisation,
                    to_tsvector(
                        COALESCE(senders.institution::text, '') || ' ' ||
                        COALESCE(receivers.institution::text, '') || ' ' ||
                        COALESCE(organisations.name::text, '') || ' ' ||
                        COALESCE(messages.data -> 'article' ->> 'doi'::text, '') || ' ' ||
                        COALESCE(messages.data -> 'article' ->> 'title'::text, '')
                    ) doc
                    FROM messages
                    LEFT JOIN participants AS senders ON messages.sender = senders.id
                    LEFT JOIN participants AS receivers ON messages.receiver = receivers.id
                    LEFT JOIN organisations ON messages.organisation = organisations.id
                `);
				await client.query(`CREATE INDEX idx_fts_search ON search USING gin(doc)`);
				await client.query(`CREATE UNIQUE INDEX idx_unq_search ON search (id)`);
			}
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error creating materialized view object");
		} finally {
			client.release();
			return null;
		}
	},

	/* ref () - returns a list of all user messages with a given ref. No pagination */
	ref: async (event) => {
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const participant = await ParticipantService.findByCognitoId(cognito_id);
			const ref = event.pathParameters.ref || utils.uuid();
			const messages = await MessageService.findByRefAndCognitoId(ref, cognito_id);
			if (messages) {
				return formatter.render({
					total: messages.rowCount,
					messages: formatFoundMessages(messages.rows, participant),
				});
			}
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving DLQ message list");
			return formatter.error(error, 500);
		}
	},

	/* detail () - returns a message detailed view */
	detail: async (event) => {
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const message_id = MessageService.findMessageIdFromEvent(event);
			if (message_id) {
				const participant = await ParticipantService.findByCognitoId(cognito_id);
				let message = await MessageService.findByIdAndParticipantId(message_id, participant.id);
				if (message) {
					const formatted = formatFoundMessage(message, participant);
					return formatter.render(formatted);
				}
			}
			return formatter.error({}, 404);
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message by Id");
			return formatter.error(error, 500);
		}
	},

	hash: async (event) => {
		try {
			const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
			const message_hash = MessageService.findMessageStringIdFromEvent(event);
			const message_type = MessageService.findMessageTypeFromEvent(event);

			if (cognito_id && message_hash && message_type) {
				const participant = await ParticipantService.findByCognitoId(cognito_id);
				const message = await MessageService.findByHashAndParticipantId(
					message_hash,
					message_type,
					participant.id
				);
				const formatted = formatFoundMessage(message, participant);
				if (formatted) {
					return formatter.render(formatted);
				}
			}
			return formatter.error("Message not found", 404);
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message by type and uuid");
			return formatter.error(error, 500);
		}
	},

	doi: async (event) => {
		let formatted = [];
		const client = await pool.connect();
		try {
			if (event.doi && event.type && event.participant) {
				const messages = await client.query(`
                    SELECT messages.*, participants.institution
                    FROM messages, participants
                    WHERE (messages.sender = participants.id OR messages.receiver = participants.id)
                    AND messages.data -> 'article' ->> 'doi' = '${event.doi}'
                    AND messages.header ->> 'type' = '${event.type}'
                    AND participants.id = '${event.participant}'
                    ORDER BY id DESC
                    LIMIT 1`);
				formatted = messages.rows.map((message) => {
					return {
						id: message.id,
						e1: message.header["e1"] || null,
						e2: message.header["e2"] || null,
						p1: message.header["p1"] || null,
						p2: message.header["p2"] || null,
					};
				});
			}
			return {
				statusCode: 200,
				body: JSON.stringify(formatted),
			};
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message by type and DOI");
			return {
				statusCode: 500,
				body: JSON.stringify(error),
			};
		} finally {
			client.release();
		}
	},

	correspondents: async (event) => {
		let formatted = [];
		const client = await pool.connect();
		const { name } = event.queryStringParameters;
		try {
			if (name && name.length > 2) {
				const cognito_id = ParticipantService.findSubFromAPIGatewayEvent(event);
				const user = await ParticipantService.findByCognitoId(cognito_id);
				const sql = `
                    SELECT
                        participants.institution,
                        participants.id
                    FROM participants
                    WHERE
                        participants.institution ILIKE '%' || $1 || '%'
                        AND (
                            EXISTS (
                                SELECT 1
                                FROM messages AS m1
                                WHERE m1.sender = participants.id AND m1.receiver = $2
                            )
                            OR
                            EXISTS (
                                SELECT 1
                                FROM messages AS m2
                                WHERE m2.receiver = participants.id AND m2.sender = $2
                            )
                        )
                    ORDER BY participants.institution ASC`;
				const participants = await client.query(sql, [name, user.id]);
				formatted = participants.rows.map((participant) => ({
					id: participant.id,
					name: participant.institution,
				}));
			}
			return formatter.render(formatted);
		} catch (error) {
			errorhandler.log(error, "[OASB API] Error retrieving message by type and DOI");
			return {
				statusCode: 500,
				body: JSON.stringify(error),
			};
		} finally {
			client.release();
		}
	},
};

function extractSearchArguments(params, required = true) {
	if (params) {
		const {
			startrow = 1,
			maxrows = 50,
			filter = "",
			searchq = "",
			orderby = "ID",
			orderdir = "DESC",
			address = "",
			participant = "",
		} = params;
		if (!searchq && required) {
			throw "No search arguments";
		}
		return {
			startrow,
			maxrows,
			filter,
			searchq: parseSearchQuery(searchq),
			address,
			participant,
			orderby,
			orderdir,
		};
	}
	throw "No arguments";
}

function parseSearchQuery(searchq) {
	return searchq.replace("https://doi.org/", "");
}

function createSearchSQLForNumerics(count = false) {
	const selection = count
		? `COUNT (messages.id)::integer AS total`
		: `messages.*, senders.institution AS sendfrom, organisations.name AS organisation, receivers.institution AS receivedby`;
	return `
        SELECT ${selection}
        FROM messages
        LEFT JOIN participants AS senders ON messages.sender = senders.id
        LEFT JOIN participants AS receivers ON messages.receiver = receivers.id
        LEFT JOIN organisations ON messages.organisation = organisations.id
        WHERE (senders.cognito_id = $1 OR receivers.cognito_id = $1)
        AND messages.id = $2
    `;
}

function createSearchSQLForStrings(count = false) {
	const selection = count
		? `COUNT (messages.id)::integer AS total`
		: `messages.*, matches.sendfrom, matches.organisation, matches.receivedby `;
	return `
        WITH matches AS (
            SELECT *
            FROM search
            WHERE (sender_id = $1 OR receiver_id = $1)
            AND (
                    search.doc @@ plainto_tsquery($2 || ':*')
                OR  search.doi ILIKE '%' || $2 || '%'
                OR  search.type = $2
                OR  search.state = $2
            )
        )
        SELECT ${selection}
        FROM matches
        JOIN messages USING (id)
        LEFT JOIN participants AS senders ON messages.sender = senders.id
        LEFT JOIN participants AS receivers ON messages.receiver = receivers.id
        LEFT JOIN organisations ON messages.organisation = organisations.id
    `;
}

function augumentSearchSQL(sql, params) {
	let { startrow, maxrows, filter, orderby, orderdir, address, participant } = params;
	if (address && address.indexOf("https://ror.org/") > -1) {
		sql += ` AND (messages.header -> 'to' ->> 'address' = '${address}')`;
	}
	if (participant && utils.isNumeric(participant)) {
		sql += ` AND (messages.sender = ${participant} OR messages.receiver = ${participant})`;
	}
	if (filter === "e-messages") {
		sql += ` AND (messages.type = 'e1' OR messages.type = 'e2')`;
	}
	if (filter === "p-messages") {
		sql += ` AND (messages.type = 'p1' OR messages.type = 'p2')`;
	}
	if (filter === "primary") {
		sql += ` AND (messages.header -> 'routingdata' ->> 'rules' = 'PRIMARY')`;
	}
	if (filter === "awaitingresponse") {
		sql += ` AND (messages.type = 'e1' AND messages.header ->> 'e2' IS NULL)`;
	}
	if (
		orderby &&
		orderdir &&
		["ID", "TYPE", "STATE", "CREATED"].includes(orderby.toUpperCase()) &&
		["ASC", "DESC"].includes(orderdir.toUpperCase())
	) {
		sql += ` ORDER BY messages.${orderby} ${orderdir}`;
	} else {
		sql += ` ORDER BY messages.header ->> 'e1' DESC,  messages.header ->> 'e2' DESC, messages.header ->> 'p1' DESC, messages.header ->> 'p2' DESC`;
	}
	if (
		startrow &&
		maxrows &&
		utils.isNumeric(startrow) &&
		utils.isNumeric(maxrows) &&
		startrow >= 1 &&
		maxrows <= 50
	) {
		const offset = startrow - 1;
		sql += ` OFFSET ${offset} LIMIT ${maxrows}`;
	} else {
		sql += ` OFFSET 0 LIMIT 50`;
	}
	return sql;
}

function formatFoundMessage(message, participant) {
	message = MessageService.cleanHeader(message);
	message = MessageService.cleanNonPersistentData(message, participant);
	message = V1ToV2(message);
	return message;
}

function formatFoundMessages(messages, participant) {
	let formatted = [];
	for (const message of messages) {
		let formattedmessage = formatFoundMessage(message, participant);
		formatted.push(formattedmessage);
	}
	return formatted;
}

async function extractReceivers(message) {
	const rors = [];
	const receivers = [];
	const lambda = new AWS.Lambda();

	const additional = await extractORCIDs(message.data);
	const unique = [...extractInstitutions(message.data), ...extractFunders(message.data), ...additional].filter(
		(value, index, self) =>
			index === self.findIndex((item) => item.name === value.name || (item.ror && item.ror === value.ror))
	);

	for (const receiver of unique) {
		if (receiver.ror) {
			receiver.address = receiver.ror;
		}
		if (receiver.address && receiver.address.indexOf("https://ror.org") > -1) {
			receivers.push(receiver);
		} else {
			console.log("Invoking lambda", receiver);
			try {
				const response = await lambda
					.invoke({
						FunctionName: process.env.LAMBDA_ROR,
						InvocationType: "RequestResponse",
						Payload: JSON.stringify({
							name: receiver.name,
						}),
					})
					.promise();
				if (response.StatusCode === 200 && response.body) {
					const enriched = JSON.parse(JSON.parse(response.Payload).body);
					console.log("enriched", enriched);
					if (enriched && enriched.rorid) {
						receiver.ror = enriched.rorid;
					}
					if (receiver.ror && !rors.includes(receiver.ror)) {
						receivers.push(receiver);
						rors.push(receiver.ror);
					}
				}
			} catch (error) {
				console.log("Error invoking lambda", error);
			}
		}
	}
	return receivers;
}

async function extractORCIDs(data) {
	const institutions = [];
	if (data.authors && Array.isArray(data.authors)) {
		for (const author of data.authors) {
			if (author.ORCID && !utils.isBlank(author.ORCID)) {
				const name = await orcid.getEmployment(author.ORCID);
				if (name) {
					const { rorid } = await ror.generic.search(name);
					const institution = {
						name,
						ror: rorid,
					};
					institutions.push(institution);
				}
			}
		}
	}
	return institutions;
}

function extractInstitutions(data) {
	const institutions = [];
	if (data.authors && Array.isArray(data.authors)) {
		data.authors.forEach((author) => {
			if (author.institutions && Array.isArray(author.institutions)) {
				author.institutions.forEach((institution) => {
					if (isInstitutionNameUnique(institutions, institution)) {
						institutions.push(institution);
					}
				});
			}
		});
	}
	return institutions;
}

function extractFunders(data) {
	if (data.article && data.article.funders) {
		return data.article.funders;
	}
	return [];
}

function isInstitutionNameUnique(institutions, institution) {
	return !institutions.filter(
		(item) => (item.name && item.name === institution.name) || (item.ror && item.ror === institution.ror)
	).length;
}
