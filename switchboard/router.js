//@ts-check
const AWS = require("aws-sdk");
const formatter = require("./../libs/formatter");
const errorhandler = require("./../libs/errorhandler");
const utils = require("./../libs/utils");

const HAVESECONDARYRECEIVERS = ["e1", "p1"];

const ingestconnector = async (event) => {
	// event = require("../tests/mocks/events/p1-pa-sqs");
	// console.log('ingest --------------------------------------------------')
	// console.log('event', JSON.stringify(event));
	const ParticipantService = require("./services/participants");
	try {
		let failed = 0;
		let passed = 0;
		let errors = [];
		for (const record of event.Records) {
			const message = JSON.parse(record.body);
			const valid = validateMessage(message);
			if (valid.success) {
				const participant = await ParticipantService.findById(message.header?.from?.id);
				const sender = participant.cognito_id;
				await startStateMachine(message, sender);
				passed++;
			} else {
				failed++;
				errors.push({ message, errors: valid.errors });
			}
		}
		return formatter.render({
			message: "Your message(s) were validated",
			failed,
			passed,
			errors,
		});
	} catch (error) {
		errorhandler.log(error, "Error ingesting message from SQS.");
		return formatter.error("Error ingesting message from SQS.", 500);
	}
};

const ingest = async (event) => {
	// console.log('ingest --------------------------------------------------')
	// const util = require('util');
	// console.log(util.inspect(event, {showHidden: false, depth: null, colors: true}))
	try {
		const message = JSON.parse(event.body);
		const valid = validateMessage(message);
		if (valid.success) {
			const sender = event.requestContext.authorizer.claims.sub;
			await startStateMachine(message, sender);
			return formatter.render(`Your ${message.header.type} message has been successfully submitted`);
		}
		return formatter.error(valid.errors, 400);
	} catch (error) {
		errorhandler.log(error, "Error ingesting message.");
		return formatter.error("Error ingesting message.", 500);
	}
};

const discover = async (event) => {
	// event = require("../tests/mocks/events/router-discover");
	// console.log('discover() --------------------------------------------------')
	// console.log('event', JSON.stringify(event));
	try {
		const { message, sender } = event;
		let receivers = extractPrimaryReceivers(message);
		if (hasSecondaryReceivers(message)) {
			const { data } = message;
			receivers = [...receivers, ...extractInstitutionReceivers(data), ...extractFunderReceivers(data)];
		}
		return {
			message,
			sender,
			receivers: deduplicateReceivers(receivers),
		};
	} catch (error) {
		console.error(error);
		throw {
			message: "Error discovering receivers",
			error,
			event,
			step: "discover",
		};
	}
};

const enrich = async (event) => {
	// event = require("../tests/mocks/events/router-enrich");
	// console.log('enrich() --------------------------------------------------')
	// console.log('event', JSON.stringify(event));
	try {
		const OrganisationService = require("./services/organisations");
		for (const receiver of event?.receivers) {
			const { ror, fundref } = receiver.organisation;

			if (utils.isRoR(ror) && !receiver.organisation.name) {
				const organisation = await OrganisationService.findByPrimaryIdentifier(ror);
				receiver.organisation.name = organisation?.name || null;
			}

			if (!utils.isRoR(ror) && hasSecondaryIdentifiers(receiver.organisation)) {
				const organisation = await OrganisationService.findBySecondaryIdentifier(fundref);
				if (organisation && utils.isRoR(organisation.ror_id)) {
					receiver.organisation.ror = organisation.ror_id;
				}
			}
			// Commented due to task https://www.pivotaltracker.com/story/show/187448410
			// if (utils.isRoR(ror) && receiver.type === 'FUNDER') {
			//     await OrganisationService.createOrganizationByFunder(receiver.organisation);
			// }
		}
		return event;
	} catch (error) {
		throw {
			message: "Error enriching receivers",
			error,
			event,
			step: "enrich",
		};
	}
};

const route = async (event) => {
	// event = require("../tests/mocks/events/router-routes");
	// console.log('route() --------------------------------------------------')
	// console.log('event', JSON.stringify(event));
	try {
		const RouteService = require("./services/routes");
		const OrganisationService = require("./services/organisations");
		for (const receiver of event?.receivers) {
			const { ror } = receiver.organisation;
			const routes = await RouteService.findByOrganisationRoR(ror);
			const organisation = await OrganisationService.selectOrganisationByRor(ror);
			receiver.routes = routes?.length
				? routes?.map((route) => formatRoute(route))
				: [formatUndeliverableRoute(organisation?.rows[0])];
			const deliveries = [];
			receiver.routes.forEach((route) => {
				route.destination = "messages";
				const key = `${route.receiver}-${route.organisation}`;
				if (route.notification === "WEBHOOK") {
					if (deliveries.includes(key)) {
						route.destination = "bcc";
					}
				}
				deliveries.push(key);
			});
		}
		return event;
	} catch (error) {
		throw {
			message: "Error retrieving routes",
			error,
			event,
			step: "route",
		};
	}
};

const merge = async (event) => {
	// event = require("../tests/mocks/events/router-merge-customrouting-primary-pio-false-cond-pio-true");
	// console.log('merge() --------------------------------------------------')
	// console.log('event', JSON.stringify(event));
	try {
		let deliverables = [];
		let { message, sender, receivers } = event;
		const ParticipantService = require("./services/participants");
		const participant = await ParticipantService.findByCognitoId(sender);
		const group = utils.uuid();
		const primary = receivers.filter((receiver) => receiver.type === "PRIMARY");
		const cc = receivers.filter((receiver) => receiver.type === "INSTITUTION" || receiver.type === "FUNDER");
		const Validator = require("jsonschema").Validator;
		const validator = new Validator();

		message.header.from = formatMessageSender(participant);
		message.header[message.header.type] = utils.messages.generateUUID();

		for (const receiver of primary) {
			for (const route of receiver.routes) {
				let deliverable = utils.clone(message);
				deliverable.header.to = formatMessageReceiver(receiver, route);
				deliverable.header.routingdata = formatRoutingDataPrimary(route);
				deliverable.header.meta = { ...deliverable.header.meta, ...formatMessageMetadata(route, group) };
				if (route.action === "CUSTOM" && route.formatter) {
					const transform = initTransformer(route.formatter);
					if (typeof transform === "function") {
						deliverable = transform(deliverable);
						deliverables.push(deliverable);
					}
				}
				if (route.action === "CUSTOM" && route.condition) {
					if (validator.validate(deliverable, route.condition).errors.length === 0) {
						deliverables.push(deliverable);
					}
				}
				if (route.action === "DEFAULT" || route.action === "DLQ") {
					deliverables.push(deliverable);
				}
				if (route.action === "CONSORTIA" && isMessageAvailableForConsortia(message)) {
					const cleaned = isPriorAgreement(deliverable) ? deliverable : stripPersonalData(deliverable);
					deliverables.push(cleaned);
				}

				if (route.action === "AUTOREJECT" && isMessageRejectable(deliverable)) {
					if (validator.validate(deliverable, route.condition).errors.length) {
						deliverable.header.type = "e2";
						const { ror, name } = participant;
						const rejectee = { organisation: { ror, name } };
						deliverable.header.from = {
							ror: receiver.organisation.ror,
							name: receiver.organisation.name,
						};
						deliverable.header.to = formatMessageReceiver(rejectee, route);
						deliverables.push(deliverable);
					}
				}
			}
		}

		if (isEnabledAutoCC(participant) && isMessageAvailableForAutoCC(message)) {
			for (const receiver of cc) {
				for (const route of receiver.routes) {
					let deliverable = utils.clone(message);
					deliverable.header.to = formatMessageReceiver(receiver, route);
					deliverable.header.meta = { ...deliverable.header.meta, ...formatMessageMetadata(route, group) };
					deliverable.header.routingdata = formatRoutingDataCC(route, receiver);
					if (route.action === "CUSTOM" && route.formatter) {
						const transform = initTransformer(route.formatter);
						if (typeof transform === "function") {
							deliverable = transform(deliverable);
						}
					}
					if (route.action === "CUSTOM" && route.condition) {
						if (!validator.validate(deliverable, route.condition).errors.length) {
							deliverables.push(deliverable);
						}
					} else {
						if (route.action === "DEFAULT" || route.action === "CONSORTIA") {
							deliverables.push(stripPersonalData(deliverable));
						}
					}
				}
			}
		}

		return deliverables;
	} catch (error) {
		throw {
			message: "Error merging messages",
			error,
			event,
			step: "merge",
		};
	}
};

const persist = async (messages) => {
	// messages = require("../tests/mocks/events/router-persist");
	// console.log('persist() --------------------------------------------------')
	// console.log('event', JSON.stringify(messages));
	const saved = [];
	try {
		for (let message of messages) {
			const insertable = await formatMessage(message);
			const persisted = await persistMessage(insertable);
			if (persisted && persisted.id) {
				saved.push(persisted);
			}
		}
		return saved;
	} catch (error) {
		throw {
			message: "Error persisting messages",
			error,
			event: messages,
			step: "persist",
		};
	}
};

const dispatch = async (messages) => {
	// messages = require("../tests/mocks/events/router-dispatch");
	// console.log('dispatch() --------------------------------------------------')
	// console.log('event', JSON.stringify(messages));
	try {
		let result = {
			success: 0,
			errors: 0,
		};
		for (const message of messages) {
			await dispatchMessage(message);
			result.success++;
		}
		return result;
	} catch (error) {
		throw {
			message: "Error dispatching messages",
			error,
			event: messages,
			step: "dispatch",
		};
	}
};

const fallback = async (event) => {
	console.log("event", JSON.stringify(event));
};

/// Exports

module.exports = {
	ingest,
	ingestconnector,
	discover,
	enrich,
	route,
	persist,
	dispatch,
	merge,
	fallback,
};

/// Implementations
function validateMessage(message) {
	const validator = require("./../libs/validator");
	return validator.validate(message);
}

function isPriorAgreement(message) {
	return !!message?.data?.charges?.prioragreement;
}

async function startStateMachine(message, sender) {
	const stepFunctions = new AWS.StepFunctions();
	const stateMachineArn = process.env.STATEMACHINE;
	const payload = { message, sender };
	const params = {
		stateMachineArn,
		input: JSON.stringify(payload),
	};
	await stepFunctions.startExecution(params).promise();
}

function extractPrimaryReceivers(payload) {
	const { address, name } = payload?.header?.to;
	return [
		{
			organisation: {
				ror: address,
				name,
			},
			type: "PRIMARY",
		},
	];
}

function extractInstitutionReceivers(payload) {
	const receivers = [];
	for (const author of payload?.authors) {
		if (author?.institutions && Array.isArray(author.institutions)) {
			for (const institution of author?.institutions) {
				receivers.push({
					organisation: formatReceiver(institution),
					type: "INSTITUTION",
				});
			}
		}
	}
	return receivers;
}

function extractFunderReceivers(payload) {
	const receivers = [];
	if (payload?.article?.funders && Array.isArray(payload.article.funders)) {
		for (const funder of payload?.article?.funders) {
			receivers.push({ organisation: formatReceiver(funder), type: "FUNDER" });
		}
	}
	return receivers;
}

function formatReceiver(organisation) {
	const response = {};
	const pids = ["name", "ror", "rorid", "isni", "ringgold", "fundref"];
	for (const key of Object.keys(organisation)) {
		if (organisation[key] && pids.includes(key)) {
			response[key] = organisation[key];
		}
	}
	return response;
}

function deduplicateReceivers(...receivers) {
	const existing = [];
	return receivers.flat().filter((receiver) => {
		if (existing.includes(receiver.organisation.ror)) {
			return false;
		}
		existing.push(receiver.organisation.ror);
		return true;
	});
}

function hasSecondaryIdentifiers(organisation) {
	return !!organisation?.fundref;
}

function formatRoute(route) {
	const { id, participant_id, organisation_id, action, notification, type, persistent, participant, receiver } =
		route;
	return {
		receivers: {
			route: id,
			sentto: receiver,
			receivedby: participant,
		},
		receiver: participant_id,
		organisation: organisation_id,
		action,
		notification,
		type,
		persistent,
	};
}

function formatUndeliverableRoute(organisation = {}) {
	const { id } = organisation;
	return {
		action: "DLQ",
		notification: "NONE",
		organisation: id,
	};
}

function formatMessageSender(participant) {
	const { id, ror, institution } = participant;
	return {
		id,
		ror,
		participant: id,
		address: ror,
		name: institution,
	};
}

function formatMessageReceiver(receiver, route) {
	const { ror, name } = receiver.organisation;
	return {
		address: ror,
		name,
		id: route.receiver,
	};
}

function isEnabledAutoCC(participant) {
	return participant && !!participant.pio;
}

function isMessageAvailableForAutoCC(message) {
	return message.header.type === "p1";
}

function isMessageAvailableForConsortia(message) {
	return message.header.type === "p1";
}

function isMessageRejectable(message) {
	return ["e1"].includes(message.header.type) && !isPriorAgreement(message);
}

/// Persisting messages
async function persistMessage(message) {
	const MessageService = require("./services/messages");
	try {
		const {
			header,
			data,
			type,
			sender,
			state,
			routing = header.meta?.routing || null,
			destination = header.routingdata?.destination || "messages",
		} = message;
		const { receiver = 0, organisation } = routing;
		const crypto = require("crypto");
		const hashable =
			JSON.stringify(data) +
			JSON.stringify(header.to) +
			JSON.stringify(header.from) +
			JSON.stringify(header.meta);
		const hash = crypto.createHash("md5").update(hashable).digest("hex");
		const isDuplicate = await MessageService.exists(hash);
		if (!isDuplicate) {
			return await MessageService.save(
				header,
				data,
				type,
				sender,
				receiver,
				state,
				organisation,
				hash,
				destination
			);
		}
		return message;
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error saving message");
	}
}

async function formatMessage(payload) {
	const type = formatMessageType(payload);
	const state = formatMessageState(payload);
	const header = await formatMessageHeader(payload);
	const data = utils.clone(payload.data);

	const message = {
		type,
		state,
		header,
		data,
		sender: payload.header.from.id || 0,
		receiver: payload.header.meta.routing.receiver || 0,
	};
	return message;
}

function formatMessageType(message) {
	return message.header.type;
}

function formatMessageState(message) {
	return utils.isNumeric(message?.header?.meta?.routing?.organisation) ? "SENT" : "UNDELIVERABLE";
}

async function formatMessageHeader(message) {
	const MessageService = require("./services/messages");
	const header = utils.clone(message.header);
	if (header.original) {
		const original = await MessageService.findOriginal(header.original, header.type);
		if (original?.rows?.length === 1) {
			header.overrides = {
				id: original.rows[0].id,
				type: header.type,
			};
		}
	}

	delete header.rules;
	delete header.template;

	return header;
}

function formatMessageMetadata(route, group) {
	const { action, notification, organisation, receiver, condition } = route;
	return {
		group,
		routing: {
			action,
			notification,
			organisation,
			receiver,
			condition,
			formatter,
		},
	};
}

function formatRoutingDataPrimary(route) {
	const { action, notification, receivers, destination } = route;
	return {
		extraction: "SENDTO",
		rules: "PRIMARY",
		route: action,
		notification: notification,
		id: receivers?.route,
		destination,
	};
}

function formatRoutingDataCC(route, receiver) {
	const { action, notification, receivers, destination } = route;
	return {
		extraction: receiver.type,
		rules: "CC",
		route: action,
		notification: notification,
		id: receivers?.route,
		destination,
	};
}

function stripPersonalData(message) {
	message.header.pio = true;
	if (message.data && message.data.authors) {
		message.data.authors.forEach((author) => {
			delete author.email;
		});
	}
	if (message.data && message.data.charges) {
		delete message.data.charges;
	}
	if (message.data && message.data.settlement) {
		delete message.data.settlement;
	}
	return message;
}

function initTransformer(serialized) {
	let transform = null;
	if (serialized && typeof serialized === "string" && serialized.indexOf("function") === 0) {
		eval("transform = " + serialized);
	}
	return transform;
}

async function dispatchMessage(message) {
	try {
		let TopicArn = null;
		switch (message.header?.meta?.routing?.action) {
			case "DEFAULT":
				TopicArn = process.env.SNS_SUCCESS;
				break;
			case "CONSORTIA":
				TopicArn = process.env.SNS_SUCCESS;
				break;
			case "AUTOCC":
				TopicArn = process.env.SNS_SUCCESS;
				break;
			case "AUTOREJECT":
				TopicArn = process.env.SNS_REJECT;
				break;
			case "DLQ":
				TopicArn = process.env.SNS_FAILURE;
				break;
			default:
				TopicArn = process.env.SNS_SUCCESS;
				break;
		}
		const sns = new AWS.SNS();
		await sns
			.publish({
				Message: JSON.stringify(message),
				Subject: "Forwarding to distribution topics",
				TopicArn,
			})
			.promise();
		return true;
	} catch (error) {
		errorhandler.log(error, "Error delivering messages to SNS");
		return false;
	}
}

function hasSecondaryReceivers(message) {
	return HAVESECONDARYRECEIVERS.includes(message.header.type);
}
