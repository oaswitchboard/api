//@ts-check

const errorhandler = require('./../libs/errorhandler');
const formatter = require('./../libs/formatter');
const utils = require('./../libs/utils');
const SQS = require('../libs/sqs');

module.exports = { ingest, send, resend }

async function ingest (event) {
    console.log('webhook.ingest() --------------------------------------------------')
    console.log('JSON.stringify', JSON.stringify(event));
    // event = require("../tests/mocks/events/webhook-ingest-ok");
    try {
        let result = {
            success: 0,
            skipped: 0,
        }
        for (const record of event.Records) {
            const message = JSON.parse(record.Sns.Message);
            if (getNotificationType (message) === 'WEBHOOK') {
                const queue = process.env.SQS_WEBHOOK_URL;
                const sender = 'Receiver webhook'
                const formatted = SQS.formatMessageForRegularQueue (message, sender, queue);
                const submitted = await SQS.sendMessage(formatted);
                result.success++;
            } else {
                result.skipped++;
            }
        }
        return formatter.render(result);
    } catch (error) {
        console.log('error', error);
        errorhandler.log(error, '[OASB API] Error queueing webhook message');
        return formatter.error('Error queueing webhook message', 500);
    }
}

async function send (event) {
    console.log('webhook.send() --------------------------------------------------')
    console.log('JSON.stringify', JSON.stringify(event));
    // event = require("../tests/mocks/events/webhook-send-ok");
    const WebhookService = require('./services/webhooks');
    const Webhook = require('../libs/webhook');
    try {
        let result = {
            success: 0,
            skipped: 0,
            errors: 0,
        }
        for (const record of event.Records) {
            const payload = JSON.parse(record.body);
            const route_id = payload?.header?.routingdata?.id || null;
            if (isValidWebhookEvent(payload) && utils.isNumeric(route_id)) {
                const targets = await WebhookService.findByRouteId(route_id);
                for (const target of targets) {
                    const { url, apikey }= target;
                    if (utils.isURL(url)) {
                        const dispatched = await Webhook.dispatch(payload, url, apikey);
                        console.log('dispatched', dispatched);
                        if (dispatched) {
                            result.success++;
                        } else {
                            result.errors++;
                        }
                    } else {
                        result.skipped++;
                    }
                }
            } else {
                result.skipped++;
            }
        }
        return formatter.render(result);
    } catch (error) {
        errorhandler.log(error, 'Error dispatching webhook request');
        return formatter.error('Error dispatching webhook request', 428);
    }
}

async function resend (event) {
    try {
        const Webhook = require('../libs/webhook');
        const logRecordId = event.pathParameters.id;
        const logRecord = await Webhook.fetchLogRecord(logRecordId);
        if (!logRecord) {
            return formatter.error('Log record with specified id not found', 404);
        }
        const { payload, url, apikey } = logRecord;
        const dispatched = await Webhook.dispatch(JSON.parse(payload), url, apikey);
        return formatter.render({ success: dispatched });
    } catch (error) {
        errorhandler.log(error, 'An error occurred while sending webhook');
        return formatter.error('An error occurred while sending webhook', 500);
    }
}


function isValidWebhookEvent(event) {
    return getNotificationType(event) === 'WEBHOOK';
}

function getNotificationType (event) {
    return event?.header?.meta?.routing?.notification;
}