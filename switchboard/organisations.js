const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const utils = require('./../libs/utils');

module.exports = {

    /* list () - returns a list of organisationa */

    search: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.body.query ];
            const query = `SELECT
                organisations.id,
                organisations.name,
                organisations.participant_id,
                participants.institution AS participant
                FROM organisations LEFT OUTER JOIN participants ON participants.id = organisations.participant_id
                WHERE (
                    organisations.name ILIKE '%'  || $1 || '%'
                    OR participants.institution ILIKE '%'  || $1 || '%'
                )`;
            const sqlcount = `SELECT
                count(organisations.id) as total
                FROM organisations LEFT OUTER JOIN participants ON participants.id = organisations.participant_id
                WHERE (
                    organisations.name ILIKE '%'  || $1 || '%'
                    OR participants.institution ILIKE '%'  || $1 || '%'
                )`;
            const organisations =  await client.query(query, arguments);
            const count =  await client.query(sqlcount, arguments);
            return {
                total: count.rows[0].total,
                rows: organisations.rows
            };
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving organisations');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* participant - show participant */

    participant: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        if (!event.path.id || !utils.isNumeric(event.path.id)) {
            return [];
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.path.id ];
            const query = `SELECT
                organisations.id,
                organisations.name,
                organisations.participant_id,
                participants.institution AS participant
                FROM organisations LEFT OUTER JOIN participants ON participants.id = organisations.participant_id
                WHERE participant_id = $1`;
            const organisations =  await client.query(query, arguments);
            return organisations.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving organisations per participant');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* detail () - returns a single organisation */

    detail: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        if (!event.path.id || !utils.isNumeric(event.path.id)) {
            return formatter.error('No journals found', 404);
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.path.id ];
            const query = `SELECT
                organisations.*,
                participants.institution AS participant
                FROM organisations LEFT OUTER JOIN participants ON participants.id = organisations.participant_id
                WHERE organisations.id = $1`;
            const organisations =  await client.query(query, arguments);
            if (organisations.rows.length === 1) {
                return organisations.rows[0];
            }
            return formatter.error('No organisations found', 404);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving organisation');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* create () - create a single organisation */

    create: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        const client = await pool.connect();
        try {
            const participant_id = event.body.participant_id || 0;
            const name = event.body.name || '';
            const ror_id = event.body.ror_id || '';
            const ringgold_id = event.body.ringgold_id || '';
            const isni = event.body.isni || false;
            const domainnames = event.body.domainnames || '';
            const fundref = event.body.fundref || '';

            const arguments = [ participant_id, name, ror_id, ringgold_id, isni, domainnames, fundref, ];
            const query = `
                INSERT INTO organisations
                (participant_id, name, ror_id, ringgold_id, isni, domainnames, fundref)
                VALUES ($1, $2, $3, $4, $5, $6, $7)
            `;
            await client.query(query, arguments);
            return formatter.render('Organisation inserted');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error creating organisation');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* update () - update a single journal */

    update: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        if (!event.path.id) {
            return formatter.error('No journals found', 404);
        }
        const client = await pool.connect();
        try {
            const participant_id = event.body.participant_id || 0;
            const name = event.body.name || '';
            const ror_id = event.body.ror_id || '';
            const ringgold_id = event.body.ringgold_id || '';
            const isni = event.body.isni || false;
            const domainnames = event.body.domainnames || '';
            const fundref = event.body.fundref || '';

            const arguments = [ event.path.id, participant_id, name, ror_id, ringgold_id, isni, domainnames, fundref, ];
            const query = `UPDATE organisations SET
                    participant_id = $2,
                    name = $3,
                    ror_id = $4,
                    ringgold_id = $5,
                    isni = $6,
                    domainnames = $7,
                    fundref = $8
                WHERE organisations.id = $1`;
            await client.query(query, arguments);
            return formatter.render('Organisation updated');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error updating organisation');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    ror: async (organisation) => {
        const ror = require('./../libs/ror');
        try {
            let found = null;
            if (organisation.name) {
                found = await ror.generic.search(organisation.name);
            }
            if (found) {
                return {
                    statusCode: 200,
                    body: JSON.stringify(found),
                };
            }
            return {
                statusCode: 404,
                body: JSON.stringify(found),
            };

        } catch (error) {
            errorhandler.log(error, '[OASB API] Error searching organisations by RoR id');
            return {
				statusCode: 500,
				body: JSON.stringify(error),
			};
        }
    },

};

