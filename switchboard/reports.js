const AWS = require('aws-sdk');
AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const path = require('path');
const fs = require('fs')
const { unlink } = require('node:fs/promises');

const Excel = require('exceljs');
const decode = require('html-entities-decoder')
const moment = require('moment');
const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const utils = require('./../libs/utils');
const errorhandler = require('./../libs/errorhandler');
const { V1ToV2 } = require('./../libs/convert');

const p1OnlyReportTypes = ['p1', 'article']

module.exports = {
    exportStart: async (event) => {
        const { sub } = event.requestContext.authorizer.claims;
        const reportFormat = event.queryStringParameters?.type === 'json' ? 'json' : 'excel';
        const reportCategory = isReportAdvanced(event.queryStringParameters?.category) ? 'advanced' : 'general';
        const reportType = JSON.parse(event.body)?.reportType;

        const payload = {
            reportFormat,
            reportType,
            criteria: createCriteria(event.body, reportCategory),
            tags: { sub, reportCategory },
        };
        const executionState = await checkExportTaskActiveExecution(sub, reportCategory);
        if (!executionState) {
            await exportTaskStartExecution(payload);
        }
        return formatter.render({ started: !executionState });
    },

    exportCount: async (event) => {
        const { reportFormat, reportType, criteria, tags: { sub, reportCategory } } = event;
        const messagesCount = await getMessagesCount(sub, criteria);
        return {
            sub,
            reportCategory,
            messagesCount,
            reportFormat,
            reportType,
            criteria,
        }
    },

    exportCreateReport: async (event) => {
        const { sub, reportCategory, reportFormat, reportType } = event;
        const filePath = reportFormat === 'json'
            ? await createJsonReport(event)
            : await createExcelReport(event);
        return {
            sub,
            reportCategory,
            reportType,
            filePath
        }
    },

    exportUploadClean: async (event) => {
        const { sub, filePath, reportCategory, reportType } = event;
        const fileNamePrefix = getFileNamePrefix(reportType);
        const s3 = new AWS.S3();
        let params = {
            Bucket: process.env.S3_BUCKET,
            Key: `cache/${sub}/${isReportAdvanced(reportCategory) ? 'advanced/' : ''}${fileNamePrefix}${moment().format('YYYYMMDD-HHmmss')}${path.extname(filePath)}`,
            Body: fs.createReadStream(filePath),
        }
        await s3.upload(params).promise();
        await unlink(filePath);
        await clearExpiredCacheObjects(sub, filePath, reportCategory);
        return {};
    },

    exportFallback: async (event) => {
        console.log(event);
    },

    report: async (event) => {
        try {
            let { req, category } = event.queryStringParameters || {};
            const { sub } = event.requestContext.authorizer.claims;

            const s3 = new AWS.S3();
            const Bucket = process.env.S3_BUCKET;
            const listParams = {
                Bucket,
                Prefix: `cache/${sub}/${isReportAdvanced(category) ? 'advanced/' : ''}`,
                MaxKeys: 100,
                Delimiter: '/' ,
            };
            let listData = await s3.listObjectsV2(listParams).promise();

            if (req) {
                let requested = listData.Contents.find(element => element.Key.includes(req));
                if (!requested) {
                    return formatter.error('[OASB API] Report not found')
                };
                const url = await s3.getSignedUrlPromise('getObject', { Bucket, Key: requested.Key, Expires: 300 });
                return formatter.render(url);
            }

            listData.Contents.sort((a, b) =>
                b.LastModified.getTime() - a.LastModified.getTime()
            );
            return formatter.render({
                inProgress: await checkExportTaskActiveExecution(sub),
                cachedReports: listData.Contents.map(element => ({
                    report: path.parse(element.Key).base,
                    created: element.LastModified
                }))
            });
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving a report');
            return formatter.error(error, 500);
        }
    },

};

async function getMessagesCount(cognitoId, criteria) {
    const client = await pool.connect();
    try {
        const sql = `
            SELECT count(messages.id) AS total FROM messages
            LEFT JOIN participants selector ON messages.sender = selector.id OR messages.receiver = selector.id
            WHERE selector.cognito_id = '${cognitoId}'
            ${criteria}`;
        const response = await client.query(sql);
        return +response.rows.pop().total;
    } finally {
        client.release();
    }
}

async function getMessagesBatch(offset, batchSize, cognitoId, criteria) {
    const client = await pool.connect();
    try {
        const sql = `SELECT
                (SELECT e1.id FROM messages e1 WHERE type = 'e1' AND e1.header ->> 'e1'::text = messages.header ->> 'e1'::text AND e1.type = 'e1' LIMIT 1) AS e1,
                (SELECT e2.id FROM messages e2 WHERE type = 'e2' AND e2.header ->> 'e2'::text = messages.header ->> 'e2'::text AND e2.type = 'e2' LIMIT 1) AS e2,
                (SELECT p1.id FROM messages p1 WHERE type = 'p1' AND p1.header ->> 'p1'::text = messages.header ->> 'p1'::text AND p1.type = 'p1' LIMIT 1) AS p1,
                (SELECT p2.id FROM messages p2 WHERE type = 'p2' AND p2.header ->> 'p2'::text = messages.header ->> 'p2'::text AND p2.type = 'p2' LIMIT 1) AS p2,
                messages.*,
                organisations.name AS organisation,
                organisations.ror_id AS organisation_ror,
                sender.institution AS sender_name,
                sender.ror AS sender_ror,
                receiver.institution AS receiver_name,
                receiver.ror AS receiver_ror
            FROM messages
            LEFT JOIN participants selector ON messages.sender = selector.id OR messages.receiver = selector.id
            LEFT JOIN participants sender ON messages.sender = sender.id
            LEFT JOIN participants receiver ON messages.receiver = receiver.id
            LEFT JOIN organisations ON messages.organisation = organisations.id
            WHERE selector.cognito_id = '${cognitoId}'
            ${criteria}
            ORDER BY id DESC
            OFFSET ${offset} LIMIT ${batchSize}`;
        const response = await client.query(sql);
        const batch = response.rows.map(row => V1ToV2(row));
        return batch;
    } finally {
        client.release();
    }
}

async function checkExportTaskActiveExecution(sub, reportCategory) {
    const stepFunctions = new AWS.StepFunctions();
    const stateMachineArn = process.env.EXPORT_TASK;
    let listParams = {
        stateMachineArn,
        statusFilter: 'RUNNING',
        maxResults: 10,
    };
    let data = await stepFunctions.listExecutions(listParams).promise();
    for (let execution of data.executions) {
        const describeParams = {
            executionArn: execution.executionArn,
        };
        let describeData = await stepFunctions.describeExecution(describeParams).promise();
        const input = JSON.parse(describeData.input);
        const executionTags = input.tags || {};
        if (executionTags?.sub === sub && executionTags.reportCategory === reportCategory) {
            return true;
        }
    }
    return false;
}

async function exportTaskStartExecution(payload) {
    const stepFunctions = new AWS.StepFunctions();
    const stateMachineArn = process.env.EXPORT_TASK;
    const params = {
        stateMachineArn,
        input: JSON.stringify(payload)
    }
    return await stepFunctions.startExecution(params).promise();
}

function getFileNamePrefix(reportType) {
    switch (reportType) {
        case "p1":
            return "report_messages_";
        case "article":
            return "report_article_";
       default:
            return "export_messages_";
    }

}

async function clearExpiredCacheObjects(sub, newObject, reportCategory) {
    const s3 = new AWS.S3();
    let params = {
        Bucket: process.env.S3_BUCKET,
        Prefix: `cache/${sub}/${isReportAdvanced(reportCategory) ? 'advanced/' : ''}`,
        MaxKeys: 100,
        Delimiter: '/' ,
    };
    let listData = await s3.listObjectsV2(params).promise();
    listData.Contents.sort((a, b) =>
        b.LastModified.getTime() - a.LastModified.getTime()
    );
    let deleteElements = listData.Contents
        .filter(element => path.extname(element.Key) === path.extname(newObject))
        .slice(1);
    if (deleteElements.length) {
        for (const element of deleteElements) {
            const deleteParams = { Bucket: process.env.S3_BUCKET, Key: element.Key };
            await s3.deleteObject(deleteParams).promise();
        }
    }
}

async function createJsonReport(parameters) {
    const {
        sub,
        messagesCount,
        criteria,
    } = parameters;
    const batchSize = 20000;
    const filePath = `${process.env.LAMBDA_MOUNT_PATH}/${sub}.json`;
    const writeStream = fs.createWriteStream(filePath);
    writeStream.write('[\n')
    let isFirst = true;
    for (let offset = 0; offset < messagesCount; offset += batchSize) {
        const batch = await getMessagesBatch(offset, batchSize, sub, criteria);
        for await (const row of batch) {
            if (!isFirst) {
                writeStream.write(',\n');
            }
            isFirst = false;
            writeStream.write(JSON.stringify(row));
        }
    }
    writeStream.write(']');
    writeStream.end();
    await new Promise((res) => writeStream.on('close', res));
    return filePath;
}

async function createExcelReport(parameters) {
    const {
        sub,
        messagesCount,
        criteria,
        reportCategory,
        reportType
    } = parameters;
    const maxMessageCount = 50000;
    const effectiveCount = Math.min(messagesCount, maxMessageCount);
    const p1Only = isReportAdvanced(reportCategory) && p1OnlyReportTypes.includes(reportType);
    const batchSize = 10000;
    const filePath = `${process.env.LAMBDA_MOUNT_PATH}/${sub}${isReportAdvanced(reportCategory) ? '-advanced' : ''}.xlsx`;
    const writeStream = fs.createWriteStream(filePath);
    const workbook = new Excel.stream.xlsx.WorkbookWriter({
        stream: writeStream,
    });
    const worksheetMessages = createWorksheetMessages(workbook, p1Only);
    const worksheetAuthors = createWorksheetAuthors(workbook);
    const worksheetFunders = createWorksheetFunders(workbook);
    for (let offset = 0; offset < effectiveCount; offset += batchSize) {
        const batch = await getMessagesBatch(offset, batchSize, sub, criteria);
        await addMessagesRows(worksheetMessages, batch, p1Only);
        await addAuthorsRows(worksheetAuthors, batch);
        await addFundersRows(worksheetFunders, batch);
    }
    const created = moment().format('YYYY-MM-DD');
    worksheetMessages.addRow([' '])
    worksheetMessages.addRow(['Export date: ', created])
    worksheetMessages.commit();
    worksheetAuthors.commit();
    worksheetFunders.commit();
    await workbook.commit();
    return filePath;
}

const createWorksheetMessages = (workbook, p1Only) => {
    const worksheet = workbook.addWorksheet('Messages');
    const fields = formatMessageFields(p1Only);
    const columns = formatColumns(fields);
    worksheet.columns = columns;
    return worksheet;
}

const createWorksheetAuthors = workbook => {
    const worksheet = workbook.addWorksheet('Authors');
    const fields = formatAuthorFields();
    const columns = formatColumns(fields);
    worksheet.columns = columns;
    return worksheet;
}

const createWorksheetFunders = workbook => {
    const worksheet = workbook.addWorksheet('Funders');
    const fields = formatFunderFields();
    const columns = formatColumns(fields);
    worksheet.columns = columns;
    return worksheet;
}

const sleep = ms => new Promise(r => setTimeout(r, ms));

const addMessagesRows = async (worksheet, rows, p1Only) => {
    const fields = formatMessageFields(p1Only);
    const formatted = rows.filter(row => row.header)
        .map(row => ({
            ...formatHeader(row, p1Only),
            ...formatRowData(row.data, p1Only),
        }));
    let iteration = 0;
    for (let row of formatted) {
        const data = fields.map(field => {
            return row[field] ? '' + row[field] : '';
        });
        worksheet.addRow(data);
        worksheet.lastRow?.commit();
        if (iteration % 1000 === 0) {
            await sleep(10);
        }
        iteration++;
    }
}

const addAuthorsRows = async (worksheet, rows) => {
    let iteration = 0;
    for (let row of rows) {
        const authors = row.data.authors;
        const columnsdoi = formatAuthorAndFunderRowDOI(row);
        const columnsmeta = formatAuthorAndFunderRowMetadata(row);
        const columnsdeal = formatAuthorAndFunderRowDeal(row);
        if (authors) {
            for (let author of authors) {
                const columnauthor = formatAuthorRow(author);
                const columncurrent = formatAuthorCurrentRow(author);
                for (const institution of author.institutions ?? []) {
                    const columninstitution = formatAuthorInstitutionRow(institution);
                    if (!utils.isObjectEmpty(columninstitution)) {
                        worksheet.addRow({
                            ...columnsdoi,
                            ...columnsmeta,
                            ...columnsdeal,
                            ...columnauthor,
                            ...columncurrent,
                            ...columninstitution,
                        });
                        worksheet.lastRow?.commit();
                        if (iteration % 1000 === 0) {
                            await sleep(10);
                        }
                        iteration++;
                    }
                }
            }
        }
    }
}

const addFundersRows = async (worksheet, rows) => {
    let iteration = 0;
    for (let row of rows) {
        const funders = row.data.article?.funders;
        if (funders) {
            const columnsdoi = formatAuthorAndFunderRowDOI(row);
            const columnsmeta = formatAuthorAndFunderRowMetadata(row);
            const columnsdeal = formatAuthorAndFunderRowDeal(row);
            for (let funder of funders) {
                const columnfunder = formatFunderRow(funder);
                if (!utils.isObjectEmpty(columnfunder)) {
                    worksheet.addRow({
                        ...columnsdoi,
                        ...columnsmeta,
                        ...columnsdeal,
                        ...columnfunder,
                    });
                    worksheet.lastRow?.commit();
                    if (iteration % 1000 === 0) {
                        await sleep(10);
                    }
                    iteration++;
                }
            }
        }
    }
}

function formatAuthorAndFunderRowDeal(row) {
    const data = {};
    if (row.data.charges && row.data.charges.agreement) {
        const name = row.data.charges.agreement.name ? decode(row.data.charges.agreement.name) : '';
        const id = row.data.charges.agreement.id || '';
        data[`deal name & id`] = `${name} ${id}`;
    }
    return data;
}

function formatAuthorAndFunderRowDOI(row) {
    const data = {};
    if (row.data.article) {

        data['message id'] = row.id || '';
        data['doi'] = row.data.article?.doi || '';
        data['journal'] = row.data.journal?.name || '';
        data['journal id'] = row.data.journal?.id || '';
    }
    return data;
}

function formatAuthorAndFunderRowMetadata(row) {
    const data = {};
    data['prioragreement'] = formatPriorAgreement(row);
    data['pio'] = formatPio(row);
    return {
        ...data,
        ...formatSenderMetadata(row),
        ...formatReceiverMetadata(row),
    };
}

function formatAuthorRow(author) {
    const data = {};
    if (author) {
        data['last name'] = author.lastName || '';
        data['first name'] = author.firstName || '';
        data['initials'] = author.initials || '';
        data['ORCID'] = author.ORCID || '';
        data['e-mail address'] = author.email || '';
        data['collaboration/project'] = formatAuthorCollaboration(author);
        data['author listing order'] = author.listingorder || '';
        data['author listing order at acceptance'] = author.listingorderAtAcceptance || '';
        data['author listing order at submission'] = author.listingorderAtSubmission || '';
        data['CRediT roles'] = formatAuthorCreditroles(author);
        data['corresponding author'] = formatBoolean(author.isCorrespondingAuthor);
        data['corresponding author at acceptance'] = author.isCorrespondingAuthorAtAcceptance || '';
        data['corresponding author at submission'] = author.isCorrespondingAuthorAtSubmission || '';
        data['affiliation, as (to be) printed in the VoR'] = author.affiliation || '';
    }
    return data;
}

function formatAuthorInstitutionRow(institution) {
    const data = {};
    if (institution) {
        data['institution name'] = institution.name || '';
        data['institution ror id'] = institution.ror || '';
        data['institution ISNI'] = institution.isni || '';
        data['institution source affiliation'] = institution.sourceaffiliation || '';
        data['institution country'] = institution.country || '';
    }
    return data;
}

function formatAuthorCurrentRow(author) {
    const data = {};

    if (author?.currentaddress) {
        data['current address'] = author.currentaddress.map((adr) => {
            return [`${adr.name || ''}`, `${adr.ror || ''}`, `${adr.isni  || ''}`].filter(Boolean).join(', ');
        }).filter(Boolean).join(' | ');
    }

    return data;
}

function formatFunderRow(funder) {
    const data = {};
    if (funder && funder.name) {
        data['funder name'] = funder.name || '';
        data['funder ror'] = funder.ror || '';
        data['funder fundref'] = funder.fundref || '';
    }
    return data;
}

function createCriteria(options, reportCategory) {

    let parameters = JSON.parse(options) || {};

    let criteria = ` AND messages.state IN ('SENT', 'AUTOREJECT', 'OVERRIDDEN','UNDELIVERABLE') `;

    if (isReportAdvanced(reportCategory) && parameters.fromDate) {
        criteria += ` AND messages.created >= '${parameters.fromDate}'`;
    }

    if (isReportAdvanced(reportCategory) && parameters.toDate) {
        criteria += ` AND messages.created <=  ('${parameters.toDate}'::date + '1 day'::interval)`;
    }

    if (isReportAdvanced(reportCategory) && parameters.reportType === 'p1') {
        // only p1 messages for 'p1' type
        criteria += ` AND messages.type = 'p1'`;
    }

    if (isReportAdvanced(reportCategory) && parameters.reportType === 'article') {
        // only p1 messages for 'article type
        criteria += ` AND messages.type = 'p1'`;
        // If multiple messages exist for the same DOI, select only the most recent one.
        criteria += ` AND messages.id = (
            SELECT MAX(id)
            FROM messages messages2
            WHERE messages.data -> 'article' ->> 'doi'::text = messages2.data -> 'article' ->> 'doi'::text
            AND (messages2.sender = selector.id OR messages2.receiver = selector.id)
        )`;
        // For publishers, select only primary messages, so not the result of auto-CC.
        criteria += ` AND (selector.type != 'publisher' OR messages.header -> 'routingdata' ->> 'rules'::text = 'PRIMARY')`;
    }

    if (utils.isNumeric(parameters.publisherId)) {
        criteria += ` AND messages.sender = '${parameters.publisherId}'`;
    }

    return criteria;
}


function formatMessageFields(p1Only) {
    const fields = {
        ...preFormatHeader(p1Only),
        ...preFormatAuthor(),
        ...preFormatJournal(),
        ...preFormatArticle(),
        ...preFormatCharges(p1Only),
        ...preFormatAcceptance(p1Only),
        "publication funders": "",
        "charges approval": ""
    };
    return Object.keys(fields);
}

function formatAuthorFields() {
    const fields = {
        ...preFormatDOIColumns(),
        ...preFormatMetadataColumns(),
        ...preFormatDealColumns(),
        ...preFormatAuthorColumns(),
    };
    return Object.keys(fields);
}

function formatFunderFields() {
    const fields = {
        ...preFormatDOIColumns(),
        ...preFormatMetadataColumns(),
        ...preFormatDealColumns(),
        ...preFormatFunderColumns(),
    };
    return Object.keys(fields);
}

function formatColumns(fields, rows) {
    return fields.map(field => {
        return { header: field, key: field };
    });
}

function formatRowData(data, p1Only) {
    let response = {};
    if (data.authors) {
        response = { ...response, ...formatAuthors(data.authors) }
    }
    if (data.article) {
        response = { ...response, ...formatArticle(data.article) }
    }
    if (data.journal) {
        response = { ...response, ...formatJournal(data.journal) }
    }
    if (data.charges) {
        response = { ...response, ...formatCharges(data.charges, p1Only) }
    }
    if (data.acceptance) {
        response = { ...response, ...formatAcceptance(data.acceptance, p1Only) }
    }
    return response;
}

function validAuthor(author) {
    return author;
}

function formatAuthors(authors) {

    const response = preFormatAuthor();
    response['corresponding author'] = [...authors].filter(author => validAuthor(author) && author.isCorrespondingAuthor).map(author => {
        const lastName = author.lastName ? decode(author.lastName) : "";
        const initials = author.initials ? decode(author.initials) : "";
        return `${lastName} ${initials}`;
    }).join(', ');

    response['authors'] = [...authors].filter(author => validAuthor(author)).map(author => {
        const lastName = author.lastName ? decode(author.lastName) : "";
        const initials = author.initials ? decode(author.initials) : "";
        return `${lastName} ${initials}`;
    }).join(', ');

    const affiliations = [];
    [...authors].filter(author => validAuthor(author)).forEach(author => {
        const authorAffiliations = author.affiliation ? author.affiliation.split(",") : [];
        authorAffiliations.forEach(affiliation => {
            if (!affiliations.includes(affiliation.trim())) {
                affiliations.push(affiliation.trim());
            }
        });
    });

    response['affiliations'] = affiliations.map(affiliation => affiliation ? decode(affiliation) : '').join(', ');

    const correspondingaffiliations = [];
    [...authors].filter(author => validAuthor(author) && author.isCorrespondingAuthor).forEach(author => {
        const authorAffiliations = author.affiliation ? author.affiliation.split(",") : [];
        authorAffiliations.forEach(affiliation => {
            if (!correspondingaffiliations.includes(affiliation.trim())) {
                correspondingaffiliations.push(affiliation.trim());
            }
        });
    });
    response['corresponding author affiliations'] = correspondingaffiliations.map(affiliation => affiliation ? decode(affiliation) : '').join(', ');

    return response;
}

function formatDOI(article) {
    const doi = article.doi;
    if (!doi || doi === 'Unknown') {
        return '';
    }
    return doi.includes('https://doi.org') ? doi : `https://doi.org/${doi}`;
}

function formatArticle(article) {
    const response = preFormatArticle();
    if (article) {

        response['manuscript / article title'] = article.title ? decode(article.title) : '';
        response['article type'] = article.type || '';
        response['article originaltype'] = article.originaltype || '';
        response['article doi'] = formatDOI(article);
        response['article funders'] = '';
        response['article grants'] = '';

        if (article.funders) {
            response['funder acknowledgement'] = article.acknowledgement || '';
            response['funders'] = [...article.funders].filter(funder => funder).map(funder => funder.name ? decode(funder.name) : '').join(', ');
        }

        if (article.grants) {
            response['grants'] = article.grants.map((gr) => {
                return [`${decode(gr.name || '')}`, `${gr.doi || ''}`].filter(Boolean).join(', ');
            }).filter(Boolean).join(' | ');
        }
        if (article.manuscript) {
            response['manuscript id'] = article.manuscript.id || '';

            const submissiondate = moment(article.manuscript.dates.submission, "YYYY-MM-DD");
            const acceptancedate = moment(article.manuscript.dates.acceptance, "YYYY-MM-DD");
            const publicationdate = moment(article.manuscript.dates.publication, "YYYY-MM-DD");

            response['manuscript submission date'] = isValidDate(submissiondate) ? article.manuscript.dates.submission : '';
            response['manuscript acceptance date'] = isValidDate(acceptancedate) ? article.manuscript.dates.acceptance : '';
            response['article publication date'] = isValidDate(publicationdate) ? article.manuscript.dates.publication : '';
        }
        if (article.preprint) {
            response[`preprint`] = article.preprint.title || '';
            response[`preprint url`] = article.preprint.url || '';
            response[`preprint id`] = article.preprint.id || '';
        }
        if (article.vor) {
            response[`journal type`] = article.vor.publication || '';
            response[`VoR license`] = article.vor.license || '';
            response[`VoR deposition`] = article.vor.deposition || '';
            response[`VoR startdate`] = article.vor.startdate || '';
            response[`VoR research data`] = article.vor.researchdata || '';
        }

        response['submission id'] = article.submissionId || '';
    }

    return response;
}

function isValidDate(date) {
    const threshold = moment('1970-01-01', 'YYYY-MM-DD');
    return date.isValid() && date.isSameOrAfter(threshold);;
}

function formatJournal(journal) {
    const response = preFormatJournal();
    if (journal) {
        response[`journal`] = journal.name ? decode(journal.name) : '';
        response[`journal id`] = journal.id || '';
        response[`issn`] = journal.issn || '';
        response[`eissn`] = journal.eissn || '';
        response[`in DOAJ`] = formatBoolean(journal.inDOAJ) || '';
        response[`journal type comment`] = journal.typecomment || '';
    }
    return response;
}

function formatCharges(charges, p1Only) {
    const response = preFormatCharges(p1Only);
    const pre = p1Only ? '' : 'E1/'
    if (charges) {
        if (charges.prioragreement && charges.agreement) {
            const name = charges.agreement && charges.agreement.name ? decode(charges.agreement.name) : '';
            const id = charges.agreement.id || '';
            response[`${pre}P1 deal name and id`] = `${name} ${id}`;
        }
        if (charges.charged) {
            response[`${pre}P1 charged`] = formatBoolean(charges.charged) || '';
        }
        if (charges.currency) {
            response[`${pre}P1 apc currency`] = charges.currency || '';
        }
        if (charges.transaction && charges.transaction.invoicenr) {
            response['Invoice number'] = charges.transaction.invoicenr;
        }
        if (charges.fees && charges.fees.apc) {
            response[`${pre}P1 apc status`] = charges.fees.apc.name || '';
            response[`${pre}P1 apc type`] = charges.fees.apc.type || '';
            response[`${pre}P1 apc amount`] = charges.fees.apc.amount || 0;
        }
        if (charges.fees && charges.fees.extra) {
            charges.fees.extra.forEach((extra, index) => {
                if (!extra.type) return;
                response[`${pre}P1 other charges and discounts`] +=
                    charges.fees.extra.length === index + 1 ?
                        `${extra.type}=${extra.amount || 0}` :
                        `${extra.type}=${extra.amount || 0}, `;
            });
        }
        if (charges.fees && charges.fees.total && charges.fees.total.name) {
            response[`${pre}P1 fees status`] = charges.fees.total.name;
        }
        if (charges.fees && charges.fees.total && utils.isNumeric(charges.fees.total.amount)) {
            response[`${pre}P1 fees charges total amount`] = charges.fees.total.amount;
        }

        if (charges.approval && charges.approval.date && charges.approval.comment) {
            response[`charges approval`] = `${charges.approval.date} ${charges.approval.comment}`;
        }

        if (charges.publicationfunders) {
            response[`publication funders`] =  charges.publicationfunders.map((pf) => {
                return  [`${pf.name || ''}`, `${pf.ror || ''}`, `${pf.comment || ''}`].filter(Boolean).join(', ');
            }).filter(Boolean).join(' | ')
        }
    }
    return response;
}

// used in general export only
function formatAcceptance(acceptance, p1Only) {
    const response = preFormatAcceptance(p1Only);
    if (acceptance && !p1Only) {
        if (acceptance.prioragreement && acceptance.agreement) {
            response[`E2 deal acceptance`] = formatBoolean(acceptance.agreement.accepts) || '';
            response[`E1/P1 deal name and id`] = `${acceptance.agreement.name} ${acceptance.agreement.id}` || '';
        }
        response[`E2 deal acceptance conditions`] = acceptance.conditions || '';
        response[`E2 deal acceptance remarks`] = acceptance.remarks || '';
        response[`E2 PO number`] = acceptance.po || '';
        response[`E2 invoice details`] = acceptance.invoicedetails || '';
        response[`E2 currency`] = acceptance.currency || '';
        if (acceptance.fees && acceptance.fees.apc) {
            response[`E2 acceptance apc`] = acceptance.fees.apc.accepts || '';
            response[`E2 accepted amount apc`] = acceptance.fees.apc.amount || '';
        }
        if (acceptance.fees && acceptance.fees.extra && Array.isArray(acceptance.fees.extra)) {
            acceptance.fees.extra.forEach((extra, index) => {
                if (extra.type && extra.accepts) {
                    response[`E2 acceptance other charges and discounts`] +=
                        acceptance.fees.extra.length === index + 1 ?
                            `${extra.type}=${extra.amount || 0}` :
                            `${extra.type}=${extra.amount || 0}, `;
                }
            });
        }
    }
    return response;
}

function formatHeader(row, p1Only) {
    return {
        ...formatRowMetadata(row, p1Only),
        ...formatSenderMetadata(row),
        ...formatReceiverMetadata(row),
    }
}

function formatSenderMetadata(row) {
    const metadata = {};
    if (row && row.sender_name) {
        metadata[`sender name`] = row.sender_name ? decode(row.sender_name) : '';
    }
    if (row && row.sender_ror) {
        metadata[`sender ror`] = row.sender_ror;
    }
    return metadata;
}

function formatReceiverMetadata(row) {
    const metadata = {};
    if (row && row.organisation) {
        metadata[`sendto name`] = row.organisation ? decode(row.organisation) : '';
    } else {
        metadata[`sendto name`] = row.header?.to?.name || '';
    }
    if (row && row.organisation_ror) {
        metadata[`sendto ror`] = row.organisation_ror ? decode(row.organisation_ror) : '';
    } else {
        metadata[`sendto ror`] = row.header?.to?.address || '';
    }
    if (row && row.receiver_name) {
        metadata[`receiver name`] = row.receiver_name ? decode(row.receiver_name) : '';
    }
    if (row && row.receiver_ror) {
        metadata[`receiver ror`] = row.receiver_ror;
    }
    return metadata;
}

function formatRowMetadata(row, p1Only) {
    let { type } = row;
    switch (type) {
        case "e1":
            return formaRowMetadataE1(row);
        case "e2":
            return formaRowMetadataE2(row);
        case "p1":
            return formaRowMetadataP1(row, p1Only);
        case "p2":
            return formaRowMetadataP2(row);
    }
}

// used in general export only
function formaRowMetadataE1(row) {
    const { id, type, state, created, } = row;
    let response = {
        id,
        type,
        state,
        created,
        pio: formatPio(row),
        prioragreement: formatPriorAgreement(row)
    };
    return response
}

// used in general export only
function formaRowMetadataE2(row) {
    const { id, type, state, created, e1, } = row;
    let response = {
        id,
        type,
        state,
        created,
    };
    if (e1) {
        response.e1 = e1;
    }
    return response
}

function formaRowMetadataP1(row, p1Only) {
    const { id, type, state, created, e1, e2, } = row;
    let response = {
        id,
        type,
        state,
        created,
        pio: formatPio(row),
        prioragreement: formatPriorAgreement(row)
    };
    if (e1 && !p1Only) {
        response.e1 = e1;
    }
    if (e2 && !p1Only) {
        response.e2 = e2;
    }
    return response
}

// used in general export only
function formaRowMetadataP2(row) {
    const { id, type, state, created, e1, e2, p1, } = row;
    let response = {
        id,
        type,
        state,
        created,
    };
    if (e1) {
        response.e1 = e1;
    }
    if (e2) {
        response.e2 = e2;
    }
    if (p1) {
        response.p1 = p1;
    }
    return response
}

function formatAuthorCollaboration(author) {
    return Array.isArray(author.collaboration) && author.collaboration.length ? author.collaboration.join(', ') : '';
}

function formatAuthorCreditroles(author) {
    return Array.isArray(author.creditroles) && author.creditroles.length ? author.creditroles.join(', ') : '';
}

function formatPriorAgreement(row) {
    const charges = row.data.charges;

    return charges && utils.isBoolean(charges.prioragreement) ? formatBoolean(charges.prioragreement) : '';
}

function formatPio(row) {
    const { pio } = row.header;
    return utils.isBoolean(pio) ? formatBoolean(pio) : '';
}

function formatBoolean(value) {
    return !!value ? "Yes" : "No";
}

function preFormatHeader(p1Only) {
    return {
        ...{
            "id": '',
            "type": '',
            "state": '',
            "timing": '',
            "created": ''
        },
        ...(!p1Only && {
            "e1": '',
            "e2": '',
            "p1": '',
            "p2": '',
        }),
        ...{
            "prioragreement": '',
            "pio": '',
            "sender name": '',
            "sender ror": '',
            "sendto name": '',
            "sendto ror": '',
            "receiver name": '',
            "receiver ror": '',
        }

    }
}

function preFormatAuthor() {
    return {
        "corresponding author": '',
        "corresponding author affiliations": '',
        "authors": '',
        "affiliations": '',
    }
}

function preFormatArticle() {
    return {
        "manuscript / article title": '',
        "article type": '',
        "article originaltype": '',
        "article doi": '',
        "funders": '',
        "funder acknowledgement": '',
        "grants": '',
        "manuscript id": '',
        "submission id": '',
        "manuscript submission date": '',
        "manuscript acceptance date": '',
        "article publication date": '',
        "preprint": '',
        "preprint url": '',
        "preprint id": '',
        "journal type": '',
        "VoR license": '',
        "VoR deposition": '',
        "VoR startdate": '',
        "VoR research data": ''
    }
}

function preFormatJournal() {
    return {
        "journal": '',
        "journal id": '',
        "in DOAJ": '',
        "issn": '',
        "eissn": '',
        "journal type comment": '',
    }
}

function preFormatCharges(p1Only) {
    const pre = p1Only ? '' : 'E1/';
    return {
        [`${pre}P1 deal name and id`]: '',
        [`${pre}P1 charged`]: '',
        [`${pre}P1 apc status`]: '',
        [`${pre}P1 apc amount`]: '',
        [`${pre}P1 apc currency`]: '',
        [`${pre}P1 apc type`]: '',
        [`${pre}P1 other charges and discounts`]: '',
        [`${pre}P1 fees status`]: '',
        [`${pre}P1 fees charges total amount`]: '',
        "Invoice number": ''
    }
}

// used in general export only
function preFormatAcceptance(p1Only) {
    return p1Only
    ? {}
    : {
        "E2 deal acceptance": '',
        "E2 deal acceptance conditions": '',
        "E2 deal acceptance remarks": '',
        "E2 acceptance apc": '',
        "E2 accepted amount apc": '',
        "E2 acceptance other charges and discounts": '',
        "E2 fees charges acceptance conditions": '',
        "E2 fees charges acceptance remarks": '',
        "E2 invoice details": '',
        "E2 PO number": ''
    };
}

function preFormatDOIColumns() {
    return {
        "message id": '',
        "doi": '',
        "journal": '',
        "journal id": '',
    }
}

function preFormatMetadataColumns() {
    return {
        "prioragreement": '',
        "pio": '',
        "sendto name": '',
        "sendto ror": '',
        "receiver name": '',
        "receiver ror": '',
        "sender name": '',
        "sender ror": '',
    }
}

function preFormatDealColumns() {
    return {
        "deal name & id": '',
    }
}

function preFormatAuthorColumns() {
    return {
        "last name": '',
        "first name": '',
        "initials": '',
        "ORCID": '',
        "e-mail address": '',
        "collaboration/project": '',
        "author listing order": '',
        "author listing order at acceptance":  '',
        "author listing order at submission": '',
        "CRediT roles": '',
        "corresponding author": '',
        "corresponding author at acceptance": '',
        "corresponding author at submission": '',
        "affiliation, as (to be) printed in the VoR": '',
        "institution name": '',
        "institution ror id": '',
        "institution ISNI": '',
        "institution source affiliation": '',
        "institution country": '',
        "current address": ''
    }
}

function preFormatFunderColumns() {
    return {
        "funder name": '',
        "funder ror": '',
        "funder fundref": ''
    }
}

function isReportAdvanced(reportCategory) {
    return reportCategory === 'advanced';
}