module.exports = {
	schema: {
		id: "/e1",
		type: "object",
		required: ["header", "data"],
		properties: {
			header: {
				$ref: "/Header",
			},
			data: {
				type: "object",
				required: ["authors"],
				properties: {
					authors: {
						type: "array",
						items: {
							properties: {
								listingorder: {
									type: "number",
								},
								listingorderAtAcceptance: {
									type: "number",
								},
								listingorderAtSubmission: {
									type: "number",
								},
								isCorrespondingAuthorAtAcceptance: {
									type: "boolean",
								},
								isCorrespondingAuthorAtSubmission: {
									type: "boolean",
								},
								lastName: {
									type: "string",
								},
								firstName: {
									type: "string",
								},
								initials: {
									type: "string",
								},
								ORCID: {
									type: "string",
								},
								email: {
									type: "string",
								},
								creditroles: {
									type: "array",
									items: {
										type: "string",
										enum: [
											"conceptualization",
											"methodology",
											"software",
											"validation",
											"formal analysis",
											"investigation",
											"resources",
											"data curation",
											"writing",
											"writing – original draft",
											"writing – review & editing",
											"visualization",
											"supervision",
											"project administration",
											"funding acquisition",
											"",
										],
									},
								},
								isCorrespondingAuthor: {
									type: "boolean",
								},
								collaboration: {
									type: "string",
								},
								institutions: {
									type: "array",
									items: {
										properties: {
											name: {
												type: "string",
											},
											ror: {
												type: ["string", "null"],
												pattern:
													"^https://ror\\.org/[0-9a-z]{9}$",
											},
											isni: {
												type: "string",
											},
											country: {
												type: "string",
											},
										},
									},
								},
								currentaddress: {
									type: "array",
									items: {
										properties: {
											name: {
												type: "string",
											},
											ror: {
												type: ["string", "null"],
												pattern:
													"^https://ror\\.org/[0-9a-z]{9}$",
											},
											isni: {
												type: "string",
											},
										},
									},
								},
								affiliation: {
									type: "string",
								},
							},
						},
					},
					article: {
						type: "object",
						required: ["vor"],
						properties: {
							title: {
								type: "string",
							},
							type: {
								type: "string",
								enum: [
									"abstract",
									"addendum",
									"announcement",
									"article-commentary",
									"book-review",
									"books-received",
									"brief-report",
									"calendar",
									"case-report",
									"collection",
									"correction",
									"discussion",
									"dissertation",
									"editorial",
									"in-brief",
									"introduction",
									"letter",
									"meeting-report",
									"news",
									"obituary",
									"oration",
									"partial-retraction",
									"product-review",
									"protocol",
									"rapid-communication",
									"reply",
									"reprint",
									"research-article",
									"retraction",
									"review-article",
									"translation",
									"other",
								],
							},
							originaltype: {
								type: "string",
							},
							funders: {
								type: "array",
								items: {
									properties: {
										name: {
											type: "string",
										},
										ror: {
											type: ["string", "null"],
											pattern:
												"^https://ror\\.org/[0-9a-z]{9}$",
										},
										fundref: {
											type: "string",
										},
									},
								},
							},
							grants: {
								type: "array",
								items: {
									properties: {
										name: {
											type: "string",
										},
										id: {
											type: "string",
										},
									},
								},
							},
							acknowledgement: {
								type: "string",
							},
							doi: {
								type: "string",
							},
							submissionId: {
								type: "string",
							},
							manuscript: {
								type: "object",
								properties: {
									id: {
										type: "string",
									},
									dates: {
										type: "object",
										properties: {
											submission: {
												type: "string",
											},
											acceptance: {
												type: "string",
											},
											publication: {
												type: "string",
											},
										},
									},
								},
							},
							preprint: {
								type: "object",
								properties: {
									title: {
										type: "string",
									},
									url: {
										type: "string",
									},
									id: {
										type: "string",
									},
								},
							},
							vor: {
								type: "object",
								required: ["publication", "license"],
								properties: {
									publication: {
										type: "string",
										enum: [
											"pure OA journal",
											"hybrid journal",
											"transformative journal",
											"author has not decided yet",
											"other",
										],
									},
									license: {
										type: "string",
										enum: [
											"CC BY",
											"CC BY-ND",
											"CC BY-NC",
											"CC BY-NC-SA",
											"CC BY-NC-ND",
											"CC BY-IGO",
											"CC BY-not specified",
											"CC BY-other",
											"non-CC",
											"CC0",
											"not yet decided",
											"not specified",
										],
									},
									deposition: {
										type: "string",
										enum: [
											"open repository, like PMC",
											"not deposited by publisher",
										],
									},
									researchdata: {
										type: "string",
										enum: [
											"Yes",
											"No",
											"not applicable",
											"not yet decided",
											"author will take care",
											"data available on request",
											"other",
										],
									},
								},
							},
						},
					},
					journal: {
						type: "object",
						properties: {
							name: {
								type: "string",
							},
							id: {
								type: "string",
							},
							inDOAJ: {
								type: "boolean",
							},
							issn: {
								type: "string",
							},
							eissn: {
								type: "string",
							},
							typecomment: {
								type: "string",
							},
						},
						required: ["name", "id"],
					},
					charges: {
						type: "object",
						properties: {
							prioragreement: {
								type: "boolean",
							},
							agreement: {
								type: ["object", "null"],
								properties: {
									name: {
										type: ["string", "null"],
									},
									id: {
										type: ["string", "null"],
									},
								},
							},
							currency: {
								type: "string",
								enum: [
									"EUR",
									"USD",
									"GBP",
									"AUD",
									"CAD",
									"CHF",
									"CNY",
									"DKK",
									"JPY",
									"NOK",
									"NZD",
									"SEK",
								],
							},
							fees: {
								type: "object",
								properties: {
									apc: {
										type: "object",
										properties: {
											type: {
												type: "string",
												enum: [
													"per article APC list price",
													"per article type APC list price",
													"per article page APC list price",
													"other",
												],
											},
											name: {
												type: "string",
												enum: [
													"firm",
													"estimated",
													"unknown",
												],
											},
											amount: {
												type: "number",
											},
										},
									},
									extra: {
										type: "array",
										items: {
											properties: {
												type: {
													type: "string",
													enum: [
														"author funding from other sources",
														"color charges",
														"discount for author covering APC from grant",
														"discounts for central institutional agreements",
														"editorial discount",
														"invitation discount",
														"license choice",
														"marketing discount",
														"page charges",
														"peer review discount",
														"recurring author discount",
														"society discount",
														"speed of publication",
														"submission fee",
														"submission format",
														"subscription discount",
														"waiver",
														"other discount",
														"other charges",
													],
												},
												amount: {
													type: "number",
												},
											},
										},
									},
									total: {
										type: "object",
										properties: {
											name: {
												type: "string",
												enum: [
													"firm",
													"estimated",
													"unknown",
												],
											},
											amount: {
												type: "number",
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
};
