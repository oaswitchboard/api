module.exports = {
	schema: {
		id: "/p2",
		type: "object",
		required: ["header", "data"],
		properties: {
			header: {
				$ref: "/Header",
			},
			data: {
				type: "object",
				properties: {
					confirmation: {
						type: "object",
						properties: {
							prioragreement: {
								type: "boolean",
							},
							agreement: {
								type: "object",
								properties: {
									confirmed: {
										type: "boolean",
									},
									name: {
										type: "string",
									},
									id: {
										type: "string",
									},
								},
							},
							charges: {
								type: "object",
								properties: {
									confirmed: {
										type: "boolean",
									},
								},
							},
							remarks: {
								type: "string",
							},
						},
					},
				},
			},
		},
	},
};
