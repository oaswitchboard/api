module.exports = {
	header: {
		id: "/Header",
		type: "object",
		required: ["type", "to"],
		properties: {
			id: {
				type: "string",
			},
			version: {
				type: ["string", "null"],
				enum: ["v1", "v2", null],
			},
			type: {
				type: "string",
				enum: ["e1", "e2", "p1", "p2"],
			},
			e1: {
				type: "string",
			},
			e2: {
				type: "string",
			},
			p1: {
				type: "string",
			},
			p2: {
				type: "string",
			},
			overrides: {
				type: "object",
				properties: {
					id: {
						type: "integer",
					},
					type: {
						type: "string",
					},
				},
			},
			overridden: {
				type: "object",
				properties: {
					id: {
						type: "integer",
					},
					type: {
						type: "string",
					},
				},
			},
			from: {
				type: "object",
				properties: {
					participant: {
						type: "integer",
					},
					address: {
						type: ["string", "null"],
						pattern: "^https://ror\\.org/[0-9a-z]{9}$",
					},
					name: {
						type: "string",
					},
					type: {
						type: "string",
					},
					email: {
						type: "string",
					},
				},
			},
			to: {
				type: "object",
				required: ["address"],
				properties: {
					participant: {
						type: "integer",
					},
					address: {
						type: "string",
						pattern: "^https://ror\\.org/[0-9a-z]{9}$",
					},
					name: {
						type: "string",
					},
					type: {
						type: "string",
					},
					email: {
						type: "string",
					},
				},
			},
			validity: {
				type: "string",
			},
			persistent: {
				type: "boolean",
			},
			subject: {
				type: "string",
			},
			pio: {
				type: "boolean",
			},
			deduplicator: {
				type: "string",
			},
			ref: {
				type: "string",
			},
			meta: {
				type: "object",
				properties: {
					prioragreement: {
						type: "boolean",
					},
					source: {
						type: "string",
					},
					status: {
						type: "string",
					},
				},
			},
		},
	},
};
