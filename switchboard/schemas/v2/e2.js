module.exports = {
	schema: {
		id: "/e2",
		type: "object",
		required: ["header", "data"],
		properties: {
			header: {
				$ref: "/Header",
			},
			data: {
				type: "object",
				properties: {
					acceptance: {
						type: "object",
						properties: {
							invoicedetails: {
								type: "string",
							},
							conditions: {
								type: "string",
							},
							remarks: {
								type: "string",
							},
							validuntil: {
								type: "string",
							},
							po: {
								type: "string",
							},
							prioragreement: {
								type: "boolean",
							},
							agreement: {
								type: "object",
								required: ["accepts"],
								properties: {
									name: {
										type: "string",
									},
									id: {
										type: "string",
									},
									accepts: {
										enum: ["Yes", "No"],
									},
								},
							},
							currency: {
								type: "string",
								enum: [
									"EUR",
									"USD",
									"GBP",
									"AUD",
									"CAD",
									"CHF",
									"CNY",
									"DKK",
									"JPY",
									"NOK",
									"NZD",
									"SEK",
								],
							},
							fees: {
								type: "object",
								properties: {
									apc: {
										type: "object",
										required: ["accepts", "amount"],
										properties: {
											accepts: {
												enum: ["Yes", "No", "Partial"],
											},
											amount: {
												type: "number",
											},
										},
									},
									extra: {
										type: "array",
										items: {
											type: "object",
											required: [
												"accepts",
												"amount",
												"type",
											],
											properties: {
												type: {
													type: "string",
													enum: [
														"author funding from other sources",
														"color charges",
														"discount for author covering APC from grant",
														"discounts for central institutional agreements",
														"editorial discount",
														"invitation discount",
														"license choice",
														"marketing discount",
														"page charges",
														"peer review discount",
														"recurring author discount",
														"society discount",
														"speed of publication",
														"submission fee",
														"submission format",
														"subscription discount",
														"waiver",
														"other discount",
														"other charges",
													],
												},
												amount: {
													type: "number",
												},
												accepts: {
													enum: [
														"Yes",
														"No",
														"Partial",
													],
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
};
