//@ts-check
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const stage = process.env.STAGE;
const mark = require('markup-js');
const utils = require('./../libs/utils');
const errorhandler = require('./../libs/errorhandler');
const formatter = require('./../libs/formatter');
const Jsontableify = require('jsontableify');

const send = async (event) => {
    // console.log('email.success() --------------------------------------------------')
    // console.log('JSON.stringify', JSON.stringify(event));
    // event = require("../tests/mocks/events/email-success");
    try {
        let result = {
            success: 0,
            skipped: 0,
        }
        for (const record of event.Records) {
            const message = JSON.parse(record.Sns.Message);
            if (getNotificationType (message) === 'EMAIL') {
                const formatted = await formatMessage(message);
                const templateName = getTemplateName(message);
                const template = await getTemplate(templateName);
                await sendEmail(template, formatted);
                result.success++;
            } else {
                result.skipped++;
            }
        }
        return formatter.render(result);
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error sending success email');
    }
}

module.exports = { send };

function getTemplateName (message) {
    const { type } = message;
    const pa = isPriorAgreement(message);
    if (message.state === "AUTOREJECT") {
        return "Autoreject.html";
    }
    switch(type) {
        case "e1":
            return pa ? "MessageE1prioragreement.html" : "MessageE1noprioragreement.html";
        case "e2":
            return "MessageE2.html";
        case "p1":
                return isPIO (message) ?
                    "MessageP1pio.html" :
                    pa ?
                        "MessageP1prioragreement.html" : "MessageP1noprioragreement.html";
        case "p2":
            return "MessageP2.html";
    }
    return null;
}

function getTemplateSet (stage) {
    switch (stage) {
        case "DEV":
            return 'templates/development';
        case "ACC":
            return 'templates/sandbox';
        case "PROD":
            return 'templates/production';
        case "TEST":
            return 'templates/test';
        default:
            return 'templates/invalid';
    }
}

async function getTemplate (template) {
    return process.env.STAGE === 'TEST' ?
        await getTemplateFromMocks (template) :
        await getTemplateFromS3 (template)
}

async function getTemplateFromS3 ( template ) {
    const set = getTemplateSet(process.env.STAGE);
    const params = {
        Bucket: process.env.S3_BUCKET,
        Key: `${set}/${template}`
    };
    const file = await s3.getObject(params).promise();
    return file?.Body?.toString();
}

async function getTemplateFromMocks ( template ) {
    const fs = require('fs').promises;
    return await fs.readFile(`./tests/mocks/templates/${template}`, 'utf8');
}

function mergeWithTemplate (template, message) {
    const body = mark.up(template, message);
    const payload = {
        Destination: {
            ToAddresses: [
                message.to.email
            ],
            // BccAddresses: [
            //     'bccoas@appetence.nl',
            // ],
        },
        Message: {
            Subject: {
                Data: `${stage?.replace(/ACC/i, 'SANDBOX')}: ${message.subject}`,
                Charset: 'UTF-8'
            },
            Body : {
                Html: {
                    Data: body,
                    Charset: 'UTF-8'
                },
            },
        },
        Source: process.env.MAIL_FROM,
        ReplyToAddresses: [
            process.env.MAIL_REPLYTO
        ],
    };

    if (message.cc && message.cc.email) {
        payload.Destination.CcAddresses = [ ...message.cc.email, ];
    }

    return payload;
}

async function formatMessage (message) {

    const organisation = await findOrganisation (message);
    const participant = await findReceiver (message);
    const to = organisation && participant ? formatTo (organisation, participant) : null;
    const cc = participant ? formatCC (participant) : null;
    const from = message?.header?.from;
    const subject = getMessageSubject (message);
    const data = await getMessageData (message);
    const id = getMessageId (message);
    const type = message.type;
    const { e1, e2, p1, p2 } = message.header;
    const baseurl = process.env.BASE_URL;
    return { id, to, cc, from, subject, data, type, baseurl, e1, e2, p1, p2 };
}

function formatTo (organisation, participant) {
    const { name,  } = organisation;
    const { email, } = participant

    const emails =  email.split(/[\s,]+/);
    return {
        name,
        email: emails.shift(),
    }
}

function formatCC (participant) {
    const { email, } = participant;
    const emails =  email.split(/[\s,]+/);
    return {
        email: emails.length > 1 ? emails.slice(1, emails.length) : null,
    }
}

async function findReceiver (message) {
    const ParticipantService = require('./services/participants');
    const id = message?.header?.meta?.routing?.receiver;
    if (utils.isNumeric(id)) {
        return await ParticipantService.findById(id);
    }
    return null;
}

async function findOrganisation (message) {
    const OrganisationService = require('./services/organisations');
    const id = message?.organisation;
    if (utils.isNumeric(id)) {
        return await OrganisationService.findById(id);
    }
    return null;
}

async function getMessageData (message) {
    const { html } = new Jsontableify().toHtml(message.data);
    return html;
}

function getMessageId (message) {
    const { header } = message;
    return header[header.type] || 'unknown';
}

function getMessageSubject(message) {
    if (message.state && message.state === "AUTOREJECT") {
        return "OA Switchboard Eligibility Enquiry Auto-reject based on participant acceptance criteria";
    }
    switch(message.type) {
        case "e1":
            return "OA Switchboard Eligibility Enquiry message";
        case "e2":
            return "OA Switchboard Eligibility Enquiry response message";
        case "p1":
            if (isPIO(message)) {
                return "OA Switchboard Publication Notification message. Public information only";
            }
            return "OA Switchboard Publication Notification message";
        case "p2":
            return "OA Switchboard Publication Notification response message";
        default:
            return null;
    }
}

function isPriorAgreement(message) {
    switch(message.header.type) {
        case "e1":
            return message.data && message.data.charges && message.data.charges.prioragreement;
        case "e2":
            return message.data && message.data.acceptance && message.data.acceptance.prioragreement;
        case "p1":
            return message.data && message.data.charges && message.data.charges.prioragreement;
        case "p2":
            return message.data && message.data.confirmation && message.data.confirmation.prioragreement;
        default:
            return null;
    }
}

function getNotificationType (message) {
    return message?.header?.meta?.routing?.notification;
}

function isPIO (message) {
    return !! message.header?.pio;
}

async function sendEmail(template, message) {
    const params = mergeWithTemplate(template, message);
    if (process.env.STAGE !== 'TEST') {
        const ses = new AWS.SES();
        await ses.sendEmail(params).promise();
    }
}

