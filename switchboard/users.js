const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const utils = require('./../libs/utils');

module.exports = {

    authorize: async (event) => {
        const client = await pool.connect();
        const CognitoSDK = require('amazon-cognito-identity-js-node');
        AWS.CognitoIdentityServiceProvider.AuthenticationDetails = CognitoSDK.AuthenticationDetails;
        AWS.CognitoIdentityServiceProvider.CognitoUserPool = CognitoSDK.CognitoUserPool;
        AWS.CognitoIdentityServiceProvider.CognitoUser = CognitoSDK.CognitoUser;

        try {
            const authenticationData = extractParticipantCredentials(event.body);
            const authenticationDetails = new AWS.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
            const cognitoUser = createCognitoUser(authenticationDetails);
            const result = await authenticate (cognitoUser, authenticationDetails);
            const token = result.getIdToken().getJwtToken();
            const args = [ utils.security.getCognitoSub(token) ]
            const participants = await pool.query(`SELECT * FROM participants WHERE cognito_id = $1`, args);
            let participant = {};
            if (participants.rowCount === 1) {
                participant.id = participants.rows[0].id;
                participant.organisation = participants.rows[0].institution;
                participant.cognito_id = participants.rows[0].cognito_id;
                participant.email = participants.rows[0].email;
                participant.fullname = participants.rows[0].fullname;
                participant.type = participants.rows[0].type;
            }
            return formatter.render({
                token,
                participant,
            });
        } catch (error) {
            errorhandler.log(error, error);
            return formatter.error('Not authorized', 401);
        } finally {
            client.release()
        }
    },

    changePassword: async (event) => {
        const Cognito = new AWS.CognitoIdentityServiceProvider({ region: process.env.AWSREGION || 'eu-central-1' });

        try {
            const { password } = JSON.parse(event.body) || {};
            if (!password) {
                throw "Password is required"
            }
            await Cognito.adminSetUserPassword({
                Username: event.requestContext.authorizer.claims['cognito:username'],
                UserPoolId: process.env.COGNITO_POOL,
                Password: password,
                Permanent: true,
            }).promise();
            return formatter.render('Password successfully changed');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error updating user password');
            return formatter.error(error, 500);
        }
    }
};

function authenticate (cognitoUser, authenticationDetails) {
    return new Promise((resolve, reject) => {
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log(`Successful login: ${cognitoUser.username}`);
                resolve(result);
            },
            onFailure: function(err) {
                console.log(`Failed login: ${cognitoUser.username}`);
                reject({
                    message: 'Not authorized',
                    username: cognitoUser.username
                });
            },
        });
    });
}

function extractParticipantCredentials (event) {
    const parsed = JSON.parse(event);
    console.log('Authenticating --------------------------------------------------')
    console.log('E-mail', parsed['email']);
    return {
        Username: parsed['email'],
        Password: parsed['password'],
    };
}

function createCognitoUser (authenticationDetails) {
    const userData = {
        Username : authenticationDetails.username,
        Pool : createUserPool(),
    };
    return new AWS.CognitoIdentityServiceProvider.CognitoUser(userData);
}

function createUserPool() {
    const poolData = {
        UserPoolId: process.env.COGNITO_POOL,
        ClientId: process.env.COGNITO_CLIENT,
    };
    return new AWS.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
}
