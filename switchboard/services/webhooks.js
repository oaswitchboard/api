const pool = require('../../connection');
const errorhandler = require('./../../libs/errorhandler');

const findByParticipantId = async (id) => {
    const client = await pool.connect();
    try {
        const sql = `SELECT url, apikey FROM webhooks WHERE participant_id = $1`;
        const webhooks = await client.query(sql, [ id ]);
        return webhooks.rows;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding webhooks for a participant');
    }
}

const findByRouteId = async (id) => {
    const client = await pool.connect();
    try {
        const sql = `
            SELECT url, apikey FROM webhooks
            LEFT JOIN routes ON webhooks.id = routes.webhook_id
            WHERE routes.id = $1`;
        const webhooks = await client.query(sql, [ id ]);
        return webhooks.rows;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding webhooks for a route');
    }
}

const findById = async (id) => {
    const client = await pool.connect();
    try {
        const sql = `SELECT url, apikey FROM webhooks WHERE id = $1`;
        const webhooks = await client.query(sql, [ id ]);
        return webhooks.rows;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding webhooks for an id');
    }
}

module.exports = { findByParticipantId, findByRouteId, findById }