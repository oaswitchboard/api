const pool = require('../../connection');
const errorhandler = require('./../../libs/errorhandler');

module.exports = {

    search: async (searchstring, startrow, maxrows ) => {
        const client = await pool.connect();
        try {
            const args = [ searchstring ];
            let sql = `
                SELECT
                    routes.*,
                    organisations.name AS organisation,
                    participants.institution AS participant
                FROM routes
                LEFT JOIN organisations ON organisations.id = routes.organisation_id
                LEFT JOIN participants ON participants.id = routes.participant_id
                WHERE (
                    organisations.name ILIKE '%'  || $1 || '%'
                    OR participants.institution ILIKE '%'  || $1 || '%'
                ) AND routes.is_deleted = false::bool
                ORDER BY organisations.name ASC, participants.institution ASC`;
            let sqlcount = `
                SELECT count(routes.id) as total
                FROM routes
                LEFT JOIN organisations ON organisations.id = routes.organisation_id
                LEFT JOIN participants ON participants.id = routes.participant_id
                WHERE (
                    organisations.name ILIKE '%'  || $1 || '%'
                    OR participants.institution ILIKE '%'  || $1 || '%'
                ) AND routes.is_deleted = false::bool`;
            if (startrow && maxrows && utils.isNumeric(startrow) && utils.isNumeric(maxrows) && startrow >= 1 && maxrows <= 50)  {
                const offset = startrow - 1;
                sql += ` OFFSET ${offset} LIMIT ${maxrows}`;
            } else {
                sql += ` OFFSET 0 LIMIT 25`;
            }
            const routes =  await client.query(sql, args);
            const count =  await client.query(sqlcount, args);
            return {
                total: count.rows[0].total,
                rows: routes.rows
            };
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving routes');
        } finally {
            client.release();
        }
    },

    findById: async (id) => {
        const client = await pool.connect();
        try {
            const args = [ id ];
            const query = `SELECT routes.* FROM routes WHERE routes.id = $1 AND routes.is_deleted = false::bool`;
            const routes = await client.query(query, args);
            return routes.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding routes for an organisation');
        } finally {
            client.release();
        }
    },

    findByParticipantId: async (id) => {
        const client = await pool.connect();
        try {
            const args = [ id ];
            const query = `
                SELECT
                    routes.id::int,
                    routes.participant_id::int,
                    routes.organisation_id::int,
                    routes.condition,
                    routes.formatter,
                    routes.action,
                    routes.notification,
                    organisations.name AS receiver,
                    organisations.ror_id AS ror,
                    participants.institution AS participant,
                    participants.email,
                    participants.type,
                    participants.persistent,
                    webhooks.url,
                    webhooks.apikey
                FROM routes
                LEFT JOIN participants ON routes.participant_id = participants.id
                LEFT JOIN organisations ON routes.organisation_id = organisations.id
                LEFT JOIN webhooks ON routes.webhook_id = webhooks.id
                WHERE routes.participant_id = $1 AND routes.is_deleted = false::bool`;
            const routes = await client.query(query, args);
            return routes.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding routes for an organisation');
        } finally {
            client.release();
        }
    },

    findByOrganisationId: async (id) => {
        const client = await pool.connect();
        try {
            const args = [ id ];
            const query = `
                SELECT
                    routes.id::int,
                    routes.participant_id::int,
                    routes.organisation_id::int,
                    routes.condition,
                    routes.formatter,
                    routes.action,
                    routes.notification,
                    organisations.name AS receiver,
                    organisations.ror_id AS ror,
                    participants.institution AS participant,
                    participants.email,
                    participants.type,
                    participants.persistent,
                    webhooks.url,
                    webhooks.apikey
                FROM routes
                LEFT JOIN participants ON routes.participant_id = participants.id
                LEFT JOIN organisations ON routes.organisation_id = organisations.id
                LEFT JOIN webhooks ON routes.webhook_id = webhooks.id
                WHERE routes.organisation_id = $1 AND routes.is_deleted = false::bool`;
            const routes = await client.query(query, args);
            return routes.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding routes for an organisation');
        } finally {
            client.release();
        }
    },

    findByOrganisationRoR: async (ror) => {
        const client = await pool.connect();
        try {
            const args = [ ror ];
            const query = `
                SELECT
                    routes.id::int,
                    routes.participant_id::int,
                    routes.organisation_id::int,
                    routes.condition,
                    routes.formatter,
                    routes.action,
                    routes.notification,
                    organisations.name AS receiver,
                    organisations.ror_id AS ror,
                    participants.institution AS participant,
                    participants.email,
                    participants.type,
                    participants.persistent,
                    webhooks.url,
                    webhooks.apikey
                FROM routes
                LEFT JOIN participants ON routes.participant_id = participants.id
                LEFT JOIN organisations ON routes.organisation_id = organisations.id
                LEFT JOIN webhooks ON routes.webhook_id = webhooks.id
                WHERE organisations.ror_id = $1 AND routes.is_deleted = false::bool`;
            const routes = await client.query(query, args);
            return routes.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding routes for an organisation');
        } finally {
            client.release();
        }
    },

    create: async (organisation_id, participant_id, action='DEFAULT', notification="EMAIL") => {
        const client = await pool.connect();
        try {
            const created = new Date();
            const args = [ organisation_id, participant_id, action, notification, created ];
            const query = `INSERT INTO routes (
                organisation_id, participant_id, action, notification, created_at
            ) VALUES ($1, $2, $3, $4, $5)`;
            await client.query(query, args);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding routes for an organisation');
        } finally {
            client.release();
        }
    },


}

