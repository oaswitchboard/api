//@ts-check
const pool = require('../../connection');
const errorhandler = require('./../../libs/errorhandler');
const utils = require('./../../libs/utils');

const findById = async (id) => {
    const client = await pool.connect();
    try {
        if (id && utils.isNumeric(id)) {
            const args = [ id ];
            const query = `SELECT * FROM organisations WHERE id = $1`;
            const organisations = await client.query(query, args);
            if (organisations.rows.length === 1) {
                return organisations.rows[0];
            }
        }
        return null;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding participant by a secondary identifier');
    } finally {
        client.release();
    }
}

const findByPrimaryIdentifier = async (address) => {
    const client = await pool.connect();
    try {
        if (address && address.length) {
            const args = [ address ];
            const query = `SELECT * FROM organisations WHERE TRIM(ror_id) = $1`;
            const organisations = await client.query(query, args);
            if (organisations.rows.length === 1) {
                return organisations.rows[0];
            }
        }
        return null;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding participant by a secondary identifier');
    } finally {
        client.release();
    }
}

const findBySecondaryIdentifier = async (address) => {
    const client = await pool.connect();
    try {
        if (address && address.length) {
            const args = [ address ];
            const query = `SELECT * FROM organisations WHERE TRIM(ringgold_id) = $1 OR TRIM(fundref) = $1`;
            const organisations = await client.query(query, args);
            if (organisations.rows.length === 1) {
                return organisations.rows[0];
            }
        }
        return null;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error finding participant by a secondary identifier');
    } finally {
        client.release();
    }
}

const createFromParticipant = async (participant) => {
    const client = await pool.connect();
    try {
        const { institution, ror, id, } = participant;
        const args = [ institution, ror, id  ];
        const sql = `INSERT INTO organisations (name, ror_id, participant_id) VALUES ($1, $2, $3) RETURNING id`;
        return await client.query(sql, args);
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error saving Organisation');
        return [];
    } finally {
        client.release();
    }
}

const createOrganizationByFunder = async (funder) => {
    const ror = require('./../../libs/ror');
    try {
        if(funder.ror) {
            const existing = await selectOrganisationByRor(funder.ror)
            if (existing?.rowCount === 0) {
                const details = await ror.getOrganizationInfoByRor(funder.ror);
                if (details) {
                    const {ror_id, name, fundRef} = details;
                    if (ror_id && name) {
                        await insertOrganisation(ror_id, name, fundRef)
                    }
                }
            }
        }
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error creating organizations by founders');
    }
}

const selectOrganisationByRor = async (ror) => {
    const client = await pool.connect();
    try {
        const sql = "SELECT * FROM organisations WHERE ror_id = $1";
        return await client.query(sql, [ ror ]);
    } catch (error) {
        console.log(error);
    } finally {
        client.release();
    }
}

const insertOrganisation = async (ror_id, name, fundRef) => {
    const client = await pool.connect();
    try {
        const sql = `INSERT INTO organisations (name, ror_id, fundref) VALUES($1, $2, $3)`;
        const args = [name, ror_id, fundRef];
        await client.query(sql, args);
    } catch (error) {
        console.log(error);
    } finally {
        client.release();
    }
  }

module.exports = {
    findById,
    findByPrimaryIdentifier,
    findBySecondaryIdentifier,
    createFromParticipant,
    createOrganizationByFunder,
    selectOrganisationByRor,
    insertOrganisation,
}


