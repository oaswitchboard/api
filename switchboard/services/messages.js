const pool = require("../../connection");
const errorhandler = require("./../../libs/errorhandler");
const utils = require("./../../libs/utils");

const findMessageIdFromEvent = (event) => {
	if (event.pathParameters && event.pathParameters.id && utils.isNumeric(event.pathParameters.id)) {
		return event.pathParameters.id;
	}
	return 0;
};

const findMessageTypeFromEvent = (event) => {
	if (event.pathParameters && event.pathParameters.type) {
		return event.pathParameters.type;
	}
	return null;
};

const findMessageStringIdFromEvent = (event) => {
	if (event.pathParameters && event.pathParameters.id) {
		return event.pathParameters.id.toString();
	}
	return null;
};

const exists = async (hash) => {
	const client = await pool.connect();
	try {
		const args = [hash];
		const sql = `SELECT * FROM messages WHERE hash = $1`;
		const existing = await client.query(sql, args);
		return existing.rowCount;
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding if message already exists");
	} finally {
		client.release();
	}
};

const findByIdAndParticipantId = async (id, participant_id) => {
	const client = await pool.connect();
	try {
		const args = [id, participant_id];
		const sql = "SELECT * FROM messages WHERE id = $1 AND (sender = $2 OR receiver = $2)";
		const messages = await client.query(sql, args);
		return messages.rowCount ? messages.rows[0] : null;
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding if message by id");
	} finally {
		client.release();
	}
};

const findByRefAndCognitoId = async (ref, cognito_id) => {
	const client = await pool.connect();
	try {
		const args = [ref, cognito_id];
		let sql = `
            SELECT messages.*, senders.institution AS sendfrom, organisations.name AS organisation, receivers.institution AS receivedby
            FROM messages
            LEFT JOIN participants AS senders ON messages.sender = senders.id
            LEFT JOIN participants AS receivers ON messages.receiver = receivers.id
            LEFT JOIN organisations ON messages.organisation = organisations.id
            WHERE messages.header ->> 'ref'::text = $1
            AND senders.cognito_id = $2
        `;
		return await client.query(sql, args);
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding if message by ref");
	} finally {
		client.release();
	}
};

const findByHashAndParticipantId = async (message_hash, message_type, participant_id) => {
	const client = await pool.connect();
	try {
		const args = [message_hash, message_type, participant_id];
		const sql = `
            SELECT messages.*, participants.institution
            FROM messages, participants
            WHERE (messages.sender = participants.id OR messages.receiver = participants.id)
            AND messages.header ->> '${message_type}'::text   = $1
            AND messages.type = $2
            AND participants.id = $3;`;
		const messages = await client.query(sql, args);
		return messages.rowCount ? messages.rows[0] : null;
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding if message by id");
	} finally {
		client.release();
	}
};

const findRelatedToId = async (id, participant_id) => {
	const client = await pool.connect();
	try {
		const args = [id, participant_id];
		const sql = `
            WITH target_message AS (
                SELECT
                    header ->> 'e1' AS e1,
                    header ->> 'e2' AS e2,
                    header ->> 'p1' AS p1,
                    header ->> 'p2' AS p2,
                    header -> 'meta' ->> 'group' AS batch
                FROM messages
                WHERE id = $1
                AND (sender = $2 OR receiver = $2)
            )
            SELECT m.*,
            senders.institution AS sendfrom,
            organisations.name AS organisation,
            receivers.institution AS receivedby

            FROM messages m
            INNER JOIN participants AS senders ON m.sender = senders.id
            INNER JOIN participants AS receivers ON m.receiver = receivers.id
            LEFT JOIN organisations ON m.organisation = organisations.id

            WHERE m.id != $1
            AND m.header -> 'meta' ->> 'group' != (SELECT batch FROM target_message)
            AND (
                m.header ->> 'e1' = (SELECT e1 FROM target_message)
                OR m.header ->> 'e2' = (SELECT e2 FROM target_message)
                OR m.header ->> 'p1' = (SELECT p1 FROM target_message)
                OR m.header ->> 'p2' = (SELECT p2 FROM target_message)
            )
            ORDER BY m.id DESC;
       `;
		const messages = await client.query(sql, args);
		return messages.rowCount ? messages.rows : [];
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding if message by id");
	} finally {
		client.release();
	}
};

const save = async (header, data, type, sender, receiver, state, organisation, hash, destination) => {
	const client = await pool.connect();
	try {
		const args = [header, data, type, sender, receiver, state, organisation, hash];
		const sql = `INSERT INTO ${destination} (header, data, type, sender, receiver, state, organisation, hash) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *`;
		const inserted = await client.query(sql, args);
		return inserted.rows[0];
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error persisting a message");
	} finally {
		client.release();
	}
};

const findOriginal = async (id, type) => {
	const client = await pool.connect();
	try {
		return await client.query(
			`SELECT * FROM messages WHERE messages.header ->> '${type}' = '${id}' AND messages.state = 'SENT'`
		);
	} catch (error) {
		errorhandler.log(error, "[OASB API] Error finding original to override");
	} finally {
		client.release();
	}
};

const getRelations = async (message, participant) => {
	const related = await findRelatedToId(message.id, participant.id);
	return related.map((item) => ({
		type: item.header.type,
		state: item.header.state,
	}));
};

const cleanHeader = (message) => {
	if (!message || !message.header) {
		return message;
	}
	const cleanedMessage = JSON.parse(JSON.stringify(message));
	const header = cleanedMessage.header;
	if (header.from && header.from.email) {
		delete header.from.email;
	}
	if (header.to && header.to.email) {
		delete header.to.email;
	}
	return cleanedMessage;
};

const cleanNonPersistentData = (message, participant) => {
	if (participant.id === message.header.from.id && message.header.from.persistent === false) {
		message.data = {};
	}
	return message;
};

module.exports = {
	findMessageIdFromEvent,
	findMessageTypeFromEvent,
	exists,
	save,
	findOriginal,
	findByIdAndParticipantId,
	findByHashAndParticipantId,
	findByRefAndCognitoId,
	findMessageStringIdFromEvent,
	findRelatedToId,
	getRelations,
	cleanHeader,
	cleanNonPersistentData,
};
