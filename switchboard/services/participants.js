const pool = require('../../connection');
const errorhandler = require('./../../libs/errorhandler');

module.exports = {

    findSubFromAPIGatewayEvent: (event) => {
        return event.requestContext.authorizer.claims.sub;
    },

    findSubFromSQSEvent: (event) => {
        return JSON.parse(event.messageAttributes.Sender.stringValue);
    },

    findByCognitoId: async (id) => {
        const client = await pool.connect();
        try {
            const args = [ id ];
            const sql = `SELECT
                id,
                email,
                ror,
                type,
                webhook,
                callback,
                apikey,
                persistent,
                oafund,
                invoice,
                rules,
                pio,
                institution,
                institution AS name
            FROM participants WHERE participants.cognito_id = $1`;
            const participants = await client.query(sql, args);
            if (participants.rows) {
                return participants.rows[0];
            }
            return null;
        } catch (error) {
            errorhandler.log(error, '[OASB API][PARTICIPANT] Error retrieving participant');
            throw error;
        } finally {
            client.release();
        }
    },

    findById: async (id) => {
        const client = await pool.connect();
        try {
            const args = [ id ];
            const query = `SELECT
                id,
                cognito_id,
                email,
                ror,
                type,
                webhook,
                callback,
                apikey,
                persistent,
                oafund,
                invoice,
                rules,
                pio,
                institution,
                institution AS name
            FROM participants WHERE participants.id = $1`;
            const participants = await client.query(query, args);
            if (participants.rows) {
                return participants.rows[0];
            }
            return null;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error finding Participant by Id');
        } finally {
            client.release();
        }
    },

    create: async (datacollection) => {
        console.log('Admin.persistParticipant() -----------------------------------------------');
        const client = await pool.connect();
        try {
            const { cognito_id, email, institution, fullname, type, ror, webhook, persistent } = datacollection;
            const args = [ cognito_id, email, institution, fullname, type, ror, webhook, persistent ];
            const sql = `INSERT INTO participants (cognito_id, email, institution, fullname, type, ror, webhook, persistent) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING type, id, institution, ror`;
            return await client.query(sql, args);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error saving Participant');
            return [];
        } finally {
            client.release();
        }
    }


}