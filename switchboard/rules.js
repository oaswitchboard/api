const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const sns = new AWS.SNS();
const utils = require('./../libs/utils');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const moment = require('moment');

module.exports = {

    reject: async (event) => {
        try {
           const original = getMessage(event.Records[0].Sns.Message);
            const message = {
                header: createHeader (original),
                data: createData(original),
            };

            /* Forwarding message to the subscribed delivery channels */
            if (message.header.from && message.header.to) {
                return await sns.publish ({
                    Message: JSON.stringify(message),
                    Subject: "Auto reject based on participant acceptance criteria",
                    TopicArn: process.env.SNS_SUCCESS
                }).promise();
            }

            /* Set message as undeliverable */
            return await sns.publish({
                Message: JSON.stringify(message),
                Subject: "Undeliverable message",
                TopicArn: process.env.SNS_FAILURE
            }).promise();

        } catch (error) {
            errorhandler.log(error, '[OASB API] Error reject a E1 message');
            return formatter.render(error, 500);
        }
    },
};


function createData (original) {
    const charges = JSON.parse(original.data).charges;
    const data = {
        acceptance: {
            prioragreement: charges.prioragreement,
        },
    };
    if (charges.agreement) {
        data.acceptance.agreement = {
            accepts: false,
            name: charges.agreement.name,
            id: charges.agreement.id,
            remarks: "Auto reject based on participant acceptance criteria"
        }
    }
    if (charges.apc) {
        data.acceptance.charges = {
            apc: {
                accepts: "No",
                amount: charges.apc.total.apc.amount || 'Unknown',
            },
            extra: {
                accepts: "No",
                amount: charges.apc.total.fees.amount || 'Unknown',
            },
            remarks: "Auto reject based on participant acceptance criteria"
        }
    }
    return data;
}

function createHeader (original) {
    return {
        id: utils.uuid(),
        type: "e2",
        "e1": original.e1,
        "e2": `${moment().format('YYYYMMDDHHmmss')}-${utils.uuid()}`,
        to: original.from,
        from: original.to,
        subject: 'OA Switchboard Eligibility Enquiry response: Auto rejection based on participant acceptance criteria',
        persistent: original.persistent,
        validity: original.validity,
        template:"templates/Autoreject.html",
        state: "AUTOREJECT",
    }
}

function getMessage (notification) {
    const message = JSON.parse(notification);
    const header = message.header;
    let data = "";
    if (message.data) {
        data = JSON.stringify(message.data);
    }
    const { to, from, subject, persistent, validity, e1 } = header;
    const template = 'templates/Message.html';
    return { to, from, subject, data, template, e1, persistent, validity };
}


