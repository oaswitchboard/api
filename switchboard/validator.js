const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const version = process.env.VERSION;
const allowedMessageTypes = ['e1', 'e2', 'p1', 'p2'];

const schema = async (event) => {
    try {
        if (allowedMessageTypes.includes(event.pathParameters.id)) {
            const schema = require(`${process.cwd()}/switchboard/schemas/${process.env.VERSION}/${event.pathParameters.id}`).schema;
            return formatter.render(schema);
        }
        return formatter.error('Schema not found', 404);
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error retrieving schema');
        return formatter.error('Error retrieving schema', 500);
    }
};

module.exports = {
    schema,
}    