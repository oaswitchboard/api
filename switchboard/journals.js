const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const utils = require('./../libs/utils');
// const convert = require('./../libs/convert');

module.exports = {

    /* list () - returns a list of journals */

    search: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.body.query ];
            const query = `SELECT
                journals.id,
                journals.title,
                journals.issn_electronic,
                participants.institution AS publisher
                FROM journals LEFT OUTER JOIN participants ON participants.id = journals.participant_id
                WHERE (
                        journals.title ILIKE '%'  || $1 || '%'
                    OR journals.issn_electronic ILIKE '%'  || $1 || '%'
                    OR journals.issn_print ILIKE '%'  || $1 || '%'
                    OR participants.institution ILIKE '%'  || $1 || '%'
                )
                LIMIT 25`;
            const sqlcount = `SELECT
                count(journals.id) as total
                FROM journals LEFT OUTER JOIN participants ON participants.id = journals.participant_id
                WHERE (
                    journals.title ILIKE '%'  || $1 || '%'
                OR journals.issn_electronic ILIKE '%'  || $1 || '%'
                OR journals.issn_print ILIKE '%'  || $1 || '%'
                OR participants.institution ILIKE '%'  || $1 || '%'
            )`;
            const journals =  await client.query(query, arguments);
            const count =  await client.query(sqlcount, arguments);
            return {
                total: count.rows[0].total,
                rows: journals.rows
            };
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving journals');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* list () - returns a list of journals */

    publisher: async (event) => {
        if (!event.path.id || !utils.isNumeric(event.path.id)) {
            return [];
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.path.id ];
            const query = `SELECT
                journals.id,
                CASE
                    WHEN journals.partner IS NULL THEN
                        journals.title
                    ELSE
                    journals.partner::text ||  ' - ' || journals.title
                END AS title,
                journals.issn_electronic AS issn,
                participants.institution AS publisher
                FROM journals LEFT OUTER JOIN participants ON participants.id = journals.participant_id
                WHERE participant_id = $1
                ORDER BY title ASC`;
            const journals =  await client.query(query, arguments);
            return journals.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving journals per publisher');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* detail () - returns a single journals */

    detail: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        if (!event.path.id || !utils.isNumeric(event.path.id)) {
            return formatter.error('No journals found', 404);
        }
        const client = await pool.connect();
        try {
            const arguments = [ event.path.id ];
            const query = `SELECT
                journals.*,
                participants.institution AS publisher
                FROM journals LEFT OUTER JOIN participants ON participants.id = journals.participant_id
                WHERE journals.id = $1`;
            const journals =  await client.query(query, arguments);
            if (journals.rows.length === 1) {
                return journals.rows[0];
            }
            return formatter.error('No journals found', 404);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving journal');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* issn () - returns a single journals */

    issn: async (event) => {
        const client = await pool.connect();
        try {
            if (!event.path.issn) {
                return formatter.error('No journal found', 404);
            }
            const arguments = [ event.path.issn ];
            const query = `SELECT
                journals.*,
                participants.institution AS publisher
                FROM journals LEFT OUTER JOIN participants ON participants.id = journals.participant_id
                WHERE journals.issn_electronic = $1 OR journals.issn_print = $1`;
            const journals =  await client.query(query, arguments);
            if (journals.rows.length === 1) {
                return journals.rows[0];
                //  return convert.mapAccessType(journals.rows[0]);
            }
            return formatter.error('No journals found', 404);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving journal by ISSN');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* create () - create a single journal */

    create: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        const client = await pool.connect();
        try {
            const participant_id = event.body.participant_id || 0;
            const title = event.body.title || '';
            const issn_print = event.body.issn_print || '';
            const issn_electronic = event.body.issn_electronic || '';
            const inDOAJ = event.body.inDOAJ || false;
            const crossref = event.body.crossref || '';
            const type = event.body.type || '';
            const deposition = event.body.deposition || '';
            const researchdata = event.body.researchdata || '';
            const license = event.body.license || '';
            const journal_id = event.body.journal_id || '';

            const arguments = [ participant_id, title, issn_print, issn_electronic, inDOAJ, crossref, type, deposition, researchdata, license, journal_id ];
            const query = `
                INSERT INTO journals
                (participant_id, title, issn_print, issn_electronic, "inDOAJ", crossref, type, deposition, researchdata, license, journal_id)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
            `;
            await client.query(query, arguments);
            return formatter.render('Journal inserted');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error inserting journal');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* update () - update a single journal */

    update: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        if (!event.path.id) {
            return formatter.error('No journals found', 404);
        }
        const client = await pool.connect();
        try {
            const participant_id = event.body.participant_id || 0;
            const title = event.body.title || '';
            const issn_print = event.body.issn_print || '';
            const issn_electronic = event.body.issn_electronic || '';
            const inDOAJ = event.body.inDOAJ || false;
            const crossref = event.body.crossref || '';
            const type = event.body.type || '';
            const deposition = event.body.deposition || '';
            const researchdata = event.body.researchdata || '';
            const license = event.body.license || '';
            const journal_id = event.body.journal_id || '';
            const arguments = [ event.path.id, participant_id, title, issn_print, issn_electronic, inDOAJ, crossref, type, deposition, researchdata, license, journal_id ];
            const query = `UPDATE journals SET
                    participant_id = $2,
                    title = $3,
                    issn_print = $4,
                    issn_electronic = $5,
                    "inDOAJ" = $6,
                    crossref = $7,
                    type = $8,
                    deposition = $9,
                    researchdata = $10,
                    license = $11,
                    journal_id = $12
                WHERE journals.id = $1`;
            await client.query(query, arguments);
            return formatter.render('Journal updated');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error updating journal');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
    },

    /* suggester () - suggester per journal */

    suggester: async (event) => {
        const client = await pool.connect();
        try {
            const journal = event.body.data.journal;
            const arguments = [ journal.id ];
            const query = `SELECT
                    participants.id AS id,
                    participants.ror AS ror,
                    participants.institution AS publisher,
                    journals.title AS title
                    FROM participants, journals
                    WHERE participants.id = journals.participant_id
                    AND (issn_print = $1 OR issn_electronic = $1 OR journal_id = $1)`;
            const journals =  await client.query(query, arguments);
            if (journals.rows.length) {
                return journals.rows[0];
            }
            return null;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error suggesting journal');
            return formatter.render(error, 500);
        } finally {
            client.release();
        }
     },

};
