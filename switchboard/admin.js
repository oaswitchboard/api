const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const CognitoSDK = require('amazon-cognito-identity-js-node');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const logger = require('./../libs/logger');
const fields = ['cognito_id', 'fullname', 'email', 'ror', 'ror_id', 'pubsweet_id', 'institution', 'type', 'persistent', 'webhook', 'password'];
const utils = require('./../libs/utils');

/// OASB Data services
const Participant = require('./../switchboard/services/participants');
const Organisation = require('./../switchboard/services/organisations');
const Route = require('./../switchboard/services/routes');

module.exports = {

    /* register () - registers new participant */

    register: async (event) => {
        try {
            AWS.CognitoIdentityServiceProvider.AuthenticationDetails = CognitoSDK.AuthenticationDetails;
            AWS.CognitoIdentityServiceProvider.CognitoUserPool = CognitoSDK.CognitoUserPool;
            AWS.CognitoIdentityServiceProvider.CognitoUser = CognitoSDK.CognitoUser;

            const parameters = {
                DEV: {
                    names: ['email', 'name', 'custom:ror', 'custom:pubsweet_id', 'custom:institution', 'custom:type', 'custom:email', 'custom:webhook', 'custom:persistent' ],
                    values: ['email', 'fullname', 'ror', 'pubsweet_id', 'institution', 'type', 'email', 'webhook', 'persistent'],
                    stringify: [false, false, false, false, false, false, false, false, true ] ,
                },
                ACC: {
                    names: ['email', 'name', 'custom:custom:ror_id', 'custom:custom:pubsweet_id', 'custom:custom:institution', 'custom:custom:type', 'custom:custom:email', 'custom:custom:webhook', 'custom:custom:persistent' ],
                    values: ['email', 'fullname', 'ror', 'pubsweet_id', 'institution', 'type', 'email', 'webhook', 'persistent'],
                    stringify: [false, false, false, false, false, false, false, false, true ] ,
                },
                PROD: {
                    names: ['email', 'name', 'custom:ror', 'custom:pubsweet_id', 'custom:institution', 'custom:type', 'custom:email', 'custom:webhook', 'custom:persistent' ],
                    values: ['email', 'fullname', 'ror', 'pubsweet_id', 'institution', 'type', 'email', 'webhook', 'persistent'],
                    stringify: [false, false, false, false, false, false, false, false, true ] ,
                },
            }

            const attributes = extractParticipantAttributes(event.body);
            const poolData = {
                UserPoolId: process.env.COGNITO_POOL,
                ClientId: process.env.COGNITO_CLIENT,
            };

            const attributeList = [];
            parameters[process.env.STAGE].names.forEach((name, index) => {
                const value = attributes[parameters[process.env.STAGE].values[index]];
                const stringify = parameters[process.env.STAGE].stringify[index];
                attributeList.push({
                    Name: name,
                    Value: stringify ? JSON.stringify(value) : value,
                });
            });

            const userPool = new AWS.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
            return await signup(userPool, attributes.email, attributes.password, attributeList);

        } catch (error) {
            errorhandler.log(error, '[OASB API] Error registering participant');
            return formatter.render(error, 500);
        }
    },

    /* confirm () - registers new participant */

    confirm: (event, context, callback) => {
        event.response.autoConfirmUser = true;
        event.response.autoVerifyEmail = true;
        callback(null, event);
    },


    /* save () - saves new participant in Postgres in after registration Cognito hook */

    save: async (event, context) => {
        try {
            await persist(extractParticipantAttributesForDatabase(event));
            context.done(null, event);
        } catch (error) {
            context.fail(error, event);
        }
    },

    /* access () - access logs */

    access: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }

        try {
            let logevents = [];
            const logGroupName = `/aws/api-gateway/oasb-switchboard-${process.env.STAGE.toLowerCase()}`;
            const streams = await logger.getLogsStreams(logGroupName);
            for (const stream of streams.logStreams) {
                const events = await logger.getLogsStreamEvents(logGroupName, stream);
                logevents = [ ...logevents, ...events.events ];
            }
            logevents.sort((a, b) => (a.timestamp < b.timestamp) ? 1 : -1);
            return logevents.map(event => event.message).slice(0, 50);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving access logs');
            return formatter.render(error, 500);
        }
    },

    /* audit () - audit access trail */

    audit: async (event) => {

        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        try {
            let logevents = [];
            const logGroupName = `/aws/lambda/oasb-switchboard-${process.env.STAGE.toLowerCase()}-messages-save`;
            const streams = await logger.getLogsStreams(logGroupName);
            for (const stream of streams.logStreams) {
                const events = await logger.getLogsStreamEvents(logGroupName, stream);
                logevents = [ ...logevents, ...events.events ];
            }
            logevents.sort((a, b) => (a.timestamp < b.timestamp) ? 1 : -1);
            return logevents.map(event => {
                const { timestamp, message } = event;
                return { timestamp, message };
            }).slice(0, 50);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving audit logs');
            return formatter.render(error, 500);
        }
    },

    /* access () - access logs */

    logs: async (event) => {
        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        try {
            return await logger.getLogsGroups();
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving logs');
            return formatter.render(error, 500);
        }
    },

    log: async (event) => {
        if(!utils.security.isAdministrator(event.headers.Authorization)) {
            return formatter.error('Unauthorised', 401);
        }
        try {
            const streams = await logger.getLogsStreams();
            for (const stream of streams.logStreams) {
                const events = await logger.getLogsStreamEvents(stream);
                logevents = [ ...logevents, ...events.events ];
            }
            logevents.sort((a, b) => (a.timestamp < b.timestamp) ? 1 : -1);
            return logevents.map(event => event.message).slice(0, 50);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving logs');
            return formatter.render(error, 500);
        }
    },

};

function signup(userPool, email, password, attribute_list) {
    return new Promise((resolve, reject) => {
      userPool.signUp(email, password, attribute_list, null, (err, result) => {
          if (err) {
            console.log(err.message);
            reject(err);
            return;
          }
          resolve(result.user);
    });
  });
}

function extractParticipantAttributes (user) {
    const attributes = {};
    Object.keys(user).forEach(key => {
        if (fields.includes(key)) {
            attributes[key] = user[key];
        }
    });
    // Defaults
    attributes.webhook = 'false';
    attributes.pubsweet_id = '0000000';
    attributes.fullname = attributes.institution || '';
    return attributes;
}

async function persist (attributes) {
    try {
        const participants = await Participant.create(attributes);
        for (const participant of participants.rows) {
            const organisation = await Organisation.createFromParticipant(participant);
            if (organisation.rows.length === 1) {
                const organisation_id = organisation.rows[0].id;
                await Route.create(organisation_id, participant.id);
            }
        }
        return participants.rows;
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error persisting participants');
        return formatter.error(error, 500);
    }
}


function extractParticipantAttributesForDatabase (event) {
    const attributes = {};
    const user = event.request.userAttributes;
    Object.keys(user).forEach(key => {
        if (fields.includes(key)) {
            attributes[key] = user[key];
        }
        if (key === 'sub') {
            attributes['cognito_id'] = user[key];
        }
        if (key === 'name') {
            attributes['fullname'] = user['name'];
            attributes['institution'] = user['name'];
        }
        if (key.indexOf('custom') > -1 )  {
            const customkey = key.split(":").pop();
            attributes[customkey] = user[key];
        }
    })
    return attributes;
}

