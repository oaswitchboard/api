const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const ParticipantService = require('./services/participants');

module.exports = routes = {

    list: async (event) => {
        const client = await pool.connect();
        try {
            const arguments = [ParticipantService.findSubFromAPIGatewayEvent(event)];
            let sql = `
                SELECT
                    routes.*,
                    organisations.name AS organisation,
                    participants.institution AS participant
                FROM routes
                    LEFT JOIN organisations ON organisations.id = routes.organisation_id
                    LEFT JOIN participants ON participants.id = routes.participant_id
                WHERE routes.is_deleted = false::bool
                    AND participants.cognito_id = $1
                ORDER BY organisations.name ASC, participants.institution ASC`;
            const routes = await client.query(sql, arguments);
            return formatter.render(routes.rows);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving routes for participant');
            return formatter.error(error, 500);
        } finally {
            client.release();
        }
    }

};