const pool = require('./../connection');
const formatter = require('./../libs/formatter');
const errorhandler = require('./../libs/errorhandler');
const utils = require('./../libs/utils');

module.exports = {
    publishers: async () => {
        const client = await pool.connect();
        try {
            const participants = await client.query(`
                SELECT participants.institution, participants.id
                FROM participants
                WHERE participants.type = 'publisher'
                ORDER BY institution ASC
            `);
            return participants.rows;
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving a list of participants');
            return formatter.error(error, 500);
        } finally {
            client.release();
        }
    },

    feedbackEmail: async (event) => {
        const client = await pool.connect();
        const arguments = [event.path.id]
        try {
            const participants = await client.query(`
                SELECT feedbackemail from participants where participants.id = $1
            `, arguments);
            console.log(participants.rows[0]);
            return {
                feedbackemail: participants.rows[0]?.feedbackemail || 'support@oaswitchboard.org'
            };
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving a list of participants');
            return formatter.error(error, 500);
        } finally {
            client.release();
        }
    },

    detail: async (event) => {
        const client = await pool.connect();
        try {
            const cognito_id = event.requestContext.authorizer.claims.sub;
            const participants = await client.query(`SELECT * FROM participants WHERE participants.cognito_id = '${cognito_id}'`);
            if (participants.rows.length === 1) {
                const { oafund } = participants.rows[0];
                return formatter.render({ oafund });
            }
            return formatter.render('Participant not found', 404);
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error retrieving user details');
            return formatter.render(error, 500);
        } finally {
            client.release()
        }
    },

    update: async (event) => {
        const client = await pool.connect();
        try {
            const cognito_id = event.requestContext.authorizer.claims.sub;
            const { oafund } = JSON.parse(event.body) || {};
            const arguments = [cognito_id, oafund];
            await client.query(`UPDATE participants
                    SET
                    oafund = $2
                    WHERE participants.cognito_id = $1
                    RETURNING * ;
                `, arguments);

            return formatter.render('Participant updated');
        } catch (error) {
            errorhandler.log(error, '[OASB API] Error updating user details');
            return formatter.render(error, 500);
        } finally {
            client.release()
        }
    },

};
