module.exports = {
    "Records": [
        {
            "messageId": "3667339a-684c-429f-bd76-b41fe755d2be",
            "receiptHandle": "AQEBEQqss6tY7G5eCukohuwwZ3K8zj7OPR5+E/GpP1A0DjvZwfP2grEls9PLCqQgi9HCpg8t4EFF4vGWpkVH3yQqSWxdBoPTrZniVWrs68OcA+f7lZ9fhcaLa0xBVV5Qr8sd2y/T0x7OOwwBI8cNSBmoMPZXgS9N5Festp4yLtFLpIGYeLuaHxwFsbur/CQjY7uKsmxZV+ca7HXC4ha2BR1oG5HeG4gGKEoMet1iKlU2Q6ngEPgaCxsO2L/p19VJMGZvY86shCAYKCBqsJErAwCpRzkrfQH4TMawIY8vapKn7gb+nurI2BFdOIzSJ6kNQDTzYSyT/P0fnAtD7PY9qIKkifcyAXlqRMQts292IX/83t/T0JlTncWCVCGzyvTTiCbYrqdAfhFZHP4uwLAVJPPg/A==",
            "body": "{\"header\":{\"to\":{\"id\":97,\"organisation\":762,\"email\":\"webhook@silverink.nl\",\"webhook\":\"https://peerwith.com\",\"type\":\"consortia\",\"address\":\"https://ror.org/041kmwe10\",\"name\":\"Imperial College London\",\"persistent\":true},\"persistent\":true,\"type\":\"p1\",\"subject\":\"Publication Notification message\",\"template\":\"templates/Message.html\",\"from\":{\"id\":108,\"type\":\"institution\",\"ror\":\"https://ror.org/008xxew50\",\"address\":\"https://ror.org/008xxew50\",\"name\":\"Vrije Universiteit Test\",\"persistent\":true},\"routing\":\"CONSORTIA\",\"notification\":\"WEBHOOK\",\"p1\":\"20220303144309-fcd19e05-1b28-46b7-bbc6-f77b2d07e8d5\"},\"data\":{\"settlement\":{},\"authors\":[{\"isCorrespondingAuthor\":true,\"lastName\":\"Test\",\"firstName\":\"Ignore\",\"initials\":\"YV\",\"listingorder\":1,\"creditroles\":[\"conceptualization\"],\"affiliation\":\"Imperial College London\",\"institutions\":[{\"name\":\"Imperial College London\",\"ror\":\"https://ror.org/041kmwe10\"}]}],\"article\":{\"grants\":[{}],\"manuscript\":{\"dates\":{}},\"preprint\":{},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"Yes\"},\"doi\":\"10.1080/15588742.2015\"},\"journal\":{\"inDOAJ\":true,\"name\":\"Pol Sociology\",\"id\":\"1111-2222\"},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"Test\",\"id\":\"test\"}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1654764877166",
                "SenderId": "AIDAUSDTX2GI7KEDKIR5T",
                "ApproximateFirstReceiveTimestamp": "1654764877170"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"Receiver webhook\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "9bcb70b903bf8a39f71414d77756aa9f",
            "md5OfBody": "9d4729ae615a687b1b8e3d0b3492fb39",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:v2-dev-webhook",
            "awsRegion": "eu-central-1"
        }
    ]
}