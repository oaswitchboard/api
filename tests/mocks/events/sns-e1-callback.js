module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success:472eaa31-229c-49e8-a1ab-90fc7e7dddef",
            "Sns": {
                "Type": "Notification",
                "MessageId": "ebf861b4-4047-5a21-8fb6-3aaec8927890",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"header\":{\"type\":\"e1\",\"version\":\"v2\",\"to\":{\"id\":2,\"organisation\":6,\"email\":\"copernicus@silverink.nl\",\"webhook\":\"https://webhook.site/8b22889a-8a29-45ee-913a-e6a04a0a6fea\",\"type\":\"institution\",\"address\":\"https://ror.org/000002\",\"name\":\"Oxford University Press\",\"persistent\":true},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"persistent\":true,\"from\":{\"id\":7,\"type\":\"publisher\",\"ror\":\"https://ror.org/000000\",\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"persistent\":true},\"routing\":\"DEFAULT\",\"notification\":\"EMAIL\",\"e1\":\"20220608112301-a82c1d54-5d41-43f1-8ccd-a0171046b461\"},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"email\":\"frodo.baggins@vu.nl\",\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true},\"settlement\":{\"charges\":{}},\"charges\":{\"prioragreement\":false,\"charged\":true,\"currency\":\"EUR\",\"fees\":{\"apc\":{\"name\":\"estimated\",\"type\":\"per article APC list price\",\"amount\":1000},\"extra\":[{\"type\":\"submission format\",\"amount\":100},{\"type\":\"license choice\",\"amount\":100}],\"total\":{\"name\":\"estimated\",\"amount\":1200}}}}}",
                "Timestamp": "2022-06-08T11:23:01.225Z",
                "SignatureVersion": "1",
                "Signature": "fIO8am46+TZgTR2WTCbZqXUxOo8/30B1BcK++UEZXZOjtkoABkeGGELpS+Ywkdzqw1ifIl7VW50zSW5mB7oxirawO00RFKuPfxUetkO3l2qxwLHExUsdNxOEybm5+flIQt6pwoyELWHGtAWeFeUp3XMF2isEwlF5ckreaNyN4Y8shdfoMLyFUTIBwz+trqYOftH278eDtJJvkYJ9Mq2B2DnQwV4hoXn2zUHykCGADn8g3M5vgFts9tXb7vqUWqbvn13VGxG6PbLZkUVEHFgpiX2+VjjVnvQp42vShnOEYcbsR4BSd8hjiR/oYP0poQpWr8sfQ1RmMpcGrQ7gZfqBMg==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:v2-dev-success:472eaa31-229c-49e8-a1ab-90fc7e7dddef",
                "MessageAttributes": {}
            }
        }
    ]
}