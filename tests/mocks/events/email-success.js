module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success:593ceb95-b6ae-452c-bc20-8d7277d5c0a1",
            "Sns": {
                "Type": "Notification",
                "MessageId": "569e08d7-17d9-54c1-9d4c-549faf531c91",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"id\":7068,\"header\":{\"type\":\"p1\",\"version\":\"v2\",\"e1\":\"20210408093308-91736471-2858-4d30-f031-ccf3c5f08466\",\"e2\":\"20210408095220-c904fe13-54c1-466d-a184-9df116f19f91\",\"to\":{\"address\":\"https://ror.org/008xxew50\",\"name\":\"VU Amsterdam\"},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"subject\":\"OA Switchboard Eligibility Enquiry request\",\"persistent\":true,\"from\":{\"id\":7,\"ror\":\"https://ror.org/000000\",\"participant\":7,\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"email\":\"yuri@appetence.nl\"},\"p1\":\"20230123163101-53edac07-a852-48e6-ca81-431968d72584\",\"meta\":{\"group\":\"07bedf59-830c-4b2d-e90f-77455e4a3674\",\"routing\":{\"action\":\"CONSORTIA\",\"notification\":\"EMAIL\",\"organisation\":39,\"receiver\":5}},\"routingdata\":{\"extraction\":\"INSTITUTION\",\"rules\":\"CC\",\"route\":\"CONSORTIA\",\"notification\":\"EMAIL\"},\"pio\":true},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"isCorrespondingAuthor\":true,\"creditroles\":[\"conceptualization\"],\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true}},\"type\":\"p1\",\"created\":\"2023-01-23T16:31:03.581Z\",\"sender\":7,\"state\":\"SENT\",\"receiver\":5,\"organisation\":39,\"hash\":\"3d3931f0d3c3139dc9ea37f095d20059\"}",
                "Timestamp": "2023-01-23T16:31:06.165Z",
                "SignatureVersion": "1",
                "Signature": "x2am9a7LZi7F1wp73aF4XHZzy3MtwwgV/Jig/MvSDsfKE6uFCQNdkNV2YRWWn2ObsUTg6At7aV+Zzl999l4zP+EOV7CY9ZUWa5c7y9+HBrvHXIEsqoFYC0d03zNEQ30YO95MYAyUjaTpuoyBxbHLCjeuOivsCmfnSQPtyxJUtEO87AYg7F+LhA/9sZK9myRT4K/4gvf8wJ4yDoC5vedDeXKuCwIF3PAJmecWteZQ5LqzHm+2rCHU7HR1h98n+1TyoVObXva0hg0sxy9aPb+FQCFbeCWQUar/ez7vMtOi3s9M3mQ+l3mMpzYYriogfq92USQ7PXqQiFUA2FdoTEj3xQ==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:v2-dev-success:593ceb95-b6ae-452c-bc20-8d7277d5c0a1",
                "MessageAttributes": {}
            }
        }
    ]
}