module.exports = {
    "Records": [
        {
            "messageId": "931d4dc3-00f5-4f69-bcd9-bc53036f5cc2",
            "receiptHandle": "AQEBBqwHTeJyavrJazNg6cCAsDP0ZcSXfi4Pb81pW63/xBfbTrCpgh6QMUIqzOAetgp+kFkRWJ+agQh1bntu079nBEuPX7QAZdinmQHJuz+xI++p+p/NHx4TpsT1h2SJi815CCw09znoGO3a7J9grVmGPjRcEmtG4rs61dDaqm51UZH57ZiYkvmRNH52Y1ebcDMBR0lO0IpLh0z6a29qqDMMiLE/FKWuxDdX7aD6x47acsuxTahcDA9zEBag7cNtnZxmG4P+8EIx+fPYC5f0SUpV0S2bdVPIa8IRIVeF6YhqZqWjtBN/MhKz1ewgGSwRHKHiySNjxGBi8rXfU1sWAwNlh+L71R8lwsuNco/84nbW8xi36uZtrgHSSiZVEGjRP8Bf/h0eVV19/a6alZL/65WFhA==",
            "body": "{\"header\":{\"type\":\"e1\",\"version\":\"v2\",\"to\":{\"address\":\"https://ror.org/000002\",\"type\":\"publisher\",\"name\":\"Royal Society of Middle-Earth\"},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"persistent\":true},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"email\":\"frodo.baggins@vu.nl\",\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true},\"settlement\":{\"charges\":{}},\"charges\":{\"prioragreement\":false,\"charged\":true,\"currency\":\"EUR\",\"fees\":{\"apc\":{\"name\":\"estimated\",\"type\":\"per article APC list price\",\"amount\":1000},\"extra\":[{\"type\":\"submission format\",\"amount\":100},{\"type\":\"license choice\",\"amount\":100}],\"total\":{\"name\":\"estimated\",\"amount\":1200}}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1654680723487",
                "SenderId": "AIDAUSDTX2GI7KEDKIR5T",
                "ApproximateFirstReceiveTimestamp": "1654680723492"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "04852bd33d04017ab92c6d34b1cf821b",
            "md5OfBody": "9462942727f755f3dbbf45f2a76d56b3",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:v2-dev-incoming",
            "awsRegion": "eu-central-1"
        }
    ]
}
