module.exports =    [
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/03yrrjy16',
        name: 'University of Southern Denmark'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'CONSORTIA',
          notification: 'NONE',
          organisation: 1337,
          receiver: 93
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'SENDTO',
        rules: 'PRIMARY',
        route: 'CONSORTIA',
        notification: 'NONE'
      },
      pio: true
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false }
    },
    id: 7003
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/03yrrjy16',
        name: 'University of Southern Denmark'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'DEFAULT',
          notification: 'EMAIL',
          organisation: 1337,
          receiver: 7
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'SENDTO',
        rules: 'PRIMARY',
        route: 'DEFAULT',
        notification: 'EMAIL'
      }
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          email: 'emuskova@rambler.ru',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          email: 'xxx.xxx@xxx.com',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          email: 'bg@gmail.com',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          email: 'ij@rambler.ua',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          email: 'ro@gmail.com',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false },
      charges: {
        prioragreement: false,
        currency: 'GBP',
        fees: {
          apc: {
            type: 'per article APC list price',
            name: 'estimated',
            amount: 1700
          },
          total: { name: 'estimated', amount: 1700 }
        }
      }
    },
    id: 7004
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/03yrrjy16',
        name: 'University of Southern Denmark'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'DEFAULT',
          notification: 'WEBHOOK',
          organisation: 1337,
          receiver: 7
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'SENDTO',
        rules: 'PRIMARY',
        route: 'DEFAULT',
        notification: 'WEBHOOK'
      }
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          email: 'emuskova@rambler.ru',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          email: 'xxx.xxx@xxx.com',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          email: 'bg@gmail.com',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          email: 'ij@rambler.ua',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          email: 'ro@gmail.com',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false },
      charges: {
        prioragreement: false,
        currency: 'GBP',
        fees: {
          apc: {
            type: 'per article APC list price',
            name: 'estimated',
            amount: 1700
          },
          total: { name: 'estimated', amount: 1700 }
        }
      }
    },
    id: 7005
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/0577sef82',
        name: 'Severtsov Institute of Ecology and Evolution RAS'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'DLQ',
          notification: 'NONE',
          organisation: undefined,
          receiver: undefined
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'INSTITUTION',
        rules: 'CC',
        route: 'DLQ',
        notification: 'NONE'
      },
      pio: true
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false }
    },
    id: 7006
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: { address: undefined, name: 'Whale and Dolphin Conservation' },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'DLQ',
          notification: 'NONE',
          organisation: undefined,
          receiver: undefined
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'INSTITUTION',
        rules: 'CC',
        route: 'DLQ',
        notification: 'NONE'
      },
      pio: true
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false }
    },
    id: 7007
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/035b05819',
        name: 'University of Copenhagen'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'CONSORTIA',
          notification: 'NONE',
          organisation: 1331,
          receiver: 93
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'INSTITUTION',
        rules: 'CC',
        route: 'CONSORTIA',
        notification: 'NONE'
      },
      pio: true
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false }
    },
    id: 7008
  },
  {
    header: {
      type: 'p1',
      from: {
        id: 7,
        ror: 'https://ror.org/000000',
        participant: 7,
        address: 'https://ror.org/000000',
        name: 'Appetence Elitex',
        email: 'yuri@appetence.nl'
      },
      to: {
        address: 'https://ror.org/04bqh5m06',
        name: 'National Geographic Society'
      },
      meta: {
        source: 'Private datastore message handler',
        group: '430f65b6-ba0b-4965-8e83-8e57d255c8f1',
        routing: {
          action: 'DLQ',
          notification: 'NONE',
          organisation: undefined,
          receiver: undefined
        }
      },
      validity: '2024-01-10',
      persistent: true,
      createdby: 'Private datastore message handler',
      p1: '20230120113139-4a7a3604-3071-4ec9-d6fc-b46b2360a665',
      routingdata: {
        extraction: 'FUNDER',
        rules: 'CC',
        route: 'DLQ',
        notification: 'NONE'
      },
      pio: true
    },
    data: {
      authors: [
        {
          lastName: 'Muskova',
          firstName: 'Elona',
          initials: 'EM',
          ORCID: '',
          collaboration: '',
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Nadezhdin',
          firstName: 'Rob',
          initials: 'EH',
          ORCID: '',
          collaboration: '',
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [ { name: 'Whale and Dolphin Conservation' } ],
          affiliation: 'Whale and Dolphin Conservation',
          currentaddress: []
        },
        {
          lastName: 'Gatesov',
          firstName: 'Bill',
          initials: 'BG',
          ORCID: '',
          collaboration: '',
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'University of Southern Denmark',
              ror: 'https://ror.org/03yrrjy16'
            },
            {
              name: 'University of Copenhagen',
              ror: 'https://ror.org/035b05819'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        },
        {
          lastName: 'Jobsov',
          firstName: 'Ilya',
          initials: 'IJ',
          ORCID: '',
          collaboration: '',
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: 'Severtsov Institute of Ecology and Evolution RAS',
              ror: 'https://ror.org/0577sef82'
            }
          ],
          affiliation: 'Severtsov Institute of Ecology and Evolution RAS',
          currentaddress: []
        },
        {
          lastName: 'Renatove',
          firstName: 'Olga',
          initials: 'RO',
          ORCID: '0000-0003-1533-3817',
          collaboration: '',
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: 'University of Southern Denmark',
              rorid: 'https://ror.org/03yrrjy16'
            }
          ],
          affiliation: 'University of Southern Denmark',
          currentaddress: []
        }
      ],
      article: {
        title: 'Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum',
        grants: [ { name: 'NGS-61285R-19' } ],
        manuscript: { dates: { submission: '2023-01-03' }, id: 'RSPB-2022-2589' },
        preprint: {},
        vor: {
          license: 'CC BY-not specified',
          publication: 'transformative journal',
          deposition: 'open repository, like PMC',
          researchdata: 'other'
        },
        funders: [
          {
            name: 'National Geographic Society',
            ror: 'https://ror.org/04bqh5m06',
            fundref: 'fundref.org/1234'
          }
        ],
        type: 'research-article',
        acknowledgement: '',
        originaltype: 'Research',
        submissionId: 'RSPB-2022-2589',
        doi: ''
      },
      journal: { name: 'Proceedings B', id: '1471-2954', inDOAJ: false }
    },
    id: 7009
  }
]