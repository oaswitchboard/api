module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:v2-acc-success:7028b254-4273-4e99-b8b1-e1f8fad4e2cf",
            "Sns": {
                "Type": "Notification",
                "MessageId": "436acb1d-2b71-5922-ac00-97d10eeec313",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:v2-acc-success",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"id\":10764,\"header\":{\"to\":{\"address\":\"https://ror.org/111111111\",\"name\":\"Test Publisher\",\"id\":106},\"persistent\":true,\"version\":\"v2\",\"type\":\"e2\",\"subject\":\"OA Switchboard Eligibility Enquiry message\",\"from\":{\"id\":107,\"ror\":\"https://ror.org/02jz4aj89\",\"participant\":107,\"address\":\"https://ror.org/02jz4aj89\",\"name\":\"Universiteit Maastricht Test\",\"email\":\"yvonne.campfens@oaswitchboard.org\"},\"e1\":\"20230417083113-cb748023-94d8-42cb-d445-dc3118cabe50\",\"routingdata\":{\"extraction\":\"SENDTO\",\"rules\":\"PRIMARY\",\"route\":\"DEFAULT\",\"notification\":\"EMAIL\",\"id\":133},\"meta\":{\"group\":\"7686045c-1ba6-400a-9fe6-eb2cc82100b9\",\"routing\":{\"action\":\"DEFAULT\",\"notification\":\"EMAIL\",\"organisation\":285,\"receiver\":106,\"formatter\":{}}},\"e2\":\"20230503090321-3066de58-9b67-4e73-ba57-5cd597ca327b\"},\"data\":{\"acceptance\":{\"prioragreement\":false,\"invoicedetails\":\"\",\"fees\":{\"apc\":{\"amount\":850,\"accepts\":\"No\"},\"extra\":[{\"amount\":350,\"type\":\"color charges\",\"accepts\":\"No\"},{\"amount\":200,\"type\":\"license choice\",\"accepts\":\"No\"},{\"amount\":100,\"type\":\"editorial discount\",\"accepts\":\"No\"}]}}},\"type\":\"e2\",\"created\":\"2023-05-03T09:03:24.731Z\",\"sender\":107,\"state\":\"SENT\",\"receiver\":106,\"organisation\":285,\"hash\":\"4e9d8e6e2d06879a2c3af23f9adcfaa7\"}",
                "Timestamp": "2023-05-03T09:03:27.374Z",
                "SignatureVersion": "1",
                "Signature": "lhfLV9OhE6mM1uKIydNVXmaWdXCkfrSYiT+Oyp+/Ui93ZK4LRJ0JQZM0WBS7HLzLwiZt6vPfe1qo8AKo6wxctS8+T3dIiRFvVwy/qEufUzxEyMQd0RMbuO5BYBmWH9xMtazvZiNB8RCCze8eZ4T3xBbd0oJ6xSfrotk1xXpQl3MIdCp4py7jeHF9CRFkTc1YmIGEKKkXYPWvmqiyjudzxGcUYNOQVJ2cFKa5oMUXlfW7BUgiVQqRKOgTbAnPQ0QhlI0m/CG8/XxrB5m8Ff4i88FjUVAnuPcAY5l05xs0qQ5/BHGRxJRZTDWR1W2NvYoopKXJZAZcM6ohWD7+NQRxXw==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:v2-acc-success:7028b254-4273-4e99-b8b1-e1f8fad4e2cf",
                "MessageAttributes": {}
            }
        }
    ]
}
