module.exports = {
    "Records": [
        {
            "messageId": "28bccf4a-a47e-4a29-8d05-394d55b07829",
            "receiptHandle": "AQEBmYi5M4ju1qzPFbYe7k3EJ3ZUaI/rx2jUC7hGvoshxnvXe4uzS9nus/Rsd6XJSw7+FQRdNk+wIk5ga0cvkVYAjJM+z7J0kzYuH4qfDAUHosAfhw6UpHu1JiWIPWm2f+7JLAss7XaDGDQUwtmbcyYK3bJQ54ay1MXE0DVO3d5f7eeaZMNFIVY6U/4m0KmxxPvz8u7WxmkWJTR/fPhE5fRbV9msxf2fw7y/7+31R94XttqMIIamUAxXlGX98ctTUHkSpcml99+CG9cLBfcCWOITLGq0pOKjOT1/WusRRa2Di39l2OxfLf3UbQHxw1ubFimKxnZhwS8S8ltWPt7QiJjAlku+HfaIywR1hrKYTK+8GWRvGNg/xMAVj2tNGQ+MN4nytzBtYrcF+BugxoKf0gv5joVxdxwJxJbThrwIsB0U0Q8=",
            "body": "{\"header\":{\"to\":{\"id\":4,\"type\":\"publisher\",\"ror\":\"https://ror.org/000004\",\"address\":\"https://ror.org/000004\"},\"persistent\":true,\"version\":\"v2\",\"type\":\"e2\",\"subject\":\"OA Switchboard Eligibility Enquiry message\",\"from\":{\"id\":7,\"organisation\":5,\"email\":\"openadmin@book.org\"}},\"data\":{\"acceptance\":{\"prioragreement\":true,\"invoicedetails\":\"Please direct invoice to Knowledge Unlatched:\",\"agreement\":{\"accepts\":\"Yes\",\"name\":\"XXX\",\"id\":\"XXX 2022\"},\"fees\":{\"apc\":{\"amount\":1750,\"accepts\":\"Yes\"},\"extra\":[]}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1661341821252",
                "SenderId": "AIDAUSDTX2GI7KEDKIR5T",
                "ApproximateFirstReceiveTimestamp": "1661341826252"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "04852bd33d04017ab92c6d34b1cf821b",
            "md5OfBody": "4ea90f03f3566c372e51dd47f629e772",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-development",
            "awsRegion": "eu-central-1"
        }
    ]
}