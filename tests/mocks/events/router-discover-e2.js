module.exports = {
    "message": {
        "header": {
            "to": {
                "id": 4,
                "type": "publisher",
                "address": "https://ror.org/03yrrjy16"
            },
            "persistent": true,
            "version": "v2",
            "type": "e2",
            "subject": "OA Switchboard Eligibility Enquiry message",
            "from": {
                "id": 7,
                "organisation": 5,
                "email": "openadmin@iastate.edu"
            }
        },
        "data": {
            "acceptance": {
                "prioragreement": true,
                "invoicedetails": "Please direct invoice to Knowledge Unlatched:",
                "agreement": {
                    "accepts": "Yes",
                    "name": "Iowa-JMIR",
                    "id": "JMIR-Iowa 2022"
                },
                "fees": {
                    "apc": {
                        "amount": 1750,
                        "accepts": "Yes"
                    },
                    "extra": []
                }
            }
        }
    },
    "sender": "37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f"
}
