module.exports = {
    "Records": [
        {
            "messageId": "bdf3dc42-d987-496a-b630-1d9c10084578",
            "receiptHandle": "AQEBARxw02OGJUrrFI+fnFxHf1wKx29r2rPAPyt+ukTA5wruJzS91HgjzXnYjfjc2Kq4Q2iHdIpESCp0S4q1mz6VE8OqByTzGjMUKe6nJqyJGY2k7/o1te1+1Tcads30ZtrhdymlPF/w/fQsL45f6azksUqTopnhstAf9wgOahneN3AxPIaT7MXXOhQF4qYLADeiFmpCFQyfmjsayqCgaQo3Digi6RsjWZwRhzLx0e6LGd/dYbi1DcjvC2TadBnhCkv9uJmX0w7HKPkm3uAHnDJ0c0bslTy/e5vkgaEvGe8sxdwM1l/X5tPFqitnEt50kPvltQ3Koa/PXSvvN28Qfsjq9rlAEeMPpntHcpqdKzNrZBVIj0dsPtb3eVW5Y5Lw6Va7rBWrfcZyYl6wSVfK+FxpNlJxPQ014JoZEkKVvXdRCYA=",
            "body": "{\"header\":{\"type\":\"p1\",\"from\":{\"id\":59},\"to\":{\"address\":\"https://ror.org/0524sp257\",\"name\":\"School of Chemistry, University of Bristol\"},\"validity\":\"2023-02-16\",\"template\":\"templates/MessageRequest.html\",\"persistent\":true,\"key\":\"production/jats/unpacked/D1SC06684F_PA/D1SC06684F.xml\",\"meta\":{\"prioragreement\":true,\"source\":\"JATS\",\"status\":\"PUBLICATION\"},\"pio\":true,\"deduplicator\":\"d778f1ec-a819-4144-8b8a-d3ab5f94946c\"},\"data\":{\"authors\":[{\"lastName\":\"Saunthwal\",\"firstName\":\"Rakesh K.\",\"initials\":\"RKS\",\"ORCID\":\"\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"School of Chemistry, University of Bristol\",\"rorid\":\"https://ror.org/0524sp257\"}],\"affiliation\":\"School of Chemistry, University of Bristol\"},{\"lastName\":\"Mortimer\",\"firstName\":\"James\",\"initials\":\"JM\",\"ORCID\":\"\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"School of Chemistry, University of Bristol\",\"rorid\":\"https://ror.org/0524sp257\"}],\"affiliation\":\"School of Chemistry, University of Bristol\"},{\"lastName\":\"Orr-Ewing\",\"firstName\":\"Andrew J.\",\"initials\":\"AJO\",\"ORCID\":\"https://orcid.org/0000-0001-5551-9609\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"School of Chemistry, University of Bristol\",\"rorid\":\"https://ror.org/0524sp257\"}],\"affiliation\":\"School of Chemistry, University of Bristol\"},{\"lastName\":\"Clayden\",\"firstName\":\"Jonathan\",\"initials\":\"JC\",\"ORCID\":\"https://orcid.org/0000-0001-5080-9535\",\"collaboration\":\"\",\"listingorder\":4,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"School of Chemistry, University of Bristol\",\"rorid\":\"https://ror.org/0524sp257\"}],\"affiliation\":\"School of Chemistry, University of Bristol\"}],\"article\":{\"title\":\"Enantioselective one-carbon expansion of aromatic rings by simultaneous formation and chromoselective irradiation of a transient coloured enolate\",\"grants\":[{\"name\":\"839739EP/S024107/1\"}],\"manuscript\":{\"dates\":{\"submission\":\"2021-11-30\",\"acceptance\":\"2022-01-25\",\"publication\":\"2022-01-25\"},\"id\":\"d1sc06684f\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"},\"type\":\"research-article\",\"funders\":[{\"name\":\"H2020 Marie Skłodowska-Curie Actions\",\"fundref\":\"100010665\"},{\"name\":\"Engineering and Physical Sciences Research Council\",\"fundref\":\"501100000266\",\"rorid\":\"https://ror.org/0439y7842\"}],\"submissionId\":\"d1sc06684f\",\"doi\":\"10.1039/d1sc06684f\",\"doiurl\":\"https://doi.org/10.1039/d1sc06684f\"},\"journal\":{\"name\":\"Chemical Science\",\"id\":\"2041-6539\",\"inDOAJ\":false}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1645016722099",
                "SenderId": "AROAUSDTX2GI6RLYRQTFW:oasb-switchboard-prod-messages-validate-connectors",
                "ApproximateFirstReceiveTimestamp": "1645016727099"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"8d79a78d-3ddb-4cfa-a4ca-c6c6c8324ae2\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfBody": "7d518753240f6dbb508f48121ea8c9f2",
            "md5OfMessageAttributes": "140f384510e7752d0a4d3ef9a0db5d7b",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-production",
            "awsRegion": "eu-central-1"
        }
    ]
};