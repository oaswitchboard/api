module.exports = {
    "Records": [
        {
            "messageId": "8820c580-11cf-4036-a282-f40648ff34b6",
            "receiptHandle": "AQEBFlEOAn9CXTyi9isq9MplG6FStPr3n5G7XKdaV8S8fXbj0dTHa6ZJNj0FzOKQsTP8ZihTQny4ChxriJV7xoKkBXr2Oo1IQM2tLhyJFVB9azrkT9btV/+g5DKcv5IG/XjT2g92FKnkPdtS52IEGoEewByqGwxFsOk7TMDuR0IPib1YooDFZN05ssJym9ZJTKQL0MbKlKqkvkU25+CtGQKYg2O55LTX7gxHserlzVd1AvNtIIe5oLuu/2qRVRI4o9pah0dWCQnrXnys5MOVbhAEgUK/ysp1OXeXSXDcDLSQiJofFa/nDNP6PCiNKpiKJuxV8iEVnWcMA6XGEOenzEpkjzrih7aEGoIn2iUPp152vnNl9hyf1lyqhtO5CfjGeUU/scBm6zpcXR1iwb+k1DxDEA==",
            "body": "{\"header\":{\"type\":\"e2\",\"version\":\"v2\",\"to\":{\"id\":4,\"email\":\"rup@silverink.nl\",\"webhook\":\"\",\"type\":\"publisher\",\"ror\":\"https://ror.org/000004\",\"name\":\"Rockefeller University Press\",\"rules\":null,\"persistent\":true},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"persistent\":true,\"from\":{\"id\":35,\"type\":\"institution\",\"ror\":\"https://ror.org/006w34k90\",\"address\":\"https://ror.org/006w34k90\",\"name\":\"Yale University\",\"persistent\":true},\"notification\":\"WEBHOOK\",\"e1\":\"20220610075401-6db57a07-fa43-45e1-e66c-7ff91e2f5aba\",\"subject\":\"OA Switchboard Eligibility Enquiry message\",\"template\":\"templates/Message.html\",\"routing\":\"DEFAULT\",\"e2\":\"20220610115803-9a753041-e185-49aa-a1fc-b24746cfe5b3\"},\"data\":{\"acceptance\":{\"prioragreement\":false,\"invoicedetails\":\"\",\"fees\":{\"apc\":{\"amount\":1000,\"accepts\":\"Yes\"},\"extra\":[{\"amount\":100,\"accepts\":\"Yes\",\"type\":\"author funding from other sources\"},{\"amount\":100,\"accepts\":\"Yes\",\"type\":\"author funding from other sources\"}]}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1654862286156",
                "SenderId": "AROAUSDTX2GIS4SAAIH3L:oasb-switchboard-v2-dev-messages-webhook-ingest",
                "ApproximateFirstReceiveTimestamp": "1654862286157"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"Receiver webhook\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "9bcb70b903bf8a39f71414d77756aa9f",
            "md5OfBody": "60f102736140584af51781c8d90c0654",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:v2-dev-webhook",
            "awsRegion": "eu-central-1"
        }
    ]
}