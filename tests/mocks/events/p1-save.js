module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:success_development:d99f18d6-bca0-4543-ac42-8e6d45703c68",
            "Sns": {
                "Type": "Notification",
                "MessageId": "012a7055-87c8-5bfb-bbe2-2a441cff0dff",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:success_development",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"header\":{\"type\":\"e1\",\"from\":{\"id\":7,\"email\":\"yuri@appetence.nl\",\"type\":\"publisher\",\"ror\":\"https://ror.org/000000\",\"address\":\"https://ror.org/000000\",\"name\":\"Appetence\",\"persistent\":true},\"to\":{\"id\":2,\"organisation\":6,\"email\":\"oup@silverink.nl\",\"webhook\":\"https://webhook.site/8b22889a-8a29-45ee-913a-e6a04a0a6fea\",\"type\":\"institution\",\"ror\":\"https://ror.org/000002\",\"receiver\":\"Oxford University Press\",\"participant\":\"Oxford University Press\",\"persistent\":true},\"subject\":\"OA Switchboard Eligibility Enquiry request\",\"template\":\"templates/Message.html\",\"persistent\":true,\"routing\":\"DEFAULT\",\"notification\":\"EMAIL\",\"e1\":\"20211119123047-b297c75e-8f11-4e16-c08a-825d588170ce\"},\"data\":{\"authors\":[{\"lastName\":\"Oscar\",\"firstName\":\"Wild\",\"initials\":\"JJ\",\"ORCID\":\"ORCID\",\"email\":\"email@email.com\",\"listingorder\":1,\"isCorrespondingAuthor\":true,\"creditrole\":[\"conceptualization\"],\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\"}]}],\"article\":{\"grants\":[{\"name\":\"Name\",\"id\":\"ID\"}],\"manuscript\":{\"dates\":{\"submission\":\"Date\"},\"title\":\"Manuscript\",\"id\":\"ID\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"No\"},\"title\":\"Article tite\",\"type\":\"addendum\",\"funders\":[{\"name\":\"Bill of Rights Institute\",\"ror\":\"https://ror.org/0267sz241\"}],\"submissionId\":\"Submission ID\"},\"journal\":{\"name\":\"Journal title\",\"id\":\"0001-0002\",\"inDOAJ\":true},\"charges\":{\"prioragreement\":false,\"apc\":{\"total\":{\"apc\":{\"name\":\"estimated\",\"amount\":1000},\"fees\":{\"name\":\"firm\",\"amount\":30}},\"currency\":\"EUR\",\"type\":\"per article APC list price\",\"extra\":[{\"name\":\"submission format\",\"amount\":10},{\"name\":\"license choice\",\"amount\":20}]}}}}",
                "Timestamp": "2021-11-19T12:30:47.374Z",
                "SignatureVersion": "1",
                "Signature": "H1Jy9MHkQenrlO68Gsfu+BeeWvW1XjXN6j1SIsGlDtFKvh7oWBGwdmDpVsVwPLdsq+KZS1Clds99/wSx2Dfkf3WAnXMQhH24niP/kVu6vcCi7Q+rnM/xI6HRp3UZcTFnKOG00FJuqS/Lp3DLq7fLbOXZnKW2n2xypN1iAwVA1KcsBjDsslfaEaUms37tyYYQbg/fqXvp1RHwcQo39lCPboRjsXUeoVW/VjN+BUjQp88g7u4JFyovmnqtsN/izoI6SpzFNzuPGD/Ld6UN2o2hHtJO+2VTtOCyES+Wrxlxol49RHfTOt/HW1/+AjFSAurOC1wVNMVEpxoayWuxjnrFHQ==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:success_development:d99f18d6-bca0-4543-ac42-8e6d45703c68",
                "MessageAttributes": {}
            }
        }
    ]
}
