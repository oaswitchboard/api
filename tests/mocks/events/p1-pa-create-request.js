module.exports = {
        "resource": "/message",
        "path": "/v1/message",
        "httpMethod": "POST",
        "headers": {
            "Authorization": "eyJraWQiOiJBQnZaOFMyTmYwb1Bzb0YrY2lQNnllZUp4MzZ5RDd4WHdaZVQwb0ltazBZPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzN2RiOWI4ZS1lMGY5LTQ2ZTAtYTJjZC1jN2U3YWU2NDU4NGYiLCJhdWQiOiI0ZXIwbTltZTFxaWk2NGoxdDlucmU2YWljdSIsImNvZ25pdG86Z3JvdXBzIjpbIkFkbWluaXN0cmF0b3JzIiwiVFJTIl0sImV2ZW50X2lkIjoiMTFmM2JmMWUtY2ZmNi00Mzc5LWFiMTAtMjNmZjc1MTU1NGM0IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MzMwMDQ5OTksImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xX29nVzNDOUIxZSIsImNvZ25pdG86dXNlcm5hbWUiOiJ5dXJpQGFwcGV0ZW5jZS5ubCIsImV4cCI6MTYzMzAwODU5OSwiaWF0IjoxNjMzMDA0OTk5LCJlbWFpbCI6Inl1cmlAYXBwZXRlbmNlLm5sIn0.mWIs1PZ4NwYJJQgvVAKRcdH7iwRxzhP5fDiiY4JnUeZHWA58nEVY5YdfVFzJA7bK5ZnEzWAud8VSn2HpwVHmPiiHTwloiENWyCxBZt3_Xu3H_J9CfOv2YVBf_qhn-zdU7QjxmybLxkNnohLX6dquXUQ6zvbkp6Eug8011fZ9db8yqmNoS6YHiTAvLQnaokM_3pZZKk3M3oY3k5fB36Lc9sAZdOQ4JJkKj2G5kLxvrZFza0GoG63BidAMVcizWIOR2mJkYhRyxIEhJKcJjRQF7oPV-Vt10-8tM0hvifblDSWJbiQ1umrbX0xDH41NNFJxIuwP6gGo3jYpETMBUZxnQQ",
            "content-type": "application/json",
        },
        "queryStringParameters": null,
        "multiValueQueryStringParameters": null,
        "pathParameters": null,
        "stageVariables": null,
        "requestContext": {
            "resourceId": "pup33f",
            "authorizer": {
                "claims": {
                    "sub": "37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f",
                    "aud": "4er0m9me1qii64j1t9nre6aicu",
                }
            },
            "resourcePath": "/message",
            "httpMethod": "POST",
            "extendedRequestId": "GegY_GmaliAFaXw=",
            "requestTime": "30/Sep/2021:12:30:10 +0000",
            "path": "/v1/message",
            "accountId": "313775346065",
            "protocol": "HTTP/1.1",
            "stage": "dev",
            "domainPrefix": "devapi",
            "requestTimeEpoch": 1633005010925,
            "requestId": "67ac6a53-53fd-4167-9cc7-b9a70bf93460",
            "identity": {
                "cognitoIdentityPoolId": null,
                "accountId": null,
                "cognitoIdentityId": null,
                "caller": null,
                "principalOrgId": null,
                "accessKey": null,
                "cognitoAuthenticationType": null,
                "cognitoAuthenticationProvider": null,
                "userArn": null,
                "user": null
            },
            "domainName": "devapi.oaswitchboard.org",
            "apiId": "fz01whbs3g"
        },
        "body": "{\n\n        \"header\": {\n            \"type\": \"p1\",\n            \"from\": {\n                \"id\": 43\n            },\n            \"to\": {\n                \"address\": \"https://ror.org/00q1fsf04\",\n                \"name\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz\"\n            },\n            \"validity\": \"2022-09-28\",\n            \"template\": \"templates/MessageRequest.html\",\n            \"persistent\": true,\n            \"key\": \"development/jats/unpacked/D0NR08191D_PA/D0NR08191D.xml\",\n            \"meta\": {\n                \"prioragreement\": true,\n                \"source\": \"JATS\",\n                \"status\": \"PUBLICATION\"\n            },\n            \"p1\": \"20210928152311-9ecce2b6-2555-4e8c-e614-3ca17d529093\"\n        },\n        \"data\": {\n            \"authors\": [\n                {\n                    \"lastName\": \"Haendel\",\n                    \"firstName\": \"Maximilian\",\n                    \"initials\": \"MB\",\n                    \"ORCID\": \"\",\n                    \"email\": \"\",\n                    \"collaboration\": \"\",\n                    \"listingorder\": 1,\n                    \"creditroles\": [],\n                    \"isCorrespondingAuthor\": false,\n                    \"institutions\": [\n                        {\n                            \"name\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz\"\n                        },\n                        {\n                            \"name\": \"Max Planck Institute for Polymer Research\"\n                        }\n                    ],\n                    \"affiliation\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz,Max Planck Institute for Polymer Research\"\n                },\n                {\n                    \"lastName\": \"Grieg\",\n                    \"firstName\": \"Johanna\",\n                    \"initials\": \"JS\",\n                    \"ORCID\": \"\",\n                    \"email\": \"\",\n                    \"collaboration\": \"\",\n                    \"listingorder\": 2,\n                    \"creditroles\": [],\n                    \"isCorrespondingAuthor\": false,\n                    \"institutions\": [\n                        {\n                            \"name\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz\"\n                        },\n                        {\n                            \"name\": \"Max Planck Institute for Polymer Research\"\n                        }\n                    ],\n                    \"affiliation\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz,Max Planck Institute for Polymer Research\"\n                },\n                {\n                    \"lastName\": \"Bach\",\n                    \"firstName\": \"Katharina\",\n                    \"initials\": \"KL\",\n                    \"email\": \"\",\n                    \"collaboration\": \"\",\n                    \"listingorder\": 3,\n                    \"creditroles\": [],\n                    \"isCorrespondingAuthor\": false,\n                    \"institutions\": [\n                        {\n                            \"name\": \"Max Planck Institute for Polymer Research\"\n                        }\n                    ],\n                    \"affiliation\": \"Max Planck Institute for Polymer Research\"\n                },\n                {\n                    \"lastName\": \"Mozart\",\n                    \"firstName\": \"Volker\",\n                    \"initials\": \"VM\",\n                    \"email\": \"\",\n                    \"collaboration\": \"\",\n                    \"listingorder\": 4,\n                    \"creditroles\": [],\n                    \"isCorrespondingAuthor\": true,\n                    \"institutions\": [\n                        {\n                            \"name\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz\"\n                        },\n                        {\n                            \"name\": \"Max Planck Institute for Polymer Research\"\n                        }\n                    ],\n                    \"affiliation\": \"Dermatology Clinic, University Medical Center of the Johannes Gutenberg-University Mainz,Max Planck Institute for Polymer Research\"\n                }\n            ],\n            \"article\": {\n                \"title\": \"The conjugation strategy affects antibody orientation and targeting properties of nanocarriers†Electronic supplementary information (ESI) available: Additional figures are summarized in the supporting information. In a separate Excel File all identified proteins (LC-MS) are summarized. See DOI: 10.1039/d0nr08191d\",\n                \"grants\": [\n                    {\n                        \"name\": \"AAAAA0000\"\n                    }\n                ],\n                \"manuscript\": {\n                    \"dates\": {\n                        \"submission\": \"2020-11-17\",\n                        \"acceptance\": \"2021-05-01\",\n                        \"publication\": \"2021-05-11\"\n                    },\n                    \"id\": \"d0nr08191d\"\n                },\n                \"preprint\": {},\n                \"vor\": {\n                    \"publication\": \"open access / hybrid journal\",\n                    \"license\": \"CC BY\",\n                    \"deposition\": \"open repository, like PMC\",\n                    \"researchdata\": \"data available on request\"\n                },\n                \"type\": \"research-article\",\n                \"funders\": [\n                    {\n                        \"name\": \"Deutsche Forschungsgemeinschaft\"\n                    }\n                ],\n                \"submissionId\": \"000000\",\n                \"doi\": \"10.2222/000000\",\n                \"doiurl\": \"https://doi.org/10.2222/j.jaci.2222.22.222\"\n            },\n            \"journal\": {\n                \"name\": \"Gigascience\",\n                \"id\": \"2040-3372\",\n                \"inDOAJ\": false\n            },\n            \"charges\": {\n                \"prioragreement\": true,\n                \"agreement\": {\n                    \"name\": \"NBA open access agreement\",\n                    \"id\": \"\"\n                }\n            },\n            \"settlement\": {\n                \"charges\": {}\n            }\n        }\n\n\n}\n",
        "isBase64Encoded": false


}