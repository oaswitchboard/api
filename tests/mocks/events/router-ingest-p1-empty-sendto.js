module.exports = {
    "resource": "/message",
    "path": "/v2/message",
    "httpMethod": "POST",
    "headers": {
        "accept": "*/*",
        "Authorization": "Bearer eyJraWQiOiJ4RFdkTGJMWXZNb2hNSVhXTkF5bkwrenU3RzJ0dUY5RmFSMlplbVRNOTdRPSIsImFsZyI6IlJTMjU2In0.eyJjdXN0b206dHlwZSI6InB1Ymxpc2hlciIsImN1c3RvbTpyb3IiOiIwMDAwMDAwIiwic3ViIjoiZTQxZjA4ZDYtMzQ0My00N2JlLTk0NDgtZjhhMjZkZmI2MjA0IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImN1c3RvbTp3ZWJob29rIjoiZmFsc2UiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9wNllJSFBKQU8iLCJjb2duaXRvOnVzZXJuYW1lIjoicHJlcHJpbnRAcGVuc29mdC5uZXQiLCJjdXN0b206cHVic3dlZXRfaWQiOiIwMDAwMDAwIiwiYXVkIjoiNWZldGo3NTVqMzQ3aWMwODhpZDJiM3NoanIiLCJjdXN0b206aW5zdGl0dXRpb24iOiJQZW5zb2Z0XHUyMDE5cyBBUlBIQSBQdWJsaXNoaW5nIFBsYXRmb3JtIiwiZXZlbnRfaWQiOiI1MWYzZTdhYS0wYmY5LTRlNWMtOTU5NS1mNDZiMTFiNzZmYWMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTY3NDYzNzg2NywibmFtZSI6IlBlbnNvZnRcdTIwMTlzIEFSUEhBIFB1Ymxpc2hpbmcgUGxhdGZvcm0iLCJjdXN0b206cGVyc2lzdGVudCI6IjEiLCJleHAiOjE2NzQ2NDE0NjcsImlhdCI6MTY3NDYzNzg2NywiZW1haWwiOiJwcmVwcmludEBwZW5zb2Z0Lm5ldCJ9.WZ_WTGyfO95lxDIT55M_JmD4kFoZrmz6niJ2ARGnlBB6L-Nrnsueyqhpgw4XNXJzzGYmbBdV8f8pY4xXfsY7WloTIRdiIhkbkm0tL-gUxLTWFvo3iNKpCAw0DmNmleGgY-vgtJ32qYVRFz5r779xo48kDBhe1njHlDQ5mJZTkCYU02aWvGkxLCVtv2Duk3m8OuYA8VnbMfD3m0wnPj5oTEFbO7dFSaHn47CJbDn8C0Wp_LXK-UbRLKuqyJOhU0P5gpscgQVvfOrewvwRJzdOikOkcp36lEWWEJ0_hHSRDxSfMcvzCsaXb0m7fH7mOvuZfQNQ15QnceC5rj1clddHeA",
        "content-type": "application/json",
        "Host": "api.oaswitchboard.org",
        "X-Amzn-Trace-Id": "Root=1-63d0f22b-7496dda966d236c067eba1ff",
        "X-Forwarded-For": "79.98.107.82",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "accept": [
            "*/*"
        ],
        "Authorization": [
            "Bearer eyJraWQiOiJ4RFdkTGJMWXZNb2hNSVhXTkF5bkwrenU3RzJ0dUY5RmFSMlplbVRNOTdRPSIsImFsZyI6IlJTMjU2In0.eyJjdXN0b206dHlwZSI6InB1Ymxpc2hlciIsImN1c3RvbTpyb3IiOiIwMDAwMDAwIiwic3ViIjoiZTQxZjA4ZDYtMzQ0My00N2JlLTk0NDgtZjhhMjZkZmI2MjA0IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImN1c3RvbTp3ZWJob29rIjoiZmFsc2UiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9wNllJSFBKQU8iLCJjb2duaXRvOnVzZXJuYW1lIjoicHJlcHJpbnRAcGVuc29mdC5uZXQiLCJjdXN0b206cHVic3dlZXRfaWQiOiIwMDAwMDAwIiwiYXVkIjoiNWZldGo3NTVqMzQ3aWMwODhpZDJiM3NoanIiLCJjdXN0b206aW5zdGl0dXRpb24iOiJQZW5zb2Z0XHUyMDE5cyBBUlBIQSBQdWJsaXNoaW5nIFBsYXRmb3JtIiwiZXZlbnRfaWQiOiI1MWYzZTdhYS0wYmY5LTRlNWMtOTU5NS1mNDZiMTFiNzZmYWMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTY3NDYzNzg2NywibmFtZSI6IlBlbnNvZnRcdTIwMTlzIEFSUEhBIFB1Ymxpc2hpbmcgUGxhdGZvcm0iLCJjdXN0b206cGVyc2lzdGVudCI6IjEiLCJleHAiOjE2NzQ2NDE0NjcsImlhdCI6MTY3NDYzNzg2NywiZW1haWwiOiJwcmVwcmludEBwZW5zb2Z0Lm5ldCJ9.WZ_WTGyfO95lxDIT55M_JmD4kFoZrmz6niJ2ARGnlBB6L-Nrnsueyqhpgw4XNXJzzGYmbBdV8f8pY4xXfsY7WloTIRdiIhkbkm0tL-gUxLTWFvo3iNKpCAw0DmNmleGgY-vgtJ32qYVRFz5r779xo48kDBhe1njHlDQ5mJZTkCYU02aWvGkxLCVtv2Duk3m8OuYA8VnbMfD3m0wnPj5oTEFbO7dFSaHn47CJbDn8C0Wp_LXK-UbRLKuqyJOhU0P5gpscgQVvfOrewvwRJzdOikOkcp36lEWWEJ0_hHSRDxSfMcvzCsaXb0m7fH7mOvuZfQNQ15QnceC5rj1clddHeA"
        ],
        "content-type": [
            "application/json"
        ],
        "Host": [
            "api.oaswitchboard.org"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-63d0f22b-7496dda966d236c067eba1ff"
        ],
        "X-Forwarded-For": [
            "79.98.107.82"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "queryStringParameters": null,
    "multiValueQueryStringParameters": null,
    "pathParameters": null,
    "stageVariables": null,
    "requestContext": {
        "resourceId": "bs1fmi",
        "authorizer": {
            "claims": {
                "custom:type": "publisher",
                "custom:ror": "0000000",
                "sub": "e41f08d6-3443-47be-9448-f8a26dfb6204",
                "email_verified": "true",
                "custom:webhook": "false",
                "iss": "https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_p6YIHPJAO",
                "cognito:username": "preprint@pensoft.net",
                "custom:pubsweet_id": "0000000",
                "aud": "5fetj755j347ic088id2b3shjr",
                "custom:institution": "Pensoft’s ARPHA Publishing Platform",
                "event_id": "51f3e7aa-0bf9-4e5c-9595-f46b11b76fac",
                "token_use": "id",
                "auth_time": "1674637867",
                "name": "Pensoft’s ARPHA Publishing Platform",
                "custom:persistent": "1",
                "exp": "Wed Jan 25 10:11:07 UTC 2023",
                "iat": "Wed Jan 25 09:11:07 UTC 2023",
                "email": "preprint@pensoft.net"
            }
        },
        "resourcePath": "/message",
        "httpMethod": "POST",
        "extendedRequestId": "fSrG5HvJFiAFaSA=",
        "requestTime": "25/Jan/2023:09:11:07 +0000",
        "path": "/v2/message",
        "accountId": "313775346065",
        "protocol": "HTTP/1.1",
        "stage": "v2-prod",
        "domainPrefix": "api",
        "requestTimeEpoch": 1674637867999,
        "requestId": "12640408-1755-455b-954c-6f0cfc764b70",
        "identity": {
            "cognitoIdentityPoolId": null,
            "accountId": null,
            "cognitoIdentityId": null,
            "caller": null,
            "sourceIp": "79.98.107.82",
            "principalOrgId": null,
            "accessKey": null,
            "cognitoAuthenticationType": null,
            "cognitoAuthenticationProvider": null,
            "userArn": null,
            "userAgent": null,
            "user": null
        },
        "domainName": "api.oaswitchboard.org",
        "apiId": "f3fryyefzb"
    },
    "body": "{\n    \"header\": {\n        \"type\": \"p1\",\n        \"to\": {\n            \"address\": \"\"\n        },\n        \"persistent\": true\n    },\n    \"data\": {\n        \"authors\": [\n            {\n                \"listingorder\": 1,\n                \"lastName\": \"Yang\",\n                \"firstName\": \"Hee-Min\",\n                \"initials\": \"HY\",\n                \"ORCID\": \"https://orcid.org/0000-0001-6288-1534\",\n                \"email\": \"allmmm@naver.com\",\n                \"isCorrespondingAuthor\": false,\n                \"affiliation\": \"Inha University, Incheon, Republic of Korea\"\n            },\n            {\n                \"listingorder\": 2,\n                \"lastName\": \"Min\",\n                \"firstName\": \"Gi-Sik\",\n                \"initials\": \"GM\",\n                \"ORCID\": \"https://orcid.org/0000-0003-2739-3978\",\n                \"email\": \"mingisik@inha.ac.kr\",\n                \"isCorrespondingAuthor\": true,\n                \"affiliation\": \"Inha University, Incheon, Republic of Korea\"\n            }\n        ],\n        \"article\": {\n            \"title\": \"\ufeffA new species of the genus Cephalodella (Rotifera, Monogononta) from Korea, with reports of four additional cephalodellid species\",\n            \"type\": \"research-article\",\n            \"funders\": [\n                {\n                    \"id\": \"45824\",\n                    \"name\": \"National Institute of Biological Resources\",\n                    \"ror_id\": \"https://ror.org/012a41834\"\n                }\n            ],\n            \"doi\": \"doi:10.3897/zookeys.1141.91147\",\n            \"doiurl\": \"https://doi.org/10.3897/zookeys.1141.91147\",\n            \"submissionId\": \"91147\",\n            \"manuscript\": {\n                \"dates\": {\n                    \"submission\": \"2022-08-02\",\n                    \"acceptance\": \"2022-12-30\",\n                    \"publication\": \"2023-01-23\"\n                },\n                \"title\": \"\ufeffA new species of the genus Cephalodella (Rotifera, Monogononta) from Korea, with reports of four additional cephalodellid species\"\n            },\n            \"vor\": {\n                \"publication\": \"pure OA journal\",\n                \"license\": \"CC BY\",\n                \"deposition\": \"open repository, like PMC\",\n                \"researchdata\": \"data available on request\"\n            }\n        },\n        \"journal\": {\n            \"name\": \"ZooKeys\",\n            \"id\": \"1313-2970\",\n            \"inDOAJ\": true\n        }\n    }\n}",
    "isBase64Encoded": false
}