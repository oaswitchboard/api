module.exports = {
    "resource": "/message",
    "path": "/v1/message",
    "httpMethod": "POST",
    "headers": {
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-GB,en;q=0.9,en-US;q=0.8,ru;q=0.7,nl;q=0.6",
        "Authorization": "Bearer eyJraWQiOiJBQnZaOFMyTmYwb1Bzb0YrY2lQNnllZUp4MzZ5RDd4WHdaZVQwb0ltazBZPSIsImFsZyI6IlJTMjU2In0.eyJjdXN0b206dHlwZSI6Imluc3RpdHV0aW9uIiwiY3VzdG9tOnJvciI6Imh0dHBzOlwvXC9yb3Iub3JnXC9tYXhpbSIsInN1YiI6IjQ2NmMyYWNiLTI0ZWUtNDAyZC1iMmEwLWQ1MTlkMzJhNTk2OSIsImNvZ25pdG86Z3JvdXBzIjpbIkFkbWluaXN0cmF0b3JzIiwiVFJTIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJjdXN0b206d2ViaG9vayI6ImZhbHNlIiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tXC9ldS1jZW50cmFsLTFfb2dXM0M5QjFlIiwiY29nbml0bzp1c2VybmFtZSI6Im1zaGVyZXBvdEBlbGl0ZXguc3lzdGVtcyIsImN1c3RvbTpwdWJzd2VldF9pZCI6IjAwMDAxMSIsImF1ZCI6IjRlcjBtOW1lMXFpaTY0ajF0OW5yZTZhaWN1IiwiY3VzdG9tOmluc3RpdHV0aW9uIjoiRWxpdGV4IC0gTWF4IFNoZXJlcG90IiwiZXZlbnRfaWQiOiIyYmExM2Q3OC1hMDFjLTRlYTQtOGRlMy1iOGMyNjlkZmE4YzgiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYzODIwMjMyMSwibmFtZSI6Ik1ha3N5bSBTaGVyZXBvdCIsImN1c3RvbTpwZXJzaXN0ZW50IjoiMSIsImV4cCI6MTYzODIwNTkyMCwiaWF0IjoxNjM4MjAyMzIxLCJlbWFpbCI6Im1zaGVyZXBvdEBlbGl0ZXguc3lzdGVtcyJ9.EeC32Y_UBroiPAz5Tpw43Vxgxx6u-5STHInLDEi147u56jwD14Qakg8CZYzp0Kd74t9vY8j3EBxh9v_XtAw0AcSO4s2Kvs4cPuj0kZhGIXA13IjDIhQ5Z8ZUslgVBtyYHlD1nTZ0llCHeiDcnqAVQwTI0Ae-vuLOPcNOt3hz7IHFNWgccEwZpLHoVs7z8F4cxQibCGD7sEOEsBobHN8DwUOKqLDzdhFPt0KjrELUnzNZ8YFaA1yhjLko5yaODp0oPzmWqJLKxc7M8XmD6W_kZDju8tFRSKiMdCzYYbJCK2nd0vHk_xnGRdyPvnCWt941XYNpShRx3f_ZK2n_t2nFWw",
        "content-type": "application/json",
        "dnt": "1",
        "Host": "devapi.oaswitchboard.org",
        "origin": "https://devhub.oaswitchboard.org",
        "referer": "https://devhub.oaswitchboard.org/",
        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"macOS\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36",
        "X-Amzn-Trace-Id": "Root=1-61a5012a-066d155a1e3a2348024feeee",
        "X-Forwarded-For": "217.123.117.17",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "accept": [
            "*/*"
        ],
        "accept-encoding": [
            "gzip, deflate, br"
        ],
        "accept-language": [
            "en-GB,en;q=0.9,en-US;q=0.8,ru;q=0.7,nl;q=0.6"
        ],
        "Authorization": [
            "Bearer eyJraWQiOiJBQnZaOFMyTmYwb1Bzb0YrY2lQNnllZUp4MzZ5RDd4WHdaZVQwb0ltazBZPSIsImFsZyI6IlJTMjU2In0.eyJjdXN0b206dHlwZSI6Imluc3RpdHV0aW9uIiwiY3VzdG9tOnJvciI6Imh0dHBzOlwvXC9yb3Iub3JnXC9tYXhpbSIsInN1YiI6IjQ2NmMyYWNiLTI0ZWUtNDAyZC1iMmEwLWQ1MTlkMzJhNTk2OSIsImNvZ25pdG86Z3JvdXBzIjpbIkFkbWluaXN0cmF0b3JzIiwiVFJTIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJjdXN0b206d2ViaG9vayI6ImZhbHNlIiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tXC9ldS1jZW50cmFsLTFfb2dXM0M5QjFlIiwiY29nbml0bzp1c2VybmFtZSI6Im1zaGVyZXBvdEBlbGl0ZXguc3lzdGVtcyIsImN1c3RvbTpwdWJzd2VldF9pZCI6IjAwMDAxMSIsImF1ZCI6IjRlcjBtOW1lMXFpaTY0ajF0OW5yZTZhaWN1IiwiY3VzdG9tOmluc3RpdHV0aW9uIjoiRWxpdGV4IC0gTWF4IFNoZXJlcG90IiwiZXZlbnRfaWQiOiIyYmExM2Q3OC1hMDFjLTRlYTQtOGRlMy1iOGMyNjlkZmE4YzgiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYzODIwMjMyMSwibmFtZSI6Ik1ha3N5bSBTaGVyZXBvdCIsImN1c3RvbTpwZXJzaXN0ZW50IjoiMSIsImV4cCI6MTYzODIwNTkyMCwiaWF0IjoxNjM4MjAyMzIxLCJlbWFpbCI6Im1zaGVyZXBvdEBlbGl0ZXguc3lzdGVtcyJ9.EeC32Y_UBroiPAz5Tpw43Vxgxx6u-5STHInLDEi147u56jwD14Qakg8CZYzp0Kd74t9vY8j3EBxh9v_XtAw0AcSO4s2Kvs4cPuj0kZhGIXA13IjDIhQ5Z8ZUslgVBtyYHlD1nTZ0llCHeiDcnqAVQwTI0Ae-vuLOPcNOt3hz7IHFNWgccEwZpLHoVs7z8F4cxQibCGD7sEOEsBobHN8DwUOKqLDzdhFPt0KjrELUnzNZ8YFaA1yhjLko5yaODp0oPzmWqJLKxc7M8XmD6W_kZDju8tFRSKiMdCzYYbJCK2nd0vHk_xnGRdyPvnCWt941XYNpShRx3f_ZK2n_t2nFWw"
        ],
        "content-type": [
            "application/json"
        ],
        "dnt": [
            "1"
        ],
        "Host": [
            "devapi.oaswitchboard.org"
        ],
        "origin": [
            "https://devhub.oaswitchboard.org"
        ],
        "referer": [
            "https://devhub.oaswitchboard.org/"
        ],
        "sec-ch-ua": [
            "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\""
        ],
        "sec-ch-ua-mobile": [
            "?0"
        ],
        "sec-ch-ua-platform": [
            "\"macOS\""
        ],
        "sec-fetch-dest": [
            "empty"
        ],
        "sec-fetch-mode": [
            "cors"
        ],
        "sec-fetch-site": [
            "same-site"
        ],
        "User-Agent": [
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-61a5012a-066d155a1e3a2348024feeee"
        ],
        "X-Forwarded-For": [
            "217.123.117.17"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "queryStringParameters": null,
    "multiValueQueryStringParameters": null,
    "pathParameters": null,
    "stageVariables": null,
    "requestContext": {
        "resourceId": "pup33f",
        "authorizer": {
            "claims": {
                "custom:type": "institution",
                "custom:ror": "https://ror.org/maxim",
                "sub": "466c2acb-24ee-402d-b2a0-d519d32a5969",
                "cognito:groups": "Administrators,TRS",
                "email_verified": "true",
                "custom:webhook": "false",
                "iss": "https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_ogW3C9B1e",
                "cognito:username": "msherepot@elitex.systems",
                "custom:pubsweet_id": "000011",
                "aud": "4er0m9me1qii64j1t9nre6aicu",
                "custom:institution": "Elitex - Max Sherepot",
                "event_id": "2ba13d78-a01c-4ea4-8de3-b8c269dfa8c8",
                "token_use": "id",
                "auth_time": "1638202321",
                "name": "Maksym Sherepot",
                "custom:persistent": "1",
                "exp": "Mon Nov 29 17:12:00 UTC 2021",
                "iat": "Mon Nov 29 16:12:01 UTC 2021",
                "email": "msherepot@elitex.systems"
            }
        },
        "resourcePath": "/message",
        "httpMethod": "POST",
        "extendedRequestId": "Jk0evHyJFiAFkbQ=",
        "requestTime": "29/Nov/2021:16:34:50 +0000",
        "path": "/v1/message",
        "accountId": "313775346065",
        "protocol": "HTTP/1.1",
        "stage": "dev",
        "domainPrefix": "devapi",
        "requestTimeEpoch": 1638203690940,
        "requestId": "c1e74436-e6c2-44cc-b8b5-9c691be83a84",
        "identity": {
            "cognitoIdentityPoolId": null,
            "accountId": null,
            "cognitoIdentityId": null,
            "caller": null,
            "sourceIp": "217.123.117.17",
            "principalOrgId": null,
            "accessKey": null,
            "cognitoAuthenticationType": null,
            "cognitoAuthenticationProvider": null,
            "userArn": null,
            "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36",
            "user": null
        },
        "domainName": "devapi.oaswitchboard.org",
        "apiId": "fz01whbs3g"
    },
    "body": "{\"header\":{\"to\":{\"address\":\"https://ror.org/0030f2a11\"},\"persistent\":true,\"type\":\"p1\",\"subject\":\"Publication/Payment Settlement Notification\",\"template\":\"templates/Message.html\"},\"data\":{\"settlement\":null,\"authors\":[{\"isCorrespondingAuthor\":true,\"lastName\":\"Lorem\",\"firstName\":\"Ipsum\",\"initials\":\"LI\",\"ORCID\":\"https://orcid.org/111\",\"email\":\"dev@oaswitchboard.org\",\"collaboration\":\"PROJ001\",\"listingorder\":1,\"creditroles\":[\"methodology\"],\"affiliation\":\"Universitätsklinikum Erlangen\",\"institutions\":[{\"name\":\"Universitätsklinikum Erlangen\",\"ror\":\"https://ror.org/0030f2a11\"}]}],\"article\":{\"grants\":[{\"name\":\"GRANT001\",\"id\":\"001\"}],\"manuscript\":{\"dates\":{\"submission\":\"2021-11-23\",\"acceptance\":\"2021-11-24\",\"publication\":\"2021-11-25\"},\"id\":\"0001\"},\"preprint\":{\"title\":\"Nullam quis risus eget urna mollis ornare vel eu leo.\",\"id\":\"0001\"},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"Yes\"},\"title\":\"Nullam quis risus eget urna mollis ornare vel eu leo.\",\"type\":\"addendum\",\"acknowledgement\":\"Gates Cambridge Trust\",\"funders\":[{\"name\":\"Gates Cambridge Trust\",\"ror\":\"https://ror.org/033sn5p83\"}],\"submissionId\":\"0001\",\"doi\":\"11111\",\"doiurl\":\"https://doi.org/1111\"},\"journal\":{\"inDOAJ\":true,\"name\":\"Consectetur Malesuada Nullam\",\"id\":\"1111-0000\"},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"Consectetur Malesuada Nullam\",\"id\":\"1111\"}}}}",
    "isBase64Encoded": false
};