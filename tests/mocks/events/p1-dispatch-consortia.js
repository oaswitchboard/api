module.exports = {
    "Records": [
        {
            "messageId": "d5205add-ee29-4f87-89ef-cc49d06a4e04",
            "receiptHandle": "AQEBb7+pY8r2yoPNZ0Z4Hd3O6lrmyzdl4/4oH72FlFLIsXUJlgDrRcqstY1qwhOtqhIrqUoqYv1uHOuOoF7tz7adGAxJDskhFO5zdpp6Xduvf4A6ABmSEMpt1qlx4oIHndM51TQKoMkqxk4v3FuuI7VoU/sJXa3fq3fAL0R5t2wTl9/IFiZZRh7yUI/TNQfQPdAjm4dRyhQe/zUtM+UEPJqPIrQ3oF7RetUOplzhjN+4sP7LFbN7zuQXqU773esIaNRtRmNjlxnE0WYcsv2zrf1LhWqSZpeNOzrDbyTHOr2TRiuuxFIPgBCh/+aRMiJcPJ9BaSYdMVrrQhMKhXw8lgdj8Gu8hPTsjph8W58iqS+FmQ8tDBGLqTu1SmcM17FJVRVVEAe+Zjwm3A+qxCFs2Xle0MzPTe8u4yp2SqIrjNS0iCU=",
            "body": "{\"header\":{\"type\":\"p1\",\"from\":{\"id\":59},\"to\":{\"address\":\"https://ror.org/02c2kyt77\",\"name\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"},\"validity\":\"2022-12-01\",\"template\":\"templates/MessageRequest.html\",\"persistent\":true,\"key\":\"production/jats/unpacked/D1SM01408K_PA/D1SM01408K.xml\",\"meta\":{\"prioragreement\":true,\"source\":\"JATS\",\"status\":\"PUBLICATION\"}},\"data\":{\"authors\":[{\"lastName\":\"Pihlajamaa\",\"firstName\":\"Ilian\",\"initials\":\"IP\",\"ORCID\":\"https://orcid.org/0000-0003-3779-4281\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"}],\"affiliation\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"},{\"lastName\":\"de Bruijn\",\"firstName\":\"René\",\"initials\":\"RdB\",\"ORCID\":\"https://orcid.org/0000-0002-7220-4630\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"}],\"affiliation\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"},{\"lastName\":\"van der Schoot\",\"firstName\":\"Paul\",\"initials\":\"PvdS\",\"ORCID\":\"https://orcid.org/0000-0001-5521-9622\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"}],\"affiliation\":\"Group of Soft Matter and Biological Physics, Eindhoven University of Technology\"}],\"article\":{\"title\":\"Continuum percolation in colloidal dispersions of hard nanorods in external axial and planar fields\",\"grants\":[{\"name\":\"193.117\"}],\"manuscript\":{\"dates\":{\"submission\":\"2021-09-30\",\"acceptance\":\"2021-11-08\",\"publication\":\"2021-11-08\"},\"id\":\"d1sm01408k\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / hybrid journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"},\"type\":\"research-article\",\"funders\":[{\"name\":\"Nederlandse Organisatie voor Wetenschappelijk Onderzoek\"}],\"submissionId\":\"d1sm01408k\",\"doi\":\"10.1039/d1sm01408k\",\"doiurl\":\"https://doi.org/10.1016/j.tsf.2021.138599\"},\"journal\":{\"name\":\"Soft Matter\",\"id\":\"1744-6848\",\"inDOAJ\":false},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"RSC open access agreement\",\"id\":\"\"}},\"settlement\":{\"charges\":{}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1638368238118",
                "SenderId": "AROAUSDTX2GI6RLYRQTFW:oasb-switchboard-prod-messages-validate-connectors",
                "ApproximateFirstReceiveTimestamp": "1638368243118"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"8d79a78d-3ddb-4cfa-a4ca-c6c6c8324ae2\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfBody": "785d9fd4e6ff0db2f5975a8dec1805a2",
            "md5OfMessageAttributes": "140f384510e7752d0a4d3ef9a0db5d7b",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-production",
            "awsRegion": "eu-central-1"
        }
    ]
}