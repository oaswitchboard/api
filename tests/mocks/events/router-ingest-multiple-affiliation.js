module.exports = {
    "resource": "/message",
    "path": "/v2/message",
    "httpMethod": "POST",
    "headers": {
        "accept": "application/json, text/plain, */*",
        "content-type": "application/json",
        "Host": "api.oaswitchboard.org",
        "User-Agent": "axios/0.21.4",
        "X-Amzn-Trace-Id": "Root=1-63bd376d-3a52a0124a39ff5052e49a6b",
        "X-Forwarded-For": "35.156.64.60",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "accept": [
            "application/json, text/plain, */*"
        ],
        "content-type": [
            "application/json"
        ],
        "Host": [
            "api.oaswitchboard.org"
        ],
        "User-Agent": [
            "axios/0.21.4"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-63bd376d-3a52a0124a39ff5052e49a6b"
        ],
        "X-Forwarded-For": [
            "35.156.64.60"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "queryStringParameters": null,
    "multiValueQueryStringParameters": null,
    "pathParameters": null,
    "stageVariables": null,
    "requestContext": {
        "resourceId": "bs1fmi",
        "resourcePath": "/message",
        "httpMethod": "POST",
        "extendedRequestId": "ehWZMGuGliAFjnQ=",
        "requestTime": "10/Jan/2023:10:01:17 +0000",
        "path": "/v2/message",
        "accountId": "313775346065",
        "protocol": "HTTP/1.1",
        "stage": "v2-prod",
        "domainPrefix": "api",
        "requestTimeEpoch": 1673344877897,
        "requestId": "92f2fe7c-f704-4e46-aeab-a15e0292e6fe",
        "domainName": "api.oaswitchboard.org",
        "apiId": "f3fxxxxx",
        "authorizer": {
            "claims": {
                "sub": "37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f",
            },
        },
    },
    "body": "{\"header\":{\"type\":\"e1\",\"from\":{\"id\":4},\"to\":{\"address\":\"https://ror.org/03yrrjy16\",\"name\":\"University of Southern Denmark\"},\"validity\":\"2024-01-10\",\"persistent\":true,\"createdby\":\"Private datastore message handler\"},\"data\":{\"authors\":[{\"lastName\":\"Muskova\",\"firstName\":\"Elona\",\"initials\":\"EM\",\"ORCID\":\"\",\"email\":\"emuskova@rambler.ru\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Severtsov Institute of Ecology and Evolution RAS\",\"ror\":\"https://ror.org/0577sef82\"}],\"affiliation\":\"Severtsov Institute of Ecology and Evolution RAS\",\"currentaddress\":[]},{\"lastName\":\"Nadezhdin\",\"firstName\":\"Rob\",\"initials\":\"EH\",\"ORCID\":\"\",\"email\":\"xxx.xxx@xxx.com\",\"collaboration\":\"\",\"listingorder\":5,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Whale and Dolphin Conservation\"}],\"affiliation\":\"Whale and Dolphin Conservation\",\"currentaddress\":[]},{\"lastName\":\"Gatesov\",\"firstName\":\"Bill\",\"initials\":\"BG\",\"ORCID\":\"\",\"email\":\"bg@gmail.com\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"University of Southern Denmark\",\"ror\":\"https://ror.org/03yrrjy16\"}],\"affiliation\":\"University of Southern Denmark\",\"currentaddress\":[]},{\"lastName\":\"Jobsov\",\"firstName\":\"Ilya\",\"initials\":\"IJ\",\"ORCID\":\"\",\"email\":\"ij@rambler.ua\",\"collaboration\":\"\",\"listingorder\":4,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Severtsov Institute of Ecology and Evolution RAS\",\"ror\":\"https://ror.org/0577sef82\"}],\"affiliation\":\"Severtsov Institute of Ecology and Evolution RAS\",\"currentaddress\":[]},{\"lastName\":\"Renatove\",\"firstName\":\"Olga\",\"initials\":\"RO\",\"ORCID\":\"0000-0003-1533-3817\",\"email\":\"ro@gmail.com\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"University of Southern Denmark\",\"ror\":\"https://ror.org/03yrrjy16\"}],\"affiliation\":\"University of Southern Denmark\",\"currentaddress\":[]}],\"article\":{\"title\":\"Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum\",\"grants\":[{\"name\":\"NGS-61285R-19\"}],\"manuscript\":{\"dates\":{\"submission\":\"2023-01-03\"},\"id\":\"RSPB-2022-2589\"},\"preprint\":{},\"vor\":{\"license\":\"CC BY-not specified\",\"publication\":\"transformative journal\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"other\"},\"funders\":[{\"name\":\"National Geographic Society\",\"ror\":\"https://ror.org/04bqh5m06\"}],\"type\":\"research-article\",\"acknowledgement\":\"\",\"originaltype\":\"Research\",\"submissionId\":\"RSPB-2022-2589\",\"doi\":\"\"},\"journal\":{\"name\":\"Proceedings B\",\"id\":\"1471-2954\",\"inDOAJ\":false},\"charges\":{\"prioragreement\":false,\"currency\":\"GBP\",\"fees\":{\"apc\":{\"type\":\"per article APC list price\",\"name\":\"estimated\",\"amount\":1700},\"total\":{\"name\":\"estimated\",\"amount\":1700}}}}}",
    "isBase64Encoded": false
}
