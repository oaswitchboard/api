module.exports = {
    "Records": [
        {
            "messageId": "383da496-fccb-4457-8326-3ceac912e909",
            "receiptHandle": "AQEB11qqYRfxDK5/Ot7pQLy8tN5RW2YDYZ/B+JhUVFKHgjMUInkNS6peExwtSqjwytRJQm6MjFXAurgIbadE1uPQtZ2oA3y/9yb0cUs6weyKxLkcWJ/A11XREvVUiIG4ntqbj801BqZ2YJJhr9wTDUPThLqOm11XEMgg2sARgXyAlvlplbOvMoSl38n0qwUE+o0SBE46S8/cnBe2fYjtnvx4O8j5U47xeXSsDQBCgd2Huzw0lL9tofbhAVs+lYd7+fdOM+V4/TxDP4JLk/O+cQbFL2VCyUwBoepNzBmh86My1Mip/7bZ5z0h0H1OzU5srUbICDNr2MmlCj+v9jQ5lElQThEMjnECdQTjIi5ur7+UUMpVpZdv46kVSsGiYquk6UBuHeFb40jsZoUmtiR9gJAjv7PtqRlwqinKs15genuXM1o=",
            "body": "{\"header\":{\"type\":\"p1\",\"from\":{\"id\":2},\"to\":{\"address\":\"erasmusmc.nl\",\"name\":\"Erasmus MC\"},\"validity\":\"2022-11-19\",\"template\":\"templates/MessageRequest.html\",\"persistent\":true,\"key\":\"development/jats/unpacked/6408455/keab769.xml\",\"meta\":{\"prioragreement\":true,\"source\":\"JATS\",\"status\":\"PUBLICATION\"}},\"data\":{\"authors\":[{\"lastName\":\"Smeele\",\"firstName\":\"Hieronymus T W\",\"initials\":\"HTWS\",\"ORCID\":\"https://orcid.org/0000-0001-7724-7712\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Department of Rheumatology\"}],\"affiliation\":\"Department of Rheumatology\"},{\"lastName\":\"Neuman\",\"firstName\":\"Rugina I\",\"initials\":\"RIN\",\"ORCID\":\"\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine\"},{\"name\":\"The Netherlands\"}],\"affiliation\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine,The Netherlands\"},{\"lastName\":\"Berenguer\",\"firstName\":\"Cecile\",\"initials\":\"CB\",\"ORCID\":\"\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Department of Rheumatology\"}],\"affiliation\":\"Department of Rheumatology\"},{\"lastName\":\"Danser\",\"firstName\":\"A H Jan\",\"initials\":\"AHJD\",\"ORCID\":\"\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":4,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine\"}],\"affiliation\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine\"},{\"lastName\":\"Visser\",\"firstName\":\"Willy\",\"initials\":\"WV\",\"ORCID\":\"\",\"email\":\"willy.visser@erasmusmc.nl\",\"collaboration\":\"\",\"listingorder\":5,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine\"},{\"name\":\"The Netherlands\"}],\"affiliation\":\"Department of Internal Medicine; Division of Pharmacology and Vascular Medicine,The Netherlands\"},{\"lastName\":\"Dolhain\",\"firstName\":\"Radboud J E M\",\"initials\":\"RJEMD\",\"ORCID\":\"\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":6,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Department of Rheumatology\"}],\"affiliation\":\"Department of Rheumatology\"}],\"article\":{\"title\":\"Comment on: The sFlt-1 to PlGF ratio in pregnant women with rheumatoid arthritis: impact of disease activity and sulfasalazine use. Reply\",\"grants\":[],\"manuscript\":{\"dates\":{\"submission\":\"2021-06-29\",\"acceptance\":\"2021-09-06\",\"publication\":\"2021-10-22\"},\"id\":\"keab769\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / hybrid journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"other\"},\"type\":\"letter\",\"funders\":[{\"name\":\"ReumaNederland (LLP project number: LLP-26)\"}],\"submissionId\":\"keab769\",\"doi\":\"10.1093/rheumatology/keab769\",\"doiurl\":\"Hieronymus T. W. Smeele and Rugina I. Neuman contributed equally to this work.Correspondence to: Willy Visser, Department of Internal Medicine, Room Ee-1418B, Erasmus Medical Center, PO Box 2040, 3000 CA Rotterdam, The Netherlands. E-mail: willy.visser@erasmusmc.nl\"},\"journal\":{\"name\":\"Rheumatology\",\"id\":\"1462-0332\",\"inDOAJ\":false},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"DUTCH UNIVERSITIES AND ACADEMIC HOSPITALS (VSNU)\"}},\"settlement\":{\"charges\":{}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1637332199387",
                "SenderId": "AROAUSDTX2GISNFNZL7MF:oasb-switchboard-dev-messages-validate-connectors",
                "ApproximateFirstReceiveTimestamp": "1637332204437"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"7489e78c-b40e-4140-bd45-860e0ad11634\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "699fcf8f9a1448e74ebb2e1e6b826c7d",
            "md5OfBody": "f8d6b891c31e896bfabaa692bbdb73f6",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-development",
            "awsRegion": "eu-central-1"
        }
    ]
}
