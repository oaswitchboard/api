module.exports = {
  message: {
    header: {
      type: "e1",
      from: { id: 4 },
      to: {
        address: "https://ror.org/03yrrjy16",
        name: "University of Southern Denmark",
      },
      meta: {
        source: "Private datastore message handler",
      },
      validity: "2024-01-10",
      persistent: true,
      createdby: "Private datastore message handler",
    },
    data: {
      authors: [
        {
          lastName: "Muskova",
          firstName: "Elona",
          initials: "EM",
          ORCID: "",
          email: "emuskova@rambler.ru",
          collaboration: "",
          listingorder: 3,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: "Severtsov Institute of Ecology and Evolution RAS",
              ror: "https://ror.org/0577sef82",
            },
          ],
          affiliation: "Severtsov Institute of Ecology and Evolution RAS",
          currentaddress: [],
        },
        {
          lastName: "Nadezhdin",
          firstName: "Rob",
          initials: "EH",
          ORCID: "",
          email: "xxx.xxx@xxx.com",
          collaboration: "",
          listingorder: 5,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [{ name: "Whale and Dolphin Conservation" }],
          affiliation: "Whale and Dolphin Conservation",
          currentaddress: [],
        },
        {
          lastName: "Gatesov",
          firstName: "Bill",
          initials: "BG",
          ORCID: "",
          email: "bg@gmail.com",
          collaboration: "",
          listingorder: 2,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: "University of Southern Denmark",
              ror: "https://ror.org/03yrrjy16",
            },
            {
              name: "University of Copenhagen",
              ror: "https://ror.org/035b05819",
            },
          ],
          affiliation: "University of Southern Denmark",
          currentaddress: [],
        },
        {
          lastName: "Jobsov",
          firstName: "Ilya",
          initials: "IJ",
          ORCID: "",
          email: "ij@rambler.ua",
          collaboration: "",
          listingorder: 4,
          creditroles: [],
          isCorrespondingAuthor: false,
          institutions: [
            {
              name: "Severtsov Institute of Ecology and Evolution RAS",
              ror: "https://ror.org/0577sef82",
            },
          ],
          affiliation: "Severtsov Institute of Ecology and Evolution RAS",
          currentaddress: [],
        },
        {
          lastName: "Renatove",
          firstName: "Olga",
          initials: "RO",
          ORCID: "0000-0003-1533-3817",
          email: "ro@gmail.com",
          collaboration: "",
          listingorder: 1,
          creditroles: [],
          isCorrespondingAuthor: true,
          institutions: [
            {
              name: "University of Southern Denmark",
              rorid: "https://ror.org/03yrrjy16",
            },
          ],
          affiliation: "University of Southern Denmark",
          currentaddress: [],
        },
      ],
      article: {
        title:
          "Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum",
        grants: [{ name: "NGS-61285R-19" }],
        manuscript: {
          dates: { submission: "2023-01-03" },
          id: "RSPB-2022-2589",
        },
        preprint: {},
        vor: {
          license: "CC BY-not specified",
          publication: "transformative journal",
          deposition: "open repository, like PMC",
          researchdata: "other",
        },
        funders: [
          {
            name: "National Geographic Society",
            ror: "https://ror.org/04bqh5m06",
            fundref: "fundref.org/1234",
          },
        ],
        type: "research-article",
        acknowledgement: "",
        originaltype: "Research",
        submissionId: "RSPB-2022-2589",
        doi: "",
      },
      journal: { name: "Proceedings B", id: "1471-2954", inDOAJ: false },
      charges: {
        prioragreement: true,
        agreement: {
          name: "Prior agreement with JISC",
          id: "12345",
        },
      },
    },
  },
  sender: "37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f",
  receivers: [
    {
      organisation: {
        name: "University of Copenhagen",
        ror: "https://ror.org/035b05819",
      },
      type: "PRIMARY",
      routes: [
        {
          receivers: {
            sentto: "University of Copenhagen",
            receivedby: "KB",
            email: "xxx@kb.dk",
            webhook: "false",
            apikey: "",
          },
          receiver: 93,
          organisation: 1331,
          action: "CONSORTIA",
          notification: "EMAIL",
          type: "consortia",
          persistent: true,
        },
      ],
    },
  ],
};
