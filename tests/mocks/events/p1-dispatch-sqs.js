module.exports = {
    "Records": [
        {
            "messageId": "ba0c5e49-4034-46da-9a1f-e15fd58d3bb0",
            "receiptHandle": "AQEBEeEPinti5uJwYl+C0EXB/fCLiERrJZ+lJLGuhhiUhwpQIJzZ8QYL/7wWPYvoqhajqHyz4vlBO1JbmaKO+shoXV52O30UK9kXxNDQhq39fHCb3JO0iENyl7LJOPTCYwQYU7ltEtvg/v83x7c9rkREw3vtZBR1etx/pXxHBz33gdlrtsFjQTUN8V9Wgk6u71JZSuy8v/n6e7Qej7ZV+FXAyJ0FYLW1V6geMSG6fVjSwVwYkCN5knZHqFYZ1TYL74eCdOlQgTORBgE/M8qC+4F3NSy8gkTxv115ZctLX06FY14pxHYsDksRmuJ5NzOkHR6UtO4UqXpCZc0zhcozqb2JEDYJixy/7egvp15yPpiHoAnWU2z9vriNA4q3lI+jP2D7AXJQ8L/S9YSh2tlivpgHPT0e7nhY0ZFGg9Q7XsWChz8=",
            "body": "{\"header\":{\"type\":\"p1\",\"from\":{\"id\":59},\"to\":{\"address\":\"https://ror.org/02ma7yv75\",\"name\":\"Karlsruhe Institute of Technology (KIT), Institute of Nanotechnology (INT), Hermann-von-Helmholtz-Platz 1\"},\"validity\":\"2022-11-25\",\"template\":\"templates/MessageRequest.html\",\"persistent\":true,\"key\":\"production/jats/unpacked/D1CC04888K_PA/D1CC04888K.xml\",\"meta\":{\"prioragreement\":true,\"source\":\"JATS\",\"status\":\"PUBLICATION\"}},\"data\":{\"authors\":[{\"lastName\":\"Miskolczy\",\"firstName\":\"Zsombor\",\"initials\":\"ZM\",\"ORCID\":\"https://orcid.org/0000-0003-2259-5052K-8469-2018\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"Monkey University\", \"ror\":\"https://ror.org/000004\"}, {\"name\":\"Deutsche Wissenshaft\", \"ror\":\"https://ror.org/000002\"}],\"affiliation\":\"biczok.laszlo@ttk.hu\"},{\"lastName\":\"Megyesi\",\"firstName\":\"Mónika\",\"initials\":\"MM\",\"ORCID\":\"https://orcid.org/0000-0001-5080-7310C-3921-2019\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"biczok.laszlo@ttk.hu\"}],\"affiliation\":\"biczok.laszlo@ttk.hu\"},{\"lastName\":\"Sinn\",\"firstName\":\"Stephan\",\"initials\":\"SS\",\"ORCID\":\"https://orcid.org/0000-0002-9676-9839H-2976-2012\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":false,\"institutions\":[{\"name\":\"frank.biedermann@kit.edu\"}],\"affiliation\":\"frank.biedermann@kit.edu\"},{\"lastName\":\"Biedermann\",\"firstName\":\"Frank\",\"initials\":\"FB\",\"ORCID\":\"https://orcid.org/0000-0002-1077-6529\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":4,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"frank.biedermann@kit.edu\"}],\"affiliation\":\"frank.biedermann@kit.edu\"},{\"lastName\":\"Biczók\",\"firstName\":\"László\",\"initials\":\"LB\",\"ORCID\":\"https://orcid.org/0000-0003-2568-5942A-6526-2009\",\"email\":\"\",\"collaboration\":\"\",\"listingorder\":5,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"biczok.laszlo@ttk.hu\"}],\"affiliation\":\"biczok.laszlo@ttk.hu\"}],\"article\":{\"title\":\"Simultaneous analyte indicator binding assay (SBA) for the monitoring of reversible host–guest complexation kinetics\",\"grants\":[{\"name\":\"Emmy-Noether Programme BI1850/2K123995\"}],\"manuscript\":{\"dates\":{\"submission\":\"2021-08-31\",\"acceptance\":\"2021-10-08\",\"publication\":\"2021-10-08\"},\"id\":\"d1cc04888k\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / hybrid journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"},\"type\":\"brief-report\",\"funders\":[{\"name\":\"Nemzeti Kutatási és Technológiai Hivatal\", \"fundref\":\"501100001659\"}, {\"name\":\"Deutsche Forschungsgemeinschaft\", \"ror\":\"https://ror.org/018mejw64\"}],\"submissionId\":\"d1cc04888k\",\"doi\":\"10.1039/d1cc04888k\",\"doiurl\":\"https://doi.org/10.1073/pnas.0706407105\"},\"journal\":{\"name\":\"Chemical Communications\",\"id\":\"1364-548X\",\"inDOAJ\":false},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"RSC open access agreement\",\"id\":\"\"}},\"settlement\":{\"charges\":{}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1637862373343",
                "SenderId": "AROAUSDTX2GI6RLYRQTFW:oasb-switchboard-prod-messages-validate-connectors",
                "ApproximateFirstReceiveTimestamp": "1637862378343"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"ecf28762-b25e-4d0c-8c7e-c0c6d001357c\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "140f384510e7752d0a4d3ef9a0db5d7b",
            "md5OfBody": "0c2de98bc387603d39e88642cfb504c9",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-production",
            "awsRegion": "eu-central-1"
        }
    ]
}

