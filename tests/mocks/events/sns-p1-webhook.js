module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:success_acceptance:5ec3e895-cff3-4788-b7a1-bf7b35aef5d3",
            "Sns": {
                "Type": "Notification",
                "MessageId": "cce5d091-078b-5d6c-bae6-8a0bd1b62480",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:success_acceptance",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"header\":{\"to\":{\"id\":97,\"organisation\":762,\"email\":\"webhook@silverink.nl\",\"webhook\":\"https://peerwith.com\",\"type\":\"consortia\",\"address\":\"https://ror.org/041kmwe10\",\"name\":\"Imperial College London\",\"persistent\":true},\"persistent\":true,\"type\":\"p1\",\"subject\":\"Publication Notification message\",\"template\":\"templates/Message.html\",\"from\":{\"id\":108,\"type\":\"institution\",\"ror\":\"https://ror.org/008xxew50\",\"address\":\"https://ror.org/008xxew50\",\"name\":\"Vrije Universiteit Test\",\"persistent\":true},\"routing\":\"CONSORTIA\",\"notification\":\"WEBHOOK\",\"p1\":\"20220303144309-fcd19e05-1b28-46b7-bbc6-f77b2d07e8d5\"},\"data\":{\"settlement\":{},\"authors\":[{\"isCorrespondingAuthor\":true,\"lastName\":\"Test\",\"firstName\":\"Ignore\",\"initials\":\"YV\",\"listingorder\":1,\"creditroles\":[\"conceptualization\"],\"affiliation\":\"Imperial College London\",\"institutions\":[{\"name\":\"Imperial College London\",\"ror\":\"https://ror.org/041kmwe10\"}]}],\"article\":{\"grants\":[{}],\"manuscript\":{\"dates\":{}},\"preprint\":{},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"Yes\"},\"doi\":\"10.1080/15588742.2015\"},\"journal\":{\"inDOAJ\":true,\"name\":\"Pol Sociology\",\"id\":\"1111-2222\"},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"Test\",\"id\":\"test\"}}}}",
                "Timestamp": "2022-03-03T14:43:09.911Z",
                "SignatureVersion": "1",
                "Signature": "ZGByWCwQlx3EAcV+Qn9FFGc3ALDeT4BoX5nLQg2DXzcpk5bOn1C0ZhRV4Gki3k2DM3TjVQH+EZKnRZEceCuXXMgVBcZ2ITnHepqHqQp75mYV84zMz7YtbDyp/wYWQKLqTCoqSv5ieKp/pZao8rIE1EjPSY7Shxu1gT1r07yp3+kpvIKagYs3MKz+1gTMIgCCcvFsOcblGxF/IXosRo3I8nZCJ7wXCiuz172wu4NteHzC0VLVKFdusClKu5LJoSlJx75Ff5qsa7qHfj2XUzw0xjqefVjftGLgMosNfIzWmqtzfxTR9mA70Jm6SWwVnSBMidfsTGYbjauLoNeLU/DR/A==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:success_acceptance:5ec3e895-cff3-4788-b7a1-bf7b35aef5d3",
                "MessageAttributes": {}
            }
        }
    ]
}