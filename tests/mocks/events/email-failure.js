module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-failure:b73d0523-bd0f-47e9-850f-3e43d8a24e35",
            "Sns": {
                "Type": "Notification",
                "MessageId": "159d3bce-d98d-570f-b769-678655311e67",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-failure",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"header\":{\"type\":\"p1\",\"version\":\"v2\",\"e1\":\"20210408093308-91736471-2858-4d30-f031-ccf3c5f08466\",\"e2\":\"20210408095220-c904fe13-54c1-466d-a184-9df116f19f91\",\"to\":{\"address\":\"https://ror.org/000002\",\"name\":\"Royal Society of Middle-Earth\"},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"subject\":\"OA Switchboard Eligibility Enquiry request\",\"persistent\":true,\"from\":{\"id\":7,\"ror\":\"https://ror.org/000000\",\"participant\":7,\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"email\":\"yuri@appetence.nl\"},\"p1\":\"20230123132407-86b70b2a-2a96-4bde-fe80-5656e8d6e272\",\"routingdata\":{\"extraction\":\"SENDTO\",\"rules\":\"PRIMARY\",\"route\":\"DLQ\",\"notification\":\"NONE\"},\"meta\":{\"group\":\"7cf02f9b-656d-40d4-f152-e7eb29749b45\",\"routing\":{\"action\":\"DLQ\",\"notification\":\"NONE\"}}},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"email\":\"frodo.baggins@vu.nl\",\"isCorrespondingAuthor\":true,\"creditroles\":[\"conceptualization\"],\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true},\"settlement\":{\"charges\":{\"type\":\"online\",\"value\":\"by creditcard\"}},\"charges\":{\"prioragreement\":true,\"charged\":false,\"agreement\":{\"name\":\"Important agreement\",\"id\":\"ME-UNIV-000001\"},\"currency\":\"EUR\",\"fees\":{\"apc\":{\"name\":\"firm\",\"type\":\"per article APC list price\",\"amount\":1000},\"extra\":[{\"type\":\"submission format\",\"amount\":100},{\"type\":\"license choice\",\"amount\":100}],\"total\":{\"name\":\"firm\",\"amount\":1200}}}},\"id\":7041}",
                "Timestamp": "2023-01-23T13:24:12.843Z",
                "SignatureVersion": "1",
                "Signature": "PoxWkjAP/nTeiG2OwE9X6oweroMyQZf4PXnyit0hByBK8o6TO5cXxbdBH1HPKKQn4t9J4OEJBrsm7w50eM0PpuZFjsP+0wMJR6RQakXV4aI76yKENAP4YFf6SBDs9AIJ55R6eyKlhKau99RwHVgLulrjVKQzeg5Mm0t/36H0AtDnbX/e17lCRYWcmTXmoRrfryNq2kk91PGxmjQVUZjTygrEA/WdErjf8U7Dz89jAvaHHRNt48AD7jX2q123wPBoKOBZA/VCYXKc21bl5G9Im17zkr4Hb9GJU4tt8PERJ+ChlveChs+4dev93UQRlpMewY0fSpEYRG2J91Kp0CWPiw==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:v2-dev-failure:b73d0523-bd0f-47e9-850f-3e43d8a24e35",
                "MessageAttributes": {}
            }
        }
    ]
}
