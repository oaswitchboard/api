module.exports = {
    "Records": [
        {
            "messageId": "a99785ca-b596-407c-9e9a-e14e9ccd9453",
            "receiptHandle": "AQEBfYWTScKVc1A9bohaH9MIRy0AjY7ERLgctdg0n+eJXmpTBMzZRQMT/+9AiMN7UxaRd2OphrN5N9UmYBeM/GPcCa2ILw/wuMTwC5KPvDyQxBCwvU6GrC4CniF5YIbAFsEHlSfY0rs4QxmF2ZeFqoAPezbojGCMjf78eS3+djj19pOl0J/J+JRUp/7pNft576uew45sP28KWkKnA5azHptcLd4lNlXxhoohL2ds9V7xYfMz0mNQ7PdU3o6DKnGZfdz6ipRD/uai5IWJcMBlSwacz0ls8R5AfNKUFgh1xIfuCE3JaQs9zaWV93MnJ8xpKtWQzlWNoIAiqCdNPt1LjaI3KtjEsLXP3JBzQR/+9H4nPkLmOW69EreCcJ+EyI82uzknWtkwvuXLnRVNAhyiWZ/RRuLLguwe8iugVc54dNnGn2c=",
            "body": "{\"header\":{\"type\":\"p1\",\"from\":{\"id\":2},\"to\":{\"address\":\"mpi-hd.mpg.de\",\"name\":\"Max Planck Institute for Nuclear Physics\"},\"validity\":\"2022-11-19\",\"template\":\"templates/MessageRequest.html\",\"persistent\":true,\"key\":\"production/jats/unpacked/6409844/ptab139.xml\",\"meta\":{\"prioragreement\":true,\"source\":\"JATS\",\"status\":\"PUBLICATION\"}},\"data\":{\"authors\":[{\"lastName\":\"Undagoitia\",\"firstName\":\"Teresa Marrod&#x00E1;n\",\"initials\":\"TMU\",\"ORCID\":\"\",\"email\":\"teresa.marrodan@mpi-hd.mpg.de\",\"collaboration\":\"\",\"listingorder\":1,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[],\"affiliation\":\"\"},{\"lastName\":\"Rodejohann\",\"firstName\":\"Werner\",\"initials\":\"WR\",\"ORCID\":\"\",\"email\":\"werner.rodejohann@mpi-hd.mpg.de\",\"collaboration\":\"\",\"listingorder\":2,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[],\"affiliation\":\"\"},{\"lastName\":\"Wolf\",\"firstName\":\"Tim\",\"initials\":\"TW\",\"ORCID\":\"\",\"email\":\"tim.wolf@mpi-hd.mpg.de\",\"collaboration\":\"\",\"listingorder\":3,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[],\"affiliation\":\"\"},{\"lastName\":\"Yaguna\",\"firstName\":\"Carlos E\",\"initials\":\"CEY\",\"ORCID\":\"\",\"email\":\"carlos.yaguna@uptc.edu.co\",\"collaboration\":\"\",\"listingorder\":4,\"creditroles\":[],\"isCorrespondingAuthor\":true,\"institutions\":[],\"affiliation\":\"\"}],\"article\":{\"title\":\"Laboratory limits on the annihilation or decay of dark matter particles\",\"grants\":[],\"manuscript\":{\"dates\":{\"publication\":\"2021-10-25\"},\"id\":\"ptab139\"},\"preprint\":{},\"vor\":{\"publication\":\"open access / pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"other\"},\"type\":\"research-article\",\"funders\":[],\"submissionId\":\"ptab139\",\"doi\":\"10.1093/ptep/ptab139\",\"doiurl\":\"teresa.marrodan@mpi-hd.mpg.dewerner.rodejohann@mpi-hd.mpg.detim.wolf@mpi-hd.mpg.decarlos.yaguna@uptc.edu.co\"},\"journal\":{\"name\":\"Progress of Theoretical and Experimental Physics\",\"id\":\"2050-3911\",\"inDOAJ\":false},\"charges\":{\"prioragreement\":true,\"agreement\":{\"name\":\"MAX PLANCK SOCIETY\"}},\"settlement\":{\"charges\":{}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1637340095134",
                "SenderId": "AIDAUSDTX2GI7KEDKIR5T",
                "ApproximateFirstReceiveTimestamp": "1637340100134"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"7489e78c-b40e-4140-bd45-860e0ad11634\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "699fcf8f9a1448e74ebb2e1e6b826c7d",
            "md5OfBody": "f239e078027a0c5b5e73c093c3df4ba4",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:sqs-incoming-development",
            "awsRegion": "eu-central-1"
        }
    ]
};
