module.exports = {
   "sender": "37db9b8e-e0f9-46e0-a2cd-c7e7ae64584f",
   "message": {
      "header": {
         "type": "e1",
         "from": {
            "id": 4
         },
         "to": {
            "address": "https://ror.org/03yrrjy16",
            "name": "University of Southern Denmark"
         },
         "validity": "2024-01-10",
         "persistent": true,
         "createdby": "Private datastore message handler"
      },
      "data": {
         "authors": [
            {
               "lastName": "Muskova",
               "firstName": "Elona",
               "initials": "EM",
               "ORCID": "",
               "email": "emuskova@rambler.ru",
               "collaboration": "",
               "listingorder": 1,
               "creditroles": [],
               "isCorrespondingAuthor": true,
               "institutions": [
                  {
                     "name": "Severtsov Institute of Ecology and Evolution RAS",
                  }
               ],
               "affiliation": "Severtsov Institute of Ecology and Evolution RAS",
               "currentaddress": []
            }
         ],
         "article": {
            "title": "Genetic and cultural evidence indicates a refugium for killer whales, Orcinus orca, off Japan during the Last Glacial Maximum",
            "grants": [
               {
                  "name": "NGS-61285R-19"
               }
            ],
            "manuscript": {
               "dates": {
                  "submission": "2023-01-03"
               },
               "id": "RSPB-2022-2589"
            },
            "preprint": {

            },
            "vor": {
               "license": "CC BY-not specified",
               "publication": "transformative journal",
               "deposition": "open repository, like PMC",
               "researchdata": "other"
            },
            "funders": [],
            "type": "research-article",
            "acknowledgement": "",
            "originaltype": "Research",
            "submissionId": "RSPB-2022-2589",
            "doi": ""
         },
         "journal": {
            "name": "Proceedings X",
            "id": "2222-1111",
            "inDOAJ": false
         },
         "charges": {
            "prioragreement": false,
            "currency": "GBP",
            "fees": {
               "apc": {
                  "type": "per article APC list price",
                  "name": "estimated",
                  "amount": 1700
               },
               "total": {
                  "name": "estimated",
                  "amount": 1700
               }
            }
         }
      }
   }
}