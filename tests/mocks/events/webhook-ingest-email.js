module.exports = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success:bae0103b-0129-42d0-9223-3ce6dafb45e3",
            "Sns": {
                "Type": "Notification",
                "MessageId": "dee7dedf-35fb-5c5d-9013-8297d0d46974",
                "TopicArn": "arn:aws:sns:eu-central-1:313775346065:v2-dev-success",
                "Subject": "Forwarding to distribution topics",
                "Message": "{\"id\":7072,\"header\":{\"type\":\"p1\",\"version\":\"v2\",\"e1\":\"20210408093308-91736471-2858-4d30-f031-ccf3c5f08466\",\"e2\":\"20210408095220-c904fe13-54c1-466d-a184-9df116f19f91\",\"to\":{\"address\":\"https://ror.org/008xxew50\",\"name\":\"VU Amsterdam\"},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"subject\":\"OA Switchboard Eligibility Enquiry request\",\"persistent\":true,\"from\":{\"id\":7,\"ror\":\"https://ror.org/000000\",\"participant\":7,\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"email\":\"yuri@appetence.nl\"},\"p1\":\"20230123194130-23937a12-1890-4f86-dc6a-b7c4a8ec9b91\",\"meta\":{\"group\":\"e98daac4-c890-4653-da1b-f1f81bae0981\",\"routing\":{\"action\":\"CONSORTIA\",\"notification\":\"EMAIL\",\"organisation\":39,\"receiver\":5}},\"routingdata\":{\"extraction\":\"INSTITUTION\",\"rules\":\"CC\",\"route\":\"CONSORTIA\",\"notification\":\"EMAIL\"},\"pio\":true},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"isCorrespondingAuthor\":true,\"creditroles\":[\"conceptualization\"],\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true}},\"type\":\"p1\",\"created\":\"2023-01-23T19:41:32.714Z\",\"sender\":7,\"state\":\"SENT\",\"receiver\":5,\"organisation\":39,\"hash\":\"aa64106264a20540c66f903b9694d3ca\"}",
                "Timestamp": "2023-01-23T19:41:35.134Z",
                "SignatureVersion": "1",
                "Signature": "iUHsDQTKVn9A80ekAh0SfIm1zizOW40kSrAYSWL91E6mtL0Avv+9OcAaRCPBdbBh0lPY8quM6++rR9bBi6udQX5pplUCNTzWTPTstl71SBG3AEeRqD2Xfq0pOXOGOakHwFIWhz3UVADoKwjJYzgr0dK42mXLTorCd6XMplIWXYYlgym/FczbeER0CUnxDX2zAgmyShMyDLOuxn9I0BvTBu8E9DerWZxzCxSsWf9w3UZH5S+obV6l2BtfCAwBKEhvQjhDimYbpEAAWGdagGziCLpGRaIqDa4O1rk1Bk5QmqzMEEIzSXiiTia2yW1L/NVtOVffJu6MtCajImQbagFkzg==",
                "SigningCertUrl": "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
                "UnsubscribeUrl": "https://sns.eu-central-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:313775346065:v2-dev-success:bae0103b-0129-42d0-9223-3ce6dafb45e3",
                "MessageAttributes": {}
            }
        }
    ]
}