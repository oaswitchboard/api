module.exports = {
    "Records": [
        {
            messageId: '9a0cd834-31b0-45ca-a799-b8a5f6f2d877',
            receiptHandle: 'AQEB6VRGnGbDEKb1IgP5bfDY0xUWrteostX68/qEfChCJLerfIooOu5AM43Zl2SwsuJMgEqJTMNJHzNQESAi3Ax1RNH0GeZp3PDi4klR0s4bZWPdMTrBCIyhFjuFGqq/AE7JaZ9w7RQUhacg6fVmUYOSk100uPqTETQ5xpjW9lU95XreoltOHwG1j+lGc8VHUyCJv8LWADKUPAFpQIeaQZL4xAceHWzaFrjEUZ2XXxEsX5Y2pXHyv2/xBzP34we9HgNh+ONemq0sHAIVsP3ouBY7FxidW9hcv1i3PwksSFR0Qz+J3Wpvd/cOkTNxL/56L4AH',
            body: '{"header":{"type":"p1","from":{"id":59},"to":{"address":"https://ror.org/00vtgdb53","name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"},"validity":"2022-11-29","template":"templates/MessageRequest.html","persistent":true,"key":"production/jats/unpacked/D1MH01663F_PA/D1MH01663F.xml","meta":{"prioragreement":true,"source":"JATS","status":"PUBLICATION"}},"data":{"authors":[{"lastName":"Bara","firstName":"Dominic","initials":"DB","ORCID":"https://orcid.org/0000-0002-4494-0227","email":"","collaboration":"","listingorder":1,"creditroles":[],"isCorrespondingAuthor":false,"institutions":[{"name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"affiliation":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"},{"lastName":"Meekel","firstName":"Emily G.","initials":"EGM","ORCID":"https://orcid.org/0000-0001-9452-0653","email":"","collaboration":"","listingorder":2,"creditroles":[],"isCorrespondingAuthor":false,"institutions":[{"name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"affiliation":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"},{"lastName":"Pakamorė","firstName":"Ignas","initials":"IP","ORCID":"","email":"","collaboration":"","listingorder":3,"creditroles":[],"isCorrespondingAuthor":false,"institutions":[{"name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"affiliation":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"},{"lastName":"Wilson","firstName":"Claire","initials":"CW","ORCID":"https://orcid.org/0000-0002-0090-5374","email":"","collaboration":"","listingorder":4,"creditroles":[],"isCorrespondingAuthor":false,"institutions":[{"name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"affiliation":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"},{"lastName":"Ling","firstName":"Sanliang","initials":"SL","ORCID":"https://orcid.org/0000-0003-1574-7476","email":"","collaboration":"","listingorder":5,"creditroles":[],"isCorrespondingAuthor":false,"institutions":[{"name":"Advanced Materials Research Group, Faculty of Engineering, University of Nottingham"}],"affiliation":"Advanced Materials Research Group, Faculty of Engineering, University of Nottingham"},{"lastName":"Forgan","firstName":"Ross S.","initials":"RSF","ORCID":"https://orcid.org/0000-0003-4767-6852","email":"","collaboration":"","listingorder":6,"creditroles":[],"isCorrespondingAuthor":true,"institutions":[{"name":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"affiliation":"WestCHEM School of Chemistry, University of Glasgow, Joseph Black Building, University Avenue"}],"article":{"title":"Exploring and expanding the Fe-terephthalate metal–organic framework phase space by coordination and oxidation modulation","grants":[{"name":"677289EP/R029431/1UnassignedUnassigned"}],"manuscript":{"dates":{"submission":"2021-07-29","acceptance":"2021-10-12","publication":"2021-10-13"},"id":"d1mh01663f"},"preprint":{},"vor":{"publication":"open access / hybrid journal","license":"CC BY","deposition":"open repository, like PMC","researchdata":"data available on request"},"type":"brief-report","funders":[{"name":"H2020 European Research CouncilEngineering and Physical Sciences Research CouncilRoyal SocietyUniversity of Glasgow"}],"submissionId":"d1mh01663f","doi":"10.1039/d1mh01663f","doiurl":"https://doi.org/10.1021/jacs.0c10777"},"journal":{"name":"Materials Horizons","id":"2051-6355","inDOAJ":false},"charges":{"prioragreement":true,"agreement":{"name":"RSC open access agreement","id":""}},"settlement":{"charges":{}}}}',
            attributes: {
            ApproximateReceiveCount: '1',
            SentTimestamp: '1638189119462',
            SequenceNumber: '18866120488291824128',
            MessageGroupId: 'JATS',
            SenderId: 'AROAUSDTX2GI2IY5AICGP:oasb-connectors-jats-rsc-prod-manuscript-p1',
            MessageDeduplicationId: '0562dee22fe7a2d395b6937ffae364d8',
            ApproximateFirstReceiveTimestamp: '1638189119462'
            },
            messageAttributes: {
            Sender: {
                stringValue: '"8d79a78d-3ddb-4cfa-a4ca-c6c6c8324ae2"',
                stringListValues: [],
                binaryListValues: [],
                dataType: 'String'
            }
            },
            md5OfMessageAttributes: '140f384510e7752d0a4d3ef9a0db5d7b',
            md5OfBody: 'c466c4cd41438a7b215c227808387133',
            eventSource: 'aws:sqs',
            eventSourceARN: 'arn:aws:sqs:eu-central-1:313775346065:sqs-ingest-connectors-prod.fifo',
            awsRegion: 'eu-central-1'
        }
    ]
}