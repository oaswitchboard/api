module.exports = {
    "Records": [
        {
            "messageId": "e5cb6fb6-78eb-47e3-9931-ab97b102dc22",
            "receiptHandle": "AQEByDmI/ZuGD0RogTxrY87mixdCM5iu7Ng1JVgftu6AuQvysgc+uCenF05cqE6gHewuuWd/nY70AU3pgcngzJJ1RYsohIbcPRwUk5qy1jCpaivNJWHj33957tCnCOS/1hlV03dNXC1nyrNDT6rM5TU7gdGBaAY7M9pGV7NQZNtjboRPdtYCkpvtvCStc/fHxd2vQUjoPgLgdzCZflwtyzdusyZGSJ5n4a/lBbGbyp3cGi7Y3i4K6L6vknYqzn6EhqJaYc1/NtrZJzYp9Jov+q3RUNKts3MGFNV6tbjzsC/yhQ76C1trRa5OM2S6db02pU+D+iK/tG29C9N1stcCpOrYPsRqRz00HnFvZ4mzKd6DI+x9OfQfohkCrjEk5gYA+Pa9fAvFUjIkgPBdXvRqsnQHpw==",
            "body": "{\"header\":{\"type\":\"e1\",\"version\":\"v2\",\"to\":{\"id\":2,\"organisation\":6,\"email\":\"copernicus@silverink.nl\",\"webhook\":\"https://webhook.site/8b22889a-8a29-45ee-913a-e6a04a0a6fea\",\"type\":\"institution\",\"address\":\"https://ror.org/000002\",\"name\":\"Oxford University Press\",\"persistent\":true},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"persistent\":true,\"from\":{\"id\":7,\"type\":\"publisher\",\"ror\":\"https://ror.org/000000\",\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"persistent\":true},\"routing\":\"DEFAULT\",\"notification\":\"EMAIL\",\"e1\":\"20220608112301-a82c1d54-5d41-43f1-8ccd-a0171046b461\"},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"email\":\"frodo.baggins@vu.nl\",\"isCorrespondingAuthor\":true,\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\",\"grid\":\"grid.12380.38\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true},\"settlement\":{\"charges\":{}},\"charges\":{\"prioragreement\":false,\"charged\":true,\"currency\":\"EUR\",\"fees\":{\"apc\":{\"name\":\"estimated\",\"type\":\"per article APC list price\",\"amount\":1000},\"extra\":[{\"type\":\"submission format\",\"amount\":100},{\"type\":\"license choice\",\"amount\":100}],\"total\":{\"name\":\"estimated\",\"amount\":1200}}}}}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "SentTimestamp": "1654688097490",
                "SenderId": "AIDAUSDTX2GI7KEDKIR5T",
                "ApproximateFirstReceiveTimestamp": "1654688097506"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"Callback webhook\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfBody": "43dd67d3cc8c3dc5853b3ae8acbc1617",
            "md5OfMessageAttributes": "fb2a424e7fc224493648dbd63d3c33e9",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:v2-dev-callback",
            "awsRegion": "eu-central-1"
        }
    ]
}