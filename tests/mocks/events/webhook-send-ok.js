module.exports = {
    "Records": [
        {
            "messageId": "8d1922be-ee87-4225-b782-b21011f35bf5",
            "receiptHandle": "AQEB8FImG+0zdi7pRz+Qfru7fbdJiiDoi5LZKsXyZz6ygCGrDHAqFjnx4PMmFkGrpSXAevjgVsye4/ri8Yp8hoaATB+xVKCfiJtqYzTwlcgAQ6F5b8L/ilyigCFnRaLzbzN88dvvVF5fbJq2D9xWCp+FeacHAl/HptbH2qzWtaet5wLyumShoyvUPwnT19x3Qxz/sAxc7FDr+maxDM8X12Y7gEQCxYrCvA+3tmxGHRc/NFC3tTURKUQxFWF4DxMOyaM2u5IllezKTH3uRXf78zSwDSNSrzY3uOT/vF2BvBhVQ6gnEz0yXlLeSrVUJwe5JCM6Ylqr0g3MRlLvqHOpEV5+9ipj1Qp2AbxXFdv/0fKqLnayI5d1It0k58juMP6PtkW+J9Ba5oO9StWB2HR7lizZ+A==",
            "body": "{\"id\":7072,\"header\":{\"type\":\"p1\",\"version\":\"v2\",\"e1\":\"20210408093308-91736471-2858-4d30-f031-ccf3c5f08466\",\"e2\":\"20210408095220-c904fe13-54c1-466d-a184-9df116f19f91\",\"to\":{\"address\":\"https://ror.org/008xxew50\",\"name\":\"VU Amsterdam\"},\"ref\":\"XXXX-0002\",\"validity\":\"2022-04-01\",\"subject\":\"OA Switchboard Eligibility Enquiry request\",\"persistent\":true,\"from\":{\"id\":7,\"ror\":\"https://ror.org/000000\",\"participant\":7,\"address\":\"https://ror.org/000000\",\"name\":\"Appetence Elitex\",\"email\":\"yuri@appetence.nl\"},\"p1\":\"20230123194130-23937a12-1890-4f86-dc6a-b7c4a8ec9b91\",\"meta\":{\"group\":\"e98daac4-c890-4653-da1b-f1f81bae0981\",\"routing\":{\"action\":\"CONSORTIA\",\"notification\":\"WEBHOOK\",\"organisation\":39,\"receiver\":5}},\"routingdata\":{\"extraction\":\"INSTITUTION\",\"rules\":\"CC\",\"route\":\"CONSORTIA\",\"notification\":\"WEBHOOK\",\"id\":2020},\"pio\":true},\"data\":{\"authors\":[{\"listingorder\":1,\"lastName\":\"Baggins\",\"firstName\":\"Frodo\",\"initials\":\"FB\",\"ORCID\":\"0000-0000-0000-0000\",\"isCorrespondingAuthor\":true,\"creditroles\":[\"conceptualization\"],\"institutions\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"currentaddress\":[{\"name\":\"VU Amsterdam\",\"ror\":\"https://ror.org/008xxew50\",\"isni\":\"0000000417549227\"}],\"affiliation\":\"VU Amsterdam\"}],\"article\":{\"title\":\"The International relations of Middle-Earth\",\"type\":\"research-article\",\"funders\":[{\"name\":\"Aragorn Foundation\",\"ror\":\"https://ror.org/999999\"},{\"name\":\"Middle-Earth Thinktank\",\"ror\":\"https://ror.org/888888\"}],\"acknowledgement\":\"Aragorn Foundation, Middle-Earth Thinktank\",\"grants\":[{\"name\":\"Generous grant\",\"id\":\"GD-000-001\"}],\"doi\":\"https://doi.org/00.0000/mearth.0000\",\"submissionId\":\"00.0000\",\"manuscript\":{\"dates\":{\"submission\":\"2021-02-01\",\"acceptance\":\"2021-03-01\",\"publication\":\"2021-04-01\"},\"title\":\"The International relations of Middle-Earth\",\"id\":\"00.0000-000\"},\"preprint\":{\"title\":\"The International relations of Middle-Earth\",\"url\":\"https://arxiv.org/00.0000-000\",\"id\":\"00.0000-000\"},\"vor\":{\"publication\":\"pure OA journal\",\"license\":\"CC BY\",\"deposition\":\"open repository, like PMC\",\"researchdata\":\"data available on request\"}},\"journal\":{\"name\":\"Middle Earth papers\",\"id\":\"0000-0000\",\"inDOAJ\":true}},\"type\":\"p1\",\"created\":\"2023-01-23T19:41:32.714Z\",\"sender\":7,\"state\":\"SENT\",\"receiver\":5,\"organisation\":39,\"hash\":\"aa64106264a20540c66f903b9694d3ca\"}",
            "attributes": {
                "ApproximateReceiveCount": "1",
                "AWSTraceHeader": "Root=1-63cee2e0-cf2c0cacd295def92e3d3d70;Parent=a61ef1f936ce37ef;Sampled=1",
                "SentTimestamp": "1674502897619",
                "SenderId": "AROAUSDTX2GIWMFLQH6SH:oasb-switchboard-v2-dev-messages-webhook-ingest",
                "ApproximateFirstReceiveTimestamp": "1674502897623"
            },
            "messageAttributes": {
                "Sender": {
                    "stringValue": "\"Receiver webhook\"",
                    "stringListValues": [],
                    "binaryListValues": [],
                    "dataType": "String"
                }
            },
            "md5OfMessageAttributes": "9bcb70b903bf8a39f71414d77756aa9f",
            "md5OfBody": "446451f442a330022c2b9a7de7239380",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:eu-central-1:313775346065:v2-dev-webhook",
            "awsRegion": "eu-central-1"
        }
    ]
}