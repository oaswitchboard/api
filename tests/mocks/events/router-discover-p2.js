module.exports = {
    "message": {
        "header": {
            "type": "p2",
            "p1":"20210423092232-282e278d-752c-44d8-bf3a-f6fb4869e57d",
            "to": {
                "address": "https://ror.org/03yrrjy16"
            },
            "from": {
                "id": 6
            },

            "validity": "31-12-2020",
            "persistent": true
        },
        "data": {
            "authors": [{
                    "listingorder": 1,
                    "lastName": "Duck",
                    "firstName": "Donald",
                    "initials": "D.D.",
                    "ORCID": "12345678",
                    "email": "donald@duck.com",
                    "creditrole": ["conceptualization"],
                    "isCorrespondingAuthor": true,
                    "collaboration": "PROJECT-001",
                    "institutions": [{
                            "name": "Max Planck Institute",
                            "ror": "ABCD999"
                        },
                        {
                            "name": "Institute of Exact Sciences",
                            "ror": "ABCD991"
                        }
                    ]
                },
                {
                    "listingorder": 2,
                    "lastName": "Dagobert",
                    "firstName": "Donald",
                    "initials": "D.D.",
                    "ORCID": "12345678",
                    "email": "dagobert@duck.com",
                    "creditrole": "conceptualization",
                    "isCorrespondingAuthor": true,
                    "collaboration": "PROJECT-001",
                    "institutions": [{
                            "name": "Duck foundation",
                            "ror": "ABCD999"
                        },
                        {
                            "name": "Institute of Non-exact Sciences",
                            "ror": "ABCDFFF"
                        }
                    ]
                }
            ],

            "article": {
                "title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod.",
                "type": "introduction",
                "funders": [{
                        "name": "Duckstad Foundation",
                        "ror": "ABCD123"
                    },
                    {
                        "name": "Katrien and Donald Duck Foundation",
                        "ror": "ABCD345"
                    }
                ],
                "grants": [{
                    "name": "Quantum superposition of viral electrons in COVID-22",
                    "id": "AAAAACX"
                }]
            },

            "doi": "DOI",

            "submissionId": "1234886",

            "manuscript": {
                "title": "Quantum superposition of viral electrons in COVID-22",
                "id": "01234567CFFD",
                "dates": {
                    "submission": "01-10-2019",
                    "acceptance": "02-10-2019",
                    "publication": "03-10-2019"
                }
            },

            "preprint": {
                "title": "Quantum superposition of viral electrons in COVID-22",
                "url": "https://archivx.org/covid-22",
                "id": "01234567CFFD"
            },

            "journal": {
                "name": "Quantum superpositions",
                "issn": "AAAAACX-123-ASFS-CCC",
                "inDOAJ": true
            },

            "vor": {
                "publication": "open access / pure OA journal",
                "license": "CC BY",
                "deposition": "PMC",
                "researchdata": "Yes"
            },

            "charges": {
                "apc": {
                    "type": "per article APC list price",
                    "currency": "EUR",
                    "extra": [{
                            "name": "submission format",
                            "amount": 100
                        },
                        {
                            "name": "license choice",
                            "amount": 100
                        }
                    ],
                    "total": {
                        "apc": [
                            {
                                "name": "firm",
                                "amount": 10000
                            },
                            {
                                "name": "estimated",
                                "amount": 10000
                            },
                            {
                                "name": "unknown",
                                "amount": 0
                            }
                        ],
                        "fees": [
                            {
                                "name": "firm",
                                "amount": 10000
                            },
                            {
                                "name": "estimated",
                                "amount": 10000
                            },
                            {
                                "name": "unknown",
                                "amount": 0
                            }
                        ]
                    }
                }
            }
        }
    }
}