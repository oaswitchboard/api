module.exports = {
   "message": {
       "header": {
           "type": "p1",
           "version": "v2",
           "to": {
               "address": "https://ror.org/03yrrjy16"
           },
           "validity": "2022-04-01",
           "subject": "OA Switchboard Eligibility Enquiry request",
           "persistent": true
       },
       "data": {
           "authors": [
               {
                   "firstName": "Jonathan",
                   "lastName": "Bricker",
                   "ORCID": "0000-0002-5694-8795",
                   "email": "jbricker@fredhutch.org",
                   "primary_contact": 1,
                   "affiliation": "Division of Public Health Sciences Fred Hutch Cancer Center ",
                   "isCorrespondingAuthor": true
               },
               {
                   "firstName": "Kristin",
                   "lastName": "Mull",
                   "ORCID": "0000-0002-7918-3078",
                   "email": "kmull@fredhutch.org",
                   "primary_contact": 0,
                   "affiliation": "Division of Public Health Sciences Fred Hutch Cancer Center ",
                   "isCorrespondingAuthor": false
               },
               {
                   "firstName": "Margarita",
                   "lastName": "Santiago-Torres",
                   "ORCID": "0000-0001-6051-3172",
                   "email": "msantiag@fredhutch.org",
                   "primary_contact": 0,
                   "affiliation": "Division of Public Health Sciences Fred Hutch Cancer Center ",
                   "isCorrespondingAuthor": false
               },
               {
                   "firstName": "Zhen",
                   "lastName": "Miao",
                   "ORCID": "0000-0001-8575-0879",
                   "email": "zhenm@uw.edu",
                   "primary_contact": 0,
                   "affiliation": "Department of Statistics University of Washington ",
                   "isCorrespondingAuthor": false
               },
               {
                   "firstName": "Jonathan",
                   "lastName": "Bricker",
                   "ORCID": "0000-0002-5694-8795",
                   "email": "jbricker@fredhutch.org",
                   "primary_contact": 1,
                   "affiliation": "Department of Psychology University of Washington ",
                   "isCorrespondingAuthor": false
               },
               {
                   "firstName": "David M",
                   "lastName": "Vock",
                   "ORCID": "0000-0002-5459-9579",
                   "email": "vock@umn.edu",
                   "primary_contact": 0,
                   "affiliation": "Division of Biostatistics University of Minnesota ",
                   "isCorrespondingAuthor": false
               }
           ],
           "article": {
               "title": "Can a Single Variable Predict Early Dropout From Digital Health Interventions? Comparison of Predictive Models From Two Large Randomized Trials",
               "doi": "https://doi.org/00.0000/mearth.0000",
               "article_id": 43629,
               "vor": {
                   "publication": "pure OA journal",
                   "license": "CC BY",
                   "deposition": "open repository, like PMC",
                   "researchdata": "data available on request"
               }
           },
           "journal": {
               "name": "Journal of Medical Internet Research",
               "id": "1",
               "inDOAJ": true
           },
           "charges": {
               "prioragreemnt": false,
               "currency": "USD",
               "fees": {
                   "apc": {
                       "name": "firm",
                       "type": "per article APC list price",
                       "amount": 3350
                   },
                   "total": {
                       "name": "firm",
                       "amount": 3350
                   }
               }
           }
       }
   },
   "sender": "e48d7599-323c-415c-a108-7321e0cef5d1"
}