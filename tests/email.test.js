//@ts-check
const fs = require("fs");
const AWS = require("aws-sdk-mock");
const email = require(`../switchboard/email`);

const { Pool } = require('pg');
jest.mock('pg', () => {
  const mPool = {
	connect: function () {
		  return { query: jest.fn(), release: jest.fn(), };
	},
	query: jest.fn(),
	end: jest.fn(),
	on: jest.fn(),
	release: jest.fn(),
  };
  return { Pool: jest.fn(() => mPool) };
});

describe("Message mailer tests", () => {

    beforeAll(async () => {
		process.env.BASE_URL = "https://api.test.com";
		process.env.S3_BUCKET = "bucket";
		process.env.STAGE = "TEST";
        process.env.MAIL_BCC = "bcc@appetence.nl";
        process.env.MAIL_FROM = "from@appetence.nl";
        process.env.MAIL_REPLYTO = "replyto@appetence.nl";
	});

	afterAll(() => {
		delete process.env.BASE_URL;
        delete process.env.S3_BUCKET;
        delete process.env.STAGE;
		delete process.env.MAIL_BCC;
        delete process.env.MAIL_FROM;
        delete process.env.MAIL_REPLYTO;
	});

	describe("Send email", () => {

        beforeAll(() => {
			jest.mock('../switchboard/services/participants', () => ({
				findById: () => ({
					id: 7,
					email: 'email@appetence.nl',
					ror: 'https://ror.org/000000',
					type: 'publisher',
					webhook: 'https://swebhook.site/fd4d38b4-52fa-4f24-88ec-d70e3a9383b8',
					callback: 'https://swebhook.site/3c2a3a07-b281-4d72-8a5d-385a106e2658',
					apikey: null,
					persistent: true,
					oafund: false,
					invoice: '',
					rules: null,
					pio: true,
					institution: 'Appetence Elitex',
					name: 'Appetence Elitex'
				  }),
			}));
			jest.mock('../switchboard/services/organisations', () => ({
				findById: () => ({
					id: 7,
					name: 'Appetence Elitex',
					ror_id: 'https://ror.org/000000',
				  }),
			}));
            AWS.mock('SES', 'sendEmail', (params, callback) => ({
                ResponseMetadata: { RequestId: '11b31c83-9a89-46d1-b3e7-37c25d78eaba' },
                MessageId: '01070185dfb0d321-2be5b859-d315-41b3-a930-12d4e2b09dff-000000'
            }))
 		});

		afterEach(() => {
			jest.clearAllMocks();
		});

		test("Send P1 message", async () => {
            const event = require("../tests/mocks/events/email-p1");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(1);
			expect(body.skipped).toBe(0);
		});

		test("Send P1 PIO message", async () => {
            const event = require("../tests/mocks/events/email-success");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(1);
			expect(body.skipped).toBe(0);
		});

		test("Send P1 Autoreject message", async () => {
            const event = require("../tests/mocks/events/email-autoreject");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(1);
			expect(body.skipped).toBe(0);
		});

		test("Send P1 webhook message", async () => {
            const event = require("../tests/mocks/events/email-webhook");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(0);
			expect(body.skipped).toBe(1);
		});

		test("Send P1 none message", async () => {
            const event = require("../tests/mocks/events/email-none");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(0);
			expect(body.skipped).toBe(1);
		});

		test("Send E2 message", async () => {
            const event = require("../tests/mocks/events/email-e2");
			const result = await email.send(event);
			const body = JSON.parse(result.body);
			expect(body).toHaveProperty('success');
			expect(body).toHaveProperty('skipped');
			expect(body.success).toBe(1);
			expect(body.skipped).toBe(0);
		});
	});

});

