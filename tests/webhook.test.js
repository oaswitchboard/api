//@ts-check
const webhook = require(`../switchboard/webhook`);
const SQS = require('../libs/sqs');
import { faker } from "@faker-js/faker";

const { Pool } = require("pg");
jest.mock("pg", () => {
  const mPool = {
    connect: function () {
      return { query: jest.fn(), release: jest.fn() };
    },
    query: jest.fn(),
    end: jest.fn(),
    on: jest.fn(),
    release: jest.fn(),
  };
  return { Pool: jest.fn(() => mPool) };
});

jest.mock('../libs/sqs', () => ({
  formatMessageForRegularQueue: () => ({
    message: 'to be sent'
  }),
  sendMessage: () => ({
    ResponseMetadata: { RequestId: 'c0cdbaeb-20cb-4d61-a6f6-45d683651616' },
    MD5OfMessageBody: 'f9dedb85-d2a4-4080-a2dd-d17b0f70df9f',
    MD5OfMessageAttributes: '8e43e164-510c-488a-9899-09b75788a590',
    MessageId: '3cc2f8cf-c342-449b-a8bb-0367162ba88f',
    success: true
  }),
}));

describe("Message mailer tests", () => {
  beforeAll(async () => {
    process.env.SQS_WEBHOOK_URL = "https://api.test.com";
  });

  afterAll(() => {
    delete process.env.SQS_WEBHOOK_URL;
  });

  describe("Queue webhook event", () => {
    beforeEach(() => {
      let mockResponseMetadata = faker.datatype.uuid();
      let mockMD5OfMessageBody = faker.datatype.uuid();
      let mockD5OfMessageAttributes = faker.datatype.uuid();
      let mockMessageId = faker.datatype.uuid();
      jest.mock("../libs/sqs", () => ({
        sendMessage: () => ({
          ResponseMetadata: { RequestId: mockResponseMetadata },
          MD5OfMessageBody: mockMD5OfMessageBody,
          MD5OfMessageAttributes: mockD5OfMessageAttributes,
          MessageId: mockMessageId,
          success: true,
        }),
        formatMessageForRegularQueue: jest.fn(),
      }));
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test("Ingest message for webhook delivery", async () => {
      const event = require("../tests/mocks/events/webhook-ingest-ok");
      const result = await webhook.ingest(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body.success).toBe(1);
      expect(body.skipped).toBe(0);
    });

    test("Ignore non-webhook delivery message", async () => {
      const event = require("../tests/mocks/events/webhook-ingest-email");
      const result = await webhook.ingest(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body.success).toBe(0);
      expect(body.skipped).toBe(1);
    });
  });

  describe("Dispatch webhook event", () => {
    beforeEach(() => {
      jest.mock("../libs/webhook", () => ({
        dispatch: () => true,
      }));
    });

    afterEach(() => {
      jest.resetModules();
    });

    test("Dispatch valid Webhook message", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            url: "https://webhook.site/d01f3bea-519e-40d9-a560-1ca0789bfa93",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
          {
            id: 5,
            url: "https://webhook.site/d01f3bea-519e-40d9-a560-1ca0789bfa93",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(2);
      expect(body.skipped).toBe(0);
      expect(body.errors).toBe(0);
    });

    test("Dispatch valid Teams message", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            url: "https://lalala.webhook.office.com/webhookb2/19691982-a12c-4292-9f4d-465e7150f1b4@67bda7ee-fd80-41ef-ac91-358418290a1e/IncomingWebhook/aaaaa",
            apikey: "",
          },
          {
            id: 5,
            url: "https://lalala.webhook.office.com/webhookb2/19691982-a12c-4292-9f4d-465e7150f1b4@67bda7ee-fd80-41ef-ac91-358418290a1e/IncomingWebhook/aaaa",
            apikey: "",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(2);
      expect(body.skipped).toBe(0);
      expect(body.errors).toBe(0);
    });

    test("Dispatch valid Slack message", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            url: "https://hooks.slack.com/services/aaaaa/aaaaa/aaaaa",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
          {
            id: 5,
            url: "https://hooks.slack.com/services/aaaaa/aaaaa/aaaaa",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(2);
      expect(body.skipped).toBe(0);
      expect(body.errors).toBe(0);
    });

    test("Dispatch valid Chorus message", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            url: "http://api-staging.chorusaccess.org/xxx.",
            apikey: "",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(1);
      expect(body.skipped).toBe(0);
      expect(body.errors).toBe(0);
    });

    test("Do not dispatch a webhook message if the participant has no webhook url", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            participant_id: 5,
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(0);
      expect(body.skipped).toBe(1);
      expect(body.errors).toBe(0);
    });

    test("Do not dispatch a webhook message if the participant has no valid url", async () => {
      jest.mock("../switchboard/services/webhooks", () => ({
        findByRouteId: () => [
          {
            id: 3,
            participant_id: 5,
            url: "https://lalala.webhook.office.com/webhookb2/19691982-a12c-4292-9f4d-465e7150f1b4@67bda7ee-fd80-41ef-ac91-358418290a1e/IncomingWebhook/aaaa",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
          {
            id: 3,
            participant_id: 5,
            url: "Quick brown fox jumps over the lazy dog",
            apikey: "eyJraWQiOiJQY2FSa1E5YXVWeGN",
          },
        ],
      }));
      const event = require("../tests/mocks/events/webhook-send-ok");
      const result = await webhook.send(event);
      const body = JSON.parse(result.body);
      expect(body).toHaveProperty("success");
      expect(body).toHaveProperty("skipped");
      expect(body).toHaveProperty("errors");
      expect(body.success).toBe(1);
      expect(body.skipped).toBe(1);
      expect(body.errors).toBe(0);
    });
  });
});
