//@ts-check
const fs = require("fs");
const awsMock = require("aws-sdk-mock");
import { faker } from '@faker-js/faker';
const router = require(`../switchboard/router`);

let response = null;
let event = null;
let handler = null;

const primaryRoR = 'https://ror.org/03yrrjy16';
const affiliationRoR1 = 'https://ror.org/0577sef82';
const affiliationRoR2 = 'https://ror.org/035b05819';
const affiliationRoR3 = 'https://ror.org/035b05821';
const emptyRoR = undefined;
const funderRoR = 'https://ror.org/04bqh5m06';

const { Pool } = require('pg');
jest.mock('pg', () => {
  const mPool = {
	connect: function () {
		  return { query: jest.fn(), release: jest.fn(), };
	},
	query: jest.fn(),
	end: jest.fn(),
	on: jest.fn(),
	release: jest.fn(),
  };
  return { Pool: jest.fn(() => mPool) };
});

describe("Message router tests", () => {
	beforeAll(async () => {
		process.env.VERSION = "v2";
		process.env.STATEMACHINE = "aws::mock::state::machine::arn";
		process.env.STAGE = "test";
		process.env.SNS_SUCCESS = "success";
		process.env.SNS_REJECT = "reject";
		process.env.SNS_FAILURE = "failure";
	});

	afterAll(() => {
		delete process.env.VERSION;
        delete process.env.STATEMACHINE;
        delete process.env.STAGE;
        delete process.env.SNS_SUCCESS;
        delete process.env.SNS_SUCCESS;
        delete process.env.SNS_FAILURE;
	});

	describe("Ingesting message into state machine", () => {
		const mockStepFunctionsResponse = jest.fn();
		const mockParams = {
			stateMachineArn: "aws::mock::state::machine::arn",
			input: JSON.stringify({}),
		};

		beforeAll(() => {
			jest.mock('../switchboard/services/participants', () => ({
				findById: () => ({
					id: 5,
					cognito_id: '7489e78c-b40e-4140-bd45-860e0ad11634',
					email: 'x@appetence.nl',
					ror: 'https://ror.org/000002a',
					type: 'publisher',
					pio: true,
					institution: 'X Press',
					name: 'X Press'
				  }),
			}));
		});

		beforeEach(() => {
			mockStepFunctionsResponse.mockResolvedValue("started execution");
			awsMock.mock(
				"StepFunctions",
				"startExecution",
				mockStepFunctionsResponse
			);
		});

		afterAll(() => {
			jest.resetModules();
			jest.clearAllMocks();
		});

		test("Ingest valid e1 message through API", async () => {
			const event = require("../tests/mocks/events/router-ingest-multiple-affiliation");
			const response = await router.ingest(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			expect(response.body).toBe('Your e1 message has been successfully submitted');
		});

		test("Ingest valid RSC e1 message through API", async () => {
			const event = require("../tests/mocks/events/e1-rsc");
			const response = await router.ingest(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			expect(response.body).toBe('Your e1 message has been successfully submitted');
		});

		test("Ingest valid p1 message through API", async () => {
			const event = require("../tests/mocks/events/router-ingest-p1");
			const response = await router.ingest(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			expect(response.body).toBe('Your p1 message has been successfully submitted');
		});

		test("Ingest p1 message through API with empty address (sendto)", async () => {
			const event = require("../tests/mocks/events/router-ingest-p1-empty-sendto");
			const response = await router.ingest(event);
			expectValidHeaders(response.headers)
			expect(response.statusCode).toBe(400);
			const body = JSON.parse(response.body);
			expect(body.error).toBe(true);
			expect(body.errorMessage[0]).toBe('instance.header.to.address does not meet minimum length of 4');
		});

		test("Ingest invalid e1 message. Fail validation through API", async () => {
			const event = require("../tests/mocks/events/router-ingest-invalid");
			const router = require(`../switchboard/router`);
			const response = await router.ingest(event);
			expect(response.statusCode).toBe(400);
			const body = JSON.parse(response.body);
			expect(body.error).toBe(true);
			expect(body.errorMessage).toBe("Error validating message");
			expectValidHeaders(response.headers)
		});

		test("Ingest valid e1 message through SQS", async () => {
			const event = require("../tests/mocks/events/p1-pa-sqs");
			const response = await router.ingestconnector(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			const body = JSON.parse(response.body);
			expect(body.message).toBe('Your message(s) were validated');
			expect(body.failed).toBe(0);
			expect(body.passed).toBe(1);
		});

		test("Ingest invalid e1 message through SQS", async () => {
			const event = require("../tests/mocks/events/p1-pa-sqs-invalid");
			const response = await router.ingestconnector(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			const body = JSON.parse(response.body);
			expect(body.message).toBe('Your message(s) were validated');
			expect(body.failed).toBe(1);
			expect(body.passed).toBe(0);
			expect(Array.isArray(body.errors)).toBe(true);
			expect(body.errors.length).toBe(1);
		});

		test("Ingest invalid e1 message through SQS with empty address (sendto)", async () => {
			const event = require("../tests/mocks/events/p1-pa-sqs-empty-sendto");
			const response = await router.ingestconnector(event);
			expect(response.statusCode).toBe(200);
			expectValidHeaders(response.headers)
			const body = JSON.parse(response.body);
			expect(body.message).toBe('Your message(s) were validated');
			expect(body.failed).toBe(1);
			expect(body.passed).toBe(0);
			expect(Array.isArray(body.errors)).toBe(true);
			expect(body.errors.length).toBe(1);
		});

	});

	describe("Discover receivers", () => {
		test("Find receivers", async () => {
			const event = require("../tests/mocks/events/router-discover");
			const response = await router.discover(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);
			expect(expect(Array.isArray(response?.receivers)).toBe(true));
			expect(response?.receivers[0].type).toBe('PRIMARY');
			expect(response?.receivers[0].organisation.ror).toBe(primaryRoR);
			expect(response?.receivers[1].organisation.ror).toBe(affiliationRoR1);
			expect(response?.receivers[2].organisation.ror).toBe(emptyRoR);
			expect(response?.receivers[3].organisation.ror).toBe(affiliationRoR2);
			expect(response?.receivers[4].organisation.ror).toBe(funderRoR);
			expect(response?.receivers[1].type).toBe('INSTITUTION');
			expect(response?.receivers[2].type).toBe('INSTITUTION');
			expect(response?.receivers[3].type).toBe('INSTITUTION');
			expect(response?.receivers[4].type).toBe('FUNDER');
		});

		test("Find receivers with empty rors", async () => {
			const event = require("../tests/mocks/events/router-discover-empty");
			const response = await router.discover(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);
			expect(response?.receivers[0].type).toBe('PRIMARY');
			expect(response?.receivers[1].type).toBe('INSTITUTION');
			expect(response?.receivers[0].organisation.ror).toBe(primaryRoR);
			expect(response?.receivers[1].organisation.ror).toBe(undefined);
		});

		test("Find receivers for messages with empty author affiliations", async () => {
			const event = require("../tests/mocks/events/router-discover-no-institutions");
			const response = await router.discover(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);
			expect(response?.receivers[0].type).toBe('PRIMARY');
			expect(response?.receivers[0].organisation.ror).toBe(primaryRoR);
		});

		test("Find receivers for e2 message", async () => {
			const event = require("../tests/mocks/events/router-discover-e2");
			const response = await router.discover(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);
			expect(response?.receivers[0].type).toBe('PRIMARY');
			expect(response?.receivers[0].organisation.ror).toBe(primaryRoR);
		});

		test("Find receivers for p2 message", async () => {
			const event = require("../tests/mocks/events/router-discover-p2");
			const response = await router.discover(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);
			expect(response?.receivers[0].type).toBe('PRIMARY');
			expect(response?.receivers[0].organisation.ror).toBe(primaryRoR);
		});
	});

	describe("Enrich receivers", () => {
		beforeEach(() => {
			jest.mock('../switchboard/services/organisations', () => ({
				findBySecondaryIdentifier: () => ({
					ror_id: 'https://ror.org/035b05821',
				}),
				createOrganizationByFunder: jest.fn(),
			}));

		});
		afterEach(() => {
			jest.clearAllMocks();
		});
		test("Find missing ror ids", async () => {
			const event = require("../tests/mocks/events/router-enrich");
			const response = await router.enrich(event);
			expect(response?.message).toBe(event.message);
			expect(response?.sender).toBe(event.sender);

			// console.log('response?.receivers[5].organisation', response.receivers );

			expect(response?.receivers[4].organisation.ror).toBe(affiliationRoR3);
		});
	});

	describe("Fetch routes for all receivers", () => {
		beforeAll(() => {
			jest.mock('../switchboard/services/routes', () => ({
				findByOrganisationRoR: () => ([
					{
					  receivers: {
						route: 1,
						sentto: 'Erasmus University Rotterdam',
						receivedby: 'University B',
						email: 'university-a@appetence.nl,university-b@appetence.nl,university-c@appetence.nl'
					  },
					  participant_id: 93,
					  organisation_id: 1337,
					  action: 'CONSORTIA',
					  notification: 'NONE',
					  type: 'consortia',
					  persistent: true
					},
					{
					  receivers: {
						route: 2,
						sentto: 'Erasmus University Rotterdam',
						receivedby: 'University A',
						email: 'university-a@appetence.nl'
					  },
					  participant_id: 7,
					  organisation_id: 1337,
					  action: 'DEFAULT',
					  notification: 'EMAIL',
					  type: 'publisher',
					  persistent: true
					},
					{
					  receivers: {
						route: 3,
						sentto: 'Erasmus University Rotterdam',
						receivedby: 'University C',
						email: 'university-b@appetence.nl'
					  },
					  participant_id: 7,
					  organisation_id: 1337,
					  action: 'DEFAULT',
					  notification: 'WEBHOOK',
					  type: 'publisher',
					  persistent: true
					},
					{
					  receivers: {
						route: 6,
						sentto: 'Erasmus University Rotterdam',
						receivedby: 'University D',
						email: 'university-d@appetence.nl'
					  },
					  participant_id: 7,
					  organisation_id: 1337,
					  action: 'DEFAULT',
					  notification: 'WEBHOOK',
					  type: 'publisher',
					  persistent: true,
					  formatter: 'formatter',
					  condition: 'condition',
					},
					{
					  receivers: {
						route: 2,
						sentto: 'University of Southern Denmark',
						receivedby: 'University D',
						email: 'university-z@appetence.nl'
					  },
					  participant_id: 7,
					  organisation_id: 1337,
					  action: 'AUTOREJECT',
					  notification: 'EMAIL',
					  type: 'publisher',
					  persistent: true,
					  formatter: 'formatter',
					},
				]),
			}));
		});

		afterEach(() => {
			jest.clearAllMocks();
		});

		test("Add full to all receivers", async () => {
			const event = require("../tests/mocks/events/router-routes");
			const response = await router.route(event);
			// console.log('response', response);
			expect(response?.payload).toBe(event.payload);
			expect(response?.receivers.length).toBe(5);
			expect(response?.receivers[0].routes.length).toBe(5);
			for (let i = 0; i < response?.receivers.length; i++) {
				if (response?.receivers[i].type === 'PRIMARY') {
					for (const route of response?.receivers[i].routes) {
						expect(route).toHaveProperty('receiver');
						expect(route).toHaveProperty('organisation');
						expect(route).toHaveProperty('action');
						expect(route).toHaveProperty('notification');
						expect(route).toHaveProperty('type');
						expect(route).toHaveProperty('receivers');
						expect(route.receivers).toHaveProperty('route');
						expect(route.receivers).toHaveProperty('sentto');
						expect(route.receivers).toHaveProperty('receivedby');
						expect(route.receivers).toHaveProperty('email');
						if (route.action === 'DEFAULT') {
							expect(route.type).toBe('publisher');
							expect(route.persistent).toBe(true);
						}
						if (route.action === 'CONSORTIA') {
							expect(route.type).toBe('consortia');
							expect(route.persistent).toBe(true);
						}
						if (route.action === 'CUSTOM') {
							expect(route).toHaveProperty('formatter');
							expect(route).toHaveProperty('condition');
						}
					}
				}
			}
		});
	});

	describe("Merge messages", () => {
		beforeAll(() => {
			jest.mock('../switchboard/services/participants', () => ({
				findByCognitoId: () => ({
					id: 7,
					email: 'email@appetence.nl',
					ror: 'https://ror.org/000000',
					type: 'publisher',
					webhook: 'https://webhook.site/fd4d38b4-52fa-4f24-88ec-d70e3a9383b8',
					callback: 'https://swebhook.site/3c2a3a07-b281-4d72-8a5d-385a106e2658',
					apikey: null,
					persistent: true,
					oafund: false,
					invoice: '',
					rules: null,
					pio: true,
					institution: 'Appetence Elitex',
					name: 'Appetence Elitex'
				  }),
			}));
		});
		afterEach(() => {
			jest.clearAllMocks();
		});

		test("Merge messages and create deliverables P1 NPA", async () => {
			const event = require("./mocks/events/router-merge-p1-npa");
			const deliverables = await router.merge(event);

			expect(deliverables?.length).toBe(4);
			for (const deliverable of deliverables) {
				expectValidMessage(deliverable)
				expectValidHeader(deliverable.header)
				expectValidData(deliverable.data)

				if (deliverable.header.routingdata?.rules === 'PRIMARY' && deliverable.header.routingdata?.route === 'DEFAULT') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).not.toHaveProperty('pio');
					expect(deliverable.data).toHaveProperty('charges');
					expect(deliverable.data.charges).toHaveProperty('prioragreement');
					expectValidNoPriorAgreement(deliverable.data.charges)
				}

				if (deliverable.header.routingdata?.rules === 'PRIMARY' && deliverable.header.routingdata?.route === 'CONSORTIA') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'DEFAULT') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'DLQ') {
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'CONSORTIA') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.pio === true) {
					expect(deliverable.data).not.toHaveProperty('charges');
					expect(deliverable.data).not.toHaveProperty('settlement');
				}
			}
		});

		test("Merge messages and create deliverables P1 PA", async () => {
			const event = require("./mocks/events/router-merge-p1-pa");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(4);
			for (const deliverable of deliverables) {

				expectValidMessage(deliverable)
				expectValidHeader(deliverable.header)
				expectValidData(deliverable.data)

				if (deliverable.header.routingdata?.rules === 'PRIMARY' && deliverable.header.routingdata?.route === 'DEFAULT') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).not.toHaveProperty('pio');
					expect(deliverable.data).toHaveProperty('charges');
					expectValidPriorAgreement (deliverable.data.charges)
				}

				if (deliverable.header.routingdata?.rules === 'PRIMARY' && deliverable.header.routingdata?.route === 'CONSORTIA') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).not.toHaveProperty('pio');
					expect(deliverable.data).toHaveProperty('charges');
					expectValidPriorAgreement (deliverable.data.charges)
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'DEFAULT') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'DLQ') {
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.routingdata?.rules === 'CC' && deliverable.header.routingdata?.route === 'CONSORTIA') {
					expectValidRoutingData (deliverable.header)
					expect(deliverable.header).toHaveProperty('pio');
				}

				if (deliverable.header.pio === true) {
					expect(deliverable.data).not.toHaveProperty('charges');
					expect(deliverable.data).not.toHaveProperty('settlement');
				}
			}
		});


		// VALIDATION WITH CONDITIONS

		// Primary flow:

			// Message: P1-PA Not a PIO message. Condition requires PIO true.
			test("Merge messages with custom primary route. PIO = true filter. Should fail", async () => {
				const event = require("./mocks/events/router-merge-customrouting-primary-pio-false-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(0);
			});

			// Message: P1-PA Not a PIO message. Condition requires PIO false or undefined.
			test("Merge messages with custom primary route. PIO = false filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-primary-pio-false-cond-pio-false");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
			});

			// Message: P1-PA PIO message. Condition requires PIO true.
			test("Merge messages with custom primary route. PIO = true filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-primary-pio-true-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
			});

			// Message: P1-PA PIO message. Condition requires PIO false or undefined.
			test("Merge messages with custom primary route. PIO = true filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-primary-pio-true-cond-pio-false");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(0);
			});

			test("Merge messages with custom primary route. PIO = true filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-primary-pio-true-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
				for (const deliverable of deliverables) {
					expectValidMessage(deliverable)
					expectValidHeader(deliverable.header)
					expectValidRoutingData (deliverable.header)
					expectValidData(deliverable.data)
				}
			});

		// CC flow:

			// Message: P1-PA Not a PIO message. Condition requires PIO true.
			test("Merge messages with custom CC route. PIO = true filter. Should fail", async () => {
				const event = require("./mocks/events/router-merge-customrouting-cc-pio-false-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(0);
			});

			// Message: P1-PA Not a PIO message. Condition requires PIO false or undefined.
			test("Merge messages with custom CC route. PIO = false filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-cc-pio-false-cond-pio-false");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
			});

			// Message: P1-PA PIO message. Condition requires PIO true.
			test("Merge messages with custom CC route. PIO = true filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-cc-pio-true-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
			});

			// Message: P1-PA PIO message. Condition requires PIO false or undefined.
			test("Merge messages with custom CC route. PIO = true filter. Should create a message", async () => {
				const event = require("./mocks/events/router-merge-customrouting-cc-pio-true-cond-pio-false");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(0);
			});

			test("Merge messages with custom CC route. PIO = true filter. Should pass", async () => {
				const event = require("./mocks/events/router-merge-customrouting-cc-pio-true-cond-pio-true");
				const deliverables = await router.merge(event);
				expect(deliverables?.length).toBe(1);
				for (const deliverable of deliverables) {
					expectValidMessage(deliverable)
					expectValidHeader(deliverable.header)
					expectValidRoutingData (deliverable.header)
					expectValidData(deliverable.data)
				}
			});

		// END VALIDATION WITH CONDITIONS

		test("Merge messages with custom route. Primary receiver PIO-only filter. Data should be removed.", async () => {
			const event = require("./mocks/events/router-merge-customrouting-primary-transformation");
			const deliverables = await router.merge(event);
			for (const deliverable of deliverables) {
				expectValidHeader(deliverable.header)
				expectValidRoutingData (deliverable.header)
				expect(deliverable).not.toHaveProperty('data');
			}
		});

		test("Merge messages with custom route. Primary receiver PIO-only filter. Charges should be removed.", async () => {
			const event = require("./mocks/events/router-merge-customrouting-primary-charges");
			const deliverables = await router.merge(event);
			for (const deliverable of deliverables) {
				expectValidHeader(deliverable.header)
				expectValidRoutingData (deliverable.header)
				expect(deliverable.data).not.toHaveProperty('charges');
				expect(deliverable.data).not.toHaveProperty('settlement');
				expect(deliverable.header).toHaveProperty('pio');
				expect(deliverable.header.pio).toBe(true);
			}
		});

		test("Merge messages with custom primary route. Transform is not defined", async () => {
			const event = require("./mocks/events/router-merge-customrouting-primary-transformation-empty");
			const deliverables = await router.merge(event);
			for (const deliverable of deliverables) {
				expectValidHeader(deliverable.header)
				expectValidRoutingData (deliverable.header)
				expectValidData(deliverable.data)
			}
		});

		test("'Merge messages with custom route. Primary receiver PIO-only filter. Data should be removed.", async () => {
			const event = require("./mocks/events/router-merge-customrouting-cc-transformation");
			const deliverables = await router.merge(event);
			for (const deliverable of deliverables) {
				expectValidHeader(deliverable.header)
				expectValidRoutingData (deliverable.header)
				expect(deliverable).not.toHaveProperty('data');
			}
		});

		test("Merge messages with custom route. Primary receiver PIO-only filter. Charges should be removed.", async () => {
			const event = require("./mocks/events/router-merge-customrouting-cc-charges");
			const deliverables = await router.merge(event);
			for (const deliverable of deliverables) {
				expectValidHeader(deliverable.header)
				expectValidRoutingData (deliverable.header)
				expect(deliverable.data).not.toHaveProperty('charges');
				expect(deliverable.data).not.toHaveProperty('settlement');
				expect(deliverable.header).toHaveProperty('pio');
				expect(deliverable.header.pio).toBe(true);
			}
		});

		test("Merge messages. E1 messages should be ignored", async () => {
			const event = require("./mocks/events/router-merge-e1-consortia");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(0);
		});

		test("Merge messages. E1 NPA message. Should autoreject", async () => {
			const event = require("./mocks/events/router-merge-e1-autoreject-create");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(1);
			for (const deliverable of deliverables) {
				expect(deliverable.header.type).toBe('e2');
			}
		});

		test("Merge messages. E1 messages. Should not autoreject - amount lower than max", async () => {
			const event = require("./mocks/events/router-merge-e1-autoreject-skip");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(0);
		});

		test("Merge messages. E1 messages. Should not autoreject - prior agreement", async () => {
			const event = require("./mocks/events/router-merge-e1-autoreject-pa");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(0);
		});

		test("Merge messages. E1 message NPA without charges. Should not autoreject", async () => {
			const event = require("./mocks/events/router-merge-e1-autoreject-nocharges");
			const deliverables = await router.merge(event);
			expect(deliverables?.length).toBe(0);
		});

	});

	describe("Persist messages", () => {
		beforeAll(() => {
			let mockId = faker.datatype.number();
			jest.mock('../switchboard/services/messages', () => ({
				exists: () => (false),
				save: () => ({ id: mockId, data: {}, header: {} })
			}));
		});
		afterEach(() => {
			jest.clearAllMocks();
		});

		test("Persist messages", async () => {
			const event = require("../tests/mocks/events/router-persist");
			const saved = await router.persist(event);
			expect(saved?.length).toBe(7);
			for (const message of saved) {
				expect(message).toHaveProperty('id');
				expect(message).toHaveProperty('data');
				expect(message).toHaveProperty('header');
			}
		});
	});

	describe("Dispatch messages", () => {
		test("Dispatch messages", async () => {
			awsMock.mock('SNS', 'publish', 'test-message');
			const event = require("../tests/mocks/events/router-dispatch");
			const dispatched = await router.dispatch(event);
			expect(dispatched).toHaveProperty('success');
			expect(dispatched).toHaveProperty('errors');
			expect(dispatched?.success).toBe(7);
		});
	});

});

function isOfArray(values = [], value) {
	return values.includes(value);
}
function expectValidHeaders(headers) {
	expect(headers["Content-Type"]).toBe("application/json");
	expect(headers["Access-Control-Allow-Origin"]).toBe("*");
	expect(headers["Access-Control-Allow-Credentials"]).toBe(false);
}

function expectValidMessage(message) {
	expect(message).toHaveProperty('header');
	expect(message).toHaveProperty('data');
}

function expectValidData(data) {
	expect(data).toHaveProperty('authors');
	expect(data).toHaveProperty('article');
	expect(data).toHaveProperty('journal');
}

function expectValidHeader(header) {
	expectValidHeaderTo(header);
	expect(header).toHaveProperty('from');
	expect(header).toHaveProperty('meta');
	expect(header).toHaveProperty('routingdata');
	expect(header).toHaveProperty('validity');
	expect(header).toHaveProperty('p1');
	expect(header).toHaveProperty('persistent');
	expect(header.meta).toHaveProperty('group');
	expect(header.meta).toHaveProperty('routing');
	expect(header).toHaveProperty('routingdata');
	expect(isOfArray(['DEFAULT', 'DLQ', 'CONSORTIA','CUSTOM'], header.meta.routing.action)).toBe(true);
	expect(isOfArray(['EMAIL', 'NONE', 'WEBHOOK'], header.meta.routing.notification)).toBe(true);
}

function expectValidHeaderTo(header) {
	expect(header).toHaveProperty('to');
	expect(header.to).toHaveProperty('address');
	expect(header.to).toHaveProperty('name');
	expect(header.to).toHaveProperty('id');
}

function expectValidHeaderFrom(header) {
	expect(header).toHaveProperty('from');
	expect(header.from).toHaveProperty('name');
	expect(header.from).toHaveProperty('id');
}

function expectValidRoutingData (header) {
	expect(header.routingdata).toHaveProperty('extraction');
	expect(header.routingdata).toHaveProperty('rules');
	expect(header.routingdata).toHaveProperty('route');
	expect(header.routingdata).toHaveProperty('notification');
	expect(isOfArray(['SENDTO', 'INSTITUTION', 'FUNDER'], header.routingdata.extraction)).toBe(true);
	expect(isOfArray(['PRIMARY', 'CC', 'AUTOREJECT'], header.routingdata.rules)).toBe(true);
	expect(isOfArray(['DEFAULT','CONSORTIA', 'DLQ','CUSTOM'], header.routingdata.route)).toBe(true);
	expect(isOfArray(['EMAIL','WEBHOOK', 'NONE'], header.routingdata.notification)).toBe(true);
}

function expectValidPriorAgreement (charges) {
	expect(charges).toHaveProperty('prioragreement');
	expect(charges.prioragreement).toBe(true);
	expect(charges).toHaveProperty('agreement');
	expect(charges.agreement).toHaveProperty('name');
	expect(charges.agreement).toHaveProperty('id');
	expect(charges.agreement.name).toBe('Prior agreement with JISC');
	expect(charges.agreement.id).toBe('12345');
}

function expectValidNoPriorAgreement (charges) {
	expect(charges.prioragreement).toBe(false);
	expect(charges.currency).toBe('GBP');
	expect(charges).toHaveProperty('fees');
	expect(charges.fees).toHaveProperty('apc');
	expect(charges.fees.apc).toHaveProperty('type');
	expect(charges.fees.apc).toHaveProperty('name');
	expect(charges.fees.apc).toHaveProperty('amount');
	expect(charges.fees.apc.type).toBe('per article APC list price');
	expect(charges.fees.apc.name).toBe('estimated');
	expect(charges.fees.apc.amount).toBe(1700);
	expect(charges.fees).toHaveProperty('total');
	expect(charges.fees.total).toHaveProperty('name');
	expect(charges.fees.total).toHaveProperty('amount');
	expect(charges.fees.total.name).toBe('estimated');
	expect(charges.fees.total.amount).toBe(1700);
}
