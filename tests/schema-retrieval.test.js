const fs = require('fs');

let response = null;
let event = null;
let handler = null;

describe('Schema retriever tests', () => {

      beforeAll(async () => {
            process.env.VERSION = "v2";
            handler = require(`../switchboard/validator`);
      });

      describe('Retrieve schema', () => {

            test('E1', async () => {
            const event = require('../tests/mocks/events/schema-request-e1');
            const response = await handler.schema(event);
            expect(response.statusCode).toBe(200);
            expect(response.headers['Content-Type']).toBe('application/json');
            expect(response.headers['Access-Control-Allow-Origin']).toBe('*');
            expect(response.headers['Access-Control-Allow-Credentials']).toBe(false);
            });

            test('E2', async () => {
            const event = require('../tests/mocks/events/schema-request-e2');
            const response = await handler.schema(event);
            expect(response.statusCode).toBe(200);
            expect(response.headers['Content-Type']).toBe('application/json');
            expect(response.headers['Access-Control-Allow-Origin']).toBe('*');
            expect(response.headers['Access-Control-Allow-Credentials']).toBe(false);
            });

            test('P1', async () => {
            const event = require('../tests/mocks/events/schema-request-p1');
            const response = await handler.schema(event);
            expect(response.statusCode).toBe(200);
            expect(response.headers['Content-Type']).toBe('application/json');
            expect(response.headers['Access-Control-Allow-Origin']).toBe('*');
            expect(response.headers['Access-Control-Allow-Credentials']).toBe(false);
            });

            test('P2', async () => {
            const event = require('../tests/mocks/events/schema-request-p2');
            const response = await handler.schema(event);
            expect(response.statusCode).toBe(200);
            expect(response.headers['Content-Type']).toBe('application/json');
            expect(response.headers['Access-Control-Allow-Origin']).toBe('*');
            expect(response.headers['Access-Control-Allow-Credentials']).toBe(false);
            });

            test('XX - should fail', async () => {
            const event = require('../tests/mocks/events/schema-request-xx');
            const response = await handler.schema(event);
            expect(response.statusCode).toBe(404);
            expect(response.headers['Content-Type']).toBe('application/json');
            expect(response.headers['Access-Control-Allow-Origin']).toBe('*');
            expect(response.headers['Access-Control-Allow-Credentials']).toBe(false);
            });
    });

});
