const fs = require('fs');
const Validator = require('jsonschema').Validator;
const header = require(`${process.cwd()}/switchboard/schemas/v2/header`).header;
const e1 = require(`${process.cwd()}/switchboard/schemas/v2/e1`).schema;
const e2 = require(`${process.cwd()}/switchboard/schemas/v2/e2`).schema;
const p1 = require(`${process.cwd()}/switchboard/schemas/v2/p1`).schema;
const p2 = require(`${process.cwd()}/switchboard/schemas/v2/p2`).schema;

describe('Schema validations', () => {

	describe('E1', () => {

		test('E1 no prior agreement', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e1-no-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('E1 prior agreement without APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e1-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('E1 prior agreement with APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e1-prior-agreement-with-apc.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(0);
			});
		});

		// Validations

		test('E1 no prior agreement. No header. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-header.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "header"');
			});
		});

		test('E1 no prior agreement. No receiver address. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-address.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "address"');
			});
		});

		test('E1 no prior agreement. No receiver. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-to.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "to"');
			});
		});

		test('E1 no prior agreement. No type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "type"');
			});
		});

		test('E1 no prior agreement. Incorrect type enum. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: e1,e2,p1,p2');
			});
		});

		test('E1 no prior agreement. Incorrect persistence. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-persistent.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('E1 no prior agreement. Incorrect participant id. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-participant-id.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) integer');
			});
		});

		test('E1 no prior agreement. No data. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-data.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "data"');
			});
		});

		test('E1 no prior agreement. No authors. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-authors.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "authors"');
			});
		});

		test('E1 no prior agreement. Wrong creditrole. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-creditrole.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: conceptualization,methodology,software,validation,formal analysis,investigation,resources,data curation,writing,writing – original draft,writing – review & editing,visualization,supervision,project administration,funding acquisition,');
			});
		});


		test('E1 no prior agreement. Wrong creditrole type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-creditrole-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('E1 no prior agreement. Wrong article type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-article-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: abstract,addendum,announcement,article-commentary,book-review,books-received,brief-report,calendar,case-report,collection,correction,discussion,dissertation,editorial,in-brief,introduction,letter,meeting-report,news,obituary,oration,partial-retraction,product-review,protocol,rapid-communication,reply,reprint,research-article,retraction,review-article,translation,other');
			});
		});

		test('E1 no prior agreement. Wrong corresponding author type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-corresponding-author.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('E1 no prior agreement. Wrong institutions type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-institutions.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('E1 no prior agreement. Wrong funders type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-funders.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('E1 no prior agreement. Wrong grants type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-grants.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('E1 no prior agreement. No VOR publications. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-vor-publication.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "publication"');
			});
		});

		test('E1 no prior agreement. Wrong VOR publication type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-vor-publication.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: pure OA journal,hybrid journal,transformative journal,author has not decided yet,other');
			});
		});

		test('E1 no prior agreement. No VOR publications. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-no-vor-license.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "license"');
			});
		});

		test('E1 no prior agreement. Wrong VOR publication type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-vor-license.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: CC BY,CC BY-ND,CC BY-NC,CC BY-NC-SA,CC BY-NC-ND,CC BY-IGO,CC BY-not specified,CC BY-other,non-CC,CC0,not yet decided,not specified');
			});
		});

		test('E1 no prior agreement. Wrong VOR deposition. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-vor-deposition.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: open repository, like PMC,not deposited by publisher');
			});
		});

		test('E1 no prior agreement. Wrong VOR research data. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-vor-researchdata.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: Yes,No,not applicable,not yet decided,author will take care,data available on request,other');
			});
		});

		test('E1 no prior agreement. No journal id. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-journal-no-id.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "id"');
			});
		});

		test('E1 no prior agreement. No journal name. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-journal-no-name.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "name"');
			});
		});

		test('E1 no prior agreement. Incorrect prior agrement type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-prioragreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('E1 no prior agreement. Incorrect prior agrement currency. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-prioragreement-currency.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: EUR,USD,GBP,AUD,CAD,CHF,CNY,DKK,JPY,NOK,NZD,SEK');
			});
		});

		test('E1 no prior agreement with APC. Incorrect prior APC type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-apc-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: per article APC list price,per article type APC list price,per article page APC list price,other');
			});
		});

		test('E1 no prior agreement with APC. Incorrect prior APC name. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-apc-name.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: firm,estimated,unknown');
			});
		});

		test('E1 no prior agreement with APC. Incorrect prior APC amount. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/e1-no-prior-agreement-wrong-apc-amount.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) number');
			});
		});

	});

	describe('E2', () => {

		test('E2 no prior agreement', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e2-no-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e2);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('E2 prior agreement without APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e2-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e2);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('E2 prior agreement with APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/e2-prior-agreement-with-apc.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e2);
				expect(valid.errors.length).toBe(0);
			});
		});

	});

	describe('P1', () => {

		test('P1 no prior agreement', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/p1-no-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), p1);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('P1 prior agreement without APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/p1-prior-agreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), p1);
				expect(valid.errors.length).toBe(0);
			});
		});

		test('P1 prior agreement with APC', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/messages/samples/p1-prior-agreement-with-apc.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), p1);
				expect(valid.errors.length).toBe(0);
			});
		});

		/// Failure validations

		test('P1 no prior agreement. No header. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-header.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "header"');
			});
		});

		test('P1 no prior agreement. No receiver address. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-address.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "address"');
			});
		});

		test('P1 no prior agreement. No receiver. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-to.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "to"');
			});
		});

		test('P1 no prior agreement. No type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "type"');
			});
		});

		test('P1 no prior agreement. Incorrect type enum. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: e1,e2,p1,p2');
			});
		});

		test('P1 no prior agreement. Incorrect persistence. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-persistent.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('P1 no prior agreement. Incorrect participant id. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-participant-id.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) integer');
			});
		});

		test('P1 no prior agreement. No data. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-data.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "data"');
			});
		});

		test('P1 no prior agreement. No authors. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-authors.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "authors"');
			});
		});

		test('P1 no prior agreement. Wrong creditrole. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-creditrole.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('P1 no prior agreement. Wrong article type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-article-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: abstract,addendum,announcement,article-commentary,book-review,books-received,brief-report,calendar,case-report,collection,correction,discussion,dissertation,editorial,in-brief,introduction,letter,meeting-report,news,obituary,oration,partial-retraction,product-review,protocol,rapid-communication,reply,reprint,research-article,retraction,review-article,translation,other');
			});
		});

		test('P1 no prior agreement. Wrong corresponding author type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-corresponding-author.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('P1 no prior agreement. Wrong institutions type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-institutions.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('P1 no prior agreement. Wrong funders type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-funders.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('P1 no prior agreement. Wrong grants type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-grants.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) array');
			});
		});

		test('P1 no prior agreement. No VOR publications. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-vor-publication.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "publication"');
			});
		});

		test('P1 no prior agreement. Wrong VOR publication type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-vor-publication.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: pure OA journal,hybrid journal,transformative journal,author has not decided yet,other');
			});
		});

		test('P1 no prior agreement. No VOR publications. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-no-vor-license.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "license"');
			});
		});

		test('P1 no prior agreement. Wrong VOR publication type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-vor-license.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: CC BY,CC BY-ND,CC BY-NC,CC BY-NC-SA,CC BY-NC-ND,CC BY-IGO,CC BY-not specified,CC BY-other,non-CC,CC0,not yet decided,not specified');
			});
		});

		test('P1 no prior agreement. Wrong VOR deposition. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-vor-deposition.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: open repository, like PMC,not deposited by publisher');
			});
		});

		test('P1 no prior agreement. Wrong VOR research data. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-vor-researchdata.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: Yes,No,not applicable,not yet decided,author will take care,data available on request,other');
			});
		});

		test('P1 no prior agreement. No journal id. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-journal-no-id.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "id"');
			});
		});

		test('P1 no prior agreement. No journal name. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-journal-no-name.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('requires property "name"');
			});
		});

		test('P1 no prior agreement. Incorrect prior agrement type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-prioragreement.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) boolean');
			});
		});

		test('P1 no prior agreement. Incorrect prior agrement currency. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-prioragreement-currency.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: EUR,USD,GBP,AUD,CAD,CHF,CNY,DKK,JPY,NOK,NZD,SEK');
			});
		});

		test('P1 no prior agreement with APC. Incorrect prior APC type. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-apc-type.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: per article APC list price,per article type APC list price,per article page APC list price,other');
			});
		});

		test('P1 no prior agreement with APC. Incorrect prior APC name. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-apc-name.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not one of enum values: firm,estimated,unknown');
			});
		});

		test('P1 no prior agreement with APC. Incorrect prior APC amount. Should fail', () => {
			const validator = new Validator();
			validator.addSchema(header, '/Header');
			fs.readFile(`${process.cwd()}/tests/mocks/messages/p1-no-prior-agreement-wrong-apc-amount.json`, 'utf8', (err, instance) => {
				if (err) {
					console.log(err);
				}
				const valid = validator.validate(JSON.parse(instance), e1);
				expect(valid.errors.length).toBe(1);
				expect(valid.errors[0].message).toBe('is not of a type(s) number');
			});
		});


	});

});
