{
    "id": "/e1",
    "type": "object",
    "required": ["header", "data"],
    "properties": {
        "data": {
            "type": "object",
            "required": ["authors"],
            "properties": {
                "authors": {
                    "type": "array",
                    "required": ["isCorrespondingAuthor", "lastName", "firstName"],
                    "items": {
                        "properties": {
                            "listingorder": {
                                "type": "number"
                            },
                            "lastName": {
                                "type": "string"
                            },
                            "firstName": {
                                "type": "string"
                            },
                            "initials": {
                                "type": "string"
                            },
                            "ORCID": {
                                "type": "string"
                            },
                            "email": {
                                "type": "string"
                            },
                            "isCorrespondingAuthor": {
                                "type": "boolean",
                                "enum": [true]
                            },
                            "collaboration": {
                                "type": "string"
                            },
                            "institutions": {
                                "type": "array",
                                "items": {
                                    "properties": {
                                        "name": {
                                            "type": "string"
                                        },
                                        "ror": {
                                            "type": "string"
                                        }
                                    }
                                }
                            },
                            "affiliation": {
                                "type": "string"
                            }
                        }
                    }
                },
                "article": {
                    "type": "object",
                    "properties": {
                        "title": {
                            "type": "string"
                        },
                        "type": {
                            "type": "string",
                            "enum": [
                                "discussion",
                                "dissertation"
                            ]
                        },
                        "funders": {
                            "type": "array",
                            "items": {
                                "properties": {
                                    "name": {
                                        "type": "string"
                                    },
                                    "ror": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "grants": {
                            "type": "array",
                            "items": {
                                "properties": {
                                    "name": {
                                        "type": "string"
                                    },
                                    "id": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "doi": {
                            "type": "string"
                        },
                        "doiurl": {
                            "type": "string"
                        },
                        "submissionId": {
                            "type": "string"
                        },

                        "manuscript": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "string"
                                },
                                "dates": {
                                    "type": "object",
                                    "properties": {
                                        "submission": {
                                            "type": "string"
                                        },
                                        "acceptance": {
                                            "type": "string"
                                        },
                                        "publication": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },

                        "preprint": {
                            "type": "object",
                            "properties": {
                                "title": {
                                    "type": "string"
                                },
                                "url": {
                                    "type": "string"
                                },
                                "id": {
                                    "type": "string"
                                }
                            }
                        },

                        "vor": {
                            "type": "object",
                            "required": ["publication", "license"],
                            "properties": {
                                "publication": {
                                    "type": "string",
                                    "enum": [
                                        "open access / pure OA journal",
                                        "open access / hybrid journal",
                                        "author hasn't decided yet"
                                    ]
                                },
                                "license": {
                                    "type": "string",
                                    "enum": [
                                        "CC BY"
                                    ]
                                },
                                "deposition": {
                                    "type": "string",
                                    "enum": [
                                        "PMC"
                                    ]
                                },
                                "researchdata": {
                                    "type": "string",
                                    "enum": [
                                        "Yes",
                                        "No",
                                        "not applicable",
                                        "not yet decided"
                                    ]
                                }
                            }
                        }
                    }
                },
                "journal": {
                    "type": "object",
                    "required": ["inDOAJ", "name", "id"],
                    "properties": {
                        "name": {
                            "type": "string"
                        },
                        "id": {
                            "type": "string"
                        },
                        "inDOAJ": {
                            "type": "boolean",
                            "enum": [true]
                        }
                    }
                },
                "charges": {
                    "type": "object",
                    "properties": {
                        "prioragreement": {
                            "type": "boolean",
                            "enum": [true]
                        },
                        "articlecharges": {
                            "type": "boolean"
                        },
                        "agreement": {
                            "type": "object",
                            "properties": {
                                "name": {
                                    "type": "string"
                                },
                                "id": {
                                    "type": "string"
                                }
                            }
                        },
                        "apc": {
                            "type": "object",
                            "required": ["currency", "total"],
                            "properties": {
                                "currency": {
                                    "type": "string",
                                    "enum": [
                                         "EUR",
                                         "USD",
                                         "GBP",
                                         "JPY",
                                         "CNY",
                                         "CHF"
                                    ]
                                },
                                "type": {
                                    "type": "string",
                                    "enum": [
                                        "per article APC list price",
                                        "per article type APC list price",
                                        "per article page APC list price",
                                        "other"
                                    ]
                                },
                                "extra": {
                                    "type": "array",
                                    "items": {
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "enum": [
                                                    "submission format",
                                                    "speed of publication",
                                                    "license choice",
                                                    "discounts for central institutional agreements",
                                                    "subscription discount",
                                                    "society discount",
                                                    "editorial discount",
                                                    "peer review discount",
                                                    "recurring author discount",
                                                    "invitation discount",
                                                    "marketing discount",
                                                    "submission fee",
                                                    "color charges",
                                                    "page charges",
                                                    "other"
                                                ]
                                            },
                                            "amount": {
                                                "type": "number"
                                            }
                                        }
                                    }
                                },

                                "total": {
                                    "type": "object",
                                    "properties": {
                                        "apc": {
                                            "type": "object",
                                            "properties": {
                                                "name": {
                                                    "type": "string",
                                                    "enum": [
                                                        "firm",
                                                        "estimated",
                                                        "unknown"
                                                    ]
                                                },
                                                "amount": {
                                                    "type": "number"
                                                }
                                            }

                                        },
                                        "fees": {
                                            "type": "object",
                                            "properties": {
                                                "name": {
                                                    "type": "string",
                                                    "enum": [
                                                        "firm",
                                                        "estimated",
                                                        "unknown"
                                                    ]
                                                },
                                                "amount": {
                                                    "type": "number"
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}