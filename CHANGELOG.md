# Changelog forms:

## 1.0.5

- Improved connectors (S1, JATS, EM and OJS)
- Improved administrative reporting
- Logging improvenments

## 1.0.4

- Separate email templates
- E-mail senders per environment
