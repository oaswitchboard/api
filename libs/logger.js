const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const cloudwatchlogs = new AWS.CloudWatchLogs();
const utils = require('./../libs/utils');

module.exports = {

	logEvent: async (message, category="success", log) => {
	    return new Promise(async (resolve, reject) => {
	        try {
	            const logStream = await createLogStream(log, category);
	            const response = await putLogEvent(message, log, logStream);
	            resolve(response);
	        } catch (err) {
	            reject(err)
	        }
	    });
	},

    getLogsGroups: (prefix) => {
        return new Promise((resolve, reject) => {
            var params = {
                limit: 50,
            };
            if (prefix) {
                params.logGroupNamePrefix = prefix;
            }
            cloudwatchlogs.describeLogGroups(params, function(err, data) {
                if (err) reject(err, err.stack);
                else resolve(data);
            });
        });
    },

    getLogsStreams: (logGroupName) => {
        return new Promise((resolve, reject) => {
            const params = {
                logGroupName,
                descending: true,
                limit: 20,
                orderBy: "LastEventTime"
            };
            cloudwatchlogs.describeLogStreams(params, function(err, data) {
                if (err) reject(err, err.stack);
                else resolve(data);
            });
        });
    },

    getLogsStreamEvents: (logGroupName, stream) => {
        return new Promise((resolve, reject) => {
            const params = {
                logGroupName,
                logStreamName: stream.logStreamName,
                startFromHead: true,
            };
            cloudwatchlogs.getLogEvents(params, function(err, data) {
                if (err) reject(err, err.stack);
                else resolve(data);
            });
        });
    },

};

async function createLogStream (logGroupName, category)  {
     return new Promise((resolve, reject) => {
        const logStreamName = `${category}_${utils.uuid()}`;
        const logstreamparams = {logGroupName, logStreamName};
        cloudwatchlogs.createLogStream(logstreamparams, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(logStreamName);
            }
        });
    });
}

async function putLogEvent (message, logGroupName, logStreamName)  {
    return new Promise((resolve, reject) => {
        const timestamp = new Date();
        const logparams = {
            logGroupName,
            logStreamName,
            logEvents: [{ message: message, timestamp: timestamp.getTime(), }],
        };
        cloudwatchlogs.putLogEvents(logparams, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}



