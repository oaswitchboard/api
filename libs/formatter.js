module.exports = {
	render: response => {
		const body = typeof response === 'string' ? response : JSON.stringify(response, null, 2);
		return {
			statusCode: 200,
			body,
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Credentials': false,
			},
		};
	},

	error: (errorMessage, statusCode = 500) => {
		return {
			statusCode,
			body: JSON.stringify({
				error: true,
				errorMessage,
			}),
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Credentials': false,
			},
		};
	},
};
