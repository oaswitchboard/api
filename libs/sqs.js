const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const sqs = new AWS.SQS();
const errorhandler = require('./../libs/errorhandler');
const utils = require('./../libs/utils');

module.exports = {
    sendMessage,
    deleteMessage,
    formatMessageForRegularQueue,
    formatMessageForFifoQueue,
}

async function sendMessage (params) {
    try {
        const receipt = await sqs.sendMessage(params).promise();
        return { ...receipt, ...{ success: true }};
    } catch (error) {
        errorhandler.log(error.message, '[OASB API] Error queueing a message');
        return { success: false };
    }
}

async function deleteMessage (params) {
    try {
        const receipt = await sqs.deleteMessage(params);
        return { ...receipt, ...{ success: true }};
    } catch (error) {
        errorhandler.log(error.message, '[OASB API] Error deleting a queued message');
        return { success: false };
    }
}

/*
    This function is used to format the message for the regular queue.
    The regular queue is used to send messages to the OASB API from the OASB UI only
1*/

function formatMessageForRegularQueue (message, sender, queueurl) {
    return {
        MessageAttributes: {
            "Sender": {
              DataType: "String",
              StringValue: JSON.stringify(sender),
            }
        },
        MessageBody: JSON.stringify(message),
        QueueUrl: queueurl,
    };
}

/*
    formatMessageForFifoQueue() is used in custom connectors and private datastores
    The message is sent to the queue with a MessageDeduplicationId
    The MessageGroupId is set to 'JATS' for all JATS-related messages
    Please note that you need a FIFO queue for this to work.
    Source key should always be present as it will be used to create a MessageDeduplicationId
*/

function formatMessageForFifoQueue (message, sender, queueurl) {
    return {
        MessageAttributes: {
            "Sender": {
              DataType: "String",
              StringValue: JSON.stringify(sender),
            }
        },
        MessageDeduplicationId: extractSourceKey(message),
        MessageGroupId: 'JATS',
        MessageBody: JSON.stringify(message),
        QueueUrl: queueurl,
    };
}

function extractSourceKey(message) {
    const key = message.header && message.header.key || utils.uuid(); // Fallback: if no key is present, generate one
    const crypto = require('crypto');
    return crypto.createHash('md5').update(JSON.stringify(key)).digest("hex");
}