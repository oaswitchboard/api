const errorhandler = require("./../libs/errorhandler");
const pool = require("../connection");

const mappedAccessTypes = new Map([
	["open access / pure OA journal", "pure OA journal"],
	["open access / hybrid journal", "hybrid journal"],
	["open access / transformative journal", "transformative journal"],
]);

module.exports = {
	V1ToV2: (message) => {
		let { header, data } = message;

		if (header.version && header.version === "v2") {
			return message;
		}

		header.version = "v2";

		if (header.type === "e1" || header.type === "p1") {
			const article = data.article;
			if (article && article.vor && article.vor.publication) {
				if (mappedAccessTypes.has(article.vor.publication)) {
					article.vor.publication = mappedAccessTypes.get(article.vor.publication);
				}
			}
			if (article && article.vor && article.vor.license) {
				if (/^non-CC BY$/.test(article.vor.license)) {
					article.vor.license = "non-CC";
				}
			}
			if (article && article.doiurl) {
				article.doi = getDoi(article.doi, article.doiurl);
				delete article.doiurl;
			}

			if (header.type === "p1" && data && data.charges) {
				data.charges.charged = null;
			}
		}

		return { ...message, header, data };
	},

	mapAccessType: (record) => {
		if (mappedAccessTypes.has(record.type)) {
			const type = mappedAccessTypes.get(record.type);
		}
		return { ...record, type };
	},
};

function getDoi(doi, doiurl) {
	if (doiurl && doiurl !== "Unknown") {
		return doiurl;
	}
	if (doi && doi !== "Unknown") {
		return `https://doi.org/${doi}`;
	}
	return doiurl;
}
