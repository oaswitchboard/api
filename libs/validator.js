const Validator = require('jsonschema').Validator;
const errorhandler = require('./../libs/errorhandler');

module.exports = {
    validate,
}

function validate (message) {
    try {
        const validator = new Validator();
        const schema = require(`${process.cwd()}/switchboard/schemas/${process.env.VERSION}/${message.header.type}`).schema;
        const header = require(`${process.cwd()}/switchboard/schemas/${process.env.VERSION}/header`).header;
        validator.addSchema(header, '/Header');
        const valid = validator.validate(message, schema);
        const errors = valid.errors.map(error => error.stack);
        return { success: !errors.length, errors };
    } catch (error) {
        errorhandler.log(error, '[OASB API] Error validating message');
        return { success: false, errors: 'Error validating message' };
    }
}

