module.exports = { mapPathToLicenceType };

function mapPathToLicenceType(path) {

	if (path.includes('licenses/by/')) {
		return 'CC BY';
	}

	if (path.includes('/licenses/by-nc/')) {
		return 'CC BY-NC';
	}

	if (path.includes('/licenses/by-nd/')) {
		return 'CC BY-ND';
	}

	if (path.includes('/licenses/by-nc-sa/')) {
		return 'CC BY-NC-SA';
	}

	if (path.includes('/licenses/by-nc-nd/')) {
		return 'CC BY-NC-ND';
	}

	if (path.includes('/licenses/by/3.0/igo')) {
		return 'CC BY-IGO';
	}

	if (path.includes('/licenses/by')) {
		return 'CC BY-not specified';
	}

	if (path.includes('/zero')) {
		return 'CC0';
	}

	return 'non-CC';

}
