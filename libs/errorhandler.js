const AWS = require('aws-sdk');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

const sns = new AWS.SNS();
const logger = require('./logger');

module.exports = {

    log: async (error, description, type='error') => {
        const log = `/aws/errors-${process.env.STAGE.toLowerCase()}`;
        const { name, stack, message, code } = error;
        const formatted = {
            description,
            name,
            message,
            stack,
        };

        if (['prod','acc', 'dev'].includes(process.env.STAGE.toLowerCase())) {
            // Display errors in console
            console.log('errorhandler.log()  --------------------------------------------------')
            console.log('error details', error);
        }

        // Dispatch a notification to SNS
        if (['prod','acc'].includes(process.env.STAGE.toLowerCase()) && process.env.SNS_ERRORS.length) {
            await sns.publish ({
                Message: JSON.stringify(formatted),
                Subject: `Error, stage: ${process.env.STAGE}`,
                TopicArn: process.env.SNS_ERRORS,
            }).promise();
            // Log errors in CloudWatch logs. Please note that the retention period might differ per stage
            await logger.logEvent(JSON.stringify(formatted), type, log);
        }
    },

}