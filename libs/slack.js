const axios = require('axios');

module.exports = {

    post: async (endpoint, data) => {
        const result = await axios.post(
            endpoint,
            JSON.stringify(data),
            { 'Content-Type': 'application/json' },
        );
        return result.status === 200;
    },

    jsontable: (rows, title = "Report") => {
        if (!rows.length) {
            return [];
        }
        return {
            "blocks": [
                {
                    "type": "header",
                    "text": {
                        "type": "plain_text",
                        "text": title
                      }
                },
                ...formatSections(rows),
            ]
        };
    }

}

function formatSections(rows) {
    return rows.map(row => formatSection(row));
}

function formatSection (row) {
    return {
        "type": "section",
        "text":  {
            "type": "mrkdwn",
            "text": formatRow(row),
        }
    };
}

function formatRow (row) {
    const columns = Object.keys(row);
    let text = '';
    columns.forEach(column => {
        text += ` ${row[column]} ;`;
    });
    return text;
}


