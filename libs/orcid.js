const axios = require('axios');

const axiosOptions = {
    baseURL: 'https://pub.orcid.org/v3.0/',
    headers: { 'Accept': 'application/json' }
};

module.exports = {

    getEmployment: async (orcid) => {
        try {
            const id = extractId(orcid);
            if (id) {
                const response = await axios.get(`${id}/employments`, axiosOptions);
                return extractOrganizationName(response.data, 'employment-summary');
            };
            return null;
        } catch (err) {
            console.error(err.message);
            return null;
        };
    }

}

function extractId(orcid) {
    return orcid.match(/([0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9X]{1})/g);
}

function dateFromObject({ year, month, day }) {
    return new Date(Date.UTC(year && year.value ? year.value : 0,
        month && month.value ? month.value - 1 : 0,
        day && day.value ? day.value : 1)
    );
}

function extractOrganizationName(data, key) {
    let output = [];
    if (data['affiliation-group'] && Array.isArray(data['affiliation-group'])) {
        for (const group of data['affiliation-group']) {
            if (group.summaries && Array.isArray(group.summaries)) {
                for (const organization of group.summaries) {
                    if (!organization[key]['end-date']) {
                        output.push({
                            name: organization[key]['organization']['name'],
                            date: dateFromObject(organization[key]['start-date'])
                        });
                    }
                }
            }
        }
    }
    return output.length > 0 ? output.sort((a, b) => b.date - a.date)[0].name : null;
}
