const AWS = require('aws-sdk');
const unzipper = require('unzipper');

AWS.config.region = process.env.AWSREGION || 'eu-central-1';

module.exports = {

	unzip: async (event) => {

		const s3 = new AWS.S3();
		const params = {
		    Key: event.Records[0].s3.object.key,
		    Bucket: process.env.S3_BUCKET,
		};

		console.log('Unzip() --------------------------------------------------')
		console.log('params', params);

		const zip = s3
		      .getObject(params)
		      .createReadStream()
		      .pipe(unzipper.Parse({ forceStream: true }));

		const promises = [];

		for await (const entry of zip) {

		    const filename = entry.path;
		    const type = entry.type;

		    if (type === 'File') {

				console.log('Unpacked as: ', `${process.env.S3_PREFIX}/jats/unpacked/${filename}`);

		      	const uploadParams = {
		        	Bucket: process.env.S3_BUCKET,
		        	Key: `${process.env.S3_PREFIX}/jats/unpacked/${filename}`,
		        	Body: entry,
		      	};

		      	promises.push(s3.upload(uploadParams).promise());

		    } else {
		      entry.autodrain();
		    }
		}

		await Promise.all(promises);

	},

};

