const axios = require('axios');
const endpoint = 'https://api.ror.org/organizations?page=1&affiliation=';
const endpoint_V2 = 'https://api.ror.org/organizations/';

module.exports = {

    generic: {
        search: async (affiliation) => {
            try {
                return process.env.STAGE === 'test' ? null : await search (affiliation);
            } catch (err) {
                console.error(err);
                return null;
            }
        }
    },

    p1: {
        enrichReceiver: async (receiver) => {
            try {
                if (receiver.ror) {
                    return receiver;
                }
                const addressee = { ...receiver };
                if (addressee.affiliations && Array.isArray(addressee.affiliations)) {
                    for (const affiliation of addressee.affiliations) {
                        if (affiliation.name && affiliation.name.length > 3) {
                            const found = await search (affiliation.name);
                            if (found) {
                                if (!addressee.rorid && found.rorid) {
                                    addressee.ror = found.rorid;
                                }
                            }
                        }
                    }
                }
                return addressee;
            } catch (err) {
                console.error(err);
                return receiver;
            }
        }
    },

    e1: {
        enrichReceiver: async (receiver) => {
            try {
                const addressee = { ...receiver };
                if (addressee.departments.institution && addressee.departments.institution && addressee.departments.institution.length > 3) {
                    const found = await search (addressee.departments.institution);;
                    if (!addressee.rorid && found && found.rorid) {
                        addressee.rorid = found.rorid;
                    }
                }
                return addressee;
            } catch (err) {
                console.error(err);
                return receiver;
            }
        }
    },

    ojs: {
        enrichReceiver: async (receiver) => {
            try {
                const addressee = { ...receiver };
                if (addressee.affiliation &&  addressee.affiliation.length > 3) {
                    const found = await search (addressee.affiliation);
                    if (!addressee.rorid && found && found.rorid) {
                        addressee.rorid = found.rorid;
                    }
                }
                return addressee;
            } catch (err) {
                console.error(err);
                return receiver;
            }
        }
    },

    getOrganizationInfoByRor: async (ror) => {
        try {
            const details = await axios.get(endpoint_V2 + encodeURIComponent(ror));
            const ror_id = details.data.id;
            const name = details.data.name;
            const fundRef = details.data.external_ids.FundRef.all[0]
            return {ror_id, name, fundRef};
        } catch (error) {
            console.log('Get organization info by ror error')
            return null;
        }
    }
}

async function search (affiliation) {
    try {
        const details = await axios.get(endpoint + encodeURIComponent(affiliation));
        if (details.data && Array.isArray(details.data.items)) {
            const filtered = details.data.items.filter(item => item.score >= 0.9);
            const sorted = filtered.sort((a, b) => (a.chosen < b.chosen) ? 1 : -1)
            if (sorted.length && !isOnStoplist(sorted[0].organization)) {
                return { rorid, name } = extractIds(sorted[0].organization);
            }
        }
    } catch (err) {
        console.log('Axios error. RoR search:')
        console.log(err);
        return null;
    }
    return null;
}

function extractIds (organization) {
    const rorid = organization.id ? organization.id : null;
    const name = organization.name ? organization.name: null;
    return { rorid, name };
}

function isOnStoplist(organization) {
    const stoplist = [{
        rorid: 'https://ror.org/0030f2a11',
        checkstring: 'Erlangen',
    }];
    const { rorid } = extractIds(organization);
    for (const item of stoplist) {
        if (item.rorid === rorid) {
            return true;
        }
    }
    return false;
}