//@ts-check
const axios = require("axios");
const pool = require('./../connection');

const dispatch = async (payload, endpoint, apikey) => {

  console.log('libs.webhook.dispatch() --------------------------------------------------')
  console.log('payload', payload);
  console.log('endpoint', endpoint);

  if (endpoint.includes("office.com")) {
    return dispatchToTeams(payload, endpoint);
  }

  if (endpoint.includes("slack.com")) {
    return dispatchToSlack(payload, endpoint);
  }

  return dispatchRegular(payload, endpoint, apikey);
};

const dispatchRegular = async (payload, endpoint, apikey) => {
  const headers = formatWebhookHeaders(apikey);
  const result = await logRequest(endpoint, payload, headers, apikey);
  return result.status === 200;
};

const dispatchToTeams = async (payload, endpoint) => {
  try {
    const card = formatTeamsPayload(payload);
    const response = await logRequest(endpoint, card, {
      headers: {
        "content-type": "application/vnd.microsoft.teams.card.o365connector",
      },
    });
    return response.status === 200;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const dispatchToSlack = async (payload, endpoint) => {
  return await logRequest(endpoint, JSON.stringify({ text: payload }), {
    headers: {
      'Content-Type': 'application/json'
    }
  });
};

const logRequest = async (url, requestBody, headers, apikey = null) => {
  const client = await pool.connect();
  try {
    let axiosResponse,
      timestamp = new Date().toISOString();
    try {
      axiosResponse = await axios.post(url, requestBody, headers);
    } catch (error) {
      if (error?.response) {
        axiosResponse = error.response;
      } else {
        axiosResponse = { status: null, data: error.message };
      }
    }
    const logQuery = `
      INSERT INTO webhooklogs (sent_timestamp, url, apikey, payload, status, response)
      VALUES ($1, $2, $3, $4, $5, $6)
    `;
    const argumentsList = [timestamp, url, apikey, JSON.stringify(requestBody), axiosResponse.status, JSON.stringify(axiosResponse.data)];
    await pool.query(logQuery, argumentsList);
    return axiosResponse;
  } catch (error) {
    console.log('logRequest error:', error);
    throw error;
  } finally {
    client.release();
  }
}

const fetchLogRecord = async (id) => {
  const client = await pool.connect();
  try {
    const fetchQuery = `SELECT url, apikey, payload FROM webhooklogs WHERE id = $1`;
    const argumentsList = [id];
    let result = await pool.query(fetchQuery, argumentsList);
    return result.rowCount === 1 ? result.rows.pop() : null;
  } catch (error) {
    throw error;
  } finally {
    client.release();
  }
};

function formatWebhookHeaders(apikey) {
  const headers = {
    "Content-Type": "application/json",
  };
  if (apikey) {
    headers["Authorization"] = apikey;
  }
  return { headers };
}

function formatTeamsPayload(payload) {
  const card = {
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    themeColor: "0072C6",
    summary: "OAS notification",
    sections: [
      {
        activityTitle: "OAS Notification",
        text: JSON.stringify(payload),
      },
    ],
  };
  return JSON.stringify(card);
}

module.exports = { dispatch, fetchLogRecord };
