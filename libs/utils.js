const moment = require('moment');
const utils = {

	messages: {
		generateUUID: () => {
			return `${moment().format('YYYYMMDDHHmmss')}-${module.exports.uuid()}`;
		},
	},

	jwt: {
		decode: (token) => {
			if (token) {
				const base64String = token.split('.')[1];
				return JSON.parse(Buffer.from(base64String, 'base64').toString('UTF-8'));
			}
			return null;
		},
		verify: (params) => {
		     try {
		        const { token, secret, user, role } = params;
		        const jwt = require('jsonwebtoken');
		        const decoded = jwt.verify(token, secret);
		        if (user === decoded.user || role === decoded.role) {
		            return true;
		        }
		        return false;
		    } catch (error) {
		        return false;
		    }
		},
		create (secret, user, role, issuer='OASB') {
			const jwt = require('jsonwebtoken');
		    const token = jwt.sign({
		        user,
		        role,
		        issuer,
		        issued: new Date().toISOString(),
		    }, secret);
		    return token;
		},
	},

	security: {
		isAdministrator: (token) => {
			if (token) {
				const decoded = utils.jwt.decode(token);
				if (decoded && decoded['cognito:groups']) {
					return decoded['cognito:groups'].includes('Administrators');
				}
			}
			return false;
		},
		isGroupMember: (token, group) => {
			if (token) {
				const decoded = utils.jwt.decode(token);
				if (decoded && decoded['cognito:groups']) {
					return decoded['cognito:groups'].includes(group);
				}
			}
			return false;
		},
		getCognitoSub: (token) => {
			if (token) {
				return utils.jwt.decode(token).sub;
			}
			return null;
		},
	},

	uuid: () => {
		let d = new Date().getTime();
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
			const r = (d + Math.random() * 16) % 16 | 0;
			d = Math.floor(d / 16);
			return (c === 'x' ? r : r & 0x7 | 0x8).toString(16);
		});
	},

	clone: (object) => JSON.parse(JSON.stringify(object)),

	isNumeric: (string) => !isNaN(parseFloat(string)) && isFinite(string),

	isBoolean: (string) => typeof string === 'boolean',

	isBlank: (str) => {
		return (!str || /^\s*$/.test(str));
	},

	isObjectEmpty: (name) => {
		return Object.keys(name).length === 0
	},

	isRoR: (string) => {
		const regex = /^https:\/\/ror\.org\/0[a-z|0-9]{6}[0-9]{2}$/gm;
		return regex.test(string);
	},

	isURL: (str) => {
		let url;
		try {
			url = new URL(str);
		} catch (_) {
			return false;
		}
		return url.protocol === "http:" || url.protocol === "https:";
	},

	formatVarChar: (payload) => `'${  payload  }'`,

	flattenArray: (arr) => ArrayFlatten(arr),

	isJson: (string) => {
		try {
			JSON.parse(string);
		} catch (err) {
			return false;
		}
		return true;
	},

	delay: (seconds) => {
		return new Promise(resolve => {
			setTimeout(resolve, seconds*1000);
		});
	},

	escapeQuotes: function(string) {
        return string
        	.replace(/"/g, '\\"');

    },

    unescapeQuotes: function(string) {
        return string
        	.replace(/\\"/g, '')
        	.replace(/\\\"/g, '');
    },
};

function ArrayFlatten(arr) {
	return arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? ArrayFlatten(toFlatten) : toFlatten), []);
}

module.exports = utils;